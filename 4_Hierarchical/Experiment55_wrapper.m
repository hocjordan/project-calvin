function [chunktosuper_synapses, supertochunk_synapses, chunk_tracked] = ExperimentWrapper55(trials, steps, ind_steps, SAtoSA_synapses, SA_decoded, walls, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SAtochunk_synapses, chunktoSA_synapses)
% As ExperimentWrapper54, with an extra layer of 'super' chunk cells.
% UNFINISHED.

%% Parameters


% World
worldSize_x = 10;
worldSize_y = 10;

% Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};

% Cells
num_sensory = [worldSize_y worldSize_x];
num_reward = [worldSize_y worldSize_x];
num_motor = [1 size(actions, 2)];
num_SA = [1 round(1.2 * prod(num_motor)*prod(num_sensory))];
num_chunk = [1 round(num_SA(2)/3)];
num_super = [1 round(num_chunk(2)/3)];

agent_firing = 0;
reward_firing = 1;

% Synapses
chunk_connectivity = 1;
chunk_weights = 0.000001;
chunk_learningRate = 0.00001; %0.00001 %0.000001; %0.01
super_connectivity = 1;
super_weights = 0.001;
super_learningRate = 0.00001;
eta = 0.9;
normalisation_threshold = 0.0001;

% Competition
SA_modifier = 0.02;
chunk_modifier = 1.8;
super_modifier = 1;

% Noise
SA_noise = 0.000;

% Analysis
Ycount = 0;
Ncount = 0;
mapping_threshold = 0.0000004;
average_time = 0;
chunk_tracked = [];

%% Create list of Trials
programme = randi([2 worldSize_x-1], [trials 4]);


% give disallowed conditions for agent and reward placement.

condition1 = 'ismember(sub2ind([worldSize_y worldSize_x], programme(trial, 1), programme(trial, 2)), walls) == 1';
condition2 = 'ismember(sub2ind([worldSize_y worldSize_x], programme(trial, 3), programme(trial, 4)), walls) == 1';
condition3 = 'programme(trial, 1) == programme(trial, 3) && programme(trial, 2) == programme(trial,4)';
condition4 = 'abs(programme(trial, 2) - programme(trial, 4)) ~= 3';





for trial = 1:size(programme,1)
    % for 1D world
    % NOTICE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    programme(trial,1) = 5; programme(trial, 3) = 5;
    while eval(condition1) || eval(condition2) || eval(condition3) || eval(condition4)
        while eval(condition1)
            programme(trial,1) = randi([1 worldSize_x],1);
            programme(trial,2) = randi([1 worldSize_x],1);
        end
        while eval(condition2)
            programme(trial,3) = randi([1 worldSize_x],1);
            programme(trial,4) = randi([1 worldSize_x],1);
        end
        while eval(condition3)
            programme(trial,:) = randi([1 worldSize_x], [1 4]);
        end
        % limit distance to 2 squares
        while eval(condition4)
            programme(trial,2) = randi([1 worldSize_x],1);
            programme(trial,4) = randi([1 worldSize_x],1);
        end
        % for 1D world
        programme(trial,1) = 5; programme(trial, 3) = 5;
    end
end
world_tracked = zeros([worldSize_y worldSize_x]);


%% Create Synapses.
% Chunk.
if ~exist('SAtochunk_synapses','var')
    disp('Generating state => chunk synapses.')
    SAtochunk_synapses = Generate_Diluted_Weights(zeros(num_SA), zeros(num_chunk), chunk_connectivity, chunk_weights);
else
    disp('Using provided state => chunk synapses.')
end
if ~exist('chunktoSA_synapses','var')
    disp('Generating chunk => state synapses.')
    chunktoSA_synapses = Generate_Diluted_Weights(zeros(num_chunk), zeros(num_SA), chunk_connectivity, chunk_weights);
else
    disp('Using provided chunk => state synapses.')
end

% Super.
if ~exist('chunktosuper_synapses','var')
    disp('Generating chunk => super synapses.')
    chunktosuper_synapses = Generate_Diluted_Weights(zeros(num_chunk), zeros(num_super), super_connectivity, super_weights);
    
else
    disp('Using provided chunk => super synapses.')
end
if ~exist('supertochunk_synapses','var')
    disp('Generating super => chunk synapses.')
    supertochunk_synapses = Generate_Diluted_Weights(zeros(num_super), zeros(num_chunk), super_connectivity, super_weights);
else
    disp('Using provided super => chunk synapses.')
end


%% Run Trials
for trial = 1:trials
    
    tic;
    
    agent_x = programme(trial, 2); agent_y = programme(trial, 1); reward_x = programme(trial, 4); reward_y = programme(trial,3);
    
    disp(' ')
    fprintf('TRIAL %d.', trial)
    disp(' ')
    fprintf('Agent at (%d,%d); Reward at (%d,%d).', agent_x, agent_y, reward_x, reward_y)
    disp(' ')
    [SAtochunk_synapses, chunktoSA_synapses, chunktosuper_synapses, supertochunk_synapses, result, world_summed] = Experiment51(steps, ind_steps, agent_x, agent_y, reward_x, reward_y, SA_decoded, walls, actions, ...
        worldSize_x, worldSize_y, num_sensory, num_reward, num_motor, num_SA, num_chunk, num_super, agent_firing, reward_firing, SA_modifier, chunk_modifier, super_modifier, SA_noise, chunk_learningRate, super_learningRate, eta, normalisation_threshold, ...
        SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SAtochunk_synapses, chunktoSA_synapses, chunktosuper_synapses, supertochunk_synapses);
    
    world_tracked = world_tracked + world_summed;
    
    if result == 'Y'
        Ycount = Ycount + 1;
    elseif result == 'N'
        Ncount = Ncount + 1;
    end
    
    time = toc;
    
    average_time = ((average_time*(trial-1)) + time) / trial;
    fprintf('Time remaining = %s', datestr(((trials-trial)*average_time)/86400, 'HH:MM:SS.FFF'))
    disp(' ')
end

%% Analyse Results

disp(Ycount)
disp(Ncount)

figure(); imagesc(world_tracked);

end


















function [SAtochunk_synapses, chunktoSA_synapses, chunktosuper_synapses, supertochunk_synapses, result, world_summed] = Experiment51(steps, ind_steps, agent_x, agent_y, reward_x, reward_y, SA_decoded, walls, actions, ...
        worldSize_x, worldSize_y, num_sensory, num_reward, num_motor, num_SA, num_chunk, num_super, agent_firing, reward_firing, SA_modifier, chunk_modifier, super_modifier, SA_noise, chunk_learningRate, super_learningRate, eta, normalisation_threshold, ...
        SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SAtochunk_synapses, chunktoSA_synapses, chunktosuper_synapses, supertochunk_synapses);

% The bursting model from Experiment50, with an added layer of chunk cells.

%% Setup
% Create World
world = create_world3(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Create cells
sensory_cells = zeros(num_sensory);
reward_cells = zeros(num_reward);
SA_cells = zeros(num_SA);
motor_cells = zeros(num_motor);
chunk_cells = zeros(num_chunk);
super_cells = zeros(num_super);
SA_trace = zeros(num_SA);
chunk_trace = zeros(num_chunk);
super_trace = zeros(num_super);

%% Walls.

% Add Walls to World
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

world(:,:,3) = zeros(size(world(:,:,2)));
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;


world_summed = zeros(size(world(:,:,1)));

%% Run Trial.
for time = 1:steps
    
    noNaNsensorytoSA = sensorytoSA_synapses;
    noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
    
    noNaNmotortoSA = motortoSA_synapses;
    noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
    
    noNaNSAtomotor = SAtomotor_synapses;
    noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
    
    noNaNSAtoSA = SAtoSA_synapses;
    noNaNSAtoSA(isnan(noNaNSAtoSA)) = 0;
    
    noNaNSAtochunk = SAtochunk_synapses;
    noNaNSAtochunk(isnan(noNaNSAtochunk)) = 0;
    
    noNaNchunktoSA = chunktoSA_synapses;
    noNaNchunktoSA(isnan(noNaNchunktoSA)) = 0;
    
    noNaNchunktosuper = chunktosuper_synapses;
    noNaNchunktosuper(isnan(noNaNchunktosuper)) = 0;
    
    noNaNsupertochunk = supertochunk_synapses;
    noNaNsupertochunk(isnan(noNaNsupertochunk)) = 0;
    
    
    %% Creation of Reward-Based Gradient
    for ind_time = 1:ind_steps
        
        % Record agent position
        world_tracked{time} = world(:,:,1) + world(:,:,2);
        
        %% Set Up Reward Gradient
        % Sensory cells fire based on current position.
        
        sensory_cells(find(world(:,:,1))) = agent_firing;
        reward_cells(find(world(:,:,2))) = reward_firing;
        
        
        
        % SA cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
        SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
        SA_cells = reshape(SA_cells,num_SA);
        
        
        % Competition in state cells:
        
        % subtractive.
        SA_cells = SA_cells - SA_modifier * mean(mean(SA_cells));
        
        % divisive.
        SA_cells = SA_cells/max(max(SA_cells));
        SA_cells(SA_cells < 0) = 0;
        
        SA_cells = reshape(SA_cells,num_SA);
        
        % add noise
        SA_cells = SA_cells(:)' + SA_noise .* std(SA_cells(:)) * randn(1,size(SA_cells(:),1));
        SA_cells(SA_cells < 0) = 0;
        SA_cells = reshape(SA_cells,num_SA);
        
        
    end
    
    %world(:,:,1) + world(:,:,2)
    %SA_decoded(find(SA_decoded(:,3) == find(SA_cells == max(SA_cells))),:)
    
    %% Check for Completion
    current_state = sub2ind(size(world(:,:,1)), agent_y, agent_x);
    reward_location = sub2ind(size(world(:,:,1)), reward_y, reward_x);
    
    world_summed(current_state) = world_summed(current_state) + 1;
    
    if current_state == reward_location
        result = 'Y';
        disp(time)
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    
    
    %% Move Agent through Bursting Sensory Activity
    
    sensory_cells(find(world(:,:,1))) = 100;
    reward_cells(find(world(:,:,2))) = reward_firing;
    
    SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
    SA_cells = reshape(SA_cells,num_SA);
    
    %SA_cells = SA_cells + dot(repmat(sensory_cells(:),[1,numel(SA_cells)]), noNaNsensorytoSA);
    
    SA_cells = WTA_Competition(SA_cells);
    
    %world(:,:,1) + world(:,:,2)
    %SA_decoded(find(SA_decoded(:,3) == find(SA_cells == max(SA_cells))),:)
    
    % Calculate activation to motor cells
    motor_cells = dot(repmat(SA_cells(:),[1,numel(motor_cells)]), noNaNSAtomotor);
    
    % Move agent.
    action = actions{find(motor_cells == max(motor_cells))};
    fprintf('%d: %s', time, action)
    disp(' ')
    try
        [world, ~] = update_world4(world, action, 0);
    catch err
        disp(err)
    end
    
    %% Feed Activity to Chunk Cells
    
    % Uses a full trace rule -- preSynaptic_trace *
    % postSynaptic_trace
    
    % Calculate activation of chunk cells from SA cells
    chunk_cells = dot(repmat(SA_cells(:),[1,numel(chunk_cells)]), noNaNSAtochunk);
    chunk_cells = reshape(chunk_cells,num_chunk);
    chunk_save = chunk_cells;
    
    % Competition in chunk cells:
    % Subtractive.
    chunk_cells = chunk_cells - chunk_modifier * mean(mean(chunk_cells));
    chunk_cells(chunk_cells < 0) = 0;
    
    if any(chunk_cells) == 0
        disp('No chunk cells firing')
    else
        % divisive
        chunk_cells = 1 * (chunk_cells/max(max(chunk_cells)));
    end
    
    % Calculate memory trace for all cells.
    chunk_trace = getTrace(chunk_cells, chunk_trace, eta);
    SA_trace = getTrace(SA_cells, SA_trace, eta);
    
    % Update synapses to and from chunk cells.
    %chunktostate_synapses = chunktostate_synapses + learningRate * chunk_trace(:) * state_cells(:)';
    %statetochunk_synapses = statetochunk_synapses + learningRate * state_cells(:) * chunk_trace(:)';
    chunktoSA_synapses = chunktoSA_synapses + chunk_learningRate * chunk_trace(:) * SA_trace(:)';
    SAtochunk_synapses = SAtochunk_synapses + chunk_learningRate * SA_trace(:) * chunk_trace(:)';
    
    % Normalise synapse weights to and from chunk cells.
    SAtochunk_synapses = normalise(SAtochunk_synapses, normalisation_threshold);
    chunktoSA_synapses = normalise(chunktoSA_synapses', normalisation_threshold);
    chunktoSA_synapses = chunktoSA_synapses';
    
    %% Feed Activity to Super Cells
    % Uses a full trace rule -- preSynaptic_trace *
    % postSynaptic_trace
    
    % Calculate activation of super cells from chunk cells
    super_cells = dot(repmat(chunk_cells(:),[1,numel(super_cells)]), noNaNchunktosuper);
    super_cells = reshape(super_cells,num_super);
    super_save = super_cells;
    
    % Competition in chunk cells:
    % Subtractive.
    super_cells = super_cells - super_modifier * mean(mean(super_cells));
    super_cells(super_cells < 0) = 0;
    
    if any(super_cells) == 0
        disp('No super cells firing')
    else
        % divisive
        super_cells = 1 * (super_cells/max(max(super_cells)));
    end
    
    % Calculate memory trace for all cells.
    % ALTERED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    super_trace = getTrace(super_cells, super_trace, eta);
    chunk_trace = getTrace(chunk_cells, chunk_trace, eta);
    
    % Display firing cells and traces
    fprintf('SA = %d.\n' , find(SA_cells))
    fprintf('SA (trace) = %s.\n' , num2str(find(SA_trace)))
    fprintf('Chunk = %s.\n', num2str(find(chunk_cells > 0.9)))
    fprintf('Chunk (trace) = %s.\n', num2str(find(chunk_trace > 0.09)))
    fprintf('Super = %s.\n', num2str(find(super_cells > 0.9)))
    fprintf('Super (trace) = %s.\n', num2str(find(super_trace > 0.09)))
    disp(' ')
    
    % Update synapses to and from chunk cells.
    supertochunk_synapses = supertochunk_synapses + super_learningRate * super_trace(:) * chunk_trace(:)';
    chunktosuper_synapses = chunktosuper_synapses + chunk_learningRate * chunk_trace(:) * super_trace(:)';
    
    % Normalise synapse weights to and from chunk cells.
    chunktosuper_synapses = normalise(chunktosuper_synapses, normalisation_threshold);
    supertochunk_synapses = normalise(supertochunk_synapses', normalisation_threshold);
    supertochunk_synapses = chunktosuper_synapses';
    
    
    
    %% Clear cells
    sensory_cells(:) = 0;
    reward_cells(:) = 0;
    SA_cells(:) = 0;
    chunk_cells(:) = 0;
    motor_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

% Display agent progress.
disp('Analysing Agent Trajectory.')
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);









end




















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches the given threshold. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end
