function [SAtochunk_synapses, chunktoSA_synapses, chunk_tracked] = ExperimentWrapper53(trials, steps, ind_steps, SAtoSA_synapses, SA_decoded, walls, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SAtochunk_synapses, chunktoSA_synapses)
% As ExperimentWrapper53, but with a fixed version of the chunk display
% graphs.

%% Parameters


% World
worldSize_x = 10;
worldSize_y = 10;

% Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};

% Cells
num_sensory = [worldSize_y worldSize_x];
num_reward = [worldSize_y worldSize_x];
num_motor = [1 size(actions, 2)];
num_SA = [1 round(1.2 * prod(num_motor)*prod(num_sensory))];
num_chunk = [1 round(num_SA(2)/3)];

agent_firing = 0;
reward_firing = 1;

% Synapses
chunk_connectivity = 1;
chunk_weights = 0.000001;
chunk_learningRate = 0.00001; %0.00001 %0.000001; %0.01
eta = 0.9;
normalisation_threshold = 0.0001;

% Competition
SA_modifier = 0.02;
chunk_modifier = 1.8;

% Noise
SA_noise = 0.000;

% Analysis
Ycount = 0;
Ncount = 0;
mapping_threshold = 0.0000004;
average_time = 0;
chunk_tracked = [];

%% Create list of Trials
programme = randi([2 worldSize_x-1], [trials 4]);


% give disallowed conditions for agent and reward placement.

condition1 = 'ismember(sub2ind([worldSize_y worldSize_x], programme(trial, 1), programme(trial, 2)), walls) == 1';
condition2 = 'ismember(sub2ind([worldSize_y worldSize_x], programme(trial, 3), programme(trial, 4)), walls) == 1';
condition3 = 'programme(trial, 1) == programme(trial, 3) && programme(trial, 2) == programme(trial,4)';
condition4 = 'abs(programme(trial, 1) - programme(trial, 3)) ~= 5';





for trial = 1:size(programme,1)
    % for 1D world
    % NOTICE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        programme(trial,2) = 5; programme(trial, 4) = 5;
    while eval(condition1) || eval(condition2) || eval(condition3) || eval(condition4)
        while eval(condition1)
            programme(trial,1) = randi([1 worldSize_x],1);
            programme(trial,2) = randi([1 worldSize_x],1);
        end
        while eval(condition2)
            programme(trial,3) = randi([1 worldSize_x],1);
            programme(trial,4) = randi([1 worldSize_x],1);
        end
        while eval(condition3)
            programme(trial,:) = randi([1 worldSize_x], [1 4]);
        end
        % limit distance to 2 squares
        while eval(condition4)
            programme(trial,1) = randi([1 worldSize_x],1);
            programme(trial,3) = randi([1 worldSize_x],1);
        end
        % for 1D world
        programme(trial,2) = 5; programme(trial, 4) = 5;
    end
end
world_tracked = zeros([worldSize_y worldSize_x]);


%% Create Chunk Synapses.
% Chunk.
if ~exist('SAtochunk_synapses','var')
    disp('Generating state => chunk synapses.')
    SAtochunk_synapses = Generate_Diluted_Weights(zeros(num_SA), zeros(num_chunk), chunk_connectivity, chunk_weights);
else
    disp('Using provided state => chunk synapses.')
end
if ~exist('chunktoSA_synapses','var')
    disp('Generating chunk => state synapses.')
    chunktoSA_synapses = Generate_Diluted_Weights(zeros(num_chunk), zeros(num_SA), chunk_connectivity, chunk_weights);
    %chunktostate_synapses = GenerateZeroWeights(numel(chunk_cells), numel(state_cells), chunk_connectivity);
else
    disp('Using provided chunk => state synapses.')
end


%% Run Trials
for trial = 1:trials
    
    tic;
    
    agent_x = programme(trial, 1); agent_y = programme(trial, 2); reward_x = programme(trial, 3); reward_y = programme(trial,4);
    
    % for 1D world:
    agent_y = 5; reward_y = 5;
    
    disp(' ')
    fprintf('TRIAL %d.', trial)
    disp(' ')
    fprintf('Agent at (%d,%d); Reward at (%d,%d).', agent_x, agent_y, reward_x, reward_y)
    disp(' ')
    [SAtochunk_synapses, chunktoSA_synapses, result, world_summed] = Experiment51(steps, ind_steps, agent_x, agent_y, reward_x, reward_y, SA_decoded, walls, actions, ...
        worldSize_x, worldSize_y, num_sensory, num_reward, num_motor, num_SA, num_chunk, agent_firing, reward_firing, SA_modifier, chunk_modifier, SA_noise, chunk_learningRate, eta, normalisation_threshold, ...
        SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SAtochunk_synapses, chunktoSA_synapses);
    
    world_tracked = world_tracked + world_summed;
    
    if result == 'Y'
        Ycount = Ycount + 1;
    elseif result == 'N'
        Ncount = Ncount + 1;
    end
    
    time = toc;
    
    average_time = ((average_time*(trial-1)) + time) / trial;
    fprintf('Time remaining = %s', datestr(((trials-trial)*average_time)/86400, 'HH:MM:SS.FFF'))
    disp(' ')
end

%% Analyse Results

disp(Ycount)
disp(Ncount)

figure(); imagesc(world_tracked);

%% Analyse Chunk Sequences:

% Import arrows
arrows = repmat({uint8(zeros(16))}, [3 3]);
arrows{1} = imread('arrow_nw.png');
arrows{2} = imread('arrow_w.png');
arrows{3} = imread('arrow_sw.png');
arrows{4} = imread('arrow_n.png');
arrows{6} = imread('arrow_s.png');
arrows{7} = imread('arrow_ne.png');
arrows{8} = imread('arrow_e.png');
arrows{9} = imread('arrow_se.png');

for arrow = 1:numel(arrows);
    arrowNormalised = arrows{arrow};
    arrowNormalised(arrowNormalised > 0) = 255;
    arrows{arrow} = arrowNormalised;
end

% Obtain useful synapses for each chunk cell
idx = [];
indices = chunktoSA_synapses < mapping_threshold;
thresholded = chunktoSA_synapses;
thresholded(indices) = 0;
thresholded(isnan(thresholded)) = 0;
%}

for cell = 1:num_chunk(2)
    if any(thresholded(cell,:))
        idx = [idx cell];
    end
end

maximum_synapses = max(max(chunktoSA_synapses));

idx

for chunk = idx
    disp(chunk)
    
    chunk_display = repmat({uint8(zeros(16))}, [10*3 10*3]);
    
    for chunk_SA = find(thresholded(chunk,:))
        
        %SA_decoded(find(SA_decoded(:,3) == chunk_SA),1)
        
        [y, x] = ind2sub([10 10], SA_decoded(find(SA_decoded(:,3) == chunk_SA),1));
        
        SA_state = (y * 3 - 1) + ((worldSize_y*3) + (worldSize_y*3) * 3 * (x-1));
        
        chunk_display{SA_state}(:) = 255;
        
        if SA_decoded(find(SA_decoded(:,3) == chunk_SA),2) == 1
            chunk_display{SA_state - 10*3 - 1} = arrows{1} * (chunktoSA_synapses(chunk,chunk_SA) / maximum_synapses);
            
        elseif SA_decoded(find(SA_decoded(:,3) == chunk_SA),2) == 2
            chunk_display{SA_state - 10*3} = arrows{2} * (chunktoSA_synapses(chunk,chunk_SA) / maximum_synapses);
            
        elseif SA_decoded(find(SA_decoded(:,3) == chunk_SA),2) == 3
            chunk_display{SA_state - 10*3 + 1} = arrows{3} * (chunktoSA_synapses(chunk,chunk_SA) / maximum_synapses);
            
        elseif SA_decoded(find(SA_decoded(:,3) == chunk_SA),2) == 4
            chunk_display{SA_state + 1} = arrows{4} * (chunktoSA_synapses(chunk,chunk_SA) / maximum_synapses);
            
        elseif SA_decoded(find(SA_decoded(:,3) == chunk_SA),2) == 6
            chunk_display{SA_state - 1} = arrows{6} * (chunktoSA_synapses(chunk,chunk_SA) / maximum_synapses);
            
        elseif SA_decoded(find(SA_decoded(:,3) == chunk_SA),2) == 7
            chunk_display{SA_state + 10*3 - 1} = arrows{7} * (chunktoSA_synapses(chunk,chunk_SA) / maximum_synapses);
            
        elseif SA_decoded(find(SA_decoded(:,3) == chunk_SA),2) == 8
            chunk_display{SA_state + 10*3} = arrows{8} * (chunktoSA_synapses(chunk,chunk_SA) / maximum_synapses);
            
        elseif SA_decoded(find(SA_decoded(:,3) == chunk_SA),2) == 9
            chunk_display{SA_state + 10*3 + 1} = arrows{9} * (chunktoSA_synapses(chunk,chunk_SA) / maximum_synapses);
            
        end
        
        %chunk_display{SA_decoded(find(SA_decoded(:,3) == chunk_SA),1)} = arrows{SA_decoded(find(SA_decoded(:,3) == chunk_SA),2)};
        
    end
    
    try
    figure(); image(cell2mat(chunk_display)); colormap(copper); title(chunk)
    catch err
        disp err.message
    end
    
    chunk_tracked{chunk} = chunk_display;
    
    try
        if chunk > idx(80)
            disp('Too many graphs created!')
            return
        end
    catch err
        disp(err.message)
    end
end
%}
end


















function [SAtochunk_synapses, chunktoSA_synapses, result, world_summed] = Experiment51(steps, ind_steps, agent_x, agent_y, reward_x, reward_y, SA_decoded, walls, actions, ...
    worldSize_x, worldSize_y, num_sensory, num_reward, num_motor, num_SA, num_chunk, agent_firing, reward_firing, SA_modifier, chunk_modifier, SA_noise, chunk_learningRate, eta, normalisation_threshold, ...
    SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SAtochunk_synapses, chunktoSA_synapses)

% The bursting model from Experiment50, with an added layer of chunk cells.

%% Setup
% Create World
world = create_world3(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Create cells
sensory_cells = zeros(num_sensory);
reward_cells = zeros(num_reward);
SA_cells = zeros(num_SA);
motor_cells = zeros(num_motor);
chunk_cells = zeros(num_chunk);
SA_trace = zeros(num_SA);
chunk_trace = zeros(num_chunk);


%% Walls.

% Add Walls to World
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

world(:,:,3) = zeros(size(world(:,:,2)));
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;


world_summed = zeros(size(world(:,:,1)));

%% Run Trial.
for time = 1:steps
    %% Creation of Reward-Based Gradient
    for ind_time = 1:ind_steps
        %{
        if ind_time == round(ind_steps/4)
            disp('...')
        elseif ind_time == round(ind_steps/2)
            disp('...')
        elseif ind_time == round(3*ind_steps/4)
            disp('...')
        end
        %}
        noNaNsensorytoSA = sensorytoSA_synapses;
        noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
        
        noNaNmotortoSA = motortoSA_synapses;
        noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
        
        noNaNSAtomotor = SAtomotor_synapses;
        noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
        
        noNaNSAtoSA = SAtoSA_synapses;
        noNaNSAtoSA(isnan(noNaNSAtoSA)) = 0;
        
        noNaNSAtochunk = SAtochunk_synapses;
        noNaNSAtochunk(isnan(noNaNSAtochunk)) = 0;
        
        noNaNchunktoSA = chunktoSA_synapses;
        noNaNchunktoSA(isnan(noNaNchunktoSA)) = 0;
        
        
        % Record agent position
        world_tracked{time} = world(:,:,1) + world(:,:,2);
        
        %% Set Up Reward Gradient
        % Sensory cells fire based on current position.
        
        sensory_cells(find(world(:,:,1))) = agent_firing;
        reward_cells(find(world(:,:,2))) = reward_firing;
        
        
        
        % SA cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
        SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
        SA_cells = reshape(SA_cells,num_SA);
        
        
        % Competition in state cells:
        
        % subtractive.
        SA_cells = SA_cells - SA_modifier * mean(mean(SA_cells));
        
        % divisive.
        SA_cells = SA_cells/max(max(SA_cells));
        SA_cells(SA_cells < 0) = 0;
        
        SA_cells = reshape(SA_cells,num_SA);
        
        % add noise
        SA_cells = SA_cells(:)' + SA_noise .* std(SA_cells(:)) * randn(1,size(SA_cells(:),1));
        SA_cells(SA_cells < 0) = 0;
        SA_cells = reshape(SA_cells,num_SA);
        
        
    end
    
    %world(:,:,1) + world(:,:,2)
    %SA_decoded(find(SA_decoded(:,3) == find(SA_cells == max(SA_cells))),:)
    
    %% Check for Completion
    current_state = sub2ind(size(world(:,:,1)), agent_y, agent_x);
    reward_location = sub2ind(size(world(:,:,1)), reward_y, reward_x);
    
    world_summed(current_state) = world_summed(current_state) + 1;
    
    if current_state == reward_location
        result = 'Y';
        disp(time)
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    
    
    %% Move Agent through Bursting Sensory Activity
    
    sensory_cells(find(world(:,:,1))) = 100;
    reward_cells(find(world(:,:,2))) = reward_firing;
    
    SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
    SA_cells = reshape(SA_cells,num_SA);
    
    %SA_cells = SA_cells + dot(repmat(sensory_cells(:),[1,numel(SA_cells)]), noNaNsensorytoSA);
    
    SA_cells = WTA_Competition(SA_cells);
    
    %world(:,:,1) + world(:,:,2)
    %SA_decoded(find(SA_decoded(:,3) == find(SA_cells == max(SA_cells))),:)
    
    % Calculate activation to motor cells
    motor_cells = dot(repmat(SA_cells(:),[1,numel(motor_cells)]), noNaNSAtomotor);
    
    % Move agent.
    action = actions{find(motor_cells == max(motor_cells))};
    fprintf('%d: %s', time, action)
    disp(' ')
    try
        [world, ~] = update_world4(world, action, 0);
    catch err
        disp(err)
    end
    
    %% Feed Activity to Chunk Cells
    
    % Uses a full trace rule -- preSynaptic_trace *
    % postSynaptic_trace
    
    % Calculate activation of chunk cells from SA cells
    chunk_cells = dot(repmat(SA_cells(:),[1,numel(chunk_cells)]), noNaNSAtochunk);
    chunk_cells = reshape(chunk_cells,num_chunk);
    chunk_save = chunk_cells;
    
    % Competition in chunk cells:
    % Subtractive.
    chunk_cells = chunk_cells - chunk_modifier * mean(mean(chunk_cells));
    chunk_cells(chunk_cells < 0) = 0;
    
    if any(chunk_cells) == 0
        disp('No chunk cells firing')
    else
        % divisive
        chunk_cells = 1 * (chunk_cells/max(max(chunk_cells)));
    end
    
    % display firing Chunk cells
    fprintf('SA = %d. ' , find(SA_cells))
    fprintf('Chunk = %s.', num2str(find(chunk_cells > 0.9)))
    disp(' ')
    
    
    % Calculate memory trace for all cells.
    chunk_trace = getTrace(chunk_cells, chunk_trace, eta);
    SA_trace = getTrace(SA_cells, SA_trace, eta);
    
    % Display traces
    fprintf('SA (trace) = %s. ' , num2str(find(SA_trace)))
    fprintf('Chunk (trace) = %s.', num2str(find(chunk_trace > 0.09)))
    disp(' ')
    
    % Update synapses to and from chunk cells.
    %chunktostate_synapses = chunktostate_synapses + learningRate * chunk_trace(:) * state_cells(:)';
    %statetochunk_synapses = statetochunk_synapses + learningRate * state_cells(:) * chunk_trace(:)';
    chunktoSA_synapses = chunktoSA_synapses + chunk_learningRate * chunk_trace(:) * SA_trace(:)';
    SAtochunk_synapses = SAtochunk_synapses + chunk_learningRate * SA_trace(:) * chunk_trace(:)';
    
    % Normalise synapse weights to and from chunk cells.
    SAtochunk_synapses = normalise(SAtochunk_synapses, normalisation_threshold);
    chunktoSA_synapses = normalise(chunktoSA_synapses', normalisation_threshold);
    chunktoSA_synapses = chunktoSA_synapses';
    
    
    %% Clear cells
    sensory_cells(:) = 0;
    reward_cells(:) = 0;
    SA_cells(:) = 0;
    chunk_cells(:) = 0;
    motor_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

% Display agent progress.
disp('Analysing Agent Trajectory.')
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);









end




















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches the given threshold. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end
