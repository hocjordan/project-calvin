function [sensory_tracked_matrix, SA_decoded, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses] = Timestep_Maze_World()

% Status: Have made SAtoSA_synapses sparse, replaced TimestepLearner
% with Experiment77. Still testing.

%% Define Sensory Information and Starting Point

global world_state

worldSize_x = 20; %10
worldSize_y = 20;

[~, ~, walls] = create_maze(worldSize_x, worldSize_y);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Define Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Define World Functions
world_update_function = @maze_update;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Define Timestep Parameters
% Cell Sizes
[sensory_cells, reward_cells] = world_update_function('', 'yes');
num_sensory         = [size(sensory_cells)];                                                                            % Get layer size of sensory (state) cells.
num_motor           = [1 size(actions, 2)];                                                                               % Define the number of motor neurons equal to the number of possible actions.
num_SAcol           = prod(num_sensory);                                                                                  % Define the number of columns in the SA layer as equal to the number of possible states.
num_SAcellsinCol    = prod(num_motor);                                                                             % Set the number of cells in each SA column as equal to the number of possible actions.
num_SA              = [num_SAcellsinCol 1 num_SAcol];                                                                        % Set layer size of SA cells as [number of cells in column * 1 * number of columns]. This is important for competition in CONTROLLER function.

num_param           = cell(1, 5);
num_param{1}        = num_sensory;
num_param{2}        = num_motor;
num_param{3}        = num_SAcol;
num_param{4}        = num_SAcellsinCol;
num_param{5}        = num_SA;

% Firing Parameters
reward_firing = 1;

% Competition Parameters
SA_modifier         = 0.9; %0.02; % 0.2                                                                                 % Modifies the subtractive inhibition of the SA cells in the inductive feedback loop.
sensory_threshold   = 1; % Necessary to adjust these to compensate for different                  % Threshold constants used when normalising synapses. Don't know if these are necessary but...
motor_threshold     = 0.25; % numbers of synapses to sensory and motor cells.                       % ...the previous comments seem to imply so.
trace_threshold     = 4; %0.01

competition_param       = cell(1, 4);
competition_param{1}    = SA_modifier;
competition_param{2}    = sensory_threshold;
competition_param{3}    = motor_threshold;
competition_param{4}    = trace_threshold;

% Learning Parameters
learningRate        = 100; %100                                                                        % Multiplier constant used in synapse update.
trace_learningRate  = 10; %0.1; %0.001; % 0.0001                                                 % Similar to learningRate but used in a specific computation. CHECK IF THIS IS NECESSARY.
eta                 = 0.0;                                                                                      % Determines the length of the memory trace used in function getTrace(). See function for more details.

learning_param      = cell(1, 3);
learning_param{1}   = learningRate;
learning_param{2}   = trace_learningRate;
learning_param{3}   = eta;


% Analysis Parameters
sensory_tracked     = {};                                                                           % Cell array to store experienced states for post-simulation analysis.
SA_tracked          = {};                                                                                % Cell array to store experienced state-action combinations for post-simulation analysis.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Setup Cells and Synapses

% Create cells                                                                                       % Create SA layer, with all activations at zero.
motor_cells = zeros(num_motor);                                                                 % Create matrix of motor cell firing rates (currently all zeros)
SA_cells = zeros(num_SA);                                                                       % Create matrix of SA cell firing rates (currently all zeros)
SA_trace = zeros(num_SA);                                                                       % Create matrix to store the SA firing rates of the last timestep.

% Create synapse weights between SA cells and motor<=>SA.
SAtoSA_synapses = zeros(numel(SA_cells), numel(SA_cells));                                      % Create matrix of synapses connecting every SA cell to every other SA cell (currently all zeros).
SAtoSA_synapses = sparse(SAtoSA_synapses);


motortoSA_synapses = rand(numel(motor_cells), numel(SA_cells));                                 % Create matrix of synapses connecting every SA cell to every motor cell. Currently random values between 0 and 1.
SAtomotor_synapses = rand(numel(SA_cells), numel(motor_cells));                                 % Create matrix of synapses connecting every motor cell to every SA cell. Currently random values between 0 and 1.
identitySynapses = eye(size(SAtoSA_synapses));                                                                  % Create identity matrix for connecting SA_reward to SA cells.

sensorytoSA_synapses = [];                                                                      % Create empty matrix.
for column = 1:num_SAcol                                                                        % For every column within the SA layer:
    sensorytoSA_synapses = [sensorytoSA_synapses repmat(rand(numel(sensory_cells), 1), [1 size(actions,2)])]; % Connect each cell in that column to every sensory cell. These synapses have randomly generated weights BUT THE WEIGHTS ARE THE SAME FOR EVERY SA CELL IN THAT COLUMN.
end

% Normalise synapses
sensorytoSA_synapses = normalise(sensorytoSA_synapses, sensory_threshold);                      % Normalise each column of the sensorytoSA_synapses matrix to the value of sensory_threshold; in other words normalise the input weights to each SA cell from the sensory layer.
motortoSA_synapses = normalise(motortoSA_synapses, motor_threshold);                            % As previous line, but with the motortoSA_synapses and motor threshold.
SAtomotor_synapses = normalise(SAtomotor_synapses', motor_threshold);                           % As previous line, but transposes the matrix so normalises the rows rather than the columns of the SAtomotor synapse matrix; in other words normalise the output weights from each SA cell to the sensory layer.
SAtomotor_synapses = SAtomotor_synapses';                                                       % Re-transpose the matrix back to its original position.

% Archive
neurons = cell(1, 5);
neurons{1} = motor_cells;
neurons{2} = SA_cells;
neurons{3} = SA_trace;

synapses = cell(1, 5);
synapses{1} = sensorytoSA_synapses;
synapses{2} = motortoSA_synapses;
synapses{3} = SAtomotor_synapses;
synapses{4} = SAtoSA_synapses;
synapses{5} = identitySynapses;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Call Learner function
tic;
for steps = 1:100; %1000
    [neurons, synapses] = TimestepLearner(actions, world_update_function, neurons, synapses, num_param, competition_param, learning_param);
    disp(steps)
end
toc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Analyse and Save Data

%{
sensory_tracked_matrix = [];
for time = 1:size(sensory_tracked,2)
    sensory_tracked_matrix(:,:,:,time) = sensory_tracked{time};
end

%{
world_tracked = {};
for time = 1:steps
    world_tracked{time} = sensory_tracked_matrix(:,:,1,time) + sensory_tracked_matrix(:,:,3,time);
end

slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
%}

% Decode SA information
SA_decoded = [];
for time = 1:size(sensory_tracked,2)
    SA_tracked{time}{1} = find(SA_tracked{time}{1});
    SA_decoded(time,:) = cell2mat(SA_tracked{time}(:)');
end


% Record every state/action combo.
SA_decoded = unique(SA_decoded,'rows');
SA_decoded = sortrows(SA_decoded, 3);
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Ask for New Agent/Goal Positions

[reward_x, reward_y, ~] = create_maze(worldSize_x, worldSize_y, walls);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Call Goal Function
for steps = 1:100
    [action, result] = TimestepController(50, actions, world_update_function, neurons, synapses, num_param, competition_param, learning_param, reward_firing);
    
    switch result
        case 'Y'
            break
        case 'N'
            disp(action)
            maze_update(action, 'no');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Analyse and Save Data
%{
sensory_tracked_matrix = [];
for time = 1:size(sensory_tracked,2)
    sensory_tracked_matrix(:,:,:,time) = sensory_tracked{time};
end

%
world_tracked = {};
for time = 1:size(sensory_tracked,2)
    world_tracked{time} = sensory_tracked_matrix(:,:,1,time) + sensory_tracked_matrix(:,:,3,time);
end

slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [sensory_cells, reward_cells] = maze_update(action, calculate_sensory)

%% Unpackage World Parameters

global world_state

world = world_state;

% find parameters of world
[size_y, size_x] = size(world(:,:,1));

% find the position of the agent within the world
[agent_y, agent_x] = find(world(:,:,1) == 1);

% find position of the reward within the world
[reward_y, reward_x] = find(world(:,:,2) == 2);

%% Action parameters
% NONE

%% Calculate effect of action.

%{
switch action
    case 'N'
        agent_y = agent_y - 1;
    case 'S'
        agent_y = agent_y + 1;
    case 'W'
        agent_x = agent_x - 1;
    case 'E'
        agent_x = agent_x + 1;
end
%}
switch action
    case 'W'
        agent_x = agent_x-1;
    case 'SW'
        agent_x = agent_x-1;
        agent_y = agent_y+1;
    case 'S'
        agent_y = agent_y+1;
    case 'SE'
        agent_x = agent_x+1;
        agent_y = agent_y+1;
    case 'E'
        agent_x = agent_x+1;
    case 'NE'
        agent_x = agent_x+1;
        agent_y = agent_y-1;
    case 'N'
        agent_y = agent_y-1;
    case 'NW'
        agent_x = agent_x-1;
        agent_y = agent_y-1;
end

if agent_x == 0 || agent_x > size(world,2) || agent_y == 0 || agent_y > size(world,1)
    valid = 0;
    return
end
if world(agent_y, agent_x, 3) == 3
    valid = 0;
    return
end

world(:,:,1) = 0;
world(agent_y,agent_x,1) = 1;


%% Reward
world(reward_y,reward_x,2) = 1;

%% Produce distributed sensory representation

sensory_cells = world(:,:,1);
reward_cells = world(:,:,2);

%% Show figure

%% Repackage World Parameters

world_state = world;

end

function [reward_x, reward_y, walls] = create_maze(worldSize_x, worldSize_y, walls)

global world_state

%   Creates an x by y by 3 matrix
world = zeros(worldSize_y,worldSize_x,3);

% Define bounding walls


if exist('walls', 'var') == 1
    
    % add pre-existing walls
    world(walls + numel(world(:,:,1:2))) = 3;
    
else
    
    % ask for wall input
    walls = [];
    
    top_wall = [];
    top_wall = [top_wall, 1:worldSize_y:worldSize_x*worldSize_y];
    
    walls = [walls top_wall];
    
    bottom_wall = [];
    bottom_wall = [bottom_wall, worldSize_y-1+1:worldSize_y:worldSize_y*worldSize_x];
    
    walls = [walls bottom_wall];
    
    left_wall = 1:worldSize_y;
    right_wall = worldSize_y*(worldSize_x-1)+1:worldSize_y*worldSize_x;
    
    walls = [walls left_wall];
    walls = [walls right_wall];
    
    % add walls into world
    walls = unique(walls);
    world(walls + numel(world(:,:,1:2))) = 3;
    
    % user interface to create maze
    user_walls = figure(); imagesc(world(:,:,3)); title('Walls: Click to Add, Click to Remove');
    while ishandle(user_walls) == 1;
        try
            [x, y] = ginput(1);
            x=round(x); y=round(y);
            if world(y,x,3) == 3;
                world(y,x,3) = 0;
            else
                world(y,x,3) = 3;
            end
            imagesc(world(:,:,3));
        end
    end
    
    walls = find(world(:,:,3) == 3);
    
end

% place agent
fig = figure(); imagesc(world(:,:,3)); title('Please select starting position.');
[x, y] = ginput(1);
x=round(x); y=round(y);
agent_x = x; agent_y = y;
close(fig)

% place reward
fig = figure(); imagesc(world(:,:,3)); title('Please select goal position.');
[x, y] = ginput(1);
x=round(x); y=round(y);
reward_x = x; reward_y = y;
close(fig)

world(agent_y,agent_x,1) = 1;
world(reward_y,reward_x,2) = 1;

world_state = world;
end

function matrix = normalise(matrix, threshold)

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));                 % Normalises a matrix so that each column sums to the threshold value given. If you want to normalise rows, transpose the matrix before using this function.

end