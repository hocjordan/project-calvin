
%% Setup Rover Interface
s=serial('/dev/cu.usbmodemfa2111');
set(s,'BaudRate',9600,'DataBits', 8, 'Parity', 'none','StopBits', 1, 'FlowControl', 'none','Terminator','CR');
fopen(s);

%% Set Up Analysis
sensory_cells = zeros(1,180);
time = 0;
count = 0;
dataset = zeros(1, 18);

%% Infinite Loop
while(1)
    % To record and display each reading taken in by sensor.
    
    %% Record and Convert to Sensory Cells
    %{
    % Record
    fprintf(s,'0');
    pause(0.5-time);
    tic
    data=fscanf(s);
    data=str2num(data);
    if data < 100
        
        % Convert data
        data = data/100;
        sensory_cells = gaussian_cells(100, data, 0.005);
        sensory_cells = reshape(sensory_cells, [10, 10]);
        imagesc(sensory_cells)
    end
    time = toc;
    %}
    
    %% For sweeping dataset.
    %
    % Every 0.5 seconds
    fprintf(s,'0');
    pause(0.5);
    tic
    count = mod(count, 18);
    
    % When sweep complete
    switch count
        case 0
            % Regulate dataset
            dataset(isnan(dataset)) = 0;
            dataset = dataset/10;
            dataset = dataset/0.6;
            disp(dataset)
            data_count = 0;
            for data = dataset
                % Convert data
                %data = dataset(sweep);
                data_cells = gaussian_cells(10, data, 0.005);
                data_cells(isnan(data_cells)) = 0;
                %disp(data_cells)
                %(data_count*10+1)
                %data_count*10+10
                sensory_cells(data_count*10+1:data_count*10+10) = data_cells;
                data_count = data_count+1;
            end
            %disp(sensory_cells)
            plot(sensory_cells)
            dataset(:) = NaN;
    end
    count = count+1;
    
    % Record range
    data=fscanf(s);
    data=str2num(data);
    
    % Take log of range
    data = log(data);
    
    % Add to sweep data
    dataset(count) = data;
    %disp(dataset)
    %plot(dataset);
    time = toc;
end
end
    
    
    %}
    
    %{
if(data<=10)
            fprintf(s,'1');
            pause(0.5);
            fprintf(s,'2');
            pause(0.5);
            fprintf(s,'3');
            pause(0.5);
             fprintf(s,'0');
            pause(0.5);
            data=fscanf(s);
            data=str2num(data);
            display('first');
            i=0;
            if(data<=10)
                fprintf(s,'2');
                pause(0.5);
                i=i+1;
            end
            display('second');
    end
    %}
    


% To stop using the port:
%delete(instrfind({'Port'},{'/dev/cu.usbmodemfa2111'}))
%delete(instrfind({'Port'},{s.Port}))