function Experiment76(neurons, synapses, num_param, competition_param, learning_param)

% An attempt to sparsify the SAtoSA_synapse matrix, or at least make clear
% why I can't.

% Status : have to re-sparse before addition, so trying to see whether the
% sparse solution is still faster. Results inconclusive, but seems to be
% when run in Timestep_Maze_World with worldSize = 20.


%% Unpack Arrays
sensory_cells           = rand(20);
motor_cells             = neurons{1};       % Variables including cell layer and synapse matrices have been stored in...
SA_cells                = neurons{2};       % ...arrays, so that they don't have to be recreated every timestep.
SA_trace                = neurons{3};       % Here they are read out from those arrays.

sensorytoSA_synapses    = synapses{1};
motortoSA_synapses      = synapses{2};
SAtomotor_synapses      = synapses{3};
SAtoSA_synapses         = synapses{4};

num_sensory         = num_param{1};
num_motor           = num_param{2};
num_SAcol           = num_param{3};
num_SAcellsinCol    = num_param{4};
num_SA              = num_param{5};

SA_modifier         = competition_param{1};
sensory_threshold   = competition_param{2};
motor_threshold     = competition_param{3};
trace_threshold     = competition_param{4};

learningRate        = learning_param{1};
trace_learningRate  = learning_param{2};
eta                 = learning_param{3};

%% MAKE SATOSA SPARSE!

SAtoSA_synapses = full(SAtoSA_synapses);



%% Testing
%
tic;
for loop = 1:10
% SA cells fire based on current sensory cells (state).
SA_cells = dot((repmat(sensory_cells(:),[1,numel(SA_cells)])), sensorytoSA_synapses);       % Calculate activation of SA cell from dot product of sensory cell firing rates and sensorytoSA_synapses.

% Update SAtoSA synapses
SAtoSA_synapses = SAtoSA_synapses + sparse((trace_learningRate * SA_cells(:) * SA_trace(:)'));      % Update the synapses between SA cells with (constant . current SA firing . SA trace firing from previous timesteps). SA_trace is zeros in first timestep.

% Normalise synapses
SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);                              % Normalise SAtoSA synapses as previous.
SAtoSA_synapses(isnan(SAtoSA_synapses)) = 0;                                                % Gets rid of any NaN values that the normalisation may have introduced.
end
toc;
%}
%{
SAtoSA_synapses = full(SAtoSA_synapses);
tic;
for loop = 1:10
% SA cells fire based on current sensory cells (state).
SA_cells = dot((repmat(sensory_cells(:),[1,numel(SA_cells)])), sensorytoSA_synapses);       % Calculate activation of SA cell from dot product of sensory cell firing rates and sensorytoSA_synapses.

% Update SAtoSA synapses
SAtoSA_synapses = SAtoSA_synapses + (trace_learningRate * SA_cells(:) * SA_trace(:)');      % Update the synapses between SA cells with (constant . current SA firing . SA trace firing from previous timesteps). SA_trace is zeros in first timestep.

% Normalise synapses
SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);                              % Normalise SAtoSA synapses as previous.
SAtoSA_synapses(isnan(SAtoSA_synapses)) = 0;                                                % Gets rid of any NaN values that the normalisation may have introduced.
end
toc;
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%disp('Complete')
end














function matrix = normalise(matrix, threshold)

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));                 % Normalises a matrix so that each column sums to the threshold value given. If you want to normalise rows, transpose the matrix before using this function.

end