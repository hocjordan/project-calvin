function [SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_tracked, sensory_tracked] = RobotLearner_140415(steps, actions)

% Based on Experiment66_learner_SAFE.

% Input parameter 'steps' is an integer describing the number of
% exploration steps to be made.

% Input parameter 'actions' is a cell array of strings designating certain
% actions, e.g.:
% ['RED_ON', 'RED_OFF', 'GREEN_ON', 'GREEN_OFF']
% ['Forward', 'Backward', 'Left', 'Right']

% Outputs synapse matrices.

% Outputs sensory_tracked, which tracks the various states the model has
% experienced.

% Outputs SA_decoded, which tracks the state/action combination currently
% associated with each neuron.

% Calls a WORLD function to receive sensory input and to perform a planned
% action.

%% Change-Log

% Created on April 13th 2015

% Apparently works on April 14th 2015. Saved SAFE version.

% Commented out global variable 'world_state' and update functions.


%% Parameters


% GET SENSORY DATA FROM WORLD (ONLY 1 CELL ACTIVE AT A TIME)
%[sensory_cells] = world_update_function('', 'yes');
%sensory_cells = zeros(10);


% Cells
num_sensory = [size(sensory_cells)];
num_motor = [1 size(actions, 2)];
num_SAcol = prod(num_sensory);
num_SAcellsinCol = prod(num_motor);
num_SA = [num_SAcellsinCol 1 num_SAcol];

% Competition and Learning
learningRate = 100; %100
trace_learningRate = 10; %0.1; %0.001; % 0.0001

sensory_threshold = 1; % Necessary to adjust these to compensate for different
motor_threshold = 0.25; % numbers of synapses to sensory and motor cells.
trace_threshold = 4; %0.01

eta = 0.0;

% Noise
state_noise = 0.002;

% Analysis
SA_tracked = {};
sensory_tracked = {};
text_on = 0;


%% Setup

disp('Setting up...')

% Make networks of sensory, motor and SA neurons.
motor_cells = zeros(num_motor);
SA_cells = zeros(num_SA);
SA_trace = zeros(num_SA);

% Create synapse weights between SA cells and motor<=>SA.
SAtoSA_synapses = zeros(numel(SA_cells), numel(SA_cells));
motortoSA_synapses = rand(numel(motor_cells), numel(SA_cells));
SAtomotor_synapses = rand(numel(SA_cells), numel(motor_cells));

% Create sensory => SA synapses that are identical for all cells in a
% column.
sensorytoSA_synapses = [];
for column = 1:num_SAcol
    sensorytoSA_synapses = [sensorytoSA_synapses repmat(rand(numel(sensory_cells), 1), [1 size(actions,2)])];
end

% Normalise synapses
sensorytoSA_synapses = normalise(sensorytoSA_synapses, sensory_threshold);
motortoSA_synapses = normalise(motortoSA_synapses, motor_threshold);
SAtomotor_synapses = normalise(SAtomotor_synapses', motor_threshold);
SAtomotor_synapses = SAtomotor_synapses';

disp('...Setup Complete.')

%% RUN TRIAL

for time = 1:steps
    
        
    %% Firing of SA cells and Recruitment of Column
    
    % GET SENSORY DATA FROM WORLD (ONLY 1 CELL ACTIVE AT A TIME)
    %[sensory_cells] = world_update_function('', 'yes');
    %sensory_cells = zeros(10);
    
    % SA cells fire based on current sensory cells (state).
    SA_cells = dot((repmat(sensory_cells(:),[1,numel(SA_cells)])), sensorytoSA_synapses);
    SA_cells = reshape(SA_cells, num_SA);
    
    % TRANSFER FUNCTION
    SA_cells(SA_cells > 0.1 & SA_cells < 0.95) = 0;
    
    % columnar WTA
    SA_cells(SA_cells == max(max(SA_cells))) = 1;
    SA_cells(SA_cells ~= max(max(SA_cells))) = 0;
    
    % RECORD STATES EXPERIENCED
    sensory_info = sensory_cells;
    sensory_tracked{time} = sensory_info;
    
    % Update sensorytoSA weights.
    sensorytoSA_synapses = sensorytoSA_synapses + (learningRate * (sensory_cells(:) * SA_cells(:)'));
    sensorytoSA_synapses = normalise(sensorytoSA_synapses, sensory_threshold);
    
    %% TRACE LEARNING: Update connections from ACTIVE COLUMN to PREVIOUS (trace) SA_CELL:
    
    % Update synapses using lr * state firing * trace rule (backwards)
    SAtoSA_synapses = SAtoSA_synapses + (trace_learningRate * SA_cells(:) * SA_trace(:)');
    
    % Normalise synapses
    SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);
    SAtoSA_synapses(isnan(SAtoSA_synapses)) = 0;
    
    %% Select an action and activate the appropriate motor cell.
    
    action = actions{randi(size(actions,2))};
    
    % UPDATE WORLD FROM ACTION (ONLY 1 CELL ACTIVE AT A TIME)
    %world_update_function(action, 'no');
    %sensory_cells = zeros(10);
    
    [~, idx] = ismember(action, actions);
    motor_cells(idx) = 1;
    
    % Activation of SA cells from sensory and motor cells simulataneously.
    SA_cells(:) = 0;
    SA_cells = dot(([repmat(motor_cells(:), [1,numel(SA_cells)]); repmat(sensory_cells(:), [1,numel(SA_cells)])]), [motortoSA_synapses; sensorytoSA_synapses]);
    SA_cells = reshape(SA_cells, num_SA);
    
    % WTA:
    SA_cells = WTA_Competition(SA_cells(:));
    SA_cells = reshape(SA_cells, num_SA);
   
    % Weights updated.
    
    motortoSA_synapses = motortoSA_synapses + learningRate * motor_cells(:) * SA_cells(:)';
    SAtomotor_synapses = SAtomotor_synapses + learningRate * SA_cells(:) * motor_cells(:)';
    
    % Weights normalised
    
    motortoSA_synapses = normalise(motortoSA_synapses, motor_threshold);
    SAtomotor_synapses = normalise(SAtomotor_synapses', motor_threshold);
    SAtomotor_synapses = SAtomotor_synapses';
    
    % RECORD STATE/ACTION COMBINATIONS EXPERIENCED
    SA_info{1} = sensory_cells;
    SA_info{2} = find(motor_cells);
    SA_info{3} = find(SA_cells > 0.1)';
    SA_tracked{time} = SA_info;
    
    
    %% Calculate SA trace
    
    % Calculate the trace value for all cells
    SA_trace = getTrace(SA_cells, SA_trace, eta);
    
    %% Reset Cells and Update parameters
    
    
    SA_cells(:) = 0;
    sensory_cells(:) = 0;
    motor_cells(:) = 0;
    
end



disp('Complete')
end















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));

end