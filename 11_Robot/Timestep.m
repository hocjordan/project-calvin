%% 1 TIMESTEP FOR HIERARCHICAL MODEL-BASED LEARNING

% Update sensory cells
% Check if in reward zone.
    % If so:
        % Check if reward obtained:
            % If so:
                % Reward signal updates connection between constant reward
                % representation and state/state-action cells
            % If not:
                % Connection between representation and state/state-action
                % lost or degraded.
% Update competitive layer with input from sensory cells (state and state-action cells become active)
% Update motor cells with input from state-action cells & action cells
% Update competitive layer with recurrent input from competitive layer
% Agent moves (take action from motor cells and update world)
% Deactivate motor cells

%% Code

function [ world, sensory_weights, motor_weights, recurrent_weights, reward_weights, trace, reward, reward_fr ] = Timestep( world, sensory_weights, motor_weights, recurrent_weights, reward_weights, trace, reward, comp_size, sparseness, slope, learningRate, eta, reward_fr )

% Clear sensory and motor firing

sensory_fr = zeros(1,size(sensory_weights,1));
motor_fr = zeros(1,size(motor_weights,1));
comp_fr = zeros(1,size(recurrent_weights,1));

% Update sensory cells according to agent position (one cell per position)

sensory_fr(world(:,:,1) == 1) = 1;
    
%% Update competitive layer with input from sensory cells (state and state-action cells become active)
%tic
[comp_fr, sensory_weights] = layer_update(sensory_fr, trace, sensory_weights, comp_size, sparseness, slope, learningRate, 0, 'Soft');
%toc

%% Reward System

% Check if in assumed reward zone.
[agent_position_y, agent_position_x, ~] = ind2sub([10 10 2],find(world == 1));
[reward_position_y, reward_position_x, ~] = ind2sub([10 10 2],find(world == 2));

    % If so:
    if agent_position_x == reward_position_x && agent_position_y == reward_position_y
        
    % Check if reward obtained i.e. if in actual reward zone (pseudocode; add assumed reward functionality later):
    
    %if agent_position == true_reward
    
    % If so:
    
        % activates reward representation.
        % updates connection between r.r. and competitive neurons.
        % NOT TRACE. High LearningRate
        
        reward = 1
        reward_fr = 1;
        
        reward_weights=reward_weights+(1*comp_fr');
        
        % normalise the weights to a total of 1 for each cell
        
        reward_weights = normalise(reward_weights);
    
        return
            % If not:
                % Connection between representation and state/state-action
                % lost or degraded.
                
    %end
      
    end
    
%% Update motor cells with input from state-action cells & action cells.
% Use WTA -- only one action can be taken at a time.
% Reverse dimensions of motor_weights b/c 10 comp to 8 motor.
%tic
[motor_fr, motor_weights] = layer_update(comp_fr, trace, motor_weights', 80, sparseness, slope, learningRate, 0, 'WTA');
%toc

%% Update competitive layer with input from motor cells.
% Reverse dimensions of motor_weights again b/c now 8 motor to 10 comp.
%tic
[comp_fr, motor_weights] = layer_update(motor_fr, trace, motor_weights', comp_size, sparseness, slope, learningRate, 0, 'Soft');
%toc

%% Update competitive layer with recurrent input from competitive layer
%tic
[comp_fr, recurrent_weights] = layer_update(comp_fr, trace, recurrent_weights, comp_size, sparseness, slope, learningRate, eta, 'Soft');
%toc
trace = comp_fr;

%% Agent moves (take output from motor cells, adds noise, performs actions and updates world)

actions = {'N' 'NE' 'E' 'SE' 'S' 'SW' 'W' 'NW'};
motor_output = ceil(find((motor_fr == max(motor_fr)))/10);
if rand(1) > 0.9
    motor_output = randi(8);
    disp('NUDGED')
end
action = actions{motor_output};
disp(action)
world = update_world(world, action);

end