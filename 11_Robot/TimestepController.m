function [action, result] = TimestepController(ind_steps, actions, world_update_function, neurons, synapses, num_param, competition_param, learning_param, reward_firing)



global world_state


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Unpack Arrays
motor_cells             = neurons{1};                                                                   % Variables including cell layer and synapse matrices have been stored in...
SA_cells                = neurons{2};                                                                   % ...arrays, so that they don't have to be recreated every timestep.
SA_trace                = neurons{3};                                                                   % Here they are read out from those arrays.

sensorytoSA_synapses    = synapses{1};
motortoSA_synapses      = synapses{2};
SAtomotor_synapses      = synapses{3};
SAtoSA_synapses         = synapses{4};
identitySynapses        = synapses{5};

num_sensory             = num_param{1};
num_motor               = num_param{2};
num_SAcol               = num_param{3};
num_SAcellsinCol        = num_param{4};
num_SA                  = num_param{5};

SA_modifier             = competition_param{1};
sensory_threshold       = competition_param{2};
motor_threshold         = competition_param{3};
trace_threshold         = competition_param{4};

learningRate            = learning_param{1};
trace_learningRate      = learning_param{2};
eta                     = learning_param{3};




%% Set Up Reward Gradient
% Place reward.
[~, reward_cells] = world_update_function('', 'yes');                                                   % Activate a set of reward cells (of the same number and structure as the state cells), denoting the position

% SA reward cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
SA_reward = dot((repmat(reward_cells(:),[1,numel(SA_cells)])), sensorytoSA_synapses);                   % Take the SA reward (filter layer) and activate each cell with the dot product of the reward cells and the sensorytoSA synapses.

% filter/transfer SA_reward function MAKE GENERAL LATER!!!!!!!!!!!!
SA_reward(SA_reward < 0.5) = 0;                                                                         % Filter out rewards that are not the primary (to avoid x/y coordinate confusion). Value depends on number of possible conflicts.3

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Induction to Set up Reward Gradient

for ind_time = 1:ind_steps
    
    
    
    % final SA update
    SA_cells = dot(([repmat(SA_reward(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [identitySynapses; SAtoSA_synapses]); % SA cells actived by [SA_reward . identity] + [SA_cells . SAtoSA_synapses].
    SA_cells = reshape(SA_cells,num_SA);                                                                    % Unimportant; makes sure SA layer has same dimensions as previous.
    
    
    % Competition in state cells (order can be reversed):
    
    % Subtractive Competition
    for column = 1:size(SA_cells,3)                                                                         % For each column in the SA layer:
        SA_cells(:,:,column) = SA_cells(:,:,column) - SA_modifier * mean(SA_cells(:,:,column));             % Subtract the mean activation of that column, modified by a constant, from the activation of every cell in that column.
    end
    
    % Divisive Inhibition
    SA_cells = SA_cells/max(max(SA_cells));                                                                 % Divide every cell in the SA.
    
    % Clean Up
    sum_save = sum(SA_cells);                                                                               % Save the sum of every SA column.
    SA_cells(SA_cells < 0) = 0;                                                                             % Make all cells with negative activation = 0.
    for col = 1:size(SA_cells,3)                                                                            % For every column:
        SA_cells(:,:,col) = normalise(SA_cells(:,:,col), sum_save(:,:,col));                                % Normalise the activation of these columns so that they sum to the sum saved before removing negative cells (i.e. redistribute the added activation)
        SA_cells(isnan(SA_cells)) = 0;                                                                      % Remove the NaN values created when a column sums to 1 (or could just
    end
    SA_cells = reshape(SA_cells,num_SA);                                                                    % See above.
    
    
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Record agent position
%sensory_info = world_state;                                                                                 % Records the states the agent passes through.
%sensory_tracked{time} = sensory_info;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Check for Completion
% ALTER FOR GENERALISATION, OR JUST CALL PART OF THE SIMULATION
[sensory_cells, ~] = world_update_function('', 'yes');                                                      % Get sensory data from world update function.
if ~any(~ismember(find(reward_cells), find(sensory_cells)))                                                 % If all nonzero elements of the reward representation are also represented in the sensory cells:
    result = 'Y';
    disp(result)
    action = '';
    return                                                                                              % End the simulation and return a positive result.
else
    result = 'N';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Move Agent through Bursting Sensory Activity
[sensory_cells, ~] = world_update_function('', 'yes');                                                      % Get sensory data from world update function.
sensory_cells(sensory_cells > 0) = 100;                                                                     % Boost the firing rate of firing sensory cells (represents a phasic burst)
reward_cells(reward_cells > 0) = reward_firing;                                                             % Set the firing rate of reward cells to that specified by the parameter 'reward_firing'. Usually 1.

SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [sensorytoSA_synapses; sensorytoSA_synapses; SAtoSA_synapses]); % Activate sensory cells from the dot product of (the bursting sensory cell activation, the reward cell activation) and the synapse weights for each.
SA_cells = WTA_Competition(SA_cells);                                                                       % Set the cell with the strongest activation to 1 and the rest to 0.
SA_cells = reshape(SA_cells,num_SA);                                                                        % See above.


% Calculate activation to motor cells
motor_cells = dot(repmat(SA_cells(:),[1,numel(motor_cells)]), SAtomotor_synapses);                          % Dot product of the SA cell layer's activation and the SA-to-motor synapses.

% Return planned action.
action = actions{find(motor_cells == max(motor_cells))};                                                    % Return the action corresponding to the motor cell with the highest activation.

%% Clear cells
sensory_cells(:) = 0;
reward_cells(:) = 0;
SA_cells(:) = 0;
motor_cells(:) = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Repack Arrays
%neurons{1}      = motor_cells;                                                                              % Writing variables back into the arrays.
%neurons{2}      = SA_cells;
%neurons{3}      = SA_trace;

synapses{1}     = sensorytoSA_synapses;
synapses{2}     = motortoSA_synapses;
synapses{3}     = SAtomotor_synapses;
synapses{4}     = SAtoSA_synapses;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end














function matrix = normalise(matrix, threshold)

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));

end