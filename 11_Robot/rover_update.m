
function [sensory_cells] = rover_update(action)
% Connect to robot.
a=serial('/dev/cu.usbmodem3a21');
set(a, 'Timeout', 30)
fopen(a);

% Pause and then pass action to robot.
pause(3)
fprintf(a,action)


% Loop the MATLAB program until movement is complete, gathering rangefinder
% data
result = 'N';
dataset = [];
while(result ~= 'Y')
    result = fscanf(a);
    data = str2double(result);
    data = log(data);
    dataset = [dataset data];
end



% Analyse and Convert to Sensory Cells
sensory_cells = zeros(1,180);

dataset(isnan(dataset)) = [];
disp(dataset)
dataset = dataset/10;
dataset = dataset/0.6;
disp(dataset)
data_count = 0;
for data = dataset
    % Convert data
    data_cells = gaussian_cells(10, data, 0.005);
    data_cells(isnan(data_cells)) = 0;
    sensory_cells(data_count*10+1:data_count*10+10) = data_cells;
    data_count = data_count+1;
end
%disp(sensory_cells)
plot(sensory_cells)

% Disconnect from Robot
delete(instrfind({'Port'},{a.Port}))

end