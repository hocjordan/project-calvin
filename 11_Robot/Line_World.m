function [sensory_tracked_matrix, SA_decoded, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses] = Line_World()


%% Define Actions
actions = {'w' 's'};

%% Define World Functions
world_update_function = @rover_update;

%% Call Learner function
[SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_tracked, sensory_tracked] = Clean_150413_LEARNER(1000, actions, world_update_function);
%[SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_tracked, sensory_tracked] = Experiment74(1000, actions, world_update_function);

%% Analyse and Save Data

sensory_tracked_matrix = [];
for time = 1:size(sensory_tracked,2)
    sensory_tracked_matrix(:,:,:,time) = sensory_tracked{time};
end

% Decode SA information
SA_decoded = [];
for time = 1:size(sensory_tracked,2)
    SA_tracked{time}{1} = find(SA_tracked{time}{1});
    SA_decoded(time,:) = cell2mat(SA_tracked{time}(:)');
end

% Record every state/action combo.
SA_decoded = unique(SA_decoded,'rows');
SA_decoded = sortrows(SA_decoded, 3);

%% Ask for New Agent/Goal Positions

[reward_x, reward_y, ~] = create_maze(worldSize_x, worldSize_y, walls);

%% Call Goal Function
sensory_tracked = Clean_150413_CONTROLLER(100, 50, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded, actions, world_update_function);
%sensory_tracked = Experiment75(100, 30, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded, actions, world_update_function);

%% Analyse and Save Data
sensory_tracked_matrix = [];
for time = 1:size(sensory_tracked,2)
    sensory_tracked_matrix(:,:,:,time) = sensory_tracked{time};
end

%
world_tracked = {};
for time = 1:size(sensory_tracked,2)
    world_tracked{time} = sensory_tracked_matrix(:,:,1,time) + sensory_tracked_matrix(:,:,3,time);
end

slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
%}

end