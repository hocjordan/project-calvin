function [neurons, synapses, SA_tracked, sensory_tracked] = TimestepLearner(actions, world_update_function, neurons, synapses, num_param, competition_param, learning_param)

%% Unpack Arrays
motor_cells             = neurons{1};       % Variables including cell layer and synapse matrices have been stored in...
SA_cells                = neurons{2};       % ...arrays, so that they don't have to be recreated every timestep.
SA_trace                = neurons{3};       % Here they are read out from those arrays.

sensorytoSA_synapses    = synapses{1};
motortoSA_synapses      = synapses{2};
SAtomotor_synapses      = synapses{3};
SAtoSA_synapses         = synapses{4};

num_sensory         = num_param{1};
num_motor           = num_param{2};
num_SAcol           = num_param{3};
num_SAcellsinCol    = num_param{4};
num_SA              = num_param{5};

SA_modifier         = competition_param{1};
sensory_threshold   = competition_param{2};
motor_threshold     = competition_param{3};
trace_threshold     = competition_param{4};

learningRate        = learning_param{1};
trace_learningRate  = learning_param{2};
eta                 = learning_param{3};

%% Firing of SA cells and Recruitment of Column

% GET SENSORY DATA FROM WORLD (ONLY 1 CELL ACTIVE AT A TIME)
[sensory_cells, ~] = world_update_function('', 'yes');
%sensory_cells = zeros(10);

% SA cells fire based on current sensory cells (state).
SA_cells = dot((repmat(sensory_cells(:),[1,numel(SA_cells)])), sensorytoSA_synapses);       % Calculate activation of SA cell from dot product of sensory cell firing rates and sensorytoSA_synapses.
SA_cells = reshape(SA_cells, num_SA);                                                       % Unimportant. Makes sure that the SA cell layer retains its initial dimensions.

% TRANSFER FUNCTION
SA_cells(SA_cells > 0.1 & SA_cells < 0.95) = 0;                                             % Sets all SA_cells with activation above 0.1 and below 0.95 to zero. Important when there are multiple cells active in sensory layer.

% columnar WTA
SA_cells(SA_cells == max(max(SA_cells))) = 1;                                               % Sets all SA cells within most activated SA column to 1.
SA_cells(SA_cells ~= max(max(SA_cells))) = 0;                                               % Sets all other SA cells to 0.

% RECORD STATES EXPERIENCED
%sensory_info = sensory_cells;                                                               % Records the current sensory input...
%sensory_tracked{time} = sensory_info;                                                       % ...and saves it to a cell array.

% Update sensorytoSA weights.
sensorytoSA_synapses = sensorytoSA_synapses + (learningRate * (sensory_cells(:) * SA_cells(:)')); % For each sensory cell, adds (constant . sensory cell activation (presynaptic SA) . postsynaptic SA)
sensorytoSA_synapses = normalise(sensorytoSA_synapses, sensory_threshold);                  % Normalise sensorytoSA synapses to sensory_threshold.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% TRACE LEARNING: Update connections from ACTIVE COLUMN to PREVIOUS (trace) SA_CELL:

% Update SAtoSA synapses
SAtoSA_synapses = SAtoSA_synapses + (trace_learningRate * SA_cells(:) * SA_trace(:)');      % Update the synapses between SA cells with (constant . current SA firing . SA trace firing from previous timesteps). SA_trace is zeros in first timestep.

% Normalise synapses
SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);                              % Normalise SAtoSA synapses as previous.
SAtoSA_synapses(isnan(SAtoSA_synapses)) = 0;                                                % Gets rid of any NaN values that the normalisation may have introduced.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Select an action

action = actions{randi(size(actions,2))};                                                   % Select a random action out of the subset of possible actions.

% UPDATE WORLD FROM ACTION (ONLY 1 CELL ACTIVE AT A TIME)
world_update_function(action, 'no');                                                        % Output that function into the world, changing the world in some way.
%sensory_cells = zeros(10);

%% Activate the appropriate motor cell.

[~, idx] = ismember(action, actions);                                                       % Activate the motor cell that represents the action you just took.
motor_cells(idx) = 1;

%% Activation of SA cells from sensory and motor cells simulataneously, updating synapses.
SA_cells(:) = 0;                                                                            % Reset all SA cells to 0.
SA_cells = dot(([repmat(motor_cells(:), [1,numel(SA_cells)]); repmat(sensory_cells(:), [1,numel(SA_cells)])]), [motortoSA_synapses; sensorytoSA_synapses]); % Activate SA cells using the dot product of (motor cell firing . motortoSA_synapses) plus the dot product of (sensory_cells . sensorytoSA)
SA_cells = reshape(SA_cells, num_SA);

% WTA:
SA_cells = WTA_Competition(SA_cells(:));                                                    % The strongest cell in the SA layer (if there is more than one, choose randomly) is set to one, and all else to 0.
SA_cells = reshape(SA_cells, num_SA);                                                       % Unimportant.

% Weights updated.

motortoSA_synapses = motortoSA_synapses + learningRate * motor_cells(:) * SA_cells(:)';     % Update motortoSA synapses...
SAtomotor_synapses = SAtomotor_synapses + learningRate * SA_cells(:) * motor_cells(:)';     % ...and SAtomotor synapses.

% Weights normalised

motortoSA_synapses = normalise(motortoSA_synapses, motor_threshold);                        % Normalise over columns of motortoSA synapses and rows of SAtomotor.
SAtomotor_synapses = normalise(SAtomotor_synapses', motor_threshold);
SAtomotor_synapses = SAtomotor_synapses';

% RECORD STATE/ACTION COMBINATIONS EXPERIENCED
%SA_info{1} = sensory_cells;                                                                 % Record sensory input, motor output and firing SA celll in a cell array.
%SA_info{2} = find(motor_cells);
%SA_info{3} = find(SA_cells > 0.1)';
%SA_tracked{time} = SA_info;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Calculate SA trace

% Calculate the trace value for all cells
SA_trace = getTrace(SA_cells, SA_trace, eta);                                               % Calculate a memory trace for each SA cell from their current firing, in preparation for the next timestep.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Reset Cells and Update parameters


SA_cells(:) = 0;                                                                            % Reset all cells to 0.
sensory_cells(:) = 0;
motor_cells(:) = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Repack Arrays
neurons{1}      = motor_cells;                                                              % Writing variables back into the arrays.
neurons{2}      = SA_cells;
neurons{3}      = SA_trace;

synapses{1}     = sensorytoSA_synapses;
synapses{2}     = motortoSA_synapses;
synapses{3}     = SAtomotor_synapses;
synapses{4}     = SAtoSA_synapses;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%disp('Complete')
end















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;                                     % The memory trace for each SA cell is its current firing added to the memory trace from the last timestep in a ratio determined by eta.
end



function matrix = normalise(matrix, threshold)

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));                 % Normalises a matrix so that each column sums to the threshold value given. If you want to normalise rows, transpose the matrix before using this function.

end