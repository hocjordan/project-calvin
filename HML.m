function [world_tracked, mappings, firingRates] = HML( comp_size )
%HIERARCHICAL MODEL-BASED LEARNING Summary of this function goes here


% Parameters
eta = 0.8;
sparseness = 70;
slope = 90;
learningRate = 0.0004;
agent_x = 5;
agent_y = 5;
reward_x = 7;
reward_y = 7;
mapping_threshold = 0.3;

% Set up world.

disp('Creating World')
[world] = create_world(agent_x, agent_y, reward_x, reward_y);

% Set up networks

disp('Creating Agent')
[recurrent_weights, sensory_weights, motor_weights, reward_weights] = setup_agent(comp_size);

%% First Trial
% Run model for a certain number of timesteps or until the goal is achieved

time = 0;
trace = zeros(1, comp_size);

reward = 0;
reward_fr = 0;

while time < 1000 && reward == 0;
    
    disp(time)
    
    [world, sensory_weights, motor_weights, recurrent_weights, reward_weights, trace, reward, reward_fr] = Timestep(world, sensory_weights, motor_weights, recurrent_weights, reward_weights, trace, reward, comp_size, sparseness, slope, learningRate, eta, reward_fr);
    
    time = time + 1;
    
    world_tracked{time} = world(:,:,1);

end

% After this has been done, display the agent's trajectory through the
% world

disp('Analysing Agent Trajectory.')
disp('...')
slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
disp('Complete')

disp('Calculating state/action mappings.')
[mappings, firingRates,] = HML_mappings(sensory_weights, motor_weights, mapping_threshold);
disp('Complete')

if reward == 0;
    disp('Trial Failed')
    return
end

disp('NEXT TRIAL')

%% Second Trial
% Run model for a certain number of timesteps or until the goal is achieved

% Reset world.

disp('Resetting World')
[world] = create_world(agent_x, agent_y, reward_x, reward_y);

time = 0;
trace = zeros(1, comp_size);

reward = 0;

while time < 1000 && reward == 0;
    
    disp(time)
    
    [world, sensory_weights, motor_weights, recurrent_weights, reward_weights, trace, reward, reward_fr] = Timestep(world, sensory_weights, motor_weights, recurrent_weights, reward_weights, trace, reward, comp_size, sparseness, slope, learningRate, eta, reward_fr);
    
    time = time + 1;
    
    world_tracked2{time} = world(:,:,1);

end

% After this has been done, display the agent's trajectory through the
% world

disp('Analysing Agent Trajectory.')
disp('...')
slider_display(world_tracked2, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
disp('Complete')

disp('Calculating state/action mappings.')
[mappings, firingRates,] = HML_mappings(sensory_weights, motor_weights);
disp('Complete')
%}

disp('DONE>>>')

end