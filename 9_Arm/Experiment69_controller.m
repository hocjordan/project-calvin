function Experiment69_controller(steps, ind_steps, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded)

% As Experiment66_controller, with an added inverse reward signal to discourage semi-rewarded SA cells.
%

%Arm
j1_angle = 0;
j2_angle = 0;
j1_pos_x = 2;
j1_pos_y = 0;
j2_pos_x = 5;
j2_pos_y = 0;

arm_info{1} = j1_angle;
arm_info{2} = j2_angle;
arm_info{3} = j1_pos_x;
arm_info{4} = j1_pos_y;
arm_info{5} = j2_pos_x;
arm_info{6} = j2_pos_y;

% Reward
reward_x = -1.1206; %-0.5000; %4; %2.4718; %-1.8191;
reward_y = -0.6840; %-0.6840; %-1.7321; %2.9957; %-0.7060;

% Actions
actions = {'j1_inc', 'j1_dec', 'j2_inc', 'j2_dec', '', 'j1_inc_j2_dec', 'j1_inc_j2_inc', 'j1_dec_j2_inc', 'j1_dec_j2_dec'};

% Cells
gauss_deviation = 0.005; %0.01;
[sensory_cells, ~] = Experiment66_world('', arm_info, gauss_deviation, 'yes');
num_sensory = [size(sensory_cells)];

reward_pos = Experiment66_position_gauss(reward_x, reward_y, gauss_deviation); % Takes in reward coordinators and converts those to position_cell representations.
num_reward_blank = [1 400];
reward_cells = [reward_pos{:}, zeros([1 400])];
num_reward = [size(reward_cells)];

num_motor = [1 size(actions, 2)];
num_SAcol = prod(num_sensory);
num_SAcellsinCol = prod(num_motor);
num_SA = [num_SAcellsinCol 1 num_SAcol];
agent_firing = 0;
reward_firing = 1;

% Competition
SA_modifier = 0.9; %0.02; % 0.2

% Noise
SA_noise = 0.100; %0.001;

% Analysis
display_tracked = {};

%% Setup

% Create cells
SA_cells = zeros(num_SA);
motor_cells = zeros(num_motor);
trace = zeros(num_SA);

%% RUN TRIAL
for time = 1:steps
    disp(time)
    tic
    % Allow activity to spread for 1:ind_time
    for ind_time = 1:ind_steps
        %
        if ind_time == round(ind_steps/4)
            disp('...')
        elseif ind_time == round(ind_steps/2)
            disp('...')
        elseif ind_time == round(3*ind_steps/4)
            disp('...')
        end
        %
        noNaNsensorytoSA = noNaN(sensorytoSA_synapses);
        noNaNmotortoSA = noNaN(motortoSA_synapses);
        noNaNSAtomotor = noNaN(SAtomotor_synapses);
        noNaNSAtoSA= noNaN(SAtoSA_synapses);
        
        
        %% Set Up Reward Gradient
        % Place reward.
        reward_pos = Experiment66_position_gauss(reward_x, reward_y, gauss_deviation);
        reward_pos{1} = WTA_Competition(reward_pos{1}); reward_pos{2} = WTA_Competition(reward_pos{2});
        reward_cells = [reward_pos{:}, zeros(num_reward_blank)];
        num_reward = [size(reward_cells)];
        reward_cells(reward_cells > 0) = reward_firing;
        reward_cells(reward_cells < 0) = -reward_firing; % negative reward signal
        
        % Sensory firing.
        %[sensory_cells, ~] = Experiment66_world('', arm_info, gauss_deviation, 'yes');
        %sensory_cells(sensory_cells > 0) = agent_firing;
        
        
        % SA cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
        %SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
        SA_cells = dot(([repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNSAtoSA]);
        SA_cells = reshape(SA_cells,num_SA);
        
        
        % Competition in state cells:
        
        % subtractive.
        %SA_cells = SA_cells - SA_modifier * mean(mean(SA_cells));
        for column = 1:size(SA_cells,3)
            SA_cells(:,:,column) = SA_cells(:,:,column) - SA_modifier * mean(SA_cells(:,:,column));
            %SA_cells(:,:,column) = mean(SA_cells(:,:,column)) * WTA_Competition(SA_cells(:,:,column));
        end
        
        % divisive.
        SA_cells = SA_cells/max(max(SA_cells));
        sum_save = sum(SA_cells);
        SA_cells(SA_cells < 0) = 0;
        for col = 1:size(SA_cells,3)
            SA_cells(:,:,col) = normalise(SA_cells(:,:,col), sum_save(:,:,col));
        end
        SA_cells = reshape(SA_cells,num_SA);
        
        
        % add noise
        %SA_cells = SA_cells(:)' + SA_noise .* std(SA_cells(:)) * randn(1,size(SA_cells(:),1));
        %SA_cells(SA_cells < 0) = 0;
        %SA_cells = reshape(SA_cells,num_SA);
        
        %display = SAcol_analyseAct_internal(200, SA_cells, SA_decoded, gauss_deviation);
        %display_tracked{ind_time} = cell2mat(display);
        
    end
    
    %slider_display(display_tracked, []);
    
    % Record agent position
    figure(); axes('XLim', [-5 5], 'YLim', [-5 5]); line([0, arm_info{3}], [0, arm_info{4}]); line([arm_info{3}, arm_info{5}], [arm_info{4}, arm_info{6}]);
    
    j0_rectangle = annotation('rectangle');
    set(j0_rectangle, 'parent', gca)
    set(j0_rectangle, 'position', [0-0.5 0-0.1 1 0.2])
    
    j1_ellipse = annotation('ellipse');
    set(j1_ellipse, 'parent', gca)
    set(j1_ellipse, 'position', [arm_info{3}-0.1 arm_info{4}-0.1 0.2 0.2])
    
    j2_ellipse = annotation('ellipse');
    set(j2_ellipse, 'parent', gca)
    set(j2_ellipse, 'position', [arm_info{5}-0.1 arm_info{6}-0.1 0.2 0.2])
    
    reward_ellipse = annotation('ellipse');
    set(reward_ellipse, 'parent', gca)
    set(reward_ellipse, 'position', [reward_x-0.1 reward_y-0.1 0.2 0.2])
    
    % save figure
    print(gcf, '-dpng', fullfile('/Network/Servers/mac0.cns.ox.ac.uk/Volumes/Data/Users/jordan/Documents/Simulations/Arm', sprintf('ArmExperiment4_%d', time)))
    %close gcf
    
    
    %% Check for Completion
    current_state = [arm_info{5}, arm_info{6}];
    reward_location = [reward_x, reward_y];
    if max(abs(current_state - reward_location)) < 0.5
        result = 'Y';
        disp(time)
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Move Agent through Bursting Sensory Activity
    [sensory_cells, ~] = Experiment66_world('', arm_info, gauss_deviation, 'yes');
    sensory_cells(sensory_cells > 0) = 100;
    reward_cells(reward_cells > 0) = reward_firing;
    
    SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
    SA_cells = WTA_Competition(SA_cells);
    SA_cells = reshape(SA_cells,num_SA);
    
    %SA_cells = SA_cells + dot(repmat(sensory_cells(:),[1,numel(SA_cells)]), noNaNsensorytoSA);
    
    
    
    
    %SA_decoded(find(SA_decoded(:,3) == find(SA_cells == max(SA_cells))),:)
    
    % Calculate activation to motor cells
    motor_cells = dot(repmat(SA_cells(:),[1,numel(motor_cells)]), noNaNSAtomotor);
    
    % Move agent.
    action = actions{find(motor_cells == max(motor_cells))};
    fprintf('%d: %s', time, action)
    disp(' ')
    [sensory_cells, arm_info] = Experiment66_world(action, arm_info, gauss_deviation, 'yes');
    
    %% Clear cells
    sensory_cells(:) = 0;
    reward_cells(:) = 0;
    SA_cells(:) = 0;
    motor_cells(:) = 0;
    
end


% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);













end













function [display] = SAcol_analyseAct_internal(angle_layer_size, SA_cells, SA_decoded, gauss_deviation)

%% Import Arrows
arrows = repmat({uint8(zeros(16))}, [3 3]);
arrows{1} = imread('arrow_nw.png');
arrows{2} = imread('arrow_w.png');
arrows{3} = imread('arrow_sw.png');
arrows{4} = imread('arrow_n.png');
arrows{6} = imread('arrow_s.png');
arrows{7} = imread('arrow_ne.png');
arrows{8} = imread('arrow_e.png');
arrows{9} = imread('arrow_se.png');

for arrow = 1:numel(arrows);
    arrowNormalised = arrows{arrow};
    arrowNormalised(arrowNormalised > 0) = 255;
    arrows{arrow} = arrowNormalised;
end

% Make an array to display the arrows in.
display = repmat({uint8(zeros(16))}, [angle_layer_size*3 angle_layer_size*3]);

%% Retrieve the SA layer's activation and display
% Thresholding by the least active SA cell (within SA_decoded).
minimum_activation = min(min(SA_cells(SA_decoded(:,6))));
SA_cells = SA_cells - minimum_activation;

% Get maximum activation that remains.
maximum_activation = max(max(SA_cells));

for column = 1:size(SA_cells,3)
    
    %for cell = 1:size(SA_cells,1)
    [~, cell] = max(SA_cells(:,:,column));
    cell = sub2ind(size(SA_cells), cell, 1, column);
    
    % Check that cell has a sensory and motor conneciton i.e. is within
    % SA_decoded.
    if any(ismember(SA_decoded(:,6), cell)) == 1
        
        SA_decoded_line = SA_decoded(SA_decoded(:,6)==cell,:);
        %[y, x] = ind2sub([angle_layer_size angle_layer_size], SA_decoded(find(SA_decoded(:,3) == cell),1));
        j1_angle = SA_decoded_line(1); j2_angle = SA_decoded_line(2);
        [angle_cells] = Experiment66_angles_gauss(j1_angle, j2_angle, gauss_deviation);
        j1_angle = find(WTA_Competition(angle_cells{1})); j2_angle = find(WTA_Competition(angle_cells{2}));
        SA_state = (j1_angle * 3 - 1) + ((angle_layer_size*3) + (angle_layer_size*3) * 3 * (j2_angle-1));
        display{SA_state}(:) = 255;% (SA_cells(cell) / maximum_activation);
        
        switch SA_decoded_line(5)
            
            case 9
                display{SA_state - angle_layer_size*3 - 1} = arrows{1};% * (SA_cells(cell) / maximum_activation);
                
            case 4
                display{SA_state - angle_layer_size*3} = arrows{2};% * (SA_cells(cell) / maximum_activation);
                
            case 6
                display{SA_state - angle_layer_size*3 + 1} = arrows{3};% * (SA_cells(cell) / maximum_activation);
                
            case 2
                display{SA_state - 1} = arrows{4};% * (SA_cells(cell) / maximum_activation);
                
            case 1
                display{SA_state + 1} = arrows{6};% * (SA_cells(cell) / maximum_activation);
                
            case 8
                display{SA_state + angle_layer_size*3 - 1} = arrows{7};% * (SA_cells(cell) / maximum_activation);
                
            case 3
                display{SA_state + angle_layer_size*3} = arrows{8};% * (SA_cells(cell) / maximum_activation);
                
            case 7
                display{SA_state + angle_layer_size*3 + 1} = arrows{9};% * (SA_cells(cell) / maximum_activation);
                
        end
    %end
    end
end

%figure(); imagesc(cell2mat(display)); colormap('gray'); ylabel('Joint 1 Angle'); xlabel('Joint 2 Angle')

end



%{
function [display] = SA_analyseAct_internal(worldSize, SA_cells, SA_decoded)

%% Import Arrows
arrows = repmat({uint8(zeros(16))}, [3 3]);
arrows{1} = imread('arrow_nw.png');
arrows{2} = imread('arrow_w.png');
arrows{3} = imread('arrow_sw.png');
arrows{4} = imread('arrow_n.png');
arrows{6} = imread('arrow_s.png');
arrows{7} = imread('arrow_ne.png');
arrows{8} = imread('arrow_e.png');
arrows{9} = imread('arrow_se.png');

for arrow = 1:numel(arrows);
    arrowNormalised = arrows{arrow};
    arrowNormalised(arrowNormalised > 0) = 255;
    arrows{arrow} = arrowNormalised;
end

% Make an array to display the arrows in.
display = repmat({uint8(zeros(16))}, [worldSize*3 worldSize*3]);

%% Retrieve the SA layer's activation and display
% Thresholding by the least active SA cell (within SA_decoded).
minimum_activation = min(min(SA_cells(SA_decoded(:,3))));
SA_cells = SA_cells - minimum_activation;

% Get maximum activation that remains.
maximum_activation = max(max(SA_cells));

for cell = 1:numel(SA_cells)
    
    % Check that cell has a sensory and motor conneciton i.e. is within
    % SA_decoded.
    if any(ismember(SA_decoded(:,3), cell)) == 1
        
        [y, x] = ind2sub([worldSize worldSize], SA_decoded(find(SA_decoded(:,3) == cell),1));
        SA_state = (y * 3 - 1) + ((worldSize*3) + (worldSize*3) * 3 * (x-1));
        display{SA_state}(:) = 255;
        
        switch SA_decoded(find(SA_decoded(:,3) == cell),2)
            
            case 1
                display{SA_state - worldSize*3 - 1} = arrows{1} * (SA_cells(cell) / maximum_activation);
                
            case 2
                display{SA_state - worldSize*3} = arrows{2} * (SA_cells(cell) / maximum_activation);
                
            case 3
                display{SA_state - worldSize*3 + 1} = arrows{3} * (SA_cells(cell) / maximum_activation);
                
            case 4
                display{SA_state - 1} = arrows{4} * (SA_cells(cell) / maximum_activation);
                
            case 6
                display{SA_state + 1} = arrows{6} * (SA_cells(cell) / maximum_activation);
                
            case 7
                display{SA_state + worldSize*3 - 1} = arrows{7} * (SA_cells(cell) / maximum_activation);
                
            case 8
                display{SA_state + worldSize*3} = arrows{8} * (SA_cells(cell) / maximum_activation);
                
            case 9
                display{SA_state + worldSize*3 + 1} = arrows{9} * (SA_cells(cell) / maximum_activation);
                
        end
    end
end

end
%}

function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end
