function [position_cells] = Experiment66_position_orthogonal(j2_pos_x, j2_pos_y)

%Gives position of manipulator as a set to sensory 'position cells'

%% Parameters

learningRate = 9000;
round_value = 0.25;
norm_threshold = 1;
num_sensoryreps = [1 50];
num_combo = [1 100];

%% Build Position Representations

jpx_cells = zeros(num_sensoryreps);
jpy_cells = zeros(num_sensoryreps);
%{
combo_cells = zeros(num_combo);

if ~exist('combo_synapses','var')
    disp('Generating Rep => Combo synapses.')
    jpx_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    jpx_synapses = combo_synapses{1};
end

if ~exist('combo_synapses','var')
    disp('Generating Rep => Combo synapses.')
    jpy_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    jpy_synapses = combo_synapses{2};
end
%}

%% Activate Sensory Representations

% Position is on a scale of -5 to +5.

% Make positive
j2_pos_x = j2_pos_x + 5;

% Round to nearest 0.25.
j2_pos_x = floor(j2_pos_x/round_value)*round_value;

% Assign cell number based on the above.
j1 = j2_pos_x/round_value + 1;

% Activate appropriate cell.
jpx_cells(j1) = 1;

j2_pos_y = j2_pos_y + 5;
j2_pos_y = floor(j2_pos_y/round_value)*round_value;
j2 = j2_pos_y/round_value + 1;
jpy_cells(j2) = 1;

%{
%% No NaN Synapses
noNaNj1 = noNaN(jpx_synapses);
noNaNj2 = noNaN(jpy_synapses);


%% Activate Combo Representations
combo_cells = dot([repmat(jpx_cells(:),[1,numel(combo_cells)]); repmat(jpy_cells(:),[1,numel(combo_cells)])], [noNaNj1; noNaNj2]);

combo_cells = WTA_Competition(combo_cells);

%% Learn Synapses
jpx_synapses = jpx_synapses + (learningRate * jpx_cells(:) * combo_cells(:)');
jpy_synapses = jpy_synapses + (learningRate * jpy_cells(:) * combo_cells(:)');

%% Normalise Synapses
jpx_synapses = normalise(jpx_synapses, norm_threshold);
jpy_synapses = normalise(jpy_synapses, norm_threshold);
%}

%% Package Synapses
position_cells = {jpx_cells, jpy_cells};
end





function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches threshold. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end