function [sensory_cells] = Experiment66_Arm2DOF_sensory(arm_info)


%% Parameters

learningRate = 9000;
round_value = 20;
norm_threshold = 1;
num_sensoryreps = [1 ceil(360/round_value)];
num_j_all = [1 400];
num_j_pos = [1 400];

%% Build Sensory Representations

ja1_cells = zeros(num_sensoryreps);
ja2_cells = zeros(num_sensoryreps);
jp1_cells = zeros(num_sensoryreps);
jp2_cells = zeros(num_sensoryreps);
j_all_cells = zeros(num_sensoryreps);
jp_all_cells = zeros(num_sensoryreps);
sensory_cells = zeros(num_sensoryreps);

combo_cells = zeros(num_combo);

if ~exist('ja1_synapses','var')
    disp('Generating Ja1 => J_all synapses.')
    ja1_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    ja1_synapses = combo_synapses{1};
end

if ~exist('ja2_synapses','var')
    disp('Generating Ja2 => J_all synapses.')
    ja2_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    ja2_synapses = combo_synapses{2};
end

if ~exist('jp1_synapses','var')
    disp('Generating Jp1 => Jp_all synapses.')
    jp1_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    jp1_synapses = combo_synapses{1};
end

if ~exist('jp2_synapses','var')
    disp('Generating Jp2 => Jp_all synapses.')
    jp2_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    jp2_synapses = combo_synapses{2};
end

if ~exist('j_all_synapses','var')
    disp('Generating J_all => Sensory synapses.')
    j_all_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    j_all_synapses = combo_synapses{1};
end

if ~exist('jp_all_synapses','var')
    disp('Generating Jp_all => Sensory synapses.')
    jp_all_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    jp_all_synapses = combo_synapses{2};
end


%% Activate Sensory Representations
% Round to higher 20.
j1_angle = ceil(j1_angle/round_value)*round_value;
% Scale to within 360 degrees.
j1_angle = mod(j1_angle, 360);
% Assign cell number based on the above.
j1 = j1_angle/round_value + 1;
% Activate appropriate cell.
ja1_cells(j1) = 1;

j2_angle = ceil(j2_angle/round_value)*round_value;
j2_angle = mod(j2_angle, 360);
j2 = j2_angle/round_value + 1;
ja2_cells(j2) = 1;


%% Activate Joint-Angle Representations


%% Activate Joint-Position Representations

%% Feed Activity from the Aforementioned to an Angle-Layer and a Position-Layer

%% Feed Activity from the Aforementioned to a Combined Sensory Cell Layer.

end