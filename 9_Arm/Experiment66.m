function Experiment66(worldSize, steps, ind_steps, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, chunktoSA_synapses, SAtochunk_synapses, SA_decoded, walls)

% As Experiment62, altered to recieve input from a layer of chunk cells.
%

% World
worldSize_x = worldSize;
worldSize_y = worldSize;
agent_x = 1;
agent_y = 1;
reward_x = 1;
reward_y = 1;

% Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};

% Cells
num_sensory = [1 size(sensorytoSA_synapses, 1)];
num_reward = [num_sensory];
num_motor = [1 size(motortoSA_synapses, 1)];
num_SAcol = prod(num_sensory);
num_SAcellsinCol = prod(num_motor);
num_SA = [num_SAcellsinCol 1 num_SAcol];
num_chunk = [1 size(chunktoSA_synapses, 1)];
agent_firing = 0;
reward_firing = 1;

% Competition
SA_modifier = 0.9; %0.02; % 0.2
chunk_modifier = 1;

% Noise
SA_noise = 0.100; %0.001;

% Analysis
display_tracked = {};

%% Setup
% Create World
world = create_world3(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Create cells
sensory_cells = zeros(num_sensory);
reward_cells = zeros(num_reward);
SA_cells = zeros(num_SA);
motor_cells = zeros(num_motor);
chunk_cells = zeros(num_chunk);
trace = zeros(num_SA);

%% Walls.

% Add Walls to World
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

% place agent
fig = figure(); imagesc(world(:,:,3)); title('Please select starting position.');
[x, y] = ginput(1);
x=round(x); y=round(y);
agent_x = x; agent_y = y;
%synapses_summed(y,x) = 0;
close(fig)

% place reward
fig = figure(); imagesc(world(:,:,3)); title('Please select a goal.')
[x, y] = ginput(1);
x=round(x); y=round(y);
reward_x = x; reward_y = y;
close(fig)

world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

world(:,:,3) = zeros(size(world(:,:,2)));
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;


%% RUN TRIAL
for time = 1:steps
    tic
    % Allow activity to spread for 1:ind_time
    for ind_time = 1:ind_steps
        %{
        if ind_time == round(ind_steps/4)
            disp('...')
        elseif ind_time == round(ind_steps/2)
            disp('...')
        elseif ind_time == round(3*ind_steps/4)
            disp('...')
        end
        %}
        noNaNsensorytoSA = noNaN(sensorytoSA_synapses);
        noNaNmotortoSA = noNaN(motortoSA_synapses);
        noNaNSAtomotor = noNaN(SAtomotor_synapses);
        noNaNSAtoSA= noNaN(SAtoSA_synapses);
        noNaNSAtochunk = noNaN(SAtochunk_synapses);
        noNaNchunktoSA = noNaN(chunktoSA_synapses);
        
        
        %% Initial SA activity.
        % Sensory cells fire based on current position.
        
        sensory_cells(find(world(:,:,1))) = agent_firing;
        reward_cells(find(world(:,:,2))) = reward_firing;
        
        
        
        % SA cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
        SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
        SA_cells = reshape(SA_cells,num_SA);
        
        SA_save = SA_cells;

        
        
        %% Feed Activity to Chunk Cells
        
        % Uses a full trace rule -- preSynaptic_trace *
        % postSynaptic_trace
        
        % Calculate activation of chunk cells from SA cells
        chunk_cells = dot(repmat(SA_cells(:),[1,numel(chunk_cells)]), noNaNSAtochunk);
        chunk_cells = reshape(chunk_cells,num_chunk);
        chunk_save = chunk_cells;
        
        % Competition in chunk cells:
        % Subtractive.
        %chunk_cells = chunk_cells - chunk_modifier * mean(mean(chunk_cells));
        %chunk_cells(chunk_cells < 0) = 0;
        
        if any(chunk_cells) == 0
            %disp('No chunk cells firing')
        else
            %disp(find(chunk_cells))
            % divisive
            chunk_cells = 1 * (chunk_cells/max(max(chunk_cells)));
        end
        
        %% Final SA cell activity.
        
        % Reset state cells.
            SA_cells(:) = 0;
            
            %Calculate activation of state cells from the combined input of
            %recurrent and hierarchical synapses
            SA_cells = dot([repmat(SA_save(:),[1,numel(SA_cells)]); repmat(chunk_cells(:),[1,numel(SA_cells)])], [noNaNSAtoSA; noNaNchunktoSA]);
            SA_cells = reshape(SA_cells,num_SA);
            
            
                % Competition in SA cells:
        % subtractive.
        %SA_cells = SA_cells - SA_modifier * mean(mean(SA_cells));
        for column = 1:size(SA_cells,3)
            SA_cells(:,:,column) = SA_cells(:,:,column) - SA_modifier * mean(SA_cells(:,:,column));
            %SA_cells(:,:,column) = mean(SA_cells(:,:,column)) * WTA_Competition(SA_cells(:,:,column));
        end
        
        % divisive.
        SA_cells = SA_cells/max(max(SA_cells));
        sum_save = sum(SA_cells);
        SA_cells(SA_cells < 0) = 0;
        for col = 1:size(SA_cells,3)
            SA_cells(:,:,col) = normalise(SA_cells(:,:,col), sum_save(:,:,col));
        end
        SA_cells = reshape(SA_cells,num_SA);
        
        
        % add noise
        %SA_cells = SA_cells(:)' + SA_noise .* std(SA_cells(:)) * randn(1,size(SA_cells(:),1));
        %SA_cells(SA_cells < 0) = 0;
        %SA_cells = reshape(SA_cells,num_SA);
        
        display = SAcol_analyseAct_internal(worldSize_y, SA_cells, SA_decoded);
        display_tracked{ind_time} = cell2mat(display);
    end
    
    %slider_display(display_tracked, []);
    
    % Record agent position
    world_tracked{time} = world(:,:,1) + world(:,:,2) + world(:,:,3);
    
    %world(:,:,1) + world(:,:,2)
    %SA_decoded(find(SA_decoded(:,3) == find(SA_cells == max(SA_cells))),:)
    
    %% Check for Completion
    current_state = sub2ind(size(world(:,:,1)), agent_y, agent_x);
    reward_location = sub2ind(size(world(:,:,1)), reward_y, reward_x);
    if current_state == reward_location
        result = 'Y';
        disp(time)
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Move Agent through Bursting Sensory Activity
    
    sensory_cells(find(world(:,:,1))) = 100;
    reward_cells(find(world(:,:,2))) = reward_firing;
    
    SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
    SA_cells = WTA_Competition(SA_cells);
    SA_cells = reshape(SA_cells,num_SA);
    
    %SA_cells = SA_cells + dot(repmat(sensory_cells(:),[1,numel(SA_cells)]), noNaNsensorytoSA);
    
    
    
    disp(world(:,:,1) + world(:,:,2))
    %SA_decoded(find(SA_decoded(:,3) == find(SA_cells == max(SA_cells))),:)
    
    % Calculate activation to motor cells
    motor_cells = dot(repmat(SA_cells(:),[1,numel(motor_cells)]), noNaNSAtomotor);
    
    % Move agent.
    action = actions{find(motor_cells == max(motor_cells))};
    fprintf('%d: %s', time, action)
    disp(' ')
    [world, ~, ~] = update_world5(world, action, 0);
    
    %% Clear cells
    sensory_cells(:) = 0;
    reward_cells(:) = 0;
    SA_cells(:) = 0;
    motor_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end


% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);













end













function [display] = SAcol_analyseAct_internal(worldSize, SA_cells, SA_decoded)

%% Import Arrows
arrows = repmat({uint8(zeros(16))}, [3 3]);
arrows{1} = imread('arrow_nw.png');
arrows{2} = imread('arrow_w.png');
arrows{3} = imread('arrow_sw.png');
arrows{4} = imread('arrow_n.png');
arrows{6} = imread('arrow_s.png');
arrows{7} = imread('arrow_ne.png');
arrows{8} = imread('arrow_e.png');
arrows{9} = imread('arrow_se.png');

for arrow = 1:numel(arrows);
    arrowNormalised = arrows{arrow};
    arrowNormalised(arrowNormalised > 0) = 255;
    arrows{arrow} = arrowNormalised;
end

% Make an array to display the arrows in.
display = repmat({uint8(zeros(16))}, [worldSize*3 worldSize*3]);

%% Retrieve the SA layer's activation and display
% Thresholding by the least active SA cell (within SA_decoded).
minimum_activation = min(min(SA_cells(SA_decoded(:,3))));
SA_cells = SA_cells - minimum_activation;

% Get maximum activation that remains.
maximum_activation = max(max(SA_cells));

for column = 1:size(SA_cells,3)
    
    [~, cell] = max(SA_cells(:,:,column));
    cell = sub2ind(size(SA_cells), cell, 1, column);
    
    % Check that cell has a sensory and motor conneciton i.e. is within
    % SA_decoded.
    if any(ismember(SA_decoded(:,3), cell)) == 1
        
        [y, x] = ind2sub([worldSize worldSize], SA_decoded(find(SA_decoded(:,3) == cell),1));
        SA_state = (y * 3 - 1) + ((worldSize*3) + (worldSize*3) * 3 * (x-1));
        display{SA_state}(:) = 255;
        
        switch SA_decoded(find(SA_decoded(:,3) == cell),2)
            
            case 1
                display{SA_state - worldSize*3 - 1} = arrows{1};
                
            case 2
                display{SA_state - worldSize*3} = arrows{2};
                
            case 3
                display{SA_state - worldSize*3 + 1} = arrows{3};
                
            case 4
                display{SA_state - 1} = arrows{4};
                
            case 6
                display{SA_state + 1} = arrows{6};
                
            case 7
                display{SA_state + worldSize*3 - 1} = arrows{7};
                
            case 8
                display{SA_state + worldSize*3} = arrows{8};
                
            case 9
                display{SA_state + worldSize*3 + 1} = arrows{9};
                
        end
    end
end



end




function [display] = SA_analyseAct_internal(worldSize, SA_cells, SA_decoded)

%% Import Arrows
arrows = repmat({uint8(zeros(16))}, [3 3]);
arrows{1} = imread('arrow_nw.png');
arrows{2} = imread('arrow_w.png');
arrows{3} = imread('arrow_sw.png');
arrows{4} = imread('arrow_n.png');
arrows{6} = imread('arrow_s.png');
arrows{7} = imread('arrow_ne.png');
arrows{8} = imread('arrow_e.png');
arrows{9} = imread('arrow_se.png');

for arrow = 1:numel(arrows);
    arrowNormalised = arrows{arrow};
    arrowNormalised(arrowNormalised > 0) = 255;
    arrows{arrow} = arrowNormalised;
end

% Make an array to display the arrows in.
display = repmat({uint8(zeros(16))}, [worldSize*3 worldSize*3]);

%% Retrieve the SA layer's activation and display
% Thresholding by the least active SA cell (within SA_decoded).
minimum_activation = min(min(SA_cells(SA_decoded(:,3))));
SA_cells = SA_cells - minimum_activation;

% Get maximum activation that remains.
maximum_activation = max(max(SA_cells));

for cell = 1:numel(SA_cells)
    
    % Check that cell has a sensory and motor conneciton i.e. is within
    % SA_decoded.
    if any(ismember(SA_decoded(:,3), cell)) == 1
        
        [y, x] = ind2sub([worldSize worldSize], SA_decoded(find(SA_decoded(:,3) == cell),1));
        SA_state = (y * 3 - 1) + ((worldSize*3) + (worldSize*3) * 3 * (x-1));
        display{SA_state}(:) = 255;
        
        switch SA_decoded(find(SA_decoded(:,3) == cell),2)
            
            case 1
                display{SA_state - worldSize*3 - 1} = arrows{1} * (SA_cells(cell) / maximum_activation);
                
            case 2
                display{SA_state - worldSize*3} = arrows{2} * (SA_cells(cell) / maximum_activation);
                
            case 3
                display{SA_state - worldSize*3 + 1} = arrows{3} * (SA_cells(cell) / maximum_activation);
                
            case 4
                display{SA_state - 1} = arrows{4} * (SA_cells(cell) / maximum_activation);
                
            case 6
                display{SA_state + 1} = arrows{6} * (SA_cells(cell) / maximum_activation);
                
            case 7
                display{SA_state + worldSize*3 - 1} = arrows{7} * (SA_cells(cell) / maximum_activation);
                
            case 8
                display{SA_state + worldSize*3} = arrows{8} * (SA_cells(cell) / maximum_activation);
                
            case 9
                display{SA_state + worldSize*3 + 1} = arrows{9} * (SA_cells(cell) / maximum_activation);
                
        end
    end
end

end


function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end
