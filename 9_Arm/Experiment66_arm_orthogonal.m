function [combo_cells, combo_synapses] = sensory_angles_orthogonal(j1_angle, j2_angle, j3_angle, j4_angle, combo_synapses)

%% Parameters

learningRate = 9000;
round_value = 20;
norm_threshold = 1;
num_sensoryreps = [1 ceil(360/round_value)];
num_combo = [1 100];

%% Build Sensory Representations

j1_cells = zeros(num_sensoryreps);
j2_cells = zeros(num_sensoryreps);
j3_cells = zeros(num_sensoryreps);
j4_cells = zeros(num_sensoryreps);

combo_cells = zeros(num_combo);

if ~exist('combo_synapses','var')
    disp('Generating Rep => Combo synapses.')
    j1_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    j1_synapses = combo_synapses{1};
end

if ~exist('combo_synapses','var')
    disp('Generating Rep => Combo synapses.')
    j2_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    j2_synapses = combo_synapses{2};
end

if ~exist('combo_synapses','var')
    disp('Generating Rep => Combo synapses.')
    j3_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    j3_synapses = combo_synapses{3};
end

if ~exist('combo_synapses','var')
    disp('Generating Rep => Combo synapses.')
    j4_synapses = GenerateWeights(prod(num_combo), prod(num_sensoryreps), 1);
else
    j4_synapses = combo_synapses{4};
end


%% Activate Sensory Representations
% Round to higher 20.
j1_angle = ceil(j1_angle/round_value)*round_value;
% Scale to within 360 degrees.
j1_angle = mod(j1_angle, 360);
% Assign cell number based on the above.
j1 = j1_angle/round_value + 1;
% Activate appropriate cell.
j1_cells(j1) = 1;

j2_angle = ceil(j2_angle/round_value)*round_value;
j2_angle = mod(j2_angle, 360);
j2 = j2_angle/round_value + 1;
j2_cells(j2) = 1;

%{
j3_angle = ceil(j3_angle/round_value)*round_value;
j3_angle = mod(j3_angle, 360);
j3 = j3_angle/round_value + 1;
j3_cells(j3) = 1;


j4_angle = ceil(j4_angle/round_value)*round_value;
j4_angle = mod(j4_angle, 360);
j4 = j4_angle/round_value + 1;
j4_cells(j4) = 1;
%}

%% No NaN Synapses
noNaNj1 = noNaN(j1_synapses);
noNaNj2 = noNaN(j2_synapses);
noNaNj3 = noNaN(j3_synapses);
noNaNj4 = noNaN(j4_synapses);


%% Activate Combo Representations
combo_cells = dot([repmat(j1_cells(:),[1,numel(combo_cells)]); repmat(j2_cells(:),[1,numel(combo_cells)]); repmat(j3_cells(:),[1,numel(combo_cells)]); repmat(j4_cells(:),[1,numel(combo_cells)])], [noNaNj1; noNaNj2; noNaNj3; noNaNj4]);

combo_cells = WTA_Competition(combo_cells);

%% Learn Synapses
j1_synapses = j1_synapses + (learningRate * j1_cells(:) * combo_cells(:)');
j2_synapses = j2_synapses + (learningRate * j2_cells(:) * combo_cells(:)');
j3_synapses = j3_synapses + (learningRate * j3_cells(:) * combo_cells(:)');
j4_synapses = j4_synapses + (learningRate * j4_cells(:) * combo_cells(:)');

%% Normalise Synapses
j1_synapses = normalise(j1_synapses, norm_threshold);
j2_synapses = normalise(j2_synapses, norm_threshold);
j3_synapses = normalise(j3_synapses, norm_threshold);
j4_synapses = normalise(j4_synapses, norm_threshold);

%% Package Synapses
combo_synapses = {j1_synapses, j2_synapses, j3_synapses, j4_synapses};
end





function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches threshold. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end