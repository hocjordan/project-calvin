function SA_cells = Experiment68(SA_cells)
% Takes SA cells and sorts them according to their firing rates. Cells that
% have activity > 0.25 (activated by at least one input) but activity < 0.75
% (not activated by all inputs) are inactivated.



SA_cells(SA_cells > 0.1 & SA_cells < 0.95) = 0;















end