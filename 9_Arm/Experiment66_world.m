function [sensory_cells, arm_info] = Experiment66_world(action, arm_info, gauss_deviation, calculate_sensory)

% 2-link arm with 2 DOF.
% ==@==@

%% Arm Parameters

j1_angle = arm_info{1};
j2_angle = arm_info{2};
j1_pos_x = arm_info{3};
j1_pos_y = arm_info{4};
j2_pos_x = arm_info{5};
j2_pos_y = arm_info{6};

%% Action parameters
size_action = 20;

%% Convert action to joint angles.

switch action
    case 'j1_inc'
        j1_angle = j1_angle + size_action;
    case 'j1_dec'
        j1_angle = j1_angle - size_action;
    case 'j2_inc'
        j2_angle = j2_angle + size_action;
    case 'j2_dec'
        j2_angle = j2_angle - size_action;
    case 'j1_inc_j2_inc'
        j1_angle = j1_angle + size_action;
        j2_angle = j2_angle + size_action;
    case 'j1_inc_j2_dec'
        j1_angle = j1_angle + size_action;
        j2_angle = j2_angle - size_action;
    case 'j1_dec_j2_inc'
        j1_angle = j1_angle - size_action;
        j2_angle = j2_angle + size_action;
    case 'j1_dec_j2_dec'
        j1_angle = j1_angle - size_action;
        j2_angle = j2_angle - size_action;
end

%% Calculate position of middle joint and end joint (end-effector)
[j1_pos_x, j1_pos_y, j2_pos_x, j2_pos_y] = Experiment66_forward_arm(j1_angle, j2_angle);

%% Produce distributed sensory representation

if strcmp(calculate_sensory,'yes') == 1
    
    %position_cells = Experiment66_position_orthogonal(j2_pos_x, j2_pos_y);
    position_cells = Experiment66_position_gauss(j2_pos_x, j2_pos_y, gauss_deviation);
    %angle_cells = Experiment66_angles_orthogonal(j1_angle, j2_angle);
    angle_cells = Experiment66_angles_gauss(j1_angle, j2_angle, gauss_deviation);
    
    % WTA
    position_cells{1} = WTA_Competition(position_cells{1});
    position_cells{2} = WTA_Competition(position_cells{2});
    angle_cells{1} = WTA_Competition(angle_cells{1});
    angle_cells{2} = WTA_Competition(angle_cells{2});
    
    sensory_cells = [position_cells{1}, position_cells{2}, angle_cells{1}, angle_cells{2}];
    
else
    
    sensory_cells = [];
    
end

%% Show figure
%figure(); axes('XLim', [-5 5], 'YLim', [-5 5]); line([0, j1_pos_x], [0, j1_pos_y]); line([j1_pos_x, j2_pos_x], [j1_pos_y, j2_pos_y]);
%{
j0_rectangle = annotation('rectangle');
set(j0_rectangle, 'parent', gca)
set(j0_rectangle, 'position', [0-0.5 0-0.1 1 0.2])

%j1_ellipse = annotation('ellipse');
%set(j1_ellipse, 'parent', gca)
%set(j1_ellipse, 'position', [j1_pos_x-0.1 j1_pos_y-0.1 0.2 0.2])

j2_ellipse = annotation('ellipse');
set(j2_ellipse, 'parent', gca)
set(j2_ellipse, 'position', [j2_pos_x-0.1 j2_pos_y-0.1 0.2 0.2])
%}
%% Repackage Arm Parameters
arm_info{1} = j1_angle;
arm_info{2} = j2_angle;
arm_info{3} = j1_pos_x;
arm_info{4} = j1_pos_y;
arm_info{5} = j2_pos_x;
arm_info{6} = j2_pos_y;

end