function [sensory_tracked] = Clean_150413_CONTROLLER(steps, ind_steps, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded, actions, world_update_function)

% Designed to be a clean, general form of the controlling neural network
% (as opposed to the learner). Experiment 72 is an attempt to get rid of
% certain legacy code and simplify things (e.g. see if SA_modifier is
% necessary).

%% ChangeLog

% Copy/Pasted from Clean_150413_Controller on 17th April 2015.

% Tried removing SA_modifier and it doesn't work.

% Tried removing SA_sum and it doesn't work.



% get world state (self and reward)

global world_state


% Cells

% GET INPUT FROM SENSES
[sensory_cells, reward_cells] = world_update_function('', 'yes');
num_sensory = [size(sensory_cells)];

% GET REWARD
SArewardtoSA_synapses = zeros(size(SAtoSA_synapses));
SArewardtoSA_synapses(logical(eye(size(SArewardtoSA_synapses)))) = 1;
SArewardtoSA_synapses = reshape(SArewardtoSA_synapses, size(SArewardtoSA_synapses));


num_motor = [1 size(actions, 2)];
num_SAcol = prod(num_sensory);
num_SAcellsinCol = prod(num_motor);
num_SA = [num_SAcellsinCol 1 num_SAcol];
reward_firing = 1;

% Competition
SA_modifier = 0.9; %0.02; % 0.2

% Analysis
display_tracked = {};

%% Setup

% Create cells
SA_cells = zeros(num_SA);
motor_cells = zeros(num_motor);
trace = zeros(num_SA);

%% RUN TRIAL
for time = 1:steps
    
    % Allow activity to spread for 1:ind_time
    for ind_time = 1:ind_steps
        
        
        %% Set Up Reward Gradient
        % Place reward.
        % GET REWARD POSITION
        [~, reward_cells] = world_update_function('', 'yes');
        
        % SA cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
        SA_reward = dot((repmat(reward_cells(:),[1,numel(SA_cells)])), sensorytoSA_synapses);
        
        % filter/transfer SA_reward function MAKE GENERAL LATER!!!!!!!!!!!!
        SA_reward(SA_reward < 0.5) = 0;
        
        % final SA update
        SA_cells = dot(([repmat(SA_reward(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [SArewardtoSA_synapses; SAtoSA_synapses]);
        SA_cells = reshape(SA_cells,num_SA);
        
        
        % Competition in state cells:
        
        % subtractive.
        for column = 1:size(SA_cells,3)
            SA_cells(:,:,column) = SA_cells(:,:,column) - SA_modifier * mean(SA_cells(:,:,column));
        end
        
        % divisive.
        SA_cells = SA_cells/max(max(SA_cells));
        sum_save = sum(SA_cells);
        SA_cells(SA_cells < 0) = 0;
        for col = 1:size(SA_cells,3)
            SA_cells(:,:,col) = normalise(SA_cells(:,:,col), SA_cells(:,:,col));
            SA_cells(isnan(SA_cells)) = 0;
        end
        SA_cells = reshape(SA_cells,num_SA);
        
        
        
    end
    
    %% Record agent position
    sensory_info = world_state;
    sensory_tracked{time} = sensory_info;
    
    
    
    %% Check for Completion
    % ALTER FOR GENERALISATION, OR JUST CALL PART OF THE SIMULATION
    %if max(abs(current_state - reward_location)) < 0.5
    % If all nonzero elements of the reward representation are also
    % represented in the sensory cells, then end.
    [sensory_cells, ~] = world_update_function('', 'yes');
    if ~any(~ismember(find(reward_cells), find(sensory_cells)))
        result = 'Y';
        disp(time)
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Move Agent through Bursting Sensory Activity
    [sensory_cells, ~] = world_update_function('', 'yes');
    sensory_cells(sensory_cells > 0) = 100;
    reward_cells(reward_cells > 0) = reward_firing;
    
    SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [sensorytoSA_synapses; sensorytoSA_synapses; SAtoSA_synapses]);
    SA_cells = WTA_Competition(SA_cells);
    SA_cells = reshape(SA_cells,num_SA);
    
    
    % Calculate activation to motor cells
    motor_cells = dot(repmat(SA_cells(:),[1,numel(motor_cells)]), SAtomotor_synapses);
    
    % Move agent.
    action = actions{find(motor_cells == max(motor_cells))};
    disp(action)
    world_update_function(action, 'no');
    
    %% Clear cells
    sensory_cells(:) = 0;
    reward_cells(:) = 0;
    SA_cells(:) = 0;
    motor_cells(:) = 0;
    
end



end














function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));

end