function weights = Experiment67

% Daniel's auto-encoder file, I think.

%Parameters
sparseness = 30;
slope = 70;
learningRate = 100;

array1 = zeros([1 100]);
array2 = zeros([1 100]);
array3 = zeros([1 100]);
array4 = zeros([1 100]);
array5 = zeros([1 100]);

array1(randi(size(array1, 2))) = 1;
array2(randi(size(array2, 2))) = 1;
array3(randi(size(array3, 2))) = 1;
array4(randi(size(array4, 2))) = 1;
array5(randi(size(array5, 2))) = 1;

output_cells = zeros([1 500]);
weights = rand(500);

for epoch = 1:1000
    % make new inputs
array1 = zeros([1 100]);
array2 = zeros([1 100]);
array3 = zeros([1 100]);
array4 = zeros([1 100]);
array5 = zeros([1 100]);

array1(randi(size(array1, 2))) = 1;
array2(randi(size(array2, 2))) = 1;
array3(randi(size(array3, 2))) = 1;
array4(randi(size(array4, 2))) = 1;
array5(randi(size(array5, 2))) = 1;
    
    input = [array1 array2 array3 array4 array5];
    
    % firing
    firingRates=dot(weights,(repmat(input',1,size(output_cells,2))));
    
    % sigmoid competition
    %firingRates = softCompetition(sparseness, slope, firingRates);
    firingRates = WTA_Competition(firingRates);
    
    % weight update
    weights=weights+(learningRate*(input'*firingRates));
    
    weights=normalise(weights);
    
end
end