function [SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded, sensory_tracked] = Experiment66_learner(steps)
        
        % Learns backward SA_column synapses in undirected fashion, as Exp63.
        % Receives sensory info from another function rather than an internal
        % world. Recieves actions as a parameter.
        
        actions = {'j1_inc', 'j1_dec', 'j2_inc', 'j2_dec', '', 'j1_inc_j2_dec', 'j1_inc_j2_inc', 'j1_dec_j2_inc', 'j1_dec_j2_dec'};
        gauss_deviation = 0.005; %0.01;
        
        %Arm
        j1_angle = 0;
        j2_angle = 0;
        j1_pos_x = 2;
        j1_pos_y = 0;
        j2_pos_x = 5;
        j2_pos_y = 0;
        
        arm_info{1} = j1_angle;
        arm_info{2} = j2_angle;
        arm_info{3} = j1_pos_x;
        arm_info{4} = j1_pos_y;
        arm_info{5} = j2_pos_x;
        arm_info{6} = j2_pos_y;
        
        [sensory_cells, arm_info] = Experiment66_world('', arm_info, gauss_deviation, 'yes');
        
        
        % Cells
        num_sensory = [size(sensory_cells)];
        num_motor = [1 size(actions, 2)];
        num_SAcol = prod(num_sensory);
        num_SAcellsinCol = prod(num_motor);
        num_SA = [num_SAcellsinCol 1 num_SAcol];
        
        % Synapses
        sensorytoSA_dilution = 1; %0.3
        motortoSA_dilution = 1; %0.2
        SAtomotor_dilution = 1; %0.2
        
        % Competition and Learning
        learningRate = 100; %100
        trace_learningRate = 10; %0.1; %0.001; % 0.0001
        
        normalisation_threshold = 1;
        sensory_threshold = 1; % Necessary to adjust these to compensate for different
        motor_threshold = 0.25; % numbers of synapses to sensory and motor cells.
        trace_threshold = 4; %0.01
        
        eta = 0.0;
        
        % Noise
        state_noise = 0.002;
        
        % Analysis
        SA_tracked = [];
        sensory_tracked = [];
        text_on = 0;
        
        
        %% Setup
        
        disp('Setting up...')
        
        % Make networks of sensory, motor and SA neurons.
        motor_cells = zeros(num_motor);
        SA_cells = zeros(num_SA);
        SA_trace = zeros(num_SA);
        
        % Create synapse weights between SA cells and motor<=>SA.
        SAtoSA_synapses = GenerateZeroWeights(numel(SA_cells), numel(SA_cells), 1);
        motortoSA_synapses = Generate_Diluted_Weights(motor_cells, SA_cells, motortoSA_dilution, 1);
        SAtomotor_synapses = Generate_Diluted_Weights(SA_cells, motor_cells, SAtomotor_dilution, 1);
        
        % Create sensory => SA synapses that are identical for all cells in a
        % column.
        sensorytoSA_synapses = [];
        for column = 1:num_SAcol
            sensorytoSA_synapses = [sensorytoSA_synapses repmat(Generate_Diluted_Weights(sensory_cells, 1, sensorytoSA_dilution, 1), [1 size(actions,2)])];
        end
        
        SAtoSA_synapses = sparse(SAtoSA_synapses);
        
        % Normalise synapses
        SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);
        sensorytoSA_synapses = normalise(sensorytoSA_synapses, sensory_threshold);
        motortoSA_synapses = normalise(motortoSA_synapses, motor_threshold);
        SAtomotor_synapses = normalise(SAtomotor_synapses', motor_threshold);
        SAtomotor_synapses = SAtomotor_synapses';
        
        disp('...Setup Complete.')
        
        %% RUN TRIAL
        
        %% Timing
        
        tic;
        duration = 0;
        for time = 1:steps
            
            if time == 2
                duration = toc;
                fprintf('Time remaining = %s', datestr((steps*duration)/86400, 'HH:MM:SS.FFF'))
                disp(' ')
                tic;
            elseif time == round(steps/10)
                disp('1/10')
                duration = toc;
                fprintf('Time remaining = %s', datestr((9*duration)/86400, 'HH:MM:SS.FFF'))
                disp(' ')
                tic;
            elseif time == round(2*steps/10)
                disp('2/10')
                duration = (toc+duration)/2
                fprintf('Time remaining = %s', datestr((8*duration)/86400, 'HH:MM:SS.FFF'))
                disp(' ')
                tic;
            elseif time == round(3*steps/10)
                disp('3/10')
                duration = (toc+duration)/2
                fprintf('Time remaining = %s', datestr((7*duration)/86400, 'HH:MM:SS.FFF'))
                disp(' ')
                tic;
            elseif time == round(4*steps/10)
                disp('4/10')
                duration = (toc+duration)/2
                fprintf('Time remaining = %s', datestr((6*duration)/86400, 'HH:MM:SS.FFF'))
                disp(' ')
                tic;
            elseif time == round(5*steps/10)
                disp('5/10')
                duration = (toc+duration)/2
                fprintf('Time remaining = %s', datestr((5*duration)/86400, 'HH:MM:SS.FFF'))
                disp(' ')
                tic;
            elseif time == round(6*steps/10)
                disp('6/10')
                duration = (toc+duration)/2
                fprintf('Time remaining = %s', datestr((4*duration)/86400, 'HH:MM:SS.FFF'))
                disp(' ')
                tic;
            elseif time == round(7*steps/10)
                disp('7/10')
                duration = (toc+duration)/2
                fprintf('Time remaining = %s', datestr((3*duration)/86400, 'HH:MM:SS.FFF'))
                disp(' ')
                tic;
            elseif time == round(8*steps/10)
                disp('8/10')
                duration = (toc+duration)/2
                fprintf('Time remaining = %s', datestr((2*duration)/86400, 'HH:MM:SS.FFF'))
                disp(' ')
                tic;
            elseif time == round(9*steps/10)
                disp('9/10')
                duration = (toc+duration)/2
                fprintf('Time remaining = %s', datestr((1*duration)/86400, 'HH:MM:SS.FFF'))
                disp(' ')
                tic;
            elseif time == round(10*steps/10)
                disp('10/10')
            end
            
            noNaNsensorytoSA = sensorytoSA_synapses;
            noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
            
            noNaNmotortoSA = motortoSA_synapses;
            noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
            
            noNaNSAtomotor = SAtomotor_synapses;
            noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
            
            %% Firing of SA cells and Recruitment of Column
            
            % Sensory cells fire according to agent position (1 cell per place).
            [sensory_cells, arm_info_tracked] = Experiment66_world('', arm_info, gauss_deviation, 'yes');
            %sensory_cells = world(:,:,1);
            
            if text_on == 1
                %fprintf('State = %d', find(sensory_cells))
                arm_info
                disp(' ')
            end
            
            % SA cells fire based on current sensory cells (state).
            SA_cells = dot((repmat(sensory_cells(:),[1,numel(SA_cells)])), noNaNsensorytoSA);
            SA_cells = reshape(SA_cells, num_SA);
            
            % TRANSFER FUNCTION
            SA_cells = Experiment68(SA_cells);
            
            % columnar WTA
            SA_cells(SA_cells == max(max(SA_cells))) = 1;
            SA_cells(SA_cells ~= max(max(SA_cells))) = 0;
            sensory_info = [mod(arm_info_tracked{1},360) mod(arm_info_tracked{2},360) arm_info_tracked{5} arm_info_tracked{6} find(SA_cells > 0.1)'];
            sensory_tracked(time,:) = sensory_info;
            
            % display firing SA cells
            if text_on == 1
                SA_idx = find(SA_cells>0.1);
                for SA_idxd = SA_idx'
                    fprintf('SA = %s', num2str(SA_idxd))
                    disp(' ')
                end
            end
            
            % Update sensorytoSA weights.
            sensorytoSA_synapses = sensorytoSA_synapses + (learningRate * (sensory_cells(:) * SA_cells(:)'));
            sensorytoSA_synapses = normalise(sensorytoSA_synapses, sensory_threshold);
            noNaNsensorytoSA = noNaN(sensorytoSA_synapses);
            
            %% TRACE LEARNING: Update connections from ACTIVE COLUMN to PREVIOUS (trace) SA_CELL:
            
            % Calculate the trace value for all cells
            %SA_trace = getTrace(SA_cells, SA_trace, eta);
            
            % Update synapses using lr * state firing * trace rule (backwards)
            SAtoSA_synapses = SAtoSA_synapses + (trace_learningRate * sparse(SA_cells(:)) * sparse(SA_trace(:)'));
            
            % Block off self-self synapses
            %SAtoSA_synapses(logical(eye(size(SAtoSA_synapses)))) = NaN;
            
            % Normalise synapses
            SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);
            
            %% Select an action and activate the appropriate motor cell.
            
            action = actions{randi(size(actions,2))};
            %disp(action)
            
            [~, arm_info] = Experiment66_world(action, arm_info, gauss_deviation, 'no');
            
            %{
            if valid_movement_selected == 0
                %disp('Invalid Move')
            end
            %}
            
            [~, idx] = ismember(action, actions);
            motor_cells(idx) = 1;
            
            if text_on == 1
                fprintf('Moved %s (%d)', action, find(motor_cells))
                disp(' ')
            end
            
            % Activation of SA cells from sensory and motor cells simulataneously.
            %SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(motor_cells(:)],[1,numel(SA_cells)]), [noNaNsensorytoSA; noNaNsensorytoSA]));
            SA_cells(:) = 0;
            SA_cells = dot(([repmat(motor_cells(:), [1,numel(SA_cells)]); repmat(sensory_cells(:), [1,numel(SA_cells)])]), [noNaNmotortoSA; noNaNsensorytoSA]);
            SA_cells = reshape(SA_cells, num_SA);
            %SA_cells = SA_cells + dot((repmat(motor_cells', [1,numel(SA_cells)])), noNaNmotortoSA);
            
            % WTA:
            SA_cells = WTA_Competition(SA_cells(:));
            SA_cells = reshape(SA_cells, num_SA);
            
            if text_on == 1
                fprintf('SA = %s', num2str(find(SA_cells > 0.1)'))
                disp(' ')
            end
            
            % Weights updated.
            
            motortoSA_synapses = motortoSA_synapses + learningRate * motor_cells(:) * SA_cells(:)';
            SAtomotor_synapses = SAtomotor_synapses + learningRate * SA_cells(:) * motor_cells(:)';
            
            % Weights normalised
            
            motortoSA_synapses = normalise(motortoSA_synapses, motor_threshold);
            SAtomotor_synapses = normalise(SAtomotor_synapses', motor_threshold);
            SAtomotor_synapses = SAtomotor_synapses';
            
            SA_info = [[mod(arm_info_tracked{1},360) mod(arm_info_tracked{2},360) arm_info_tracked{5} arm_info_tracked{6}], find(motor_cells), find(SA_cells > 0.1)'];
            SA_tracked{time} = SA_info;
            
            if text_on == 1
                disp(' ')
            end
            
            %% Calculate SA trace
            
            % Calculate the trace value for all cells
            SA_trace = getTrace(SA_cells, SA_trace, eta);
            
            %% Reset Cells and Update parameters
            
            % Record agent position
            %world_tracked{time} = world(:,:,1);
            %world_summed = world_summed + intended_outcome;
            
            SA_cells(:) = 0;
            sensory_cells(:) = 0;
            motor_cells(:) = 0;
            
        end
        
        
        %% ANALYSE RESULTANT MAPPING
        % Display agent progress.
        disp('Analysing Agent Trajectory.')
        disp('...')
        disp('Complete')
        
        disp('Calculating state/action mappings.')
        disp('Complete')
        

        %}
        % Record every state/action combo.
        SA_decoded = unique(cell2mat(SA_tracked(:)),'rows');
        SA_decoded = sortrows(SA_decoded, 6);
        
        SA_decoded_rounded = SA_decoded;
        SA_decoded_rounded(:,1:2) = round(SA_decoded_rounded(:,1:2));
        
        
        [~,sensory_idx] = sort(sensory_tracked(:,13));
        sensory_tracked = sensory_tracked(sensory_idx,:);
        SA_decoded_rounded(:,3:4) = round(SA_decoded_rounded(:,3:4)*10000)/10000;
        SA_decoded = unique(SA_decoded_rounded,'rows');
        
        %% Package Synaptic Data
        
        %synapticData = {SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded};
        
        disp('Complete')
    end















    function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)
        
        trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
    end


    function matrix = normalise(matrix, threshold)
    
    matrix = threshold * (matrix ./ repmat( sum(matrix), size(matrix, 1), 1));
    
    end
    

    function matrix = normalise_old(matrix, threshold)
        
        % Normalise matrix so that each column's sum approaches threshold. Ignores NaN.
        
        %get number of rows and columns in matrix
        [rows, columns] = size(matrix);
        
        % sum each column
        summed = nansum(matrix);
        
        for column = 1:columns
            if summed(column) ~= 0
                %if summed(column) > threshold
                %divide each row in that column by that sum
                
                for row = 1:rows
                    matrix(row,column) = threshold * (matrix(row,column)/summed(column));
                end
            end
        end
    end