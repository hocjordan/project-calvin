function [angle_cells] = Experiment66_angles_gauss(j1_angle, j2_angle, gauss_deviation)

%Turns angle signal into a gaussian activation on a layer of cells. Size of
%layer and standard deviation can be altered.

%% Parameters

learningRate = 9000;
round_value = 20;
norm_threshold = 1;
num_anglecells = 100; %200;


%% Activate Sensory Representations

% Scale to within 360 degrees.
j1_angle = mod(j1_angle, 360);

% Convert to scale of 0 to 1.
j1_angle = j1_angle/360;

% Create gaussian cell layer
ja1_cells = gaussian_cells(num_anglecells, j1_angle, gauss_deviation);


j2_angle = mod(j2_angle, 360);
j2_angle = j2_angle/360;
ja2_cells = gaussian_cells(num_anglecells, j2_angle, gauss_deviation);

%% Package Cells
angle_cells = {ja1_cells, ja2_cells};
end




function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches threshold. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end