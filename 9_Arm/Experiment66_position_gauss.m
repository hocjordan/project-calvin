function [position_cells] = Experiment66_position_gauss(j2_pos_x, j2_pos_y, gauss_deviation)

%Gives position of manipulator as a set to sensory 'position cells'

%% Parameters

learningRate = 9000;
round_value = 0.25;
norm_threshold = 1;
num_positioncells = 100; %200;

%% Activate Sensory Representations

% Position is on a scale of -5 to +5.
% Position converted to scale of 0 to 1.

% Make positive
j2_pos_x = j2_pos_x + 5;

% Divide by 10.
j2_pos_x = j2_pos_x / 10;

% Produce gaussian layer
jpx_cells = gaussian_cells(num_positioncells, j2_pos_x, gauss_deviation);

j2_pos_y = j2_pos_y + 5;
j2_pos_y = j2_pos_y / 10;
jpy_cells = gaussian_cells(num_positioncells, j2_pos_y, gauss_deviation);

%{
%% No NaN Synapses
noNaNj1 = noNaN(jpx_synapses);
noNaNj2 = noNaN(jpy_synapses);


%% Activate Combo Representations
combo_cells = dot([repmat(jpx_cells(:),[1,numel(combo_cells)]); repmat(jpy_cells(:),[1,numel(combo_cells)])], [noNaNj1; noNaNj2]);

combo_cells = WTA_Competition(combo_cells);

%% Learn Synapses
jpx_synapses = jpx_synapses + (learningRate * jpx_cells(:) * combo_cells(:)');
jpy_synapses = jpy_synapses + (learningRate * jpy_cells(:) * combo_cells(:)');

%% Normalise Synapses
jpx_synapses = normalise(jpx_synapses, norm_threshold);
jpy_synapses = normalise(jpy_synapses, norm_threshold);
%}

%% Package Synapses
position_cells = {jpx_cells, jpy_cells};
end





function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches threshold. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end