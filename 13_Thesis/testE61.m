function testE61()
clear all
disp('Beginning testing. Please set variable "testing" to true, then press enter.')
pause
profile on
synapses = E61_Thesis(10, 1000, []);
E61_Thesis(10, 1000, [], synapses.walls);
profile off
disp('Ending testing. Please set variable "testing" to false, then press enter.')
pause
end