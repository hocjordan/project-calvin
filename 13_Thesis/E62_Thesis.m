function [result, activation_tracked] = E62_Thesis(steps, ind_steps, dakota, synapses, walls)

% As Experiment60 (version of E50, first burster, altered to use rec, altered to interface with the columnar architecture and to use columnar inhibition.
% first burster (E50) > modified for non-reciprocal synapses (E60) >
% finally modified for columns learned by E61

% Experimental
useMaxPropagate = true;
dakotaMode = false;
verbose = true;
runAnalysis = true;
addNoise = true;

global world

world = struct();

% World
worldSize_x = 10;
worldSize_y = 10;
world.agent_x = 1;
world.agent_y = 1;
reward_x = 1;
reward_y = 1;
randomisedTrials = false;
randomTrialDist = 0; % set to 0 or false to have variable agent/goal distances
useBursting = false;
%allowUserWalls = false;

% Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};

% Cells
num_sensory = [worldSize_y, worldSize_x];
num_reward = [num_sensory];
num_motor = [1 size(actions, 2)];
num_SAcol = prod(num_sensory);
num_SAcellsinCol = prod(num_motor);
num_SA = [num_SAcellsinCol 1 num_SAcol];
sensory_bursting = 100;
reward_firing = 1;
noise = 0.01;

% Competition
SA_modifier = 0.7;
if dakotaMode == true; SA_modifier = dakota(1); end; %0.02; % 0.2

% Analysis
display_tracked = {};


modeCheck('useMaxPropagate', useMaxPropagate)
modeCheck('addNoise', addNoise)
modeCheck('dakotaMode', dakotaMode)

%% Setup
% Create World
world.state = create_world3(worldSize_x, worldSize_y, world.agent_x, world.agent_y, reward_x, reward_y);

% Create cells
sensory_cells = zeros(num_sensory);
reward_cells = zeros(num_reward);
SA_cells = zeros(num_SA);
motor_cells = zeros(num_motor);

%% Walls.

% Add Walls to World
walls = unique(walls);
world.state(walls + numel(world.state(:,:,1:2))) = 3;

if randomisedTrials == true
    world.agent_x = randi(worldSize_x); world.agent_y = randi(worldSize_y);
    reward_x = randi(worldSize_x); reward_y = randi(worldSize_y);
    if randomTrialDist ~= 0
        if randomTrialDist >= worldSize_x-2 || randomTrialDist >= worldSize_y-2
            error('Trial Dist larger than world.')
        end
        while chebyshevDistance(worldSize_x, worldSize_y, [world.agent_x, world.agent_y], [reward_x, reward_y]) ~= randomTrialDist
            world.agent_x = randi(worldSize_x); world.agent_y = randi(worldSize_y);
            reward_x = randi(worldSize_x); reward_y = randi(worldSize_y);
        end
    end
    
else
    
    % place agent
    fig = figure(); imagesc(world.state(:,:,3)); title('Please select starting position.');
    [x, y] = ginput(1);
    x=round(x); y=round(y);
    world.agent_x = x; world.agent_y = y;
    close(fig)
    
    % place reward
    fig = figure(); imagesc(world.state(:,:,3)); title('Please select a goal.')
    [x, y] = ginput(1);
    x=round(x); y=round(y);
    reward_x = x; reward_y = y;
    close(fig)
end



world.state = create_world2(worldSize_x, worldSize_y, world.agent_x, world.agent_y, reward_x, reward_y);

world.state(:,:,3) = zeros(size(world.state(:,:,2)));
walls = unique(walls);
world.state(walls + numel(world.state(:,:,1:2))) = 3;


% check if agent or reward is within a wall
agent_location = find(world.state(:,:,1),1);
goal_location = find(world.state(:,:,2),2);
if ismember(agent_location, walls)
    error('Agent is located in a wall.')
elseif ismember(goal_location, walls)
    error('Reward is located in a wall.')
elseif agent_location == goal_location
    error('Agent & Reward have same position.')
end






%% RUN TRIAL
for time = 1:steps
    % Allow activity to spread for 1:ind_time
    for ind_time = 1:ind_steps
        
        
        %% Set Up Reward Gradient
        % Sensory cells fire based on current position.
        
        reward_cells(world.state(:,:,2)>0) = reward_firing;
        
        % SA cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
        if useMaxPropagate == true
            SA_cells = maxPropagate(SA_cells, {reward_cells(:), SA_cells(:)}, {synapses.sens2SA, synapses.SA2SA});
        else
            SA_cells = cellPropagate(SA_cells, reward_cells, SA_cells, [], synapses.sens2SA, synapses.SA2SA, []);
        end
        
        % Competition in state cells:
        
        % subtractive.
        if ~useMaxPropagate == true
            for column = 1:size(SA_cells,3)
                SA_cells(:,:,column) = SA_cells(:,:,column) - SA_modifier * mean(SA_cells(:,:,column));
            end
        end
        
        % divisive.
        if ~any(SA_cells(:)) == true
            error('No firing SA cells.');
        end
        SA_cells = SA_cells/max(max(SA_cells));
        sum_save = sum(SA_cells);
        SA_cells(SA_cells < 0) = 0;
        if ~useMaxPropagate == true
            for col = 1:size(SA_cells,3)
                SA_cells(:,:,col) = normalise(SA_cells(:,:,col), sum_save(:,:,col));
            end
        end
        if addNoise == true
            SA_cells = SA_cells + (rand(size(SA_cells)) * 0.1);
        end
        SA_cells = reshape(SA_cells,num_SA);
        
        
        if runAnalysis == true
            display = SA_analyseAct(worldSize_x, SA_cells, synapses.SAdec, agent_location, goal_location, false, true);
            display_tracked{ind_time} = cell2mat(display);
        end
        
        SA_tracked{ind_time} = SA_cells;
        if length(SA_tracked) >= 2
            if isequal(SA_tracked{end}, SA_tracked{end-1}) == true
                %disp('finish')
                if verbose == true; disp(ind_time); end
                break
            end
        end
        
    end
    
    display = SA_analyseAct(worldSize_x, SA_cells, synapses.SAdec, agent_location, goal_location, false, true);
    activation_tracked{time} = cell2mat(display);
    clear SA_tracked
    
    if runAnalysis == true; slider_display(display_tracked, [], false); end;
    
    % Record agent position
    world_tracked{time} = world.state(:,:,1) + world.state(:,:,2) + world.state(:,:,3);
    
    
    
    
    %% Move Agent
    
    if useBursting == false
        
        % find current state column
        current_col = synapses.SAdec(synapses.SAdec(:,1)==agent_location,3);
        
        [~, idx] = max(SA_cells(current_col));
        chosen_SA = current_col(idx);
        
        if size(chosen_SA,1) > 1
            disp('RANDOM ACTION SELECTED')
        end
        
        motor_cells(:) = 0;
        motor_cells(synapses.SAdec(synapses.SAdec(:,3)==chosen_SA,2)) = 1;
        
    else
        
        sensory_cells(world.state(:,:,1)>0) = sensory_bursting;
        reward_cells(world.state(:,:,2)>0) = reward_firing;
        
        %SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [synapses.sens2SA; synapses.sens2SA; synapses.SA2SA]);
        SA_cells = cellPropagate(SA_cells, sensory_cells, reward_cells, SA_cells, synapses.sens2SA, synapses.sens2SA, synapses.SA2SA);
        SA_cells = WTA_Competition(SA_cells);
        %SA_cells = reshape(SA_cells,num_SA);
        
        
        % Calculate activation to motor cells
        %motor_cells = dot(repmat(SA_cells(:),[1,numel(motor_cells)]), synapses.SA2m);
        motor_cells = cellPropagate(motor_cells, SA_cells, [], [], synapses.SA2m, [], []);
        
    end
    
    % Move agent.
    action = actions{find(motor_cells == max(motor_cells))};
    
    % Quit if stuck
    actions_tracked{time} = action;
    if length(actions_tracked) >= 2
        switch actions_tracked{time}
            case 'NW'
                if strcmp(actions_tracked{time-1},'SE'); result = 'N'; disp(result); break; end;
            case 'W'
                if strcmp(actions_tracked{time-1},'E'); result = 'N'; disp(result); break; end;
            case 'SW'
                if strcmp(actions_tracked{time-1},'NE'); result = 'N'; disp(result); break; end;
            case 'N'
                if strcmp(actions_tracked{time-1},'S'); result = 'N'; disp(result); break; end;
            case 'S'
                if strcmp(actions_tracked{time-1},'N'); result = 'N'; disp(result); break; end;
            case 'NE'
                if strcmp(actions_tracked{time-1},'SW'); result = 'N'; disp(result); break; end;
            case 'E'
                if strcmp(actions_tracked{time-1},'W'); result = 'N'; disp(result); break; end;
            case 'SE'
                if strcmp(actions_tracked{time-1},'NW'); result = 'N'; disp(result); break; end;
        end
    end
    
    
    % Quit if many invalid actions chosen (still for too long)
    if length(world_tracked) > 3
        if isequal(world_tracked{time}, world_tracked{time-1}, world_tracked{time-2}); result = 'N'; disp(result); break; end;
    end
    
    if verbose == true; fprintf('%d: %s', time, action); disp(' '); end;
    [world.state, ~] = update_world5(world.state, action, false);
    
    
    %% Check for Completion
    agent_location = find(world.state(:,:,1));
    goal_location = find(world.state(:,:,2));
    if agent_location == goal_location
        result = 'Y';
        disp(time)
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    
    
    %% Clear cells
    sensory_cells(:) = 0;
    reward_cells(:) = 0;
    SA_cells(:) = 0;
    motor_cells(:) = 0;
    %[world.agent_y, world.agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end


% Display agent progress.
if verbose == true; disp('Analysing Agent Trajectory.'); disp('...'); end
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);













end







function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)
% Normalises a matrix so that each column sums to the threshold value given. If you want to normalise rows, transpose the matrix before using this function.

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));                 % Divides each element in the matrix by (the sum of that element's column multiplied by one over the threshold).

end
