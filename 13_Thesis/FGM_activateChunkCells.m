function agent = activateChunkCells(chunkSubCompetition, learnChunks, agent)


%% Parameters
chunk_learningRate = 1; %11 %0.00001 %0.000001; %0.01
chunk_modifier = 1.8;
chunk_threshold = 1;
eta = 0.9;
global set_verbose

%% Feed Activity to Chunk Cells

% Uses a full trace rule -- preSynaptic_trace *
% postSynaptic_trace

% Calculate activation of chunk cells from SA cells
agent.chunk_cells = dot(repmat(agent.SA_cells(:),[1,numel(agent.chunk_cells)]), agent.SAtochunk_synapses);
agent.chunk_cells = reshape(agent.chunk_cells,agent.num_chunk);
chunk_save = agent.chunk_cells;

if chunkSubCompetition == true
    
    % Competition in chunk cells:
    % Subtractive.
    agent.chunk_cells = agent.chunk_cells - chunk_modifier * mean(mean(agent.chunk_cells));
    agent.chunk_cells(agent.chunk_cells < 0) = 0;
    
    if any(agent.chunk_cells) == 0
        disp('No chunk cells firing')
    else
        % divisive
        agent.chunk_cells = 1 * (agent.chunk_cells/max(max(agent.chunk_cells)));
    end
    
end

% Display firing cells and traces
if set_verbose == true
    fprintf('SA = %d.\n' , find(agent.SA_cells))
    fprintf('SA (trace) = %s.\n' , num2str(find(agent.SA_trace)'))
    fprintf('Chunk = %s.\n', num2str(find(agent.chunk_cells > 0.9)))
    fprintf('Chunk (trace) = %s.\n', num2str(find(agent.chunk_trace > 0.09)))
    %fprintf('Super = %s.\n', num2str(find(super_cells > 0.9)))
    %fprintf('Super (trace) = %s.\n', num2str(find(super_trace > 0.09)))
    disp(' ')
end

if learnChunks == true
    
    % Calculate memory trace for all cells.
    agent.chunk_trace = getTrace(agent.chunk_cells, agent.chunk_trace, eta);
    agent.SA_trace = getTrace(agent.SA_cells, agent.SA_trace, eta);
    
    % Update synapses to and from chunk cells.
    agent.chunktoSA_synapses = agent.chunktoSA_synapses + chunk_learningRate * agent.chunk_trace(:) * agent.SA_trace(:)';
    agent.SAtochunk_synapses = agent.SAtochunk_synapses + chunk_learningRate * agent.SA_trace(:) * agent.chunk_trace(:)';
    
    % Normalise synapse weights to and from chunk cells.
    agent.SAtochunk_synapses = normalise(agent.SAtochunk_synapses, chunk_threshold);
    agent.chunktoSA_synapses = normalise(agent.chunktoSA_synapses', chunk_threshold);
    agent.chunktoSA_synapses = agent.chunktoSA_synapses';
    
end

end