function [synapses, percentageSA] = E61_Thesis(worldSize, steps, dakota, extrawalls)

% Learns backward SA synapses, using an explicitly columnar architecture.

%% Parameters
% World
worldSize_x = worldSize;
worldSize_y = worldSize;
agent_x = 2;
agent_y = 2;
reward_x = 3;
reward_y = 3;

% Actions
actions = {'NW' 'W' 'SW' 'N' 'S' 'NE' 'E' 'SE'};

% Cells
num_sensory = [worldSize_y, worldSize_x];
num_motor = [1 size(actions, 2)];
num_SAcol = prod(num_sensory);
num_SAcellsinCol = prod(num_motor);
num_SA = [num_SAcellsinCol 1 num_SAcol];

% Synapses
sensorytoSA_dilution = 1; %0.3
motortoSA_dilution = 1; %0.2
SAtomotor_dilution = 1; %0.2

% Competition and Learning
sparseness = 99;
slope = 90000000000000000000;
learningRate = 100;
trace_learningRate = 10; %0.1; %0.001; % 0.0001

normalisation_threshold = 1;
trace_threshold = 4; %0.01

eta = 0.0;

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Noise
state_noise = 0.002;

% Analysis
SA_tracked = [];
verbose = true;
reallyVerbose = false;

testing = false;
runDakota = false;
global allowAssertions
allowAssertions = true;
allowUserCreatedWalls = true;
randomisedPositions = false;

if isempty(dakota) == false
    runDakota = true;
end

if testing == true
    if ~isequal(input('In "Testing" mode. Continue? Y/N: ', 's'),'Y')
        return
    end
    verbose = true;
    allowAssertions = true;
    
end

if runDakota == true
    
    allowUserCreatedWalls = false;
    randomisedPositions = true;
    verbose = false;
    
    %%**TESTING**%%
    steps = dakota(1);

end

if testing == true && runDakota == true
    
    error('Incompatible Running Modes are Active')
    
end

%% Setup
% Create world and place agent within it.
world = create_world3(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);
world_summed = zeros(size(world(:,:,1)));

% Make networks of sensory, motor and SA neurons.
sensory_cells = zeros(num_sensory);
motor_cells = zeros(num_motor);
SA_cells = zeros(num_SA);
SA_trace = zeros(num_SA);

% Create synapse weights between SA cells and motor<=>SA.
SAtoSA_synapses = GenerateZeroWeights(numel(SA_cells), numel(SA_cells), 1);
motortoSA_synapses = Generate_Diluted_Weights(motor_cells, SA_cells, motortoSA_dilution, 1);
SAtomotor_synapses = Generate_Diluted_Weights(SA_cells, motor_cells, SAtomotor_dilution, 1);

% Create sensory => SA synapses that are identical for all cells in a
% column.
sensorytoSA_synapses = [];
for column = 1:num_SAcol
    sensorytoSA_synapses = [sensorytoSA_synapses repmat(Generate_Diluted_Weights(sensory_cells, 1, sensorytoSA_dilution, 1), [1 size(actions,2)])];
end

% Normalise synapses
sensorytoSA_synapses = normalise(sensorytoSA_synapses, normalisation_threshold);
motortoSA_synapses = normalise(motortoSA_synapses, normalisation_threshold);
SAtomotor_synapses = normalise(SAtomotor_synapses', normalisation_threshold);
SAtomotor_synapses = SAtomotor_synapses';

%% Walls.

world(:,:,3) = zeros(size(world(:,:,2)));

rows = size(world,1);
col = size(world,2);

walls = [];

top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

walls = [walls top_wall];

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

walls = [walls bottom_wall];

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

walls = [walls left_wall];
walls = [walls right_wall];

% add extra walls if given
if exist('extrawalls', 'var') == 1
    walls = [walls extrawalls'];
end

% walls are represented in the world
%
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

if allowUserCreatedWalls == true
    % user interface to create maze
    user_walls = figure(); imagesc(world(:,:,3));
    while ishandle(user_walls) == 1;
        try
            [x, y] = ginput(1);
            x=round(x); y=round(y);
            if world(y,x,3) == 3;
                world(y,x,3) = 0;
            else
                world(y,x,3) = 3;
            end
            imagesc(world(:,:,3));
        end
    end
end

walls = find(world(:,:,3) == 3);

% place agent
if randomisedPositions == true
    
    % Generate positions that are in the world and not in the wall
    validAll = false;
    while validAll == false
        [agent_x, agent_y] = makePositions(10, 10);
        [reward_x, reward_y] = makePositions(10, 10);
        if world(agent_y, agent_x, 3) ~= 3 && world(reward_y, reward_x, 3) ~= 3
            validAll = true;
        end
    end
else
    
    fig = figure(); imagesc(world(:,:,3)); title('Please select starting position.');
    [x, y] = ginput(1);
    x=round(x); y=round(y);
    agent_x = x; agent_y = y;
    
    close(fig)
    
end

world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

world(:,:,3) = zeros(size(world(:,:,2)));
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

%% RUN TRIAL

%% Timing

stepsTimer = tic;
duration = 0;
for time = 1:steps
    
    if verbose == true
        if time == round(steps/10)
            disp('1/10')
            duration = toc(stepsTimer);
            fprintf('Time remaining = %s', datestr((9*duration)/86400, 'HH:MM:SS.FFF'))
            disp(' ')
            stepsTimer = tic;
        elseif time == round(2*steps/10)
            disp('2/10')
            duration = (toc(stepsTimer)+duration)/2;
            fprintf('Time remaining = %s', datestr((8*duration)/86400, 'HH:MM:SS.FFF'))
            disp(' ')
            stepsTimer = tic;
        elseif time == round(3*steps/10)
            disp('3/10')
            duration = (toc(stepsTimer)+duration)/2;
            fprintf('Time remaining = %s', datestr((7*duration)/86400, 'HH:MM:SS.FFF'))
            disp(' ')
            stepsTimer = tic;
        elseif time == round(4*steps/10)
            disp('4/10')
            duration = (toc(stepsTimer)+duration)/2;
            fprintf('Time remaining = %s', datestr((6*duration)/86400, 'HH:MM:SS.FFF'))
            disp(' ')
            stepsTimer = tic;
        elseif time == round(5*steps/10)
            disp('5/10')
            duration = (toc(stepsTimer)+duration)/2;
            fprintf('Time remaining = %s', datestr((5*duration)/86400, 'HH:MM:SS.FFF'))
            disp(' ')
            stepsTimer = tic;
        elseif time == round(6*steps/10)
            disp('6/10')
            duration = (toc(stepsTimer)+duration)/2;
            fprintf('Time remaining = %s', datestr((4*duration)/86400, 'HH:MM:SS.FFF'))
            disp(' ')
            stepsTimer = tic;
        elseif time == round(7*steps/10)
            disp('7/10')
            duration = (toc(stepsTimer)+duration)/2;
            fprintf('Time remaining = %s', datestr((3*duration)/86400, 'HH:MM:SS.FFF'))
            disp(' ')
            stepsTimer = tic;
        elseif time == round(8*steps/10)
            disp('8/10')
            duration = (toc(stepsTimer)+duration)/2;
            fprintf('Time remaining = %s', datestr((2*duration)/86400, 'HH:MM:SS.FFF'))
            disp(' ')
            stepsTimer = tic;
        elseif time == round(9*steps/10)
            disp('9/10')
            duration = (toc(stepsTimer)+duration)/2;
            fprintf('Time remaining = %s', datestr((1*duration)/86400, 'HH:MM:SS.FFF'))
            disp(' ')
            stepsTimer = tic;
        elseif time == round(10*steps/10)
            disp('10/10')
        end
    end
    
    noNaNsensorytoSA = sensorytoSA_synapses;
    noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
    
    noNaNmotortoSA = motortoSA_synapses;
    noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
    
    noNaNSAtomotor = SAtomotor_synapses;
    noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
    
    %% Firing of SA cells and Recruitment of Column
    
    % Sensory cells fire according to agent position (1 cell per place).
    sensory_cells = world(:,:,1);
    
    if reallyVerbose == 1
        fprintf('State = %d', find(sensory_cells))
        disp(' ')
    end
    
    % SA cells fire based on current sensory cells (state).
    SA_cells = cellPropagate(SA_cells, sensory_cells, [], [], noNaNsensorytoSA, [], []);
    
    % columnar WTA
    SA_cells(SA_cells == max(max(SA_cells))) = 1;
    SA_cells(SA_cells ~= max(max(SA_cells))) = 0;
    
    assert(any(SA_cells(:)));
    
    if reallyVerbose == 1
        fprintf('SA = %d \n', find(SA_cells > 0.1))
        disp(' ')
    end
    
    % Update sensorytoSA weights.
    sensorytoSA_synapses = sensorytoSA_synapses + (learningRate * (sensory_cells(:) * SA_cells(:)'));
    sensorytoSA_synapses = normalise(sensorytoSA_synapses, normalisation_threshold);
    noNaNsensorytoSA = noNaN(sensorytoSA_synapses);
    
    assert(any(noNaNsensorytoSA(:)))
    
    %% TRACE LEARNING: Update connections from ACTIVE COLUMN to PREVIOUS (trace) SA_CELL:
    
    % Calculate the trace value for all cells
    %SA_trace = getTrace(SA_cells, SA_trace, eta);
    
    % Update synapses using lr * state firing * trace rule (backwards)
    SAtoSA_synapses = SAtoSA_synapses+(trace_learningRate * SA_cells(:) * SA_trace(:)');
    SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);
    
    %% Select an action and activate the appropriate motor cell.
    
    motor_cells(randi([1, numel(motor_cells)])) = 1;
    assert(any(motor_cells));
    assert(sum(motor_cells(:))==1);
    assert(numel(find(motor_cells(:)))==1)
    action = actions{motor_cells>0};
    [world, ~] = update_world4(world, action, 0);
    
    if reallyVerbose == 1
        fprintf('Moved %s (%d)', action, find(motor_cells))
        disp(' ')
    end
    
    % Extra activation of SA cells from motor cells.
    SA_cells(:) = 0;
    SA_cells = cellPropagate(SA_cells, motor_cells, sensory_cells, [], noNaNmotortoSA, noNaNsensorytoSA, []);
    
    % WTA:
    SA_cells = WTA_Competition(SA_cells(:));
    SA_cells = reshape(SA_cells, num_SA);
    
    if reallyVerbose == 1
        fprintf('SA = %s', num2str(find(SA_cells > 0.1)'))
        disp(' ')
    end
    
    % Weights updated.
    
    motortoSA_synapses = motortoSA_synapses + learningRate * motor_cells(:) * SA_cells(:)';
    SAtomotor_synapses = SAtomotor_synapses + learningRate * SA_cells(:) * motor_cells(:)';
    
    % Weights normalised
    
    motortoSA_synapses = normalise(motortoSA_synapses, normalisation_threshold);
    SAtomotor_synapses = normalise(SAtomotor_synapses', normalisation_threshold);
    SAtomotor_synapses = SAtomotor_synapses';
    
    SA_info = [find(sensory_cells), find(motor_cells), find(SA_cells > 0.1)'];
    SA_tracked{time} = SA_info;
    
    if reallyVerbose == 1
        disp(' ')
    end
    
    %% Learn Sequence Through Trace Learning
    
    % Calculate the trace value for all cells
    SA_trace = getTrace(SA_cells, SA_trace, eta);
    
    
    % Record agent position
    world_tracked{time} = world(:,:,1) + world(:,:,3);
    world_summed = world_summed + world_tracked{time};
    
    
    % End timestep. Repeat.
    if time == steps
        final = find(SA_cells);
    end
    
    %% Reset Cells and Update parameters
    SA_cells(:) = 0;
    motor_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

if verbose == true; figure(); imagesc(world_summed); title('Sum of Time Spent in Each Location'); end

% Record every state/action combo.
SA_decoded = sortrows(cell2mat(SA_tracked(:)),[1 2]);
SA_decoded = unique(SA_decoded, 'rows');

potentialSA = (worldSize*worldSize-numel(walls))*numel(actions);
percentageSA = (size(SA_decoded,1) / potentialSA) * 100;
assert(percentageSA<=100)


%% Package Synaptic Data

%synapticData = {SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded};
synapses = struct();
synapses.sens2SA = sensorytoSA_synapses;
synapses.SA2SA = SAtoSA_synapses;
synapses.SAdec = SA_decoded;
synapses.walls = walls;
synapses.m2SA = motortoSA_synapses;
synapses.SA2m = SAtomotor_synapses;



disp('Complete')
end















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

if any(isnan(matrix(:))) || any(sum(matrix) == 0)
    
    [rows, columns] = size(matrix);
    
    % sum each column
    summed = nansum(matrix);
    for column = 1:columns
        if summed(column) ~= 0
            %divide each row in that column by that sum
            for row = 1:rows
                matrix(row,column) = (matrix(row,column)/(1/threshold * summed(column)));
            end
        end
    end
    y = matrix;
    
else
    
    matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));
    
end
end