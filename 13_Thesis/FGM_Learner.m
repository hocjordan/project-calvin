function [agent, SA_tracked, sensory_tracked3] = Learner(steps, agent)

% Based on Experiment66_learner_SAFE.

% Input parameter 'steps' is an integer describing the number of
% exploration steps to be made.

% Input parameter 'world.actions' is a cell array of strings designating certain
% world.actions, e.g.:
% ['RED_ON', 'RED_OFF', 'GREEN_ON', 'GREEN_OFF']
% ['Forward', 'Backward', 'Left', 'Right']

% Outputs synapse matrices.

% Outputs sensory_tracked, which tracks the various states the model has
% experienced.

% Outputs SA_decoded, which tracks the state/action combination currently
% associated with each neuron.

% Calls a WORLD function to receive sensory input.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% Parameters

%
global world_state
global world
global experimental
global set_verbose

% Competition and Learning
learningRate = 100; %100
trace_learningRate = 10; %0.1; %0.001; % 0.0001
sensory_learningRate = 1;
bandpass = [0.06 0.95]; %[0.1 0.95];

eta = 0.0;

% Noise
state_noise = 0.002;

% Analysis
SA_tracked = {};
sensory_tracked = [];
sensory_tracked2 = {};
sensory_tracked3 = cell(numel(agent.SA_cells), 1);
text_on = 0;
counter = 0;


%% RUN TRIAL

disp('Learning...')

%if experimental

%% Timing

tic;
duration = 0;
for time = 1:steps
    
    if set_verbose == true
        disp(time)
    end
    
    assert(isequal(size(agent.SA_cells), agent.num_SA));
    assert(isequal(size(agent.motor_cells), agent.num_motor));
    assert(isequal(size(agent.SA_trace), agent.num_SA));
    
    %% Firing of SA cells and Recruitment of Column
    
    % GET SENSORY DATA FROM WORLD (ONLY 1 CELL ACTIVE AT A TIME)
    agent.sensory_cells = world.observe();
    
    agent = columnRecruitment(agent, sensory_learningRate, bandpass);
    
    %{
    if experimental
        % check for reward
        if ~any(~ismember(find(agent.sensory_cells), find(world_state(:,:,2))))
            % activate goal cell
            agent.goal_cells = cellPropagate(agent.goal_cells, agent.SA_cells,
            
            % hebbian learning
            agent.goaltoSA_synapses = agent.goaltoSA_synapses + learningRate * agent.goal_cells(:) * agent.SA_cells(:)';
            agent.SAtogoal_synapses = agent.SAtogoal_synapses + learningRate * agent.SA_cells(:) * agent.goal_cells(:)';
        end
    end
    %}
    
    % RECORD STATES EXPERIENCED
    sensory_info = [world.agent_r world.agent_c];
    sensory_info_full = {world.agent_r world.agent_c find(agent.SA_cells)};
    sensory_tracked = [sensory_tracked; sensory_info];
%sensory_tracked2{end+1} = reshape(agent.sensory_cells, [numel(agent.sensory_cells)/numel(agent.sensory_cells) numel(agent.sensory_cells)/numel(agent.sensory_cells)]);
for i = 1:numel(sensory_info_full{3})
    tracked_cell = sensory_info_full{3}(i);
    
    sensory_tracked3{tracked_cell} = [sensory_tracked3{tracked_cell}; [sensory_info_full{1} sensory_info_full{2}]];
end

if mod(time, floor(steps/150)) == 0 && counter <= 50 % if a certain cell fires, its first fifty firings will be tracked.
    if counter == 0 % if the first analysis, find a firing cell
        firingSA = find(agent.SA_cells(:));
        cell_var = firingSA(randperm(length(firingSA),1));
        fprintf('Analysing cell %d\n', cell_var);
    end
    if agent.SA_cells(cell_var) > 0
    continuous_analysis([10 10], [50 50], agent, cell_var, sensory_tracked3, true);
    subplot(2,2,3); imagesc(reshape(agent.sensory_cells, [10 10])); colorbar; title('Current Sensory Firing');
    counter = counter +1;
    end
end

    %% TRACE LEARNING: Update connections from ACTIVE COLUMN to PREVIOUS (trace) SA_CELL:
    %find(agent.SA_cells)
    %find(agent.SA_trace)
    agent = traceLearning(agent, trace_learningRate);
    
    %% Select an action and activate the appropriate motor cell.
    
    action = world.actions{randi(size(world.actions,2))};
    
    % UPDATE WORLD FROM ACTION (ONLY 1 CELL ACTIVE AT A TIME)
    if isequal(world.update, @continuous_update) && isequal(world.movement_type, 'top')
        world.update(action, world.top_speed);
    elseif isequal(world.update, @continuous_update) && isequal(world.movement_type, 'random')
        world.update(action, randi([world.min_speed world.top_speed]))
    elseif isequal(world.update, @continuous_update)
        error('Invalid Movement Type')
    else
        world.update(action);
    end
    
    [~, idx] = ismember(action, world.actions);
    agent.motor_cells(idx) = 1;
    
    agent = SA_learning(agent, learningRate);
    
    % RECORD STATE/ACTION COMBINATIONS EXPERIENCED
    SA_info{1} = sensory_info;
    SA_info{2} = find(agent.motor_cells);
    SA_info{3} = find(agent.SA_cells > 0.1)';
    SA_tracked{time} = SA_info;
    
    
    %% Calculate SA trace
    
    % Calculate the trace value for all cells
    agent.SA_trace = getTrace(agent.SA_cells, agent.SA_trace, eta);
    
    %% Reset Cells and Update parameters
    
    
    agent.SA_cells(:) = 0;
    agent.sensory_cells(:) = 0;
    agent.motor_cells(:) = 0;
    
end



disp('Learning Complete')
end






function agent = columnRecruitment(agent, learningRate, bandpass)

% SA cells fire based on current sensory cells (state).
%agent.SA_cells = dot((repmat(agent.sensory_cells(:),[1,numel(agent.SA_cells)])), agent.sensorytoSA_synapses);
%agent.SA_cells = reshape(agent.SA_cells, agent.num_SA);

agent.SA_cells = cellPropagate(agent.SA_cells, agent.sensory_cells, [],[], agent.sensorytoSA_synapses, [], []);

assert(any(agent.SA_cells(:)));
%assert(max(agent.SA_cells(:)) ~= min(agent.SA_cells(:)))

% TRANSFER FUNCTION
agent.SA_cells(agent.SA_cells > bandpass(1) & agent.SA_cells < bandpass(2)) = 0;

% columnar WTA
agent.SA_cells(agent.SA_cells == max(max(agent.SA_cells))) = 1;
agent.SA_cells(agent.SA_cells ~= max(max(agent.SA_cells))) = 0;

% Update sensorytoSA weights.
agent.sensorytoSA_synapses = agent.sensorytoSA_synapses + (learningRate * (agent.sensory_cells(:) * agent.SA_cells(:)'));
agent.sensorytoSA_synapses = normalise(agent.sensorytoSA_synapses, agent.sensory_threshold);
agent.sensorytoSA_synapses = noNaN(agent.sensorytoSA_synapses);

end





function agent = traceLearning(agent, trace_learningRate)
% Update synapses using lr * state firing * trace rule (backwards)
agent.SAtoSA_synapses = agent.SAtoSA_synapses + (trace_learningRate * agent.SA_cells(:) * agent.SA_trace(:)');

% Normalise synapses
agent.SAtoSA_synapses = normalise(agent.SAtoSA_synapses, agent.trace_threshold);
agent.SAtoSA_synapses(isnan(agent.SAtoSA_synapses)) = 0;
end






function agent = SA_learning(agent, learningRate)

global set_verbose

% Activation of SA cells from sensory and motor cells simulataneously.
agent.SA_cells(:) = 0;
agent.SA_cells = cellPropagate(agent.SA_cells, agent.motor_cells, agent.sensory_cells, [], agent.motortoSA_synapses, agent.sensorytoSA_synapses, []);

% WTA:
agent.SA_cells = WTA_Competition(agent.SA_cells(:));
agent.SA_cells = reshape(agent.SA_cells, agent.num_SA);

%{
if set_verbose == 1
    fprintf('SA = %s', num2str(find(agent.SA_cells > 0.1)'))
    disp(' ')
end
%}
% Weights updated.

agent.motortoSA_synapses = agent.motortoSA_synapses + learningRate * agent.motor_cells(:) * agent.SA_cells(:)';
agent.SAtomotor_synapses = agent.SAtomotor_synapses + learningRate * agent.SA_cells(:) * agent.motor_cells(:)';

% Weights normalised

agent.motortoSA_synapses = normalise(agent.motortoSA_synapses, agent.motor_threshold);
agent.SAtomotor_synapses = normalise(agent.SAtomotor_synapses', agent.motor_threshold);
agent.SAtomotor_synapses = agent.SAtomotor_synapses';

end



function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));

end