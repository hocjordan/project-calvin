%% Trace Experiment

function [agent, world, switches] = E3_Thesis(experiment, dakota)

% As Experiment2, but introduces competition into the network as well as
% 'actions' that take the agent from one state to another. Also includes a
% display of agent progress and the linear attractor.

E3_Path()

%% SETUP TRIAL
switches = struct();
switches = E3_defaults(switches);

if ~isequal(dakota, {})
    switches.main.suppressChecks = true;
    switches.main.suppressFigures = true;
end

if ~isequal(experiment, '')
    switches = readParams(sprintf('Experiments/%s.txt', experiment), switches, dakota);
    if ~switches.main.suppressChecks; if ~isequal(input(sprintf('Running "%s" experiment. Continue? Y/N: ', experiment), 's'),'Y'); return; end; end
else
    if ~switches.main.suppressChecks; if ~isequal(input('No experiment selected. Continue? Y/N: ', 's'),'Y'); return; end; end
end
if switches.params.learning_steps > 10
    switches.main.text = 'Low';
end

[agent, world] = E3_setup(switches);

%% RUN TRIAL

[agent, world] = E3_learning(agent, world, switches);

%% BACKWARD INDUCTION

activation_tracked = E3_induction(agent, world, switches);



%{
activation = dot(repmat(place_cells(:),[1,numel(place_cells)]), noNaNweights); activation(final) = 1
activation_display = reshape(activation,[10,10]);
activation_tracked{1} = activation_display;
for time = 1:steps
    activation = dot(repmat(activation(:),[1,numel(place_cells)]), noNaNweights); activation(final) = 1;
    %activation = softCompetition(49,90,activation);
    activation_display = reshape(activation,[10,10]);
    activation_tracked{end+1} = activation_display;
end
%}

% Display diagram of activation pathway
switch switches.main.suppressFigures
    case true
    case false
        slider_display(world.tracked, [], true);
        slider_display(activation_tracked, [], true);
end

end


















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end


function y = normalise(matrix)

% Normalise matrix so that each column's sum approaches 1. Ignores NaN.

%get number of rows and columns in matrix

[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);
for column = 1:columns
    if summed(column) ~= 0
        %divide each row in that column by that sum
        for row = 1:rows
            matrix(row,column) = (matrix(row,column)/summed(column));
        end
    end
end
y = matrix;


end