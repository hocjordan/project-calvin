function [largeRecallMat, largePrecisionMat] = recallExperiment(create_figures)

E3_Path()

vals = [100 200 300 400 500 600 700 800 900 1000 ...
    1200 1400 1600 1800 2000 2500 3000 3500 4000 4500 5000 ...
    6000 7000 8000 9000 10000];
num_vals = numel(vals);
recallMat = zeros([1 num_vals]);
precisionMat = zeros([1 num_vals]);

num_runs = 50;
largeRecallMat = zeros([num_runs, num_vals]);
largePrecisionMat = zeros([num_runs, num_vals]);

for i = 1:num_runs
    disp(i)
    for j = 1:num_vals
        [agent, world, switches] = E3_Thesis('SynapseTesting', {vals(j)});
        results = testStateMap(agent, world, switches, false);
        recallMat(j) = results.recall;
        precisionMat(j) = results.precision;
    end
    largeRecallMat(i, :) = recallMat;
    largePrecisionMat(i, :) = precisionMat;
end

if create_figures
    figure();
    hold on
    for i = 1:num_runs
        plot(vals, largeRecallMat(i, :)*100, 'LineWidth', 0.5, 'Color', [0.9 0.9 0.9])
    end
    plot(vals, mean(largeRecallMat)*100, 'LineWidth', 4, 'Color', [0 0 0])
    hold off
    xlabel('Learning Time (steps)'); ylabel('Recall (percent)')
    
    figure();
    hold on
    for i = 1:num_runs
        plot(vals, largePrecisionMat(i, :)*100, 'LineWidth', 0.5, 'Color', [0.9 0.9 0.9])
    end
    plot(vals, mean(largePrecisionMat)*100, 'LineWidth', 4, 'Color', [0 0 0])
    hold off
    ylim([0 100])
    xlabel('Learning Time (steps)'); ylabel('Precision (percent)')
end

end