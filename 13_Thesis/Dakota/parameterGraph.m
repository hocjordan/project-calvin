function parameterGraph(param1, param1_name, param1_bounds, param2, param2_name, param2_bounds, obj_fun, obj_bounds, resolution)

x = param1; y = param2; z = obj_fun;
[xq, yq] = meshgrid(param1_bounds(1):resolution:param1_bounds(2), param2_bounds(1):resolution:param2_bounds(2));
vq = griddata(x, y, z, xq, yq);
%figure(); mesh(xq, yq, vq);

% Mesh graph
figure(); mesh(xq, yq, vq); xlim(param1_bounds); ylim(param2_bounds); zlim(obj_bounds);
xlabel(param1_name); ylabel(param2_name); zlabel('objective function');

% Heatmap graph
figure(); imagesc(vq); xlim(param1_bounds); ylim(param2_bounds);
xlabel(param1_name); ylabel(param2_name);
colorbar

end