% matlab_rosen_wrapper.m
function thesis_dakota_wrapper(params,results)

dakotaMode = 'E10';
fprintf('Experiment Type: %s\n', dakotaMode)

%--------------------------------------------------------------
% Set any fixed/default values needed for your analysis .m code
%--------------------------------------------------------------

addpath('~/Documents/Repos/projectcalvin/Walls')
switch dakotaMode
    case 'E10'
        addpath('~/Documents/Repos/projectcalvin/Support Functions')
        addpath('~/Documents/Repos/projectcalvin/13_Thesis')
        addpath('~/Documents/Repos/projectcalvin/Diagnostics')
    case 'E62'
        addpath('~/Documents/Repos/projectcalvin/Support Functions')
        addpath('~/Documents/Repos/projectcalvin/13_Thesis')
        addpath('~/Documents/Repos/projectcalvin/Diagnostics')
    case 'FGM'
        addpath('~/Documents/Repos/projectcalvin/13_Thesis/Final Gradient Model/Support Functions')
        addpath('~/Documents/Repos/projectcalvin/13_Thesis/Final Gradient Model/')
        addpath('~/Documents/Repos/projectcalvin/13_Thesis/Final Gradient Model/Diagnostics')
    case 'E61'
        addpath('~/Documents/Repos/projectcalvin/Support Functions')
        addpath('~/Documents/Repos/projectcalvin/13_Thesis')
        addpath('~/Documents/Repos/projectcalvin/Diagnostics')
    otherwise
        error('Invalid Experiment Specified by Dakota')
end

%------------------------------------------------------------------
% READ params.in (or params.in.num) from DAKOTA and set Matlab variables
%
% read params.in (no aprepro) -- just one param per line
% continuous design, then U.C. vars
% --> 1st cell of C has values, 2nd cell labels
% --> variables are in rows 2-->?
%------------------------------------------------------------------

fid = fopen(params,'r');
C = textscan(fid,'%n%s');
fclose(fid);

num_vars = C{1}(1);
disp(num_vars)
	

% set design variables -- could use vector notation
% rosenbrock x1, x2

for i = 1:num_vars
x(i) = C{1}(i+1);
end

disp(datetime)
disp(x)
dakota = x;

%------------------------------------------------------------------
% CALL your analysis code to get the function value
%------------------------------------------------------------------

%[f] = rosenbrock(x,alpha);
if isequal(dakotaMode,'E10') || isequal(dakotaMode,'E62') || isequal(dakotaMode,'FGM')
    [~, ~, resultMat] = runControllerTrials(dakota, dakotaMode);
    f = numel(find(resultMat=='Y'));
elseif isequal(dakotaMode,'E61')
    resultMat = runLearnerTrials(dakota, dakotaMode);
    f = mean(resultMat);
end

%------------------------------------------------------------------
% WRITE results.out
%------------------------------------------------------------------
fprintf('%20.10e     f\n', f);
fid = fopen(results,'w');
fprintf(fid,'%20.10e     f\n', f);
%fprintf(fid,'%20.10e     params\n', C{1}(2:5));

fclose(fid);

% alternately
%save(results,'vContact','-ascii');
