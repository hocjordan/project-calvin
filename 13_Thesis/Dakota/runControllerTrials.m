function [activationY, activationN, resultsMat] = runControllerTrials(dakota, dakotaMode)

if isequal(dakota, [])
    if ~input('No dakota parameters. Continue?', 's') == 'Y'
        error('No dakota parameters.')
    end
end

%load('smallopen_walls.mat')
%load('smallmaze_walls.mat')
%load('smallmaze_E61.mat')
load('open_walls.mat')
%load('maze_walls.mat')
%load('largemaze_tmp.mat')
%load('../Final Gradient Model/smallopen_FGM.mat');

activationY = {};
activationN = {};

numTrials = 100;
results = cell(1, numTrials);

ignore_errors = true;

i = 1;
while i <= numTrials
    
    %try
        disp(i);
        
        switch dakotaMode
            case 'E10'
                [results{i}, activation_tracked] = runE10(dakota, walls);
            case 'E62'
                [results{i}, activation_tracked] = runE62(dakota, synapses);
            case 'FGM'
                [results{i}] = runFinalGradient([], dakota, walls);
                assert(results{i} == 'Y' || results{i} == 'N')
            
        end
        
        if strcmp(dakotaMode,'E10') || strcmp(dakotaMode,'E62')
            switch results{i}
                case 'Y'
                    activationY{end+1} = activation_tracked;
                case 'N'
                    activationN{end+1} = activation_tracked;
            end
        end
        
        i = i + 1;
    %catch err
     %   disp(err.message)
    %end
end
resultsMat = cell2mat(results);
numel(find(resultsMat=='Y'))
end



function [result, activation_tracked] = runE10(dakota, walls)
testing = [];
runningTrialBlock = true;
[result, activation_tracked] = E10_Thesis(100,1000, dakota, testing, 'subdiv', runningTrialBlock, walls);
end

function [result, activation_tracked] = runE62(dakota, synapses)
[result, activation_tracked] = E62_Thesis(100, 100, dakota, synapses, synapses.walls);
end

function result = runFinalGradient(agent, dakota, walls)
[~, result, ~] = ChunkShell([], dakota, walls);
end
