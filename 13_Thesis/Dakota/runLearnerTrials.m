function resultsMat = runLearnerTrials(dakota, dakotaMode)

if isequal(dakota, [])
    error('No dakota parameters.')
end

load('smallopen_walls.mat')
%load('smallmaze_walls.mat')
%load('smallmaze_E61.mat')
%load('open_walls.mat')
%load('maze_walls.mat')
%load('largemaze_tmp.mat')
%load('../Final Gradient Model/smallopen_FGM.mat');

numTrials = 100;
assert(numTrials>0);

results = cell(1, numTrials);

i = 1;
while i <= numTrials
    
    try
        disp(i);
        
        switch dakotaMode
            case 'E61'
                percentageSA = runE61(dakota,walls);
                results{i} = percentageSA;
        end
        
        i = i + 1;
    catch err
        disp(err.message)
        if isequal(err.identifier, 'MATLAB:assertion:failed') == 1
            for k = 1:length(err.stack)
                disp(err.stack(k))
            end
            error('Assertion Failed')
        end
    end
end
resultsMat = cell2mat(results);
end


function percentageSA = runE61(dakota,walls)
worldSize = 10;
steps = 1000;
[~, percentageSA] = E61_Thesis(worldSize, steps, dakota, walls);
end
