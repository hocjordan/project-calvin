
function [result, activation_tracked] = E10_Thesis(steps, ind_steps, dakota, testVars, competitionType, runningTrialBlock, extrawalls)

global world
world = struct();
world.state = [];

%dbstop if error

% As experiment 8, but moves the agent (start point) in the direction of
% the strongest activated state cell.

%% MODES
checkModes = true; % SHOULD ALWAYS BE LEFT TRUE
runDakota = false; % automatic, leave false
testing = false;
randomisedPositions = false;
allowUserWalls = true;
verbose = true;
sustainAgent = false;
sustainGoal = true;
generateGaussianStateSynapses = false;
weightNormalisation = false;
gaussianGoal = false;
useMaxPropagate = true;
fakechunk = false;
truechunk = false;

%% Parameters
world.actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};

% attractors
agent_firing = 0.01;
reward_firing = 1;
goal_sigma = 10;

% connections
weakinternal_weights = 0;
internal_weights = 0.9;
synapse_sigma = 1;

% competition
mean_modifier = 0.8;
sparseness = 100;
slope = 15.8;
divisive = 1;

% world
worldSize_x = 20;
worldSize_y = 20;

% cells
num_state = [worldSize_x worldSize_y];

if ~isempty(testVars)
    testing = true;
end

if runningTrialBlock == true
    
    verbose = false;
    checkModes = false;
    randomisedPositions = true;
    allowUserWalls = false;
    
    if isempty(dakota) == false
        runDakota = true;
        
        %%**TESTING**%%
        agent_firing = dakota(1);
        reward_firing = dakota(2);
        mean_modifier = dakota(3);
        divisive = dakota(4);
    end
    
end

if testing == true
    
    checkModes = false;
    verbose = true;
    allowAssertions = true;
    
    for testVar = testVars
        testVar = testVar{:};
        switch testVar
            case 'sustainAgent'
                sustainAgent = true;
            case 'sustainGoal'
                sustainGoal = true;
            case 'generateGaussianStateSynapses'
                generateGaussianStateSynapses = true;
            case 'weightNormalisation'
                weightNormalisation = true;
            otherwise
                error('Unrecognised Test Variable')
        end
        
    end
end

if testing == true && runDakota == true
    
    error('Incompatible Running Modes are Active')
    
end

if checkModes == true
    modeCheck('testing', testing)
    modeCheck('sustainAgent', sustainAgent)
    modeCheck('sustainGoal', sustainGoal)
    modeCheck('generateGaussianSynapses', generateGaussianStateSynapses)
    modeCheck('weightNormalisation', weightNormalisation)
    modeCheck('gaussianGoal', gaussianGoal);
    modeCheck('maxPropagate', useMaxPropagate);
    modeCheck('fakechunk', fakechunk);
    modeCheck('truechunk', truechunk);
end

%% Setup World

if verbose == true; disp('Setup begun.'); end
if verbose == true; disp('Creating world.'); end

if ~exist('extrawalls', 'var')
    [~, ~, walls] = create_maze(worldSize_x, worldSize_y, randomisedPositions, allowUserWalls);                                                                      % Creates the world as a set of matrices, where the first layer shows the position of the agent, the second layer show the position of the agent's goal, and the third layer records the position of walls.
else
    [~, ~, walls] = create_maze(worldSize_x, worldSize_y, randomisedPositions, allowUserWalls, extrawalls);
end

tmp = world.state(:,:,1);
assert(sum(tmp(:))==1)
tmp2 = world.state(:,:,2);
assert(sum(tmp2(:))==1)
assert(isequal(size(world.state), [worldSize_y, worldSize_x, 3]))
assert(all(isfinite(world.state(:))));

[world.agent_y, world.agent_x, ~] = ind2sub(size(world.state(:,:,1)), find(world.state(:,:,1) == 1));
[reward_y, reward_x, ~] = ind2sub(size(world.state(:,:,2)), find(world.state(:,:,2) == 1));

%% Setup Network

if verbose == true; disp('Setting Up Network'); end

state_cells = zeros(num_state);

if verbose == true; disp('Generating synapses.'); end;

internal_synapses = zeros(numel(state_cells));
internal_synapses(:) = weakinternal_weights;

assert(all(isfinite(internal_synapses(:))))

if truechunk == true
    chunk_cells = zeros([1 5]);
    chunk_idx = [67 87];
    chunk_idx2 = [267 287];
    chunk2state = zeros([numel(chunk_cells), numel(state_cells)]);
    state2chunk = zeros([numel(state_cells), numel(chunk_cells)]);
    chunk2state(1, chunk_idx) = 0.5;
    chunk2state(2, chunk_idx2) = 0.5;
    state2chunk(chunk_idx, 1) = 0.5;
    state2chunk(chunk_idx2, 2) = 0.5;
    chunk2chunk = zeros([numel(chunk_cells), numel(chunk_cells)]);
    chunk2chunk = eye(size(chunk2chunk));
end

if generateGaussianStateSynapses == true
    internal_synapses = generateTrueGaussianSynapses(worldSize_x, worldSize_y, synapse_sigma);
else
    for presynaptic_cell = 1:numel(state_cells)
        for postsynaptic_cell = 1:numel(state_cells)
            if chebyshevDistance(worldSize_x, worldSize_y, presynaptic_cell, postsynaptic_cell) == 1
                internal_synapses(presynaptic_cell, postsynaptic_cell) = internal_weights;
            end
        end
    end
end

assert(all(isfinite(internal_synapses(:))))

internal_synapses(:, walls) = 0;
internal_synapses(walls, :) = 0;

if weightNormalisation == true
    internal_synapses = normalise(internal_synapses, 1, false); %% Test normalising weights according number of cell's incoming connections
end

assert(any(internal_synapses(:)))
assert(isequal(size(internal_synapses), [numel(state_cells) numel(state_cells)]));
assert(all(isfinite(internal_synapses(:))))

if gaussianGoal == true
    reward_synapses = generateTrueGaussianSynapses(worldSize_x, worldSize_y, goal_sigma);
    reward_synapses = reward_synapses(find(world.state(:,:,2) == 1), :);
end

if verbose == true; disp('Setup complete.'); end

%% Run Step:



for time = 1:steps
    
    if verbose == true; disp(time); end;
    
    % Activate agent state
    state_cells(world.agent_y, world.agent_x) = agent_firing;
    
    % Activate reward state(s)
    if gaussianGoal == true
    else
        state_cells(reward_y, reward_x) = reward_firing;
    end
    
    %assert(any(state_cells(:)))
    if ~gaussianGoal; assert(sum(state_cells(:)) == agent_firing + reward_firing); end;
    assert(all(isfinite(state_cells(:))))
    
    % Feed activation back into network
    noNaNweights = internal_synapses;
    noNaNweights(isnan(noNaNweights)) = 0;
    
    for ind_time = 1:ind_steps
        
        % state_cells(world.agent_y, world.agent_x) = max(max(state_cells)); state_cells(reward_y, reward_x) = max(max(state_cells));
        if sustainAgent == true
            state_cells(world.agent_y, world.agent_x) = agent_firing;
        end
        
        if sustainGoal == true
            if gaussianGoal == true % CHECK
            else
                state_cells(reward_y, reward_x) = reward_firing;
            end
        end
        
        if fakechunk == true
            idx = [107 127];
            state_cells(idx) = state_cells(idx) + 0.05;
            idx2 = [267 287];
            state_cells(idx2) = state_cells(idx2) + 0.05;
        end
        
        if truechunk == true
            
        end
        
        if gaussianGoal == true
            state_cells = cellPropagate(state_cells, state_cells, reward_firing, [], noNaNweights, reward_synapses, []);
        elseif useMaxPropagate == true
            statecellCopy = state_cells;
            for state = 1:numel(state_cells)
                maxafferent = 0;
                for afferentcell = find(internal_synapses(state, :))
                    afferent_activity = (statecellCopy(afferentcell) * internal_synapses(state,afferentcell));
                    if  afferent_activity > maxafferent
                        maxafferent = afferent_activity;
                    end
                end
                state_cells(state) = maxafferent;
            end
            if truechunk == true
                %chunk_cells(:) = 0;
            chunk_cells = cellPropagate(chunk_cells, state_cells, [], [], state2chunk, [], []);
            chunk_cells(chunk_cells > 1) = 1;
            state_cells = state_cells + cellPropagate(state_cells, chunk_cells, [], [], chunk2state, [], []);
            end
        elseif truechunk == true    
            chunk_cells(:) = 0;
            chunk_cells = cellPropagate(chunk_cells, state_cells, [], [], state2chunk, [], []);
            state_cells = cellPropagate(state_cells, chunk_cells, state_cells, [], chunk2state, noNaNweights, []);
        else
            state_cells = cellPropagate(state_cells, state_cells, [], [], noNaNweights, [], []);
        end
        
        if sustainAgent == true
            state_cells(world.agent_y, world.agent_x) = agent_firing;
        end
        
        if sustainGoal == true
            if gaussianGoal == true
            else
                state_cells(reward_y, reward_x) = reward_firing;
            end
        end
        
        % competition
        switch competitionType
            
            case 'subdiv'
                
                state_cells = state_cells - mean_modifier * mean(mean(state_cells));
                state_cells = state_cells/max(max(state_cells)); state_cells = state_cells*divisive;
                state_cells(state_cells < 0) = 0;
                
            case 'sigmoid'
                %nan
                state_cells = newsoftCompetition(sparseness, slope, state_cells(:));
                state_cells = state_cells/max(max(state_cells));
                state_cells(state_cells < 0) = 0;
                
            case 'sigsubdiv'
                state_cells = softCompetition(sparseness, slope, state_cells(:));
                state_cells = state_cells - mean_modifier * mean(mean(state_cells));
                state_cells = state_cells/max(max(state_cells));
                state_cells(state_cells < 0) = 0;
                
            case 'divisive'
                state_cells = state_cells/max(max(state_cells));
                state_cells(state_cells < 0) = 0;
                
            case 'none'
                
            otherwise
                error('No competition specified')
        end
        state_cells = reshape(state_cells,num_state);
        
        %assert(any(state_cells(:)))
        assert(all(isfinite(state_cells(:))))
        assert(all(state_cells(:) >= 0))
        
        if sustainAgent == true
            state_cells(world.agent_y, world.agent_x) = agent_firing;
        end
        
        if sustainGoal == true
            if gaussianGoal == true
            else
                state_cells(reward_y, reward_x) = reward_firing;
            end
        end
        
        activation_display = reshape(state_cells, num_state);
        induction_tracked{ind_time} = activation_display;
        
        if length(induction_tracked) >= 2
            if isequal(induction_tracked{end}, induction_tracked{end-1}) == true
                %disp('finish')
                if verbose == true; disp(ind_time); end
                break
            end
        end
        
        
    end
    %disp('Induction complete.')
    
    %% Gather Step Data
    
    % Save activation pathway
    activation_tracked{time} = activation_display;
    clear induction_tracked
    
    % Save
    world_tracked{time} = world.state(:,:,1);
    if length(world_tracked) > 3
        if isequal(world_tracked{time}, world_tracked{time-1}, world_tracked{time-2}); result = 'N'; disp(result); break; end;
    end
    
    %% Move Agent
    neighbourInd = chebyshevDistance(worldSize_x, worldSize_y, find(world.state(:,:,1)), state_cells, 1);
    neighbourInd(find(world.state(:,:,1))) = 1;
    neighbours = state_cells(logical(neighbourInd));
    neighbours(5) = 0; % prevent cell from being its own neighbour
    
    next_turn = find(neighbours == max(max(neighbours)));
    if size(next_turn,1) > 1
        disp(['Random action selected from: ' sprintf('%s ', world.actions{next_turn})])
    end
    
    % Activate one cell
    action = world.actions{next_turn(randi(size(next_turn,1)))};
    if verbose == true; disp(action); end;
    actions_tracked{time} = action;
    if length(actions_tracked) >= 2
        switch actions_tracked{time}
            case 'NW'
                if strcmp(actions_tracked{time-1},'SE'); result = 'N'; disp(result); break; end;
            case 'W'
                if strcmp(actions_tracked{time-1},'E'); result = 'N'; disp(result); break; end;
            case 'SW'
                if strcmp(actions_tracked{time-1},'NE'); result = 'N'; disp(result); break; end;
            case 'N'
                if strcmp(actions_tracked{time-1},'S'); result = 'N'; disp(result); break; end;
            case 'S'
                if strcmp(actions_tracked{time-1},'N'); result = 'N'; disp(result); break; end;
            case 'NE'
                if strcmp(actions_tracked{time-1},'SW'); result = 'N'; disp(result); break; end;
            case 'E'
                if strcmp(actions_tracked{time-1},'W'); result = 'N'; disp(result); break; end;
            case 'SE'
                if strcmp(actions_tracked{time-1},'NW'); result = 'N'; disp(result); break; end;
        end
    end
    maze_update(action);
    
    agent_location = find(world.state(:,:,1),1);
    reward_location = find(world.state(:,:,2),2);
    if agent_location == reward_location
        %if ismember(reward_firing, neighbours) == 1 % alter this to check for actual location. will get bugs otherwise.
        result = 'Y';
        disp(result)
        break
    elseif time == steps
        result = 'N';
        disp(result)
        break
    end
    
    %% Update Parameters
    
    state_cells(:) = 0;
    [world.agent_y, world.agent_x, ~] = ind2sub(size(world.state(:,:,1)), find(world.state(:,:,1) == 1));
    
end

%% Analyse Step

%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1])
%slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1])

end