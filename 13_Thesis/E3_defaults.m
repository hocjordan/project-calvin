function switches = E3_defaults(switches)

switches.main.worldType = 'maze';
switches.main.diagonalActions = true;
switches.main.text = 'High';
switches.main.manual_walls = false;
switches.main.load_walls = false;
switches.main.agentGoalPositions = false;
switches.main.suppressChecks = false;
switches.main.suppressFigures = false;
switches.main.reset_agent = false;

switches.diagnostic.occupancy_grid = false;
switches.diagnostic.track_SA = false;
switches.diagnostic.track_SA_FORCE = false;
switches.diagnostic.track_sensory = false;
switches.diagnostic.track_firing = false;
switches.diagnostic.clear_firing = false;
switches.diagnostic.decodeSA = false;
switches.diagnostic.decodegate = false;
switches.diagnostic.resultMat = false;
switches.diagnostic.stepsMat = false;
switches.diagnostic.processingMat = false;
switches.diagnostic.total_steps = false;
switches.diagnostic.total_processing = false;
switches.diagnostic.stepsByResult = false;
switches.diagnostic.IT = false;

switches.learning.traceType = false;
switches.learning.inductionType = false;
switches.learning.two_step = false;

switches.params.learning_steps = 8;
switches.params.induction_steps = 100;
switches.params.synapse_threshold = 1;
switches.params.learningRate = 1;
switches.params.eta = 0.0;
switches.params.place_threshold = 1;
switches.params.worldSize_x = 10;
switches.params.worldSize_y = 10;
switches.params.walls = '';
switches.params.startPosition_x = 1;
switches.params.startPosition_y = 1;
switches.params.goalPosition_x = 1;
switches.params.goalPosition_y = 1;
switches.params.agent_to_load = '';
switches.params.inductionPoint = 55;
end