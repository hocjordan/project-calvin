% find SA decoded entries corresponding to agent/goal state
synapses.SAdec(synapses.SAdec(:,1)==goal_location,:)

% find info associated with a cell
synapses.SAdec(synapses.SAdec(:,3)==cell,:)

% Analyse 50 cells that have been tracked
tmp =  randperm(800);
counter = 0;
for i = 1:800
if ~isempty(tracked{tmp(i)})
if counter < 50
continuous_analysis([10 10], [50 50], agent, tmp(i), tracked)
counter = counter+1;
end
end
end