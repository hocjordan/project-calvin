function tracked = E78_Controller(net, competitionType)

global world
global tracking

tracking = struct;
tracking.net = {};
tracking.world = {};

size = 10;
verbose = true;
ind_steps = 1000;

make_continuous(size);

agent_modifier = 0;
reward_modifier = 1;

world.reward_x = 5.0;
world.reward_y = 1.0;

% Activate agent state
for cell = 1:numel(net.state)
    distance = euclidDistance(net.centers(cell, 1), net.centers(cell, 2), world.agent_x, world.agent_y);
    net.state(cell) = agent_modifier * normpdf(distance, net.mu, net.sigma);
end

% Activate reward state(s)
for cell = 1:numel(net.state)
    distance = euclidDistance(net.centers(cell, 1), net.centers(cell, 2), world.reward_x, world.reward_y);
    net.state(cell) = reward_modifier * normpdf(distance, net.mu, net.sigma);
end

% Feed activation back into network
for ind_time = 1:ind_steps
    
    % Activate agent state
    for cell = 1:numel(net.state)
        distance = euclidDistance(net.centers(cell, 1), net.centers(cell, 2), world.agent_x, world.agent_y);
        net.state(cell) = agent_modifier * normpdf(distance, net.mu, net.sigma);
    end
    
    % Activate reward state(s)
    for cell = 1:numel(net.state)
        distance = euclidDistance(net.centers(cell, 1), net.centers(cell, 2), world.reward_x, world.reward_y);
        net.state(cell) = reward_modifier * normpdf(distance, net.mu, net.sigma);
    end
    
    % Update cells
    net.state = cellPropagate(net.state, net.state, [], [], net.synapses, [], []);
    
    % competition
    switch competitionType
        
        case 'subdiv'
            
            net.state = net.state - mean_modifier * mean(mean(net.state));
            net.state = net.state/max(max(net.state)); net.state = net.state*divisive;
            net.state(net.state < 0) = 0;
            
        case 'sigmoid'
            %nan
            net.state = newsoftCompetition(sparseness, slope, net.state(:));
            net.state = net.state/max(max(net.state));
            net.state(net.state < 0) = 0;
            
        case 'sigsubdiv'
            net.state = softCompetition(sparseness, slope, net.state(:));
            net.state = net.state - mean_modifier * mean(mean(net.state));
            net.state = net.state/max(max(net.state));
            net.state(net.state < 0) = 0;
            
        case 'divisive'
            net.state = net.state/max(max(net.state));
            net.state(net.state < 0) = 0;
            
        case 'none'
            
        otherwise
            error('No competition specified')
    end
    
    net.state = reshape(net.state,net.num_state);
    
    %tracking.world{end+1} = world.representation;
    tracking.net{end+1} = reshape(net.state, [10 10]);
    
    assert(any(net.state(:)))
    assert(all(isfinite(net.state(:))))
    assert(all(net.state(:) >= 0))
    
    
end
%disp('Induction complete.')

%% Gather Step Data

% Save activation pathway

% Save

%% Move Agent

% FOR CHECKING LOOPS
%{
if verbose == true; disp(action); end;
actions_tracked{time} = action;
if length(actions_tracked) >= 2
    switch actions_tracked{time}
        case 'NW'
            if strcmp(actions_tracked{time-1},'SE'); result = 'N'; disp(result); break; end;
        case 'W'
            if strcmp(actions_tracked{time-1},'E'); result = 'N'; disp(result); break; end;
        case 'SW'
            if strcmp(actions_tracked{time-1},'NE'); result = 'N'; disp(result); break; end;
        case 'N'
            if strcmp(actions_tracked{time-1},'S'); result = 'N'; disp(result); break; end;
        case 'S'
            if strcmp(actions_tracked{time-1},'N'); result = 'N'; disp(result); break; end;
        case 'NE'
            if strcmp(actions_tracked{time-1},'SW'); result = 'N'; disp(result); break; end;
        case 'E'
            if strcmp(actions_tracked{time-1},'W'); result = 'N'; disp(result); break; end;
        case 'SE'
            if strcmp(actions_tracked{time-1},'NW'); result = 'N'; disp(result); break; end;
    end
end
%}


%{
agent_location = find(world_state(:,:,1),1);
reward_location = find(world_state(:,:,2),2);
if agent_location == reward_location
    %if ismember(reward_firing, neighbours) == 1 % alter this to check for actual location. will get bugs otherwise.
    result = 'Y';
    disp(result)
    break
elseif time == steps
    result = 'N';
    disp(result)
    break
end
%}

tracked = tracking;

end
