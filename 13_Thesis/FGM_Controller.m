function [sensory_tracked, result, agent] = FGM_Controller(steps, ind_steps, agent, dakota, actions, world_update_function)

%%%%%%%%%%%%%%

%150811: Based on Clean_1504. Has not yet been altered.

%%%%%%%%%%%%%%%

global world_state
global set_verbose
global world
global experimental

% GET INPUT FROM SENSES
[agent.sensory_cells, reward_cells] = world_update_function('', 'yes');

% Cell Sizes
reward_firing = 1;

% Competition
SA_modifier = 0.9; %0.02; % 0.2

% Analysis
display_tracked = {};
SA_tracked = {};
global set_verbose

%% RUN TRIAL
for time = 1:steps
    
    % Allow activity to spread for 1:ind_time
    for ind_time = 1:ind_steps
        
        
        %% Set Up Reward Gradient
        % Place reward.
        % GET REWARD POSITION
        [~, reward_cells] = world_update_function('', 'yes');
        
        % SA cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
        SA_reward = dot((repmat(reward_cells(:),[1,numel(agent.SA_cells)])), agent.sensorytoSA_synapses);
        
        % filter/transfer SA_reward function MAKE GENERAL LATER!!!!!!!!!!!!
        SA_reward(SA_reward < 0.5) = 0;
        
        % final SA update
        agent.SA_cells = cellPropagate(agent.SA_cells, SA_reward, agent.SA_cells, [], agent.SArewardtoSA_synapses, agent.SAtoSA_synapses, []);
        
        
        % Competition in state cells:
        
        % subtractive.
        agent.SA_cells = agent.SA_cells - SA_modifier * repmat(mean(agent.SA_cells), [prod(agent.num_motor) 1 1]);
        
        % divisive.
        agent.SA_cells = agent.SA_cells/max(max(agent.SA_cells));
        sum_save = sum(agent.SA_cells);
        agent.SA_cells(agent.SA_cells < 0) = 0;
        for col = 1:size(agent.SA_cells,3)
            agent.SA_cells(:,:,col) = normalise(agent.SA_cells(:,:,col), sum_save(:,:,col));
            agent.SA_cells(isnan(agent.SA_cells)) = 0;
        end
        agent.SA_cells = reshape(agent.SA_cells,agent.num_SA);
        
        
        
    end
    
    %% Record agent position
    sensory_info = world_state;
    sensory_tracked{time} = sensory_info;
    
    % Record final SA gradient.
    SA_tracked{end+1} = cell2mat(SA_analyseAct(size(world_state,1), agent.SA_cells, agent.SA_decoded, false));
    
    %% Move Agent through Bursting Sensory Activity
    
    if experimental == true
        goaltoSA_synapses = GenerateZeroWeights(1, numel(agent.SA_cells), 1);
        goaltoSA_synapses = goaltoSA_synapses+ 1 * 1 * agent.SA_cells(:)';
        agent.SA_cells(:) = 0;
    end
        
    [agent.sensory_cells, ~] = world_update_function('', 'yes');
    agent.sensory_cells(agent.sensory_cells > 0) = 100;
    reward_cells(reward_cells > 0) = reward_firing;
    
    if experimental == true
        agent.SA_cells = cellPropagate(agent.SA_cells, agent.sensory_cells, 1, agent.SA_cells, agent.sensorytoSA_synapses, goaltoSA_synapses, agent.SAtoSA_synapses);
    else
        agent.SA_cells = cellPropagate(agent.SA_cells, agent.sensory_cells, reward_cells, agent.SA_cells, agent.sensorytoSA_synapses, agent.sensorytoSA_synapses, agent.SAtoSA_synapses);
    end
        
    agent.SA_cells = WTA_Competition(agent.SA_cells);
    agent.SA_cells = reshape(agent.SA_cells,agent.num_SA);
    
    
    % Calculate activation to motor cells
    agent.motor_cells = dot(repmat(agent.SA_cells(:),[1,numel(agent.motor_cells)]), agent.SAtomotor_synapses);
    
    % Move agent.
    action = actions{find(agent.motor_cells == max(agent.motor_cells))};
    if set_verbose == true
        disp(action)
    end
    world_update_function(action, 'no');
    
    %% Feed Activity to Chunk Cells
    
    % Uses a full trace rule -- preSynaptic_trace *
    % postSynaptic_trace
    
    %agent = activateChunkCells(true, false, agent);
    
    %% Check for Completion
    % ALTER FOR GENERALISATION, OR JUST CALL PART OF THE SIMULATION
    %if max(abs(current_state - reward_location)) < 0.5
    % If all nonzero elements of the reward representation are also
    % represented in the sensory cells, then end.
    [agent.sensory_cells, ~] = world_update_function('', 'yes');
    if ~any(~ismember(find(reward_cells), find(agent.sensory_cells)))
        result = 'Y';
        disp(time)
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Clear Cells
    agent.sensory_cells(:) = 0;
    reward_cells(:) = 0;
    agent.SA_cells(:) = 0;
    agent.motor_cells(:) = 0;
    agent.chunk_cells(:) = 0;
    
end



end


















function matrix = normalise(matrix, threshold)

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));

end