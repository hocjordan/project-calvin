function [tracked, net] = E78_Thesis()

global world
global tracking

tracking = struct;
tracking.net = {};
tracking.world = {};

size = 10;
top_speed = 7;

% Make world
make_continuous(size);

world.agent_x = 5.0;
world.agent_y = 5.0;

% Make network
net = struct();
net.num_state = [1 100];
net.state = zeros(net.num_state);
net.centers = zeros(numel(net.state), 2);
net.synapses = zeros(net.num_state(2));
net.learningRate = 1;
net.threshold = 1;
net.mu = 0;
net.sigma = 3;


tmp = 0:(world.Size/9):world.Size;

for i = 1:10
    for j = 1:10
        net.centers(10*(i-1)+j, 1) = tmp(i);
        net.centers(10*(i-1)+j, 2) = tmp(j);
    end
end

% TRIAL LOOP

for time = 1:10000
    
    % Update network
    for cell = 1:numel(net.state)
        distance = euclidDistance(net.centers(cell, 1), net.centers(cell, 2), world.agent_x, world.agent_y);
        net.state(cell) = normpdf(distance, net.mu, net.sigma);
    end
    
    % Implement Hebbian Learning
    net.synapses = net.synapses + net.learningRate * net.state(:) * net.state(:)';
    net.synapses = normalise(net.synapses, net.threshold);
    
    
    tracking.world{end+1} = world.representation;
    tracking.net{end+1} = reshape(net.state, [10 10]);
    
    action = world.actions{randi(numel(world.actions))};
    speed = rand(1) * top_speed;
    
    continuous_update(action, speed);
    
end

tracked = tracking;

end

