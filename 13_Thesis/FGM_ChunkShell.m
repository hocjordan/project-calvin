function [agent, result, walls] = ChunkShell(agent, dakota, walls)
% Runs one full navigational experiment:

% The model starts off with a 10x10 world surrounded by walls (I don't advise using more than 20x20).

% It then gives you the opportunity to add or remove more walls.

% It defines a set of possible actions (here, the 8 cardinal directions
% plus inaction.

% It sets the 'maze_update' function given below as the update function
% that the learning/control models will use to perform actions and recieve
% sensory feedback. The learning/control functions are written so that
% many types of update function can swapped in and out.

% It then calls the learner function for 1000 timesteps. This function will
% explore the world for 1000 timesteps, learning its structure and how the
% model can interact with it -- in this case by moving along the cardinal
% directions. It returns a set of synapses that reflect the learning
% process.

% The results of the learning process are analysed to give 'SA_decoded', a
% list of all the SA cells that fired during the learning process and the
% state/action combinations that they fired to.

% Maze_World then tests this learning. It asks for a start and end
% position, then runs the controller function to move an agent between
% them.

% The controller function takes as parameters the synapses produced by the learner
% function as well as the action set and the world update function. It will
% spend 50 iterations performing reasoning for every timestep, and has an
% upper limit of 100 timesteps to reach its goal before it gives up.

% After the controller function succeeds or fails in moving the agent to
% its goal, the model will analyse the agent's progress and display it as a
% figure. The slider on this figure can be moved and represents time.

%%%%%%%%%%%%%%%%%%%%%%%%%

% 150811: Based on Clean_1504. Has not yet been altered.

%% Parameters
runLearner = true;
runController = true;
global set_verbose
set_verbose = false;
global allowAssertions
allowAssertions = true;
global experimental
experimental = false;
global goal_formation
goal_formation = false;
allowUserWalls = true;
randomisedPositions = true;
testing = false;
learningTime = 3000;
goalIterations = 70;

if ~isequal(dakota, [])
    runDakota = true;
else
    runDakota = false;
end

if runDakota == true
    allowUserWalls = false;
    runLearner = true;
    runController = true;
    set_verbose = true;
    randomisedPositions = true;
    learningTime = dakota(1);
    goalIterations = 1;
end

if testing == true
    if ~isequal(input('In "Testing" mode. Continue? Y/N: ', 's'),'Y')
        return
    end
    runLearner = true;
    runController = true;
    allowAssertions = true;
    allowUserWalls = true;
    randomisedPositions = true;
    goalIterations = 2;
end

if experimental == true
    if ~isequal(input('In "Experimental" mode. Continue? Y/N: ', 's'),'Y')
        return
    end
end

if goal_formation == true
    if ~isequal(input('In "Goal Formation" mode. Continue? Y/N: ', 's'),'Y')
        return
    end
end

if testing == true && runDakota == true
    error('Incompatible Running Modes are Active')
end

%% Define Sensory Information and Starting Point                                                                                                         % Sets the world as a global variable, available to the learner, controller and update functions without being explictly called as an argument.

worldSize_x = 10;                                                                                                           % Set the x and y size of the simulated 2D world.
worldSize_y = 10;

if ~exist('walls', 'var')
    [~, ~, walls] = FGM_create_maze(worldSize_x, worldSize_y, randomisedPositions, allowUserWalls);                                                                      % Creates the world as a set of matrices, where the first layer shows the position of the agent, the second layer show the position of the agent's goal, and the third layer records the position of walls.
else
    [~, ~, walls] = FGM_create_maze(worldSize_x, worldSize_y, randomisedPositions, allowUserWalls, walls);
end                                                                     % Creates the world as a set of matrices, where the first layer shows the position of the agent, the second layer show the position of the agent's goal, and the third layer records the position of walls.

%% Define Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};                                                                         % The actions available to the agent, represented as a set of strings.

%% Define World Functions
world_update_function = @maze_update;                                                                                       % Sets the maze_update function (see below) as a function handle that can be given to the learner and controller functions as arguments.

%% Setup Agent

if ~exist('agent', 'var') == 1 || isequal(agent, []);
    agent = createAgent(actions, @maze_update);
end

%% Call Learner function
if runLearner == true
    [agent, SA_tracked, sensory_tracked] = FGM_Learner(learningTime, agent, actions, world_update_function);
    
    %% Analyse and Save Data
    
    sensory_tracked_matrix = [];
    for time = 1:size(sensory_tracked,2)
        sensory_tracked_matrix(:,:,:,time) = sensory_tracked{time};
    end
    
    %{
world_tracked = {};
for time = 1:steps
    world_tracked{time} = sensory_tracked_matrix(:,:,1,time) + sensory_tracked_matrix(:,:,3,time);
end

slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
    %}
    
    % Decode SA information
    agent.SA_decoded = [];
    for time = 1:size(sensory_tracked,2)
        SA_tracked{time}{1} = find(SA_tracked{time}{1});
        agent.SA_decoded(time,:) = cell2mat(SA_tracked{time}(:)');
    end
    
    
    % Record every state/action combo.
    agent.SA_decoded = unique(agent.SA_decoded,'rows');
    agent.SA_decoded = sortrows(agent.SA_decoded, 3);
    
else if ~isfield(agent, 'SA_decoded') == 1 && runController == true
        
        error('SA decoded must be provided if learning skipped.')
    end
end

%% Goal Loop for Chunk Learning

if runController == true
    
    for gI = 1:goalIterations
        
        disp(' ')
        fprintf('** %d/%d **', gI, goalIterations);
        disp(' ')
        
        %% Create Agent/Goal Positions
        [reward_x, reward_y, ~] = FGM_create_maze(worldSize_x, worldSize_y, randomisedPositions, allowUserWalls, walls);
        
        %% Call Goal Function
        [sensory_tracked, result, agent] = FGM_Controller(100, 50, agent, dakota, actions, world_update_function);  % The controller function: runs for a maximum of 100 timesteps or until the goal is reached. Reasons over 50 iterations. If the agent is not moving in the right direction, increase the reasoning value.
        
        %% Analyse and Save Data
        sensory_tracked_matrix = [];
        for time = 1:size(sensory_tracked,2)
            sensory_tracked_matrix(:,:,:,time) = sensory_tracked{time};
        end
        
        %
        world_tracked = {};
        for time = 1:size(sensory_tracked,2)
            world_tracked{time} = sensory_tracked_matrix(:,:,1,time) + sensory_tracked_matrix(:,:,3,time);
        end
        
        %% Reset Agent
        agent = resetAgent(agent);
        
        %slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
        %}
        
    end
    
end

end
