function [agent, world] = E3_learning(agent, world, switches)

learning = switches.learning;
params = switches.params;

% Begin timestep:
for time = 1:params.learning_steps
    %tic
    % Update place cells
    if learning.two_step == true
        agent.place_cells = world.state(:,:,1) + world_saved;
        agent.place_cells(agent.place_cells > 1) = 1;
        assert(~any(agent.place_cells(:) > 1));
        assert(~any(agent.place_cells(:) < 0));
    else
        agent.place_cells = world.state(:,:,1);
    end
    
    if strcmp(switches.main.text, 'High'); find(agent.place_cells); end
    
    % Calculate the firing rate of all cells and update synapses
    agent.synapses(isnan(agent.synapses)) = 0;
    switch learning.traceType
        case 'forward'
            agent.synapses = agent.synapses+(params.learningRate* agent.place_trace(:) * agent.place_cells(:)');
        case 'inverse'
            agent.synapses = agent.synapses+(params.learningRate* agent.place_cells(:) * agent.place_trace(:)');
        case 'both'
            %agent.synapses = agent.synapses+(params.learningRate* trace(:) * trace(:)');
            agent.synapses = agent.synapses+(params.learningRate* agent.place_trace(:) * agent.place_cells(:)');
            agent.synapses = agent.synapses+(params.learningRate* agent.place_cells(:) * agent.place_trace(:)');
        case 'two_step'
            agent.synapses = agent.synapses+(params.learningRate* agent.place_cells(:) * agent.place_cells(:)');
        case false
        otherwise
            error('Invalid trace type');
    end
    
    assert(all(isfinite(agent.synapses(:))))
    %agent.synapses(logical(eye(size(agent.synapses)))) = NaN;
    
    % Normalise synapses
    agent.synapses(agent.synapses > params.synapse_threshold) = params.synapse_threshold;
    %synapses = normalise(synapses);
    
    % End timestep. Repeat.
    if time == params.learning_steps
        final = find(agent.place_cells);
    else
        % Activate one cell
        action = world.actions{randi(numel(world.actions))};
        if strcmp(switches.main.text, 'High'); disp(action); end;
        [~, ~, ~, world] =  update_world(world, action, switches);
        world.tracked{end+1} = agent.place_cells;
    end
    
    % Calculate the trace value for all cells
    agent.place_trace = getTrace(agent.place_cells, agent.place_trace, params.eta);
    
    %toc
end

end