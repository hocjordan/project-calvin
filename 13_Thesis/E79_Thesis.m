function [agent, sensory_cells, sensory_tracked3] = E79_Thesis()

%% Parameters
global world
global tracking

runLearner = true;
runController = true;
global set_verbose
set_verbose = true;
global allowAssertions
allowAssertions = true;
allowUserWalls = true;
randomisedPositions = true;
learningTime = 30000;
goalIterations = 70;
world_update_function = @continuous_update;
world_observe_function = @continuous_observation;

worldSize = 50;
min_speed = 3;
top_speed = 7;
movement_type = 'random';

%% Setup

% Make world
make_continuous(worldSize);

% Define World Functions
world.update = world_update_function;
world.observe = world_observe_function;
world.top_speed = top_speed;
world.min_speed = min_speed;
world.movement_type = movement_type;

world.agent_r = 25.0;
world.agent_c = 25.0;

tracking = struct;
tracking.net = {};
tracking.world = {};

% Make sensory layer
global sensory_cells
sensory_cells = broadRF_net([1 100]);

% Make SA network
if ~exist('agent', 'var') == 1 || isequal(agent, []);
    agent = createAgent(world.actions, world.observe);
end


%% Learning

[agent, SA_tracked, sensory_tracked3] = FGM_Learner(learningTime, agent);

%% Analyse and Save Data

% Decode SA information
agent.SA_decoded = [];
for time = 1:numel(SA_tracked)
    agent.SA_decoded(time,:) = cell2mat(SA_tracked{time}(:)');
end

% Record every state/action combo.
agent.SA_decoded = unique(agent.SA_decoded,'rows');
agent.SA_decoded = sortrows(agent.SA_decoded, 4);

%{

%% Planning


%{
world_tracked = {};
for time = 1:steps
    world_tracked{time} = sensory_tracked_matrix(:,:,1,time) + sensory_tracked_matrix(:,:,3,time);
end

slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
%}







tracked = tracking;
%}
end

