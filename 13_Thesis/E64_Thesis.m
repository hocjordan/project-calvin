function [SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded, walls] = Experiment64(worldSize, steps, extrawalls)

% Learns backward SA_column synapses as Exp63, but removes the restrictions
% on exploring into walls, allowing the agent to learn for itself what it
% can and cannot do.

% Ultimately produces same synapses as Experiment61 but requires many more
% trials.

%% Parameters
% World
worldSize_x = worldSize;
worldSize_y = worldSize;
agent_x = 2;
agent_y = 2;
reward_x = 3;
reward_y = 3;

% Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};

% Cells
num_sensory = [worldSize_y, worldSize_x];
num_motor = [1 size(actions, 2)];
num_SAcol = prod(num_sensory);
num_SAcellsinCol = prod(num_motor);
num_SA = [num_SAcellsinCol 1 num_SAcol];

% Synapses
sensorytoSA_dilution = 1; %0.3
motortoSA_dilution = 1; %0.2
SAtomotor_dilution = 1; %0.2

% Competition and Learning
sparseness = 99;
slope = 90000000000000000000;
learningRate = 100;
trace_learningRate = 10; %0.1; %0.001; % 0.0001

normalisation_threshold = 1;
trace_threshold = 4; %0.01

eta = 0.0;

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Noise
state_noise = 0.002;

% Analysis
SA_tracked = [];
text_on = 0;


%% Setup
% Create world and place agent within it.
world = create_world3(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);
world_summed = zeros(size(world(:,:,1)));

% Make networks of sensory, motor and SA neurons.
sensory_cells = zeros(num_sensory);
motor_cells = zeros(num_motor);
SA_cells = zeros(num_SA);
SA_trace = zeros(num_SA);

% Create synapse weights between SA cells and motor<=>SA.
SAtoSA_synapses = GenerateZeroWeights(numel(SA_cells), numel(SA_cells), 1);
motortoSA_synapses = Generate_Diluted_Weights(motor_cells, SA_cells, motortoSA_dilution, 1);
SAtomotor_synapses = Generate_Diluted_Weights(SA_cells, motor_cells, SAtomotor_dilution, 1);

% Create sensory => SA synapses that are identical for all cells in a
% column.
sensorytoSA_synapses = [];
for column = 1:num_SAcol
sensorytoSA_synapses = [sensorytoSA_synapses repmat(Generate_Diluted_Weights(sensory_cells, 1, sensorytoSA_dilution, 1), [1 size(actions,2)])];
end

% Normalise synapses
SAtoSA_synapses = normalise(SAtoSA_synapses, normalisation_threshold);
sensorytoSA_synapses = normalise(sensorytoSA_synapses, normalisation_threshold);
motortoSA_synapses = normalise(motortoSA_synapses, normalisation_threshold);
SAtomotor_synapses = normalise(SAtomotor_synapses', normalisation_threshold);
SAtomotor_synapses = SAtomotor_synapses';

%% Walls.

world(:,:,3) = zeros(size(world(:,:,2)));

rows = size(world,1);
col = size(world,2);

walls = [];

top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

walls = [walls top_wall];

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

walls = [walls bottom_wall];

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

walls = [walls left_wall];
walls = [walls right_wall];

% add extra walls if given
if exist('extrawalls', 'var') == 1
    walls = [walls extrawalls'];
end

% walls are represented in the world
%
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

% user interface to create maze
user_walls = figure(); imagesc(world(:,:,3)); title('Walls: Click to Add, Click to Remove');
while ishandle(user_walls) == 1;
    try
        [x, y] = ginput(1);
        x=round(x); y=round(y);
        if world(y,x,3) == 3;
            world(y,x,3) = 0;
        else
            world(y,x,3) = 3;
        end
        imagesc(world(:,:,3));
    end    
end

walls = find(world(:,:,3) == 3);

% place agent
fig = figure(); imagesc(world(:,:,3)); title('Please select starting position.');
[x, y] = ginput(1);
x=round(x); y=round(y);
agent_x = x; agent_y = y;

close(fig)

world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

world(:,:,3) = zeros(size(world(:,:,2)));
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

%% RUN TRIAL

%% Timing

tic;
duration = 0;
for time = 1:steps
    
    if time == round(steps/10)
        disp('1/10')
        duration = toc;
        fprintf('Time remaining = %s', datestr((9*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(2*steps/10)
        disp('2/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((8*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(3*steps/10)
        disp('3/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((7*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(4*steps/10)
        disp('4/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((6*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(5*steps/10)
        disp('5/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((5*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(6*steps/10)
        disp('6/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((4*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(7*steps/10)
        disp('7/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((3*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(8*steps/10)
        disp('8/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((2*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(9*steps/10)
        disp('9/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((1*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(10*steps/10)
        disp('10/10')
    end
    
    noNaNsensorytoSA = sensorytoSA_synapses;
    noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
    
    noNaNmotortoSA = motortoSA_synapses;
    noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
    
    noNaNSAtomotor = SAtomotor_synapses;
    noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
    
    %% Firing of SA cells and Recruitment of Column
    
    % Sensory cells fire according to agent position (1 cell per place).
    sensory_cells = world(:,:,1);
    
    if text_on == 1
        fprintf('State = %d', find(sensory_cells))
        disp(' ')
    end
    
    % SA cells fire based on current sensory cells (state).
    SA_cells = dot((repmat(sensory_cells(:),[1,numel(SA_cells)])), noNaNsensorytoSA);
    SA_cells = reshape(SA_cells, num_SA);
    
    % columnar WTA
    SA_cells(SA_cells == max(max(SA_cells))) = 1;
    SA_cells(SA_cells ~= max(max(SA_cells))) = 0;
    
    if text_on == 1
        fprintf('SA = %s', num2str(find(SA_cells > 0.1)))
        disp(' ')
    end
    
    % Update sensorytoSA weights.
    sensorytoSA_synapses = sensorytoSA_synapses + (learningRate * (sensory_cells(:) * SA_cells(:)'));
    sensorytoSA_synapses = normalise(sensorytoSA_synapses, normalisation_threshold);
    noNaNsensorytoSA = noNaN(sensorytoSA_synapses);
    
    %% TRACE LEARNING: Update connections from ACTIVE COLUMN to PREVIOUS (trace) SA_CELL:
    
    % Calculate the trace value for all cells
    %SA_trace = getTrace(SA_cells, SA_trace, eta);
    
    % Update synapses using lr * state firing * trace rule (backwards)
    SAtoSA_synapses = SAtoSA_synapses + (trace_learningRate * SA_cells(:) * SA_trace(:)');
    
    % Block off self-self synapses
    %SAtoSA_synapses(logical(eye(size(SAtoSA_synapses)))) = NaN;
    
    % Normalise synapses
    SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);
    
    %% Select an action and activate the appropriate motor cell.
    %valid_movement_selected = 0;
    world_summed_copy = world_summed;
    %world_summed_copy(world(:,:,3) == 3) = NaN;
        
        % find the most active state cell(s) next to the current position
        worldSize_y = size(world_summed_copy,1);
        worldSize_x = size(world_summed_copy,2);
        
        current_state = sub2ind(size(world_summed), agent_y, agent_x);
        
        w = current_state-worldSize_y; n = current_state-1; s = current_state+1; e = current_state+worldSize_y;
        nw = w-1; sw = w+1; ne = e-1; se = e+1;
        
        test = [n e s w nw ne se sw];
        idx = find(test <= 0);
        test(idx) = test(idx) + worldSize_y*worldSize_x;
        idx = find(test > worldSize_y*worldSize_x);
        test(idx) = test(idx) - worldSize_y*worldSize_x;
        
        n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
        
        neighbours = zeros(3);
        
        neighbours(1) = world_summed_copy(nw);
        neighbours(2) = world_summed_copy(w);
        neighbours(3) = world_summed_copy(sw);
        neighbours(4) = world_summed_copy(n);
        neighbours(5) = NaN;
        neighbours(6) = world_summed_copy(s);
        neighbours(7) = world_summed_copy(ne);
        neighbours(8) = world_summed_copy(e);
        neighbours(9) = world_summed_copy(se);
        
        
        next_turn = find(neighbours == min(min(neighbours)));
        if size(next_turn,1) > 1
            %disp('RANDOM ACTION SELECTED')
        end
        
        action = actions{next_turn(randi(size(next_turn,1)))};
        %disp(action)
        [world, valid_movement_selected, intended_outcome] = update_world5(world, action, 0);
        if valid_movement_selected == 0
            %disp('Invalid Move')
        end
    
    [~, idx] = ismember(action, actions);
    motor_cells(idx) = 1;
    
    if text_on == 1
        fprintf('Moved %s (%d)', action, find(motor_cells))
        disp(' ')
    end
    
    % Activation of SA cells from sensory and motor cells simulataneously.
    %SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(motor_cells(:)],[1,numel(SA_cells)]), [noNaNsensorytoSA; noNaNsensorytoSA]));
    SA_cells(:) = 0;
    SA_cells = dot(([repmat(motor_cells(:), [1,numel(SA_cells)]); repmat(sensory_cells(:), [1,numel(SA_cells)])]), [noNaNmotortoSA; noNaNsensorytoSA]);
    SA_cells = reshape(SA_cells, num_SA);
    %SA_cells = SA_cells + dot((repmat(motor_cells', [1,numel(SA_cells)])), noNaNmotortoSA);
    
    % WTA:
    SA_cells = WTA_Competition(SA_cells(:));
    SA_cells = reshape(SA_cells, num_SA);
    
    if text_on == 1
        fprintf('SA = %s', num2str(find(SA_cells > 0.1)'))
        disp(' ')
    end
    
    % Weights updated.
    
    motortoSA_synapses = motortoSA_synapses + learningRate * motor_cells(:) * SA_cells(:)';
    SAtomotor_synapses = SAtomotor_synapses + learningRate * SA_cells(:) * motor_cells(:)';
    
    % Weights normalised
    
    motortoSA_synapses = normalise(motortoSA_synapses, normalisation_threshold);
    SAtomotor_synapses = normalise(SAtomotor_synapses', normalisation_threshold);
    SAtomotor_synapses = SAtomotor_synapses';
    
    SA_info = [find(sensory_cells), find(motor_cells), find(SA_cells > 0.1)'];
    SA_tracked{time} = SA_info;
        
    if text_on == 1
        disp(' ')
    end
    
    %% Calculate SA trace
    
        % Calculate the trace value for all cells
    SA_trace = getTrace(SA_cells, SA_trace, eta);
    
    %% Reset Cells and Update parameters
    
    % Record agent position
    world_tracked{time} = world(:,:,1);
    world_summed = world_summed + intended_outcome;
    
    SA_cells(:) = 0;
    sensory_cells(:) = 0;
    motor_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);

figure(); imagesc(world_summed); title('Sum of Time Spent in Each Location')

%% ANALYSE RESULTANT MAPPING
% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
disp('Complete')

disp('Calculating state/action mappings.')
%HML_mappings(sensorytoSA_synapses, motortoSA_synapses, mapping_threshold);
disp('Complete')

%figure(); imagesc(sensorytoSA_synapses); colorbar; title('Sensory => S Synapses'); xlabel('State Cells'); ylabel('Sensory Cells)');
%figure(); imagesc(motortoSA_synapses); colorbar; title('Motor => SA Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');
%figure(); imagesc(SAtomotor_synapses'); colorbar; title('SA => Motor Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');

% Display repeatedly firing SA cells and the state/action that causes them
% to fire. This allows you to check that they are always firing for the
% same combo.
%{
SA_tracked_copy = SA_tracked;
thirdElement = cellfun(@(x)x(3), SA_tracked_copy);
[~, idx] = sort(thirdElement);
SA_tracked_copy = SA_tracked_copy(idx);
thirdElement = cellfun(@(x)x(3), SA_tracked_copy);
thirdElement_repeats = thirdElement;
SA_tracked_copy(~ismember(thirdElement_repeats, thirdElement_repeats(diff(thirdElement_repeats) == 0))) = [];

for count = 1:numel(SA_tracked_copy)
    disp(SA_tracked_copy{count})
end
%}
% Record every state/action combo.
SA_decoded = sortrows(cell2mat(SA_tracked(:)),[1 2]);
SA_decoded = unique(SA_decoded, 'rows');


%% Package Synaptic Data

%synapticData = {SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded};

disp('Complete')
end















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches threshold. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end