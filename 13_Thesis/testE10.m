function testE10()
clear all

E10_Path()

runningTrialBlock = true;

addpath('~/Documents/Repos/projectcalvin/Walls')
load('maze_walls.mat')

disp('Starting testing. Please set variable "testing" to true, then press enter.')
pause

profile on
E10_Thesis(100,100, [], {}, 'subdiv', runningTrialBlock);
E10_Thesis(100,100, [], {}, 'sigmoid', runningTrialBlock);
E10_Thesis(100,100, [], {}, 'sigsubdiv', runningTrialBlock);
E10_Thesis(100,100, [], {}, 'divisive', runningTrialBlock);
E10_Thesis(100,100,[], {}, 'subdiv', runningTrialBlock, walls);

E10_Thesis(100,100, [], {'sustainFiring'}, 'subdiv', runningTrialBlock);
E10_Thesis(100,100, [], {'sustainFiring'}, 'sigmoid', runningTrialBlock);
E10_Thesis(100,100, [], {'sustainFiring'}, 'sigsubdiv', runningTrialBlock);
E10_Thesis(100,100, [], {'sustainFiring'}, 'divisive', runningTrialBlock);
E10_Thesis(100,100,[],  {'sustainFiring'}, 'subdiv', runningTrialBlock, walls);

E10_Thesis(100,100, [], {'sustainFiring', 'weightNormalisation'}, 'subdiv', runningTrialBlock);
E10_Thesis(100,100, [], {'sustainFiring', 'weightNormalisation'}, 'sigmoid', runningTrialBlock);
E10_Thesis(100,100, [], {'sustainFiring', 'weightNormalisation'}, 'sigsubdiv', runningTrialBlock);
E10_Thesis(100,100, [], {'sustainFiring', 'weightNormalisation'}, 'divisive', runningTrialBlock);
E10_Thesis(100,100,[],  {'sustainFiring', 'weightNormalisation'}, 'subdiv', runningTrialBlock, walls);

E10_Thesis(100,100, [], {'sustainFiring', 'generateGaussianStateSynapses'}, 'subdiv', runningTrialBlock);
E10_Thesis(100,100, [], {'sustainFiring', 'generateGaussianStateSynapses'}, 'sigmoid', runningTrialBlock);
E10_Thesis(100,100, [], {'sustainFiring', 'generateGaussianStateSynapses'}, 'sigsubdiv', runningTrialBlock);
E10_Thesis(100,100, [], {'sustainFiring', 'generateGaussianStateSynapses'}, 'divisive', runningTrialBlock);
E10_Thesis(100,100,[], {'sustainFiring', 'generateGaussianStateSynapses'}, 'subdiv', runningTrialBlock, walls);
profile off

disp('Ending testing. Please set variable "testing" to false, then press enter.')
pause

end