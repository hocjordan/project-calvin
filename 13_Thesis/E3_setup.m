function [agent, world] = E3_setup(switches)

% Assign parameters

dilution = 1;
world = struct();
world.worldSize_x = switches.params.worldSize_x;
world.worldSize_y = switches.params.worldSize_y;
world.tracked = {};

%actions = {'E'};
%actions = {'N' 'E'};
world.actions = {'N' 'S' 'E' 'W' 'NE' 'SE' 'SW' 'NW'};

% Create world
if switches.main.load_walls
    switch switches.params.walls
        case 'SmallOpen'
            tmp_walls = load(fullfile(pwd, '..', 'Walls', 'smallopen_walls.mat'), 'walls');
            world.walls = tmp_walls.walls;
        case 'SmallMaze'
            tmp_walls = load(fullfile(pwd, '..', 'Walls', 'smallmaze_walls.mat'), 'walls');
            world.walls = tmp_walls.walls;
        case 'SmallBisected'
            tmp_walls = load(fullfile(pwd, '..', 'Walls', 'smallbisected_walls.mat'), 'walls');
            world.walls = tmp_walls.walls;
        case 'LargeOpen'
            tmp_walls = load(fullfile(pwd, '..', 'Walls', 'open_walls.mat'), 'walls');
            world.walls = tmp_walls.walls;
        case 'LargeMaze'
            tmp_walls = load(fullfile(pwd, 'Walls', 'maze_walls.mat'), 'walls');
            world.walls = tmp_walls.walls;
        otherwise
            error('Switch Error')
    end
    assert(switches.main.manual_walls == false)
end

if  isfield(world, 'walls')
    [~, ~, world] = create_world(world, switches, world.walls);
else
    [~, ~, world] = create_world(world, switches);
end

assert(numel(find(world.state(:,:,1))) == 1);
assert(numel(find(world.state(:,:,2))) == 1);

% Create grid of place cells
agent.place_cells = world.state(:,:,1);
initial = find(world.state(:,:,1));
agent.place_trace = zeros([10 10]);
assert(isequal(size(agent.place_cells), size(agent.place_trace)));
world_tracked{1} = world.state(:,:,1);

% Create synapses with full connectivity, save for self-self
agent.synapses = GenerateZeroWeights(numel(agent.place_cells), numel(agent.place_cells), dilution);

switch switches.learning.traceType
    case 'two_step'
        two_step = true;
        world_saved = zeros(size(agent.place_cells));
    otherwise
        two_step = false;
end

end