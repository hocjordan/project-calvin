function activation_tracked = E3_induction(agent, world, switches)

% feed current to final position and see how it affects the other cells in
% the network.
noNaNweights=agent.synapses;
noNaNweights(isnan(noNaNweights)) = 0;
agent.place_cells(:) = 0;
activation_tracked = {};

learning = switches.learning;
params = switches.params;

for time = 1:params.induction_steps
    
    % set reward representation at final position.
    switch learning.inductionType
        case 'initial'
            agent.place_cells(initial) = 1;
        case 'final'
            agent.place_cells(final) = 1;
        case 'both'
            agent.place_cells(initial) = 1;
            agent.place_cells(final) = 1;
        case 'set'
            agent.place_cells(params.inductionPoint) = 1;
        case false
        otherwise
            error('Invalid Induction Type')
    end
    agent.place_cells = cellPropagate(agent.place_cells, agent.place_cells, [], [], noNaNweights, [], []);
    switch learning.inductionType
        case 'initial'
            agent.place_cells(initial) = 1;
        case 'final'
            agent.place_cells(final) = 1;
        case 'both'
            agent.place_cells(initial) = 1;
            agent.place_cells(final) = 1;
        case 'set'
            agent.place_cells(params.inductionPoint) = 1;
        case false
        otherwise
            error('Invalid Induction Type')
    end
    
    % divisive.
    nan_check = false;
    agent.place_cells = agent.place_cells/max(max(agent.place_cells));
    agent.place_cells(:) = normalise(agent.place_cells(:), params.place_threshold, nan_check);
    
    activation_display = reshape(agent.place_cells,[10,10]);
    activation_tracked{end+1} = activation_display;
end

end