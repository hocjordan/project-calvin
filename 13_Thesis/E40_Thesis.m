%% Trace Experiment 40

function [SA_decoded] = E40_Thesis(steps)

% As Experiment 5, but with order changed and using create_world3 and
% update_world4. Added separate synapses for SA => M and M => SA.


%% Parameters
verbose_on = true;

% World
worldSize_x = 4;
worldSize_y = 4;
agent_x = 2;
agent_y = 2;
reward_x = 3;
reward_y = 3;

% Cells
num_place = [worldSize_y, worldSize_x];
num_action = [1 4];
num_SA = [10 10];

% Synapses
statetoSA_dilution = 1; %0.3
actiontoSA_dilution = 1; %0.2
SAtoAction_dilution = 1; %0.2

% Competition and Learning
eta = 0.8;
learningRate = 40000; % 0.004
mapping_threshold = 0.6;
sensory_normalisation = 1; % 1
motor_normalisation = 1; % 0.2

%% SETUP TRIAL


% Create 4x4 world and place agent within it.
world = create_world3(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Make networks of sensory (4x4), motor (4) and SA (4x4x4) neurons.
state_cells = zeros(num_place);
action_cells = zeros(num_action);
SA_cells = zeros(num_SA);

% Create synapse weights between sensory-SA and motor-SA.
statetoSA_synapses = Generate_Diluted_Weights(state_cells, SA_cells, statetoSA_dilution, 1);
actiontoSA_synapses = Generate_Diluted_Weights(action_cells, SA_cells, actiontoSA_dilution, 1);
SAtoaction_synapses = Generate_Diluted_Weights(SA_cells, action_cells, SAtoAction_dilution, 1);

list = ones(numel(world(:,:,1)), numel(action_cells));

% Synapses Before Learning
figure(); imagesc(statetoSA_synapses); colormap(gray); colorbar; title('Sensory => SA Synapses'); xlabel('SA Cells'); ylabel('State (Sensory Cells)');
figure(); imagesc(actiontoSA_synapses); colormap(gray); colorbar; title('Motor => SA Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');
figure(); imagesc(SAtoaction_synapses'); colormap(gray); colorbar; title('SA => Motor Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');


%% RUN TRIAL
% Begin timestep:
disp('Running Timesteps')
disp('...')
for time = 1:steps
    
    fprintf('%d percent', floor((time / steps) * 100))
    disp(' ')
    
    if time == round(steps/4)
        disp('...')
    elseif time == round(steps/2)
        disp('...')
    elseif time == round(3*steps/4)
        disp('...')
    end
    
    noNaNsensorytoSA = statetoSA_synapses;
    noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
    
    noNaNmotortoSA = actiontoSA_synapses;
    noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
    
    noNaNSAtomotor = SAtoaction_synapses;
    noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
    
    % Sensory cells fire according to agent position (1 cell per place).
    state_cells = world(:,:,1);
    if verbose_on == true
        fprintf('State = %d', find(state_cells))
        disp(' ')
    end
    
    % SA cells fire based on current state (sensory input).
    SA_cells = dot((repmat(state_cells(:),[1,numel(SA_cells)])), noNaNsensorytoSA);
    
    
    
    % Competition
    %SA_cells = WTA_Competition(SA_cells);
    if verbose_on == true
        fprintf('SA = %s', num2str(find(SA_cells > 0.1)))
        disp(' ')
    end
    
    % Activate a random motor cell
    actions = {'N' 'E' 'S' 'W'};
    action_cells(:) = 0;
    action_cells(randi(4)) = 1;
    action = actions{find(action_cells)};
    if verbose_on == true
        fprintf('Moved %c (%d)', action, find(action_cells))
        disp(' ')
    end
    
    % SA cells activate based on combined state (original) and action
    % inputs.
    SA_cells = dot((repmat([state_cells(:); action_cells'], [1,numel(SA_cells)])), [noNaNsensorytoSA; noNaNmotortoSA]);
    % TRANSFER FUNCTION
    SA_cells(SA_cells > 0.95 & SA_cells < 1.95) = 0;
    
    % Competition
    SA_cells = WTA_Competition(SA_cells);
    if verbose_on == true
        fprintf('SA = %d', find(SA_cells))
        disp(' ')
    end
    
    % Weights updated.
    statetoSA_synapses = statetoSA_synapses + (learningRate * (state_cells(:) * SA_cells));
    actiontoSA_synapses = actiontoSA_synapses + learningRate * action_cells(:) * SA_cells(:)';
    SAtoaction_synapses = SAtoaction_synapses + learningRate * SA_cells(:) * action_cells(:)';
    
    % Weights normalised
    statetoSA_synapses = normalise(statetoSA_synapses, sensory_normalisation);
    actiontoSA_synapses = normalise(actiontoSA_synapses, motor_normalisation);
    SAtoaction_synapses = normalise(SAtoaction_synapses', motor_normalisation);
    SAtoaction_synapses = SAtoaction_synapses';
    
    
    % RECORD STATE/ACTION COMBINATIONS EXPERIENCED
    SA_info{1} = state_cells;
    SA_info{2} = find(action_cells);
    SA_info{3} = find(SA_cells > 0.1)';
    SA_tracked{time} = SA_info;
    
    
    % Agent moves
    world = update_world4(world, action, 1);
    
    % End timestep:
    world_tracked{time} = world(:,:,1);
    
    if verbose_on == true
        list(find(state_cells), find(action_cells)) = 0;
        disp(' ')
    end
    
end
disp('Complete')

%% ANALYSE RESULTANT MAPPING
% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
disp('Complete')

disp('Calculating state/action mappings.')
% Decode SA information
    SA_decoded = [];
    for time = 1:steps
        SA_tracked{time}{1} = find(SA_tracked{time}{1});
        SA_decoded(time,:) = cell2mat(SA_tracked{time}(:)');
    end
    
    
    % Record every state/action combo.
    %SA_decoded = unique(SA_decoded,'rows');
    %SA_decoded = sortrows(SA_decoded, 3);
    
%HML_mappings(sensorytoSA_synapses, motortoSA_synapses, mapping_threshold);
disp('Complete')

figure(); imagesc(statetoSA_synapses); colormap(gray); colorbar; title('Sensory => SA Synapses'); xlabel('SA Cells'); ylabel('State (Sensory Cells)');
figure(); imagesc(actiontoSA_synapses); colormap(gray); colorbar; title('Motor => SA Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');
figure(); imagesc(SAtoaction_synapses'); colormap(gray); colorbar; title('SA => Motor Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');

end















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end
