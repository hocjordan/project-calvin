function Experiment45(ind_steps, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded, walls)

% uses the SA => SA synapses learned by Experiment49 to produce a line
% attractors (I hope).

% World
worldSize_x = 10;
worldSize_y = 10;
agent_x = 1;
agent_y = 1;
reward_x = 1;
reward_y = 1;

% Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};

% Cells
num_sensory = [worldSize_y worldSize_x];
num_reward = [worldSize_y worldSize_x];
num_motor = [1 size(actions, 2)];
num_SA = [1 round(1.2 * prod(num_motor)*prod(num_sensory))];
agent_firing = 2;
reward_firing = 0.1;

% Competition
state_modifier = 0.2; % 0.2
sparseness = 99;
slope = 90000000000000000000;

% Noise
SA_noise = 0.000;

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;


%% Setup
% Create World
world = create_world3(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Create cells
sensory_cells = zeros(num_sensory);
reward_cells = zeros(num_reward);
SA_cells = zeros(num_SA);
trace = zeros(num_SA);

%% Walls.

% Add Walls to World
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

% place agent
fig = figure(); imagesc(world(:,:,3)); title('Please select starting position.');
[x, y] = ginput(1);
x=round(x); y=round(y);
agent_x = x; agent_y = y;
%synapses_summed(y,x) = 0;
close(fig)

% place reward
fig = figure(); imagesc(world(:,:,3)); title('Please select a goal.')
[x, y] = ginput(1);
x=round(x); y=round(y);
reward_x = x; reward_y = y;
close(fig)

world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

world(:,:,3) = zeros(size(world(:,:,2)));
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;


%% RUN TRIAL

% Allow activity to spread for 1:ind_time
for ind_time = 1:ind_steps
    
    if ind_time == round(ind_steps/4)
        disp('...')
    elseif ind_time == round(ind_steps/2)
        disp('...')
    elseif ind_time == round(3*ind_steps/4)
        disp('...')
    end
    
    noNaNsensorytoSA = sensorytoSA_synapses;
    noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
    
    noNaNmotortoSA = motortoSA_synapses;
    noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
    
    noNaNSAtomotor = SAtomotor_synapses;
    noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
    
    noNaNSAtoSA= SAtoSA_synapses;
    noNaNSAtoSA(isnan(noNaNSAtoSA)) = 0;
    
    
    %% Activate SA cells based on state of agent and the rewarded state
    % Sensory cells fire based on current position.
    
    sensory_cells(find(world(:,:,1))) = agent_firing;
    reward_cells(find(world(:,:,2))) = reward_firing;
    
    
    
    % SA cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
    SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
    SA_cells = reshape(SA_cells,num_SA);
    
    
    % Competition in state cells:
    
    % subtractive.
    SA_cells = SA_cells - state_modifier * mean(mean(SA_cells));
    
    % divisive.
    SA_cells = SA_cells/max(max(SA_cells));
    SA_cells(SA_cells < 0) = 0;
    
    SA_cells = reshape(SA_cells,num_SA);
    
    
    % add noise
    SA_cells = SA_cells(:)' + SA_noise .* std(SA_cells(:)) * randn(1,size(SA_cells(:),1));
    SA_cells(SA_cells < 0) = 0;
    SA_cells = reshape(SA_cells,num_SA);
    
    % Calculate activation to motor cells
    
    
    % Move agent.
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
end

SA_cells

figure(); imagesc(reshape(statetostate_synapses(sub2ind(num_state,agent_y,agent_x),:), [20 20])); colorbar; title('Synapses of State Cell at Agent Position')

SA_cells = dot(repmat(SA_cells(:),[1,numel(SA_cells)]), noNaNstatetostate);
SA_cells = reshape(SA_cells,num_state);
figure(); imagesc(SA_cells); colorbar

% Move agent toward greatest activity


% Check for reward.


end


















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end
