%% Trace Experiment

function [statetostate_synapses] = Experiment31(steps, ind_steps, dilution)

% Will use the trace learning mechanisms from Experiment 31 to learn a
% single room of the style used in Experiments 22 - 29, forming reciprocal
% pseudo-Gaussian synapses.

%% Parameters

% trial
eta = 0.9;
learningRate = 0.0001;
worldSize_x = 10;
worldSize_y = 10;
agent_x = 5;
agent_y = 5;
reward_x = 7;
reward_y = 7;

% World

% Cells
num_state = [worldSize_y worldSize_x];
agent_firing = 2;

% Synapses
normalisation_threshold = 0.01;

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Analysis
%% Setup
% Create World
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Create grid of place cells
state_cells = zeros(num_state);
trace = zeros(num_state);

% Create synapses with full connectivity, save for self-self
statetostate_synapses = GenerateZeroWeights(numel(state_cells), numel(state_cells), dilution);

%% Walls.

rows = size(state_cells,1);
col = size(state_cells,2);

top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

% walls have no presynaptic connections
statetostate_synapses(top_wall,:) = NaN;
statetostate_synapses(bottom_wall,:) = NaN;
statetostate_synapses(left_wall,:) = NaN;
statetostate_synapses(right_wall,:) = NaN;

% walls have no postsynaptic connections
statetostate_synapses(:, top_wall) = NaN;
statetostate_synapses(:, bottom_wall) = NaN;
statetostate_synapses(:, left_wall) = NaN;
statetostate_synapses(:, right_wall) = NaN;

%% RUN TRIAL

% Begin timestep:
state_cells(agent_y, agent_x) = agent_firing;
initial = find(state_cells);

for time = 1:steps
    
    if time == round(steps/4)
        disp('...')
    elseif time == round(steps/2)
        disp('...')
    elseif time == round(3*steps/4)
        disp('...')
    end
    
    % Update state cells
    state_cells(agent_y, agent_x) = agent_firing;
    
    % Calculate the trace value for all cells
    trace = getTrace(state_cells, trace, eta);
    
    % Update synapses using lr * state firing * trace rule
    statetostate_synapses = statetostate_synapses+(learningRate * state_cells(:) * trace(:)');
    
    % Update synapses using lr * trace * state firing
    statetostate_synapses = statetostate_synapses+(learningRate * trace(:) * state_cells(:)');
    
    % Block off self-self synapses
    statetostate_synapses(logical(eye(size(statetostate_synapses)))) = NaN;
    
    % Normalise synapses
    statetostate_synapses = normalise(statetostate_synapses, normalisation_threshold);
    
    % Record agent position
    world_tracked{time} = state_cells;
    
    % End timestep. Repeat.
    if time == steps
        final = find(state_cells);
    else
        % Activate one cell
        actions = {'NW' 'W' 'SW' 'N' 'S' 'NE' 'E' 'SE'};
        action = actions{randi(8)};
        %disp(action)
        world = update_world3(world, action, 0);
    end
    
    % Update parameters
    state_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
end

% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
disp('Complete')

%% BACKWARD INDUCTION
%{
% set reward representation at final position.
state_cells(:) = 0;
state_cells(final) = 1;

% feed current to final position and see how it affects the other cells in
% the network.
noNaNstatetotstate=statetostate_synapses;
noNaNstatetotstate(isnan(noNaNstatetotstate)) = 0;
activation = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNstatetotstate); activation(final) = 1
for time = 1:ind_steps
    activation = dot(repmat(activation(:),[1,numel(state_cells)]), noNaNstatetotstate); activation(final) = 1;
    %activation = softCompetition(49,90,activation);
    activation_display = reshape(activation,[10,10]);
    activation_tracked{time} = activation_display;
end

% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
%}

%% Forward Induction
%{
% set reward representation at initial position.
state_cells(:) = 0;
state_cells(initial) = 1;

% feed current to initial position and see how it affects the other cells in
% the network.
noNaNstatetotstate=statetostate_synapses;
noNaNstatetotstate(isnan(noNaNstatetotstate)) = 0;
activation = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNstatetotstate); activation(initial) = 1;
for time = 1:ind_steps
    activation = dot(repmat(activation(:),[1,numel(state_cells)]), noNaNstatetotstate); activation(initial) = 1;
    %activation = softCompetition(49,90,activation);
    activation_display = reshape(activation,[10,10]);
    activation_tracked{time} = activation_display;
end

% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);

% Display synapses:
%figure(); imagesc(reshape(synapses(41,:), [10 10])); colorbar;
%}
end


















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
    %if summed(column) > threshold
        %divide each row in that column by that sum

        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end