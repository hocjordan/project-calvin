function world_tracked = InputSpace_Calvin(trials, worldSize)

world_combined = zeros(worldSize, worldSize, trials);
correct = 0;
incorrect = 0;


for trial = 1:trials
    
    disp(trial)
    
    agent_x = randi(worldSize);
    agent_y = randi(worldSize);
    
    reward_x = randi(worldSize);
    reward_y = randi(worldSize);
    
    try
        [world_display, result] = Experiment12(30, 2000, worldSize, agent_x, agent_y, reward_x, reward_y);
    catch err
        disp(err.message)
        result = 'X';
        world_display = zeros(worldSize, worldSize, 2);
    end
    
    if result == 'Y'
        correct = correct + 1;
    elseif result == 'N'
        incorrect = incorrect + 1;
    end
    
    disp(result)
        
        world_tracked{trial} = world_display(:,:,1) + world_display(:,:,2);
        %world_combined(:,:,trial) = world_tracked{trial};

end
    
    slider_display(world_tracked, [])
    
    disp(correct)
    disp(incorrect)

end