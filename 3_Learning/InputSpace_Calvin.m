function [activation_alltrials, world_tracked] = InputSpace_Calvin(Experiment, trials, worldSize)

correct = 0;
incorrect = 0;
activation_alltrials = zeros(worldSize, worldSize);

for trial = 1:trials
    
    disp(trial)
    
    agent_x = randi(worldSize);
    agent_x = randi(10);
    agent_y = randi(worldSize);
    agent_y = randi(10);
    
    reward_x = randi(worldSize);
    reward_x = 12;
    
    reward_y = randi(worldSize);
    reward_y = 5;
    
    try
        [activation_combined, world_display, result] = Experiment(40, 1000, worldSize, agent_x, agent_y, reward_x, reward_y);
    catch err
        disp(err.message)
        result = 'X';
        world_display = zeros(worldSize, worldSize, 2);
        continue
    end
    
    if result == 'Y'
        correct = correct + 1;
    elseif result == 'N'
        incorrect = incorrect + 1;
    end
    
    disp(result)
    
    world_tracked{trial} = world_display(:,:,1) + world_display(:,:,2);
    activation_combined = activation_combined/max(max(activation_combined));
    activation_alltrials = activation_alltrials + activation_combined;
    
end
    
    slider_display(world_tracked, [])
    figure(); imagesc(activation_alltrials);
    
    
    disp(correct)
    disp(incorrect)

end