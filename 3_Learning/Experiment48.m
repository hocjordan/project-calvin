%% Trace Experiment

function Experiment48(steps, ind_steps, statetostate_synapses, walls)

% As Experiment47, but with a basic nociceptive system added.

%% Parameters

% trial
worldSize_x = 20;
worldSize_y = 20;
agent_x = 13;
agent_y = 18;
reward_x = 13;
reward_y = 3;

% World

% Cells
num_state = [worldSize_y worldSize_x];
agent_firing = 0;
reward_firing = 2;

% Synapses
normalisation_threshold = 4;
learningRate = 1;
eta = 0.9;

% Competition
state_modifier = 0.012; %0.05; % 0.2

% Noise
state_noise = 0.02;

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;
experience_modifier = 0.85;

% Analysis
activation_tracked = {};
%% Setup
% Create World
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Create grid of place cells
state_cells = zeros(num_state);
trace = zeros(num_state);


%% Walls.
%{

% Maze walls
%
% H-Maze
wall_a = left_wall + 40;                                            walls = [walls wall_a];
wall_b = left_wall + 120; wall_b = [wall_b(1:4), wall_b(8:20)];     walls = [walls wall_b];
wall_c = left_wall + 200; wall_c = [wall_c(1:10), wall_c(14:20)];   walls = [walls wall_c];
wall_d = left_wall + 280;                                           walls = [walls wall_d];
%}

% Circular Maze

%{
wall_a = left_wall + 20;                                            walls = [walls wall_a];
wall_b = left_wall + 140; wall_b([3:7 14:18]) = [];                 walls = [walls wall_b];
wall_c = right_wall - 40;                                           walls = [walls wall_c];
wall_d = right_wall - 140; wall_d([3:7 14:18]) = [];                walls = [walls wall_d];
wall_e = top_wall + 1;                                              walls = [walls wall_e];
wall_f = top_wall + 7; wall_f([3:7 14:18]) = [];                    walls = [walls wall_f];
wall_g = bottom_wall - 1;                                           walls = [walls wall_g];
wall_h = bottom_wall - 7; wall_h([3:7 14:18]) = [];                 walls = [walls wall_h];
%}





% walls are represented in the world
%
world(:,:,3) = zeros(size(world(:,:,2)));
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

% user interface to update maze
user_walls = figure(); imagesc(world(:,:,3));
while ishandle(user_walls) == 1;
    try
        [x, y] = ginput(1);
        x=round(x); y=round(y);
        if world(y,x,3) == 3;
            world(y,x,3) = 0;
        else
            world(y,x,3) = 3;
        end
        imagesc(world(:,:,3));
    end    
end

walls = find(world(:,:,3) == 3);

% place agent
fig = figure(); imagesc(world(:,:,3)); title('Please select starting position.');
[x, y] = ginput(1);
x=round(x); y=round(y);
agent_x = x; agent_y = y;
%synapses_summed(y,x) = 0;
close(fig)

% place reward
fig = figure(); imagesc(world(:,:,3)); title('Please select a goal.')
[x, y] = ginput(1);
x=round(x); y=round(y);
reward_x = x; reward_y = y;
close(fig)

world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

world(:,:,3) = zeros(size(world(:,:,2)));
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;



% world has another layer for known walls
world(:,:,4) = zeros(size(world(:,:,3)));

%}
%% RUN TRIAL
% For each step:

for time = 1:steps
    
    % Allow activity to spread for 1:ind_time
    for ind_time = 1:ind_steps
        %{
    if ind_time == round(ind_steps/4)
        disp('...')
    elseif ind_time == round(ind_steps/2)
        disp('...')
    elseif ind_time == round(3*ind_steps/4)
        disp('...')
    end
        %}
        
        noNaNstatetostate = statetostate_synapses;
noNaNstatetostate(isnan(noNaNstatetostate)) = 0;

        % Set firing rate of agent & reward representations.
        state_cells(reward_y, reward_x) = reward_firing; %state_cells(agent_y, agent_x) = agent_firing;
        %state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        % Calculate activation of state cells from the recurrent
        % connections within that layer, and save it.
        state_cells = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNstatetostate);
        state_cells = reshape(state_cells,num_state);
        state_cells(reward_y, reward_x) = reward_firing; %state_cells(agent_y, agent_x) = agent_firing;
        %state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        % Competition in state cells:
        
        state_cells(reward_y, reward_x) = reward_firing; %state_cells(agent_y, agent_x) = agent_firing;
        %state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        % subtractive.
        state_cells = state_cells - state_modifier * mean(mean(state_cells));
        
        % divisive.
        state_cells = state_cells/max(max(state_cells));
        state_cells(state_cells < 0) = 0;
        
        state_cells = reshape(state_cells,num_state);
        
        state_cells(reward_y, reward_x) = reward_firing; %state_cells(agent_y, agent_x) = agent_firing;
        %state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        % add noise
        state_cells = state_cells(:)' + state_noise .* std(state_cells(:)) * randn(1,size(state_cells(:),1));
        state_cells(state_cells < 0) = 0;
        state_cells = reshape(state_cells,num_state);
        
        % Calculate the trace value for all cells
        trace = getTrace(state_cells, trace, eta);
        
    end
    
    % Save activation
    activation_display = state_cells;
    activation_display(agent_y, agent_x) = 0;
    activation_display(reward_y, reward_x) = 0;
    activation_display(find(world(:,:,4))) = 0;
    %fig = figure(); imagesc(activation_display); print(fig, '-dpng', fullfile('/Users/Shared/MATLAB/MATLAB/Simulations/H-Maze/Blocked R1R2', sprintf('%s_T%d', inputname(3), time))); close(fig)
    activation_tracked{time} = activation_display;
    
    %% Check for Completion
    current_state = sub2ind(size(state_cells), agent_y, agent_x);
    reward_location = sub2ind([worldSize_x worldSize_y], reward_y, reward_x);
    if current_state == reward_location
        result = 'Y';
        disp(time)
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Move Agent
    
    valid_movement_selected = 0; count = 0;
    %while valid_movement_selected == 0 && count < 500
        count = count+1;
        % find the most active state cell(s) next to the current position
        rows = size(state_cells,1);
        col = size(state_cells,2);
        
        current_state = sub2ind(size(state_cells), agent_y, agent_x);
        
        w = current_state-rows; n = current_state-1; s = current_state+1; e = current_state+rows;
        nw = w-1; sw = w+1; ne = e-1; se = e+1;
        
        test = [n e s w nw ne se sw];
        idx = find(test <= 0);
        test(idx) = test(idx) + rows*col;
        idx = find(test > rows*col);
        test(idx) = test(idx) - rows*col;
        
        n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
        
        neighbours = zeros(3);
        
        state_cells_copy = state_cells;
        if any(any(world(:,:,4))) == 1
        %state_cells_copy(find(world(:,:,4))) = 0;
        end
        neighbours(1) = state_cells_copy(nw);
        neighbours(2) = state_cells_copy(w);
        neighbours(3) = state_cells_copy(sw);
        neighbours(4) = state_cells_copy(n);
        neighbours(5) = 0;
        neighbours(6) = state_cells_copy(s);
        neighbours(7) = state_cells_copy(ne);
        neighbours(8) = state_cells_copy(e);
        neighbours(9) = state_cells_copy(se);
        
        %neighbours = round(neighbours * 100)/100;
        
        next_turn = find(neighbours == max(max(neighbours)));
        if size(next_turn,1) > 1
            disp('RANDOM ACTION SELECTED')
        end
        
        % Activate one cell
        
        actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};
        action = actions{next_turn(randi(size(next_turn,1)))};
        fprintf('%d: %s', time, action)
        disp(' ')
        world_saved = world(:,:,4);
        [world, valid_movement_selected, intended_outcome] = update_world5(world, action, 0);
        world(:,:,4) = world_saved;
        if valid_movement_selected == 0
            
            world(find(intended_outcome) + numel(world(:,:,1:3))) = 4;
        
            % Synapse Depression of Failed Choices
            
            statetostate_synapses(find(intended_outcome), :) = 0.95*(statetostate_synapses(find(intended_outcome), :));
            statetostate_synapses(:,find(intended_outcome)) = 0.85*(statetostate_synapses(:,find(intended_outcome)));
            
            %{
            wall_cells = find(world(:,:,4));
            for elm = 1:numel(wall_cells)
                if nansum(statetostate_synapses(wall_cells(elm), :)) > 0
                    statetostate_synapses(wall_cells(elm),:) = 0.95*(statetostate_synapses(wall_cells(elm), :));
                end
            end
            %}
            % Update synapses using -lr * state firing * trace rule
            %statetostate_synapses = statetostate_synapses+((-learningRate) * intended_outcome(:) * state_cells(:)');
            
        % Update synapses using -lr * trace * state firing
        %statetostate_synapses = statetostate_synapses+((-learningRate) * state_cells(:) * intended_outcome(:)');
        
        %statetostate_synapses(statetostate_synapses < 0) = 0;
        
        %statetostate_synapses = normalise(statetostate_synapses, normalisation_threshold);
        end
    
    %% Update Parameters
    
    state_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

%% Display Activation
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
end


















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) > 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end