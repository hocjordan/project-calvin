%% Trace Experiment

function [statetostate_synapses, walls] = Experiment39(steps)

% As Experiments 32, 33, 35 but with (slightly) more intelligent
% exploration -- looks for the least explored square next to it.

%% Parameters

% trial
eta = 0.9;
learningRate = 0.1; % 0.0001
worldSize_x = 20;
worldSize_y = 20;
agent_x = 4;
agent_y = 2;
reward_x = 7;
reward_y = 7;

% World

% Cells
num_state = [worldSize_y worldSize_x];
agent_firing = 2;

% Noise
state_noise = 0.002;

% Synapses
normalisation_threshold = 4; %0.01

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Analysis

%% Setup
% Create World
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);
world_summed = zeros(size(world(:,:,1)));

% Create grid of place cells
state_cells = zeros(num_state);
trace = zeros(num_state);

% Create synapses with full connectivity, save for self-self
%statetostate_synapses = Generate_Diluted_Weights(state_cells, state_cells, 1, 0.000001);
statetostate_synapses = GenerateZeroWeights(numel(state_cells), numel(state_cells), 1);

%% Walls.

world(:,:,3) = zeros(size(world(:,:,2)));

rows = size(world,1);
col = size(world,2);

walls = [];

top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

walls = [walls top_wall];

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

walls = [walls bottom_wall];

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

walls = [walls left_wall];
walls = [walls right_wall];

% Maze walls
%
% H-Maze
%{
wall_a = left_wall + 40;                                            walls = [walls wall_a];
wall_b = left_wall + 120; wall_b = [wall_b(1:4), wall_b(8:20)];    walls = [walls wall_b];
wall_c = left_wall + 200; wall_c = [wall_c(1:10), wall_c(14:20)];   walls = [walls wall_c];
wall_d = left_wall + 280;                                           walls = [walls wall_d];
%}

% Circular Maze

%{
wall_a = left_wall + 20;                                            walls = [walls wall_a];
wall_b = left_wall + 140; wall_b([3:7 14:18]) = [];                 walls = [walls wall_b];
wall_c = right_wall - 40;                                           walls = [walls wall_c];
wall_d = right_wall - 140; wall_d([3:7 14:18]) = [];                walls = [walls wall_d];
wall_e = top_wall + 1;                                              walls = [walls wall_e];
wall_f = top_wall + 7; wall_f([3:7 14:18]) = [];                    walls = [walls wall_f];
wall_g = bottom_wall - 1;                                           walls = [walls wall_g];
wall_h = bottom_wall - 7; wall_h([3:7 14:18]) = [];                 walls = [walls wall_h];
%}

% walls are represented in the world
%
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

walls = find(world(:,:,3) == 3);



%% RUN TRIAL

% Begin timestep:
state_cells(agent_y, agent_x) = agent_firing;
initial = find(state_cells);

for time = 1:steps
    
    if time == round(steps/10)
        disp('1/10')
    elseif time == round(2*steps/10)
        disp('2/10')
    elseif time == round(3*steps/10)
        disp('3/10')
    elseif time == round(4*steps/10)
        disp('4/10')
    elseif time == round(5*steps/10)
        disp('5/10')
    elseif time == round(6*steps/10)
        disp('6/10')
    elseif time == round(7*steps/10)
        disp('7/10')
    elseif time == round(8*steps/10)
        disp('8/10')
    elseif time == round(9*steps/10)
        disp('9/10')
    elseif time == round(10*steps/10)
        disp('10/10')
        
    end
    
    % Update state cells
    state_cells(agent_y, agent_x) = agent_firing;
    
    % Calculate the trace value for all cells
    trace = getTrace(state_cells, trace, eta);
    
    % Update synapses using lr * state firing * trace rule
    statetostate_synapses = statetostate_synapses+(learningRate * state_cells(:) * trace(:)');
    
    % Update synapses using lr * trace * state firing
    statetostate_synapses = statetostate_synapses+(learningRate * trace(:) * state_cells(:)');
    
    % Block off self-self synapses
    statetostate_synapses(logical(eye(size(statetostate_synapses)))) = NaN;
    
    % Normalise synapses
    statetostate_synapses = normalise(statetostate_synapses, normalisation_threshold);
    
    % Record agent position
    world_tracked{time} = state_cells;
    world_summed = world_summed + world_tracked{time};
    
    % End timestep. Repeat.
    if time == steps
        final = find(state_cells);
    else
        % Select an action:
        valid_movement_selected = 0;
        world_summed_copy = world_summed;
        world_summed_copy(world(:,:,3) == 3) = NaN;
        
        while valid_movement_selected == 0
            
            
            % find the least visited square next to the current position
            rows = size(world_summed_copy,1);
            col = size(world_summed_copy,2);
            
            current_state = sub2ind(size(world_summed), agent_y, agent_x);
            
            w = current_state-rows; n = current_state-1; s = current_state+1; e = current_state+rows;
            nw = w-1; sw = w+1; ne = e-1; se = e+1;
            
            test = [n e s w nw ne se sw];
            idx = find(test <= 0);
            test(idx) = test(idx) + rows*col;
            idx = find(test > rows*col);
            test(idx) = test(idx) - rows*col;
            
            n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
            
            neighbours = zeros(3);
            
            neighbours(1) = world_summed_copy(nw);
            neighbours(2) = world_summed_copy(w);
            neighbours(3) = world_summed_copy(sw);
            neighbours(4) = world_summed_copy(n);
            neighbours(5) = NaN;
            neighbours(6) = world_summed_copy(s);
            neighbours(7) = world_summed_copy(ne);
            neighbours(8) = world_summed_copy(e);
            neighbours(9) = world_summed_copy(se);
            
            
            next_turn = find(neighbours == min(min(neighbours)));
            if size(next_turn,1) > 1
                %disp('RANDOM ACTION SELECTED')
            end
            
            actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};
            action = actions{next_turn(randi(size(next_turn,1)))};
            %disp(action)
            [world, valid_movement_selected] = update_world4(world, action, 0);
            if valid_movement_selected == 1
                break
            else
                % add noise
                world_summed_copy = world_summed_copy(:)' + 0.001 .* std(world_summed_copy(:)) * randn(1,size(world_summed_copy(:),1));
                world_summed_copy(world_summed_copy < 0) = 0;
                world_summed_copy = reshape(world_summed_copy,num_state);
                disp('Noise added.')
            end
        end
    end
    
    % Update parameters
    state_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
end

% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);


for count = 1:numel(world_tracked)
    
end
figure(); imagesc(world_summed); title('Sum of Time Spent in Each Location')
disp('Complete')

%% BACKWARD INDUCTION
%{
% set reward representation at final position.
state_cells(:) = 0;
state_cells(final) = 1;

% feed current to final position and see how it affects the other cells in
% the network.
noNaNstatetotstate=statetostate_synapses;
noNaNstatetotstate(isnan(noNaNstatetotstate)) = 0;
activation = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNstatetotstate); activation(final) = 1
for time = 1:ind_steps
    activation = dot(repmat(activation(:),[1,numel(state_cells)]), noNaNstatetotstate); activation(final) = 1;
    %activation = softCompetition(49,90,activation);
    activation_display = reshape(activation,[10,10]);
    activation_tracked{time} = activation_display;
end

% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
%}

%% Forward Induction
%{
% set reward representation at initial position.
state_cells(:) = 0;
state_cells(initial) = 1;

% feed current to initial position and see how it affects the other cells in
% the network.
noNaNstatetotstate=statetostate_synapses;
noNaNstatetotstate(isnan(noNaNstatetotstate)) = 0;
activation = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNstatetotstate); activation(initial) = 1;
for time = 1:ind_steps
    activation = dot(repmat(activation(:),[1,numel(state_cells)]), noNaNstatetotstate); activation(initial) = 1;
    %activation = softCompetition(49,90,activation);
    activation_display = reshape(activation,[10,10]);
    activation_tracked{time} = activation_display;
end

% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);

% Display synapses:
%figure(); imagesc(reshape(synapses(41,:), [10 10])); colorbar;
%}
end


















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end