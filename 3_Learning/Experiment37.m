
function Experiment37(steps, ind_steps, dilution)

% Using learned synapses, sends out a pulse that activates a
% self-sustaining reward, then forms a line attractor and moves toward that
% reward.

%% Parameters

% trial
eta = 0.9;
learningRate = 0.0001; % 0.00001
worldSize_x = 20;
worldSize_y = 20;
agent_x = 10;
agent_y = 4;
reward_x = 7;
reward_y = 17;

% World

% Cells
num_state = [worldSize_y worldSize_x];
agent_firing = 0.5;
reward_firing = 2;

% Synapses
normalisation_threshold = 0.01;

% Competition
state_modifier = 0.2; % 0.2

% Noise
state_noise = 0.001;

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Analysis
activation_tracked = {};

%% Walls
rows = size(state_cells,1);
col = size(state_cells,2);

top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

% Maze walls

% H-Maze
%{
wall_a = left_wall + 40;
wall_b = left_wall + 100; wall_b = [wall_b(1:9), wall_b(12:20)];
wall_c = left_wall + 160;
%}

% Circular Maze
wall_a = left_wall + 20;
wall_b = left_wall + 140; wall_b([3:7 14:18]) = [];
wall_c = right_wall - 40;
wall_d = right_wall - 140; wall_d([3:7 14:18]) = [];
wall_e = top_wall + 1;
wall_f = top_wall + 7; wall_f([3:7 14:18]) = [];
wall_g = bottom_wall - 1;
wall_h = bottom_wall - 7; wall_h([3:7 14:18]) = [];

% walls are represented in the world
%
world(:,:,3) = zeros(size(world(:,:,2)));
world(wall_a + numel(world(:,:,1:2))) = 3;
world(wall_b + numel(world(:,:,1:2))) = 3;
world(wall_c + numel(world(:,:,1:2))) = 3;
world(wall_d + numel(world(:,:,1:2))) = 3;
world(wall_e + numel(world(:,:,1:2))) = 3;
world(wall_f + numel(world(:,:,1:2))) = 3;
world(wall_g + numel(world(:,:,1:2))) = 3;
world(wall_h + numel(world(:,:,1:2))) = 3;
%}

%% Run Trials
for time = 1:steps
    %% Induction
    
    % Send out a pulse every 10 timesteps that can activate a reward.
    if mod(time, 10) == 0
        state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
    end
    
    % Allow activity to spread:
    for ind_time = 1:ind_steps
        
        % Set firing rate of agent & reward representations.
        state_cells(reward_y, reward_x) = reward_firing;
        
        noNaNstatetostate = statetostate_synapses;
        noNaNstatetostate(isnan(noNaNstatetostate)) = 0;
        
        % Calculate activation of state cells from the recurrent
        % connections within that layer, and save it.
        state_cells = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNstatetostate);
        state_cells = reshape(state_cells,num_state);
        
        if mod(time, 10) == 0
        state_cells(reward_y, reward_x) = reward_firing;
        end
        
        % Competition in state cells:
        
        % subtractive.
        state_cells = state_cells - state_modifier * mean(mean(state_cells));
        
        % divisive.
        state_cells = state_cells/max(max(state_cells));
        state_cells(state_cells < 0) = 0;
        
        state_cells = reshape(state_cells,num_state);
        
        if mod(time, 10) == 0
        state_cells(reward_y, reward_x) = reward_firing;
        end
        
        % add noise
        state_cells = state_cells(:)' + state_noise .* std(state_cells(:)) * randn(1,size(state_cells(:),1));
        state_cells(state_cells < 0) = 0;
        state_cells = reshape(state_cells,num_state);
        
        if mod(time, 10) == 0
        state_cells(reward_y, reward_x) = reward_firing;
        end
        
    end
    
    % Save activation
    activation_display = state_cells;
    activation_display(agent_y, agent_x) = 0;
    activation_display(reward_y, reward_x) = 0;
    activation_tracked{time} = activation_display;
    
    %% Check for Completion
    current_state = sub2ind(size(state_cells), agent_y, agent_x);
    reward_location = sub2ind([worldSize_x worldSize_y], reward_y, reward_x);
    if current_state == reward_location
        result = 'Y';
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Move Agent
    
    valid_movement_selected = 0; count = 0;
    while valid_movement_selected == 0 && count < 500
        count = count+1;
        % find the most active state cell(s) next to the current position
        rows = size(state_cells,1);
        col = size(state_cells,2);
        
        current_state = sub2ind(size(state_cells), agent_y, agent_x);
        
        w = current_state-rows; n = current_state-1; s = current_state+1; e = current_state+rows;
        nw = w-1; sw = w+1; ne = e-1; se = e+1;
        
        test = [n e s w nw ne se sw];
        idx = find(test <= 0);
        test(idx) = test(idx) + rows*col;
        idx = find(test > rows*col);
        test(idx) = test(idx) - rows*col;
        
        n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
        
        neighbours = zeros(3);
        
        neighbours(1) = state_cells(nw);
        neighbours(2) = state_cells(w);
        neighbours(3) = state_cells(sw);
        neighbours(4) = state_cells(n);
        neighbours(5) = 0;
        neighbours(6) = state_cells(s);
        neighbours(7) = state_cells(ne);
        neighbours(8) = state_cells(e);
        neighbours(9) = state_cells(se);
        
        %neighbours = round(neighbours * 100)/100;
        
        next_turn = find(neighbours == max(max(neighbours)));
        if size(next_turn,1) > 1
            disp('RANDOM ACTION SELECTED')
        end
        
        % Activate one cell
        
        actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};
        action = actions{next_turn(randi(size(next_turn,1)))};
        disp(action)
        [world, valid_movement_selected] = update_world4(world, action, 0);
        if valid_movement_selected == 1
            break
        end
    end
    
    
    %% Learn Trace
    
    % Calculate the trace value for all cells
    trace = getTrace(state_cells, trace, eta);
    
    % Update synapses using lr * state firing * trace rule
    statetostate_synapses = statetostate_synapses+(learningRate * state_cells(:) * trace(:)');
    
    % Update synapses using lr * trace * state firing
    statetostate_synapses = statetostate_synapses+(learningRate * trace(:) * state_cells(:)');
    
    % Block off self-self synapses
    statetostate_synapses(logical(eye(size(statetostate_synapses)))) = NaN;
    
    % Normalise synapses
    statetostate_synapses = normalise(statetostate_synapses, normalisation_threshold);
    
    % Record agent position
    world_tracked{time} = state_cells;
    
    
    
end
end







function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end