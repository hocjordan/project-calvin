%% Trace Experiment

function Experiment33(steps, ind_steps, dilution, statetostate_synapses)

% Will use the learned place (state) cells produced by Experiment 32 to
% form a linear attractor between an agent and reward.

%% Parameters

% trial
eta = 0.9;
learningRate = 0.000001;
worldSize_x = 20;
worldSize_y = 20;
agent_x = 7;
agent_y = 17;
reward_x = 9;
reward_y = 17;

% World

% Cells
num_state = [worldSize_y worldSize_x];
agent_firing = 2;
reward_firing = 0.5;

% Synapses
normalisation_threshold = 0.01;

% Competition
state_modifier = 0.2; % 0.2

% Noise
state_noise = 0.000;

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Analysis
%% Setup
% Create World
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Create grid of place cells
state_cells = zeros(num_state);
trace = zeros(num_state);

% Create synapses with full connectivity, save for self-self
%statetostate_synapses = GenerateZeroWeights(numel(state_cells), numel(state_cells), dilution);

%% Walls.

rows = size(state_cells,1);
col = size(state_cells,2);

top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

% Maze walls
%{
% H-Maze
wall_a = left_wall + 40;
wall_b = left_wall + 100; wall_b = [wall_b(1:9), wall_b(12:20)];
wall_c = left_wall + 160;
%}

% Circular Maze
wall_a = left_wall + 40;
wall_b = left_wall + 100; wall_b([4 5 16 17]) = [];
wall_c = right_wall - 40;
wall_d = right_wall - 100; wall_d([4 5 16 17]) = [];
wall_e = top_wall + 2;
wall_f = top_wall + 5; wall_f([4 5 16 17]) = [];
wall_g = bottom_wall - 2;
wall_h = bottom_wall - 5; wall_h([4 5 16 17]) = [];


% walls have no presynaptic connections
statetostate_synapses(top_wall,:) = NaN;
statetostate_synapses(bottom_wall,:) = NaN;
statetostate_synapses(left_wall,:) = NaN;
statetostate_synapses(right_wall,:) = NaN;

statetostate_synapses(wall_a,:) = NaN;
statetostate_synapses(wall_b,:) = NaN;
statetostate_synapses(wall_c,:) = NaN;

% walls have no postsynaptic connections
statetostate_synapses(:, top_wall) = NaN;
statetostate_synapses(:, bottom_wall) = NaN;
statetostate_synapses(:, left_wall) = NaN;
statetostate_synapses(:, right_wall) = NaN;

statetostate_synapses(:,wall_a) = NaN;
statetostate_synapses(:,wall_b) = NaN;
statetostate_synapses(:,wall_c) = NaN;

% walls are represented in the world
%
world(:,:,3) = zeros(size(world(:,:,2)));
ind_y = []; ind_x = [];
rows = size(world,1); col = size(world,2);
[y, x] = ind2sub([rows, col, 3], bottom_wall); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
[y, x] = ind2sub([rows, col, 3], top_wall); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
[y, x] = ind2sub([rows, col, 3], left_wall); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
[y, x] = ind2sub([rows, col, 3], right_wall); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
[y, x] = ind2sub([rows, col, 3], wall_a); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
[y, x] = ind2sub([rows, col, 3], wall_b); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
[y, x] = ind2sub([rows, col, 3], wall_c); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
[y, x] = ind2sub([rows, col, 3], wall_d); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
[y, x] = ind2sub([rows, col, 3], wall_e); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
[y, x] = ind2sub([rows, col, 3], wall_f); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
[y, x] = ind2sub([rows, col, 3], wall_g); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
[y, x] = ind2sub([rows, col, 3], wall_h); ind_y = [ind_y, y]; ind_x = [ind_x, x]; world(ind_y,ind_x,3) = 3; ind_y = []; ind_x = [];
%}

%% RUN TRIAL
        
        % Allow activity to spread for 1:ind_time
        for ind_time = 1:ind_steps
            
            if ind_time == round(ind_steps/4)
                disp('...')
            elseif ind_time == round(ind_steps/2)
                disp('...')
            elseif ind_time == round(3*ind_steps/4)
                disp('...')
            end
           
            % Set firing rate of agent & reward representations.
            state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
            
            noNaNstatetostate = statetostate_synapses;
            noNaNstatetostate(isnan(noNaNstatetostate)) = 0;

            % Calculate activation of state cells from the recurrent
            % connections within that layer, and save it.            
            state_cells = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNstatetostate);
            state_cells = reshape(state_cells,num_state);
            state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
            
            % Competition in state cells:
            
            state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
            
            % subtractive.
            state_cells = state_cells - state_modifier * mean(mean(state_cells));
            
            % divisive.
            state_cells = state_cells/max(max(state_cells));
            state_cells(state_cells < 0) = 0;
            
            state_cells = reshape(state_cells,num_state);
            
            state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
            
            % add noise
            state_cells = state_cells(:)' + state_noise .* std(state_cells(:)) * randn(1,size(state_cells(:),1));
            state_cells(state_cells < 0) = 0;
            state_cells = reshape(state_cells,num_state);
            
        end
        
        state_cells
        
        figure(); imagesc(reshape(statetostate_synapses(sub2ind(num_state,agent_y,agent_x),:), [20 20])); colorbar; title('Synapses of State Cell at Agent Position')
        
        state_cells = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNstatetostate);
            state_cells = reshape(state_cells,num_state);
        figure(); imagesc(state_cells); colorbar

% Move agent toward greatest activity


% Check for reward.


end


















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
    %if summed(column) > threshold
        %divide each row in that column by that sum

        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end