function [SAtoSA_synapses, SA_decoded] = Experiment44(steps)

% Uses the search technique and trace mechanisms of Experiment39 to make
% the directional cells produced by Experiment42alt learn the layout of a
% room.

%% Parameters
% World
worldSize_x = 10;
worldSize_y = 10;
agent_x = 2;
agent_y = 2;
reward_x = 3;
reward_y = 3;

% Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};

% Cells
num_sensory = [worldSize_y, worldSize_x];
num_motor = [1 size(actions, 2)];
num_SA = [1 round(1.2 * prod(num_motor)*prod(num_sensory))];
agent_firing = 2;

% Synapses
sensorytoSA_dilution = 1; %0.3
motortoSA_dilution = 1; %0.2
SAtomotor_dilution = 1; %0.2

% Competition and Learning
eta = 0.8;
sparseness = 99;
slope = 90000000000000000000;
learningRate = 100; % 0.004

normalisation_threshold = 1;
trace_threshold = 4; %0.01

eta = 0.9;
trace_learningRate = 0.1; % 0.0001

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Noise
state_noise = 0.002;

% Analysis
SA_tracked = [];
mapping_threshold = 0.6;
text_on = 0;


%% Setup
% Create world and place agent within it.
world = create_world3(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);
world_summed = zeros(size(world(:,:,1)));

% Make networks of sensory, motor and SA neurons.
sensory_cells = zeros(num_sensory);
motor_cells = zeros(num_motor);
SA_cells = zeros(num_SA);
SA_trace = zeros(num_SA);

% Create synapse weights between sensory-SA and motor-SA.
SAtoSA_synapses = GenerateZeroWeights(numel(SA_cells), numel(SA_cells), 1);
sensorytoSA_synapses = Generate_Diluted_Weights(sensory_cells, SA_cells, sensorytoSA_dilution, 1);
motortoSA_synapses = Generate_Diluted_Weights(motor_cells, SA_cells, motortoSA_dilution, 1);
SAtomotor_synapses = Generate_Diluted_Weights(SA_cells, motor_cells, SAtomotor_dilution, 1);

% Normalise synapses
SAtoSA_synapses = normalise(SAtoSA_synapses, normalisation_threshold);
sensorytoSA_synapses = normalise(sensorytoSA_synapses, normalisation_threshold);
motortoSA_synapses = normalise(motortoSA_synapses, normalisation_threshold);
SAtomotor_synapses = normalise(SAtomotor_synapses', normalisation_threshold);
SAtomotor_synapses = SAtomotor_synapses';

list = ones(numel(world(:,:,1)), numel(motor_cells));

%% Walls.

top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:worldSize_y:worldSize_x*worldSize_y];
    top_thick = top_thick - 1;
end

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, worldSize_y-bottom_thick+1:worldSize_y:worldSize_y*worldSize_x];
    bottom_thick = bottom_thick - 1;
end

left_wall = 1:worldSize_y*left_thick;
right_wall = worldSize_y*(worldSize_x-right_thick)+1:worldSize_y*worldSize_x;

% Maze walls
%{
% H-Maze
wall_a = left_wall + 40;
wall_b = left_wall + 100; wall_b = [wall_b(1:9), wall_b(12:20)];
wall_c = left_wall + 160;
%}

% Circular Maze
%{
wall_a = left_wall + 20;
wall_b = left_wall + 140; wall_b([3:7 14:18]) = [];
wall_c = right_wall - 40;
wall_d = right_wall - 140; wall_d([3:7 14:18]) = [];
wall_e = top_wall + 1;
wall_f = top_wall + 7; wall_f([3:7 14:18]) = [];
wall_g = bottom_wall - 1;
wall_h = bottom_wall - 7; wall_h([3:7 14:18]) = [];
%}

% Add Borders to World
world(left_wall + numel(world(:,:,1:2))) = 3;
world(right_wall + numel(world(:,:,1:2))) = 3;
world(top_wall + numel(world(:,:,1:2))) = 3;
world(bottom_wall + numel(world(:,:,1:2))) = 3;

% Add Maze Walls to World
try
    world(wall_a + numel(world(:,:,1:2))) = 3;
    world(wall_b + numel(world(:,:,1:2))) = 3;
    world(wall_c + numel(world(:,:,1:2))) = 3;
    world(wall_d + numel(world(:,:,1:2))) = 3;
    world(wall_e + numel(world(:,:,1:2))) = 3;
    world(wall_f + numel(world(:,:,1:2))) = 3;
    world(wall_g + numel(world(:,:,1:2))) = 3;
    world(wall_h + numel(world(:,:,1:2))) = 3;
catch err
    disp('Some/All Maze Walls Not Specified.')
end

%% RUN TRIAL

for time = 1:steps
    
    if time == round(steps/10)
        disp('1/10')
    elseif time == round(2*steps/10)
        disp('2/10')
    elseif time == round(3*steps/10)
        disp('3/10')
    elseif time == round(4*steps/10)
        disp('4/10')
    elseif time == round(5*steps/10)
        disp('5/10')
    elseif time == round(6*steps/10)
        disp('6/10')
    elseif time == round(7*steps/10)
        disp('7/10')
    elseif time == round(8*steps/10)
        disp('8/10')
    elseif time == round(9*steps/10)
        disp('9/10')
    elseif time == round(10*steps/10)
        disp('10/10')
    end
    
    noNaNsensorytoSA = sensorytoSA_synapses;
    noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
    
    noNaNmotortoSA = motortoSA_synapses;
    noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
    
    noNaNSAtomotor = SAtomotor_synapses;
    noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
    
    %% Move and Learn SA CombO
    
    % Sensory cells fire according to agent position (1 cell per place).
    sensory_cells = world(:,:,1);
    
    if text_on == 1
        fprintf('State = %d', find(sensory_cells))
        disp(' ')
    end
    
    % SA cells fire based on current sensory cells (state).
    SA_cells = dot((repmat(sensory_cells(:),[1,numel(SA_cells)])), noNaNsensorytoSA);
    
    % sigmoid
    SA_cells = softCompetition(sparseness, slope, SA_cells(:));
    SA_cells = SA_cells';
    
    if text_on == 1
        fprintf('SA = %s', num2str(find(SA_cells > 0.1)))
        disp(' ')
    end
    
    % Update sensorytoSA weights.
    sensorytoSA_synapses = sensorytoSA_synapses + (learningRate * (sensory_cells(:) * SA_cells(:)'));
    sensorytoSA_synapses = normalise(sensorytoSA_synapses, normalisation_threshold);
    
    % Select an action and activate the appropriate motor cell.
    valid_movement_selected = 0;
    world_summed_copy = world_summed;
    world_summed_copy(world(:,:,3) == 3) = NaN;
    
    while valid_movement_selected == 0
        
        % find the most active state cell(s) next to the current position
        worldSize_y = size(world_summed_copy,1);
        worldSize_x = size(world_summed_copy,2);
        
        current_state = sub2ind(size(world_summed), agent_y, agent_x);
        
        w = current_state-worldSize_y; n = current_state-1; s = current_state+1; e = current_state+worldSize_y;
        nw = w-1; sw = w+1; ne = e-1; se = e+1;
        
        test = [n e s w nw ne se sw];
        idx = find(test <= 0);
        test(idx) = test(idx) + worldSize_y*worldSize_x;
        idx = find(test > worldSize_y*worldSize_x);
        test(idx) = test(idx) - worldSize_y*worldSize_x;
        
        n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
        
        neighbours = zeros(3);
        
        neighbours(1) = world_summed_copy(nw);
        neighbours(2) = world_summed_copy(w);
        neighbours(3) = world_summed_copy(sw);
        neighbours(4) = world_summed_copy(n);
        neighbours(5) = NaN;
        neighbours(6) = world_summed_copy(s);
        neighbours(7) = world_summed_copy(ne);
        neighbours(8) = world_summed_copy(e);
        neighbours(9) = world_summed_copy(se);
        
        
        next_turn = find(neighbours == min(min(neighbours)));
        if size(next_turn,1) > 1
            %disp('RANDOM ACTION SELECTED')
        end
        
        action = actions{next_turn(randi(size(next_turn,1)))};
        %disp(action)
        [world, valid_movement_selected] = update_world4(world, action, 0);
        if valid_movement_selected == 1
            break
        else
            % add noise
            world_summed_copy = world_summed_copy(:)' + 0.001 .* std(world_summed_copy(:)) * randn(1,size(world_summed_copy(:),1));
            world_summed_copy(world_summed_copy < 0) = 0;
            world_summed_copy = reshape(world_summed_copy,num_state);
            disp('Noise added.')
        end
    end
    
    [~, idx] = ismember(action, actions);
    motor_cells(idx) = 1;
    
    if text_on == 1
        fprintf('Moved %s (%d)', action, find(motor_cells))
        disp(' ')
    end
    
    % Extra activation of SA cells from motor cells.
    SA_cells = SA_cells + dot((repmat(motor_cells', [1,numel(SA_cells)])), noNaNmotortoSA);
    
    % WTA:
    SA_cells = WTA_Competition(SA_cells);
    
    if text_on == 1
        fprintf('SA = %s', num2str(find(SA_cells > 0.1)'))
        disp(' ')
    end
    
    % Weights updated.
    
    motortoSA_synapses = motortoSA_synapses + learningRate * motor_cells(:) * SA_cells(:)';
    SAtomotor_synapses = SAtomotor_synapses + learningRate * SA_cells(:) * motor_cells(:)';
    
    % Weights normalised
    
    motortoSA_synapses = normalise(motortoSA_synapses, normalisation_threshold);
    SAtomotor_synapses = normalise(SAtomotor_synapses', normalisation_threshold);
    SAtomotor_synapses = SAtomotor_synapses';
    
    SA_info = [find(sensory_cells), find(motor_cells), find(SA_cells > 0.1)'];
    SA_tracked{time} = SA_info;
    
    list(find(sensory_cells), find(motor_cells)) = 0;
    
    if text_on == 1
        disp(' ')
    end
    
    %% Learn Sequence Through Trace Learning
    
    
    % Update state cells
    %SA_cells(agent_y, agent_x) = agent_firing; % Shouldn't need this--an
    %SA cell is already active.
    
    % Calculate the trace value for all cells
    SA_trace = getTrace(SA_cells, SA_trace, eta);
    
    % Update synapses using lr * state firing * trace rule
    SAtoSA_synapses = SAtoSA_synapses+(trace_learningRate * SA_cells(:) * SA_trace(:)');
    
    % Update synapses using lr * trace * state firing
    SAtoSA_synapses = SAtoSA_synapses+(trace_learningRate * SA_trace(:) * SA_cells(:)');
    
    % Block off self-self synapses
    SAtoSA_synapses(logical(eye(size(SAtoSA_synapses)))) = NaN;
    
    % Normalise synapses
    SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);
    
    % Record agent position
    world_tracked{time} = world(:,:,1);
    world_summed = world_summed + world_tracked{time};
    
    % End timestep. Repeat.
    if time == steps
        final = find(SA_cells);
    end
    
    %% Reset Cells and Update parameters
    SA_cells(:) = 0;
    sensory_cells(:) = 0;
    motor_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);

figure(); imagesc(world_summed); title('Sum of Time Spent in Each Location')

%% ANALYSE RESULTANT MAPPING
% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
disp('Complete')

disp('Calculating state/action mappings.')
%HML_mappings(sensorytoSA_synapses, motortoSA_synapses, mapping_threshold);
disp('Complete')

figure(); imagesc(sensorytoSA_synapses); colorbar; title('Sensory => S Synapses'); xlabel('State Cells'); ylabel('Sensory Cells)');
figure(); imagesc(motortoSA_synapses); colorbar; title('Motor => SA Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');
figure(); imagesc(SAtomotor_synapses'); colorbar; title('SA => Motor Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');

% Display repeatedly firing SA cells and the state/action that causes them
% to fire. This allows you to check that they are always firing for the
% same combo.
SA_tracked_copy = SA_tracked;
thirdElement = cellfun(@(x)x(3), SA_tracked_copy);
[~, idx] = sort(thirdElement);
SA_tracked_copy = SA_tracked_copy(idx);
thirdElement = cellfun(@(x)x(3), SA_tracked_copy);
thirdElement_repeats = thirdElement;
SA_tracked_copy(~ismember(thirdElement_repeats, thirdElement_repeats(diff(thirdElement_repeats) == 0))) = [];

for count = 1:numel(SA_tracked_copy)
    disp(SA_tracked_copy{count})
end

% Display every state/action combo.
SA_decoded = sortrows(cell2mat(SA_tracked(:)),[1 2]);
SA_decoded = unique(SA_decoded, 'rows');


disp('Complete')
end















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end