function Experiment50(steps, ind_steps, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded, walls)

% A version of the SA model that uses bursting inputs from sensory cells to
% produce temporarily agent-driven cells in a reward-gradient model.
%

% World
worldSize_x = 10;
worldSize_y = 10;
agent_x = 1;
agent_y = 1;
reward_x = 1;
reward_y = 1;

% Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};

% Cells
num_sensory = [worldSize_y worldSize_x];
num_reward = [worldSize_y worldSize_x];
num_motor = [1 size(actions, 2)];
num_SA = [1 round(1.2 * prod(num_motor)*prod(num_sensory))];
agent_firing = 0;
reward_firing = 1;

% Competition
SA_modifier = 0.02; % 0.2
%sparseness = 99;
%slope = 1;

% Noise
SA_noise = 0.001;

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;


%% Setup
% Create World
world = create_world3(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Create cells
sensory_cells = zeros(num_sensory);
reward_cells = zeros(num_reward);
SA_cells = zeros(num_SA);
motor_cells = zeros(num_motor);
trace = zeros(num_SA);

%% Walls.

% Add Walls to World
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

% place agent
fig = figure(); imagesc(world(:,:,3)); title('Please select starting position.');
[x, y] = ginput(1);
x=round(x); y=round(y);
agent_x = x; agent_y = y;
%synapses_summed(y,x) = 0;
close(fig)

% place reward
fig = figure(); imagesc(world(:,:,3)); title('Please select a goal.')
[x, y] = ginput(1);
x=round(x); y=round(y);
reward_x = x; reward_y = y;
close(fig)

world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

world(:,:,3) = zeros(size(world(:,:,2)));
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;


%% RUN TRIAL
for time = 1:steps
    % Allow activity to spread for 1:ind_time
    for ind_time = 1:ind_steps
        %{
        if ind_time == round(ind_steps/4)
            disp('...')
        elseif ind_time == round(ind_steps/2)
            disp('...')
        elseif ind_time == round(3*ind_steps/4)
            disp('...')
        end
        %}
        noNaNsensorytoSA = sensorytoSA_synapses;
        noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
        
        noNaNmotortoSA = motortoSA_synapses;
        noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
        
        noNaNSAtomotor = SAtomotor_synapses;
        noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
        
        noNaNSAtoSA= SAtoSA_synapses;
        noNaNSAtoSA(isnan(noNaNSAtoSA)) = 0;
        
        
        
        %% Set Up Reward Gradient
        % Sensory cells fire based on current position.
        
        sensory_cells(find(world(:,:,1))) = agent_firing;
        reward_cells(find(world(:,:,2))) = reward_firing;
        
        
        
        % SA cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
        SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
        SA_cells = reshape(SA_cells,num_SA);
        
        
        % Competition in state cells:
        
        % subtractive.
        SA_cells = SA_cells - SA_modifier * mean(mean(SA_cells));
        
        % divisive.
        SA_cells = SA_cells/max(max(SA_cells));
        SA_cells(SA_cells < 0) = 0;
        
        SA_cells = reshape(SA_cells,num_SA);
        
        
        % add noise
        SA_cells = SA_cells(:)' + SA_noise .* std(SA_cells(:)) * randn(1,size(SA_cells(:),1));
        SA_cells(SA_cells < 0) = 0;
        SA_cells = reshape(SA_cells,num_SA);
        
    end
    
    % Record agent position
        world_tracked{time} = world(:,:,1) + world(:,:,2) + world(:,:,3);
   
    %world(:,:,1) + world(:,:,2)
    %SA_decoded(find(SA_decoded(:,3) == find(SA_cells == max(SA_cells))),:)
    
    %% Check for Completion
    current_state = sub2ind(size(world(:,:,1)), agent_y, agent_x);
    reward_location = sub2ind(size(world(:,:,1)), reward_y, reward_x);
    if current_state == reward_location
        result = 'Y';
        disp(time)
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Move Agent through Bursting Sensory Activity
    
    sensory_cells(find(world(:,:,1))) = 100;
    reward_cells(find(world(:,:,2))) = reward_firing;
    
    SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
    SA_cells = reshape(SA_cells,num_SA);
    
    %SA_cells = SA_cells + dot(repmat(sensory_cells(:),[1,numel(SA_cells)]), noNaNsensorytoSA);
    
    SA_cells = WTA_Competition(SA_cells);
    
    world(:,:,1) + world(:,:,2)
    SA_decoded(find(SA_decoded(:,3) == find(SA_cells == max(SA_cells))),:)
    
    % Calculate activation to motor cells
    motor_cells = dot(repmat(SA_cells(:),[1,numel(motor_cells)]), noNaNSAtomotor);
    
    % Move agent.
    action = actions{find(motor_cells == max(motor_cells))};
    fprintf('%d: %s', time, action)
    disp(' ')
    [world, ~, ~] = update_world5(world, action, 0);
    
    %% Clear cells
    sensory_cells(:) = 0;
    reward_cells(:) = 0;
    SA_cells(:) = 0;
    motor_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end


% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);













end


function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end
