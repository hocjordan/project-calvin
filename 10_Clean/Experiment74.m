function [SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_tracked, sensory_tracked] = Experiment74(steps, actions, world_update_function)

% Based on Clean150413_Leaner

% Input parameter 'steps' is an integer describing the number of
% exploration steps to be made.

% Input parameter 'actions' is a cell array of strings designating certain
% actions, e.g.:
% ['RED_ON', 'RED_OFF', 'GREEN_ON', 'GREEN_OFF']
% ['Forward', 'Backward', 'Left', 'Right']

% Outputs synapse matrices.

% Outputs sensory_tracked, which tracks the various states the model has
% experienced.

% Outputs SA_decoded, which tracks the state/action combination currently
% associated with each neuron.

% Calls a WORLD function to receive sensory input and to perform a planned
% action.                                                                                                                                                     
                                                                                                                                                               %
                                                                                                                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Change-Log

% Learner function created on April 13th 2015

% Apparently works on April 14th 2015. Saved SAFE version.

% Commented out global variable 'world_state' and update functions.

% Got rid of dilution terms and diluted synapse generator functions.

% Made into Experiment74, to test if sensorytoSA weights have to be the
% same for every cell within a column.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Parameters

global world_state

% GET SENSORY DATA FROM WORLD (ONLY 1 CELL ACTIVE AT A TIME)
[sensory_cells, ~] = world_update_function('', 'yes');
%sensory_cells = zeros(10);


% Cells
num_sensory = [size(sensory_cells)];                                                            % Get layer size of sensory (state) cells.
num_motor = [1 size(actions, 2)];                                                               % Set layer size of motor (action) cells as [1 * number of possible actions].
num_SAcol = prod(num_sensory);                                                                  % Set the number of columns in the SA (state-action) layer as equal to the size of the state layer.
num_SAcellsinCol = prod(num_motor);                                                             % Set the number of cells in each SA column as equal to the number of possible actions.
num_SA = [num_SAcellsinCol 1 num_SAcol];                                                        % Set layer size of SA cells as [number of cells in column * 1 * number of columns]. This is important for competition in CONTROLLER function.

% Competition and Learning
learningRate = 100; %100                                                                        % Multiplier constant used in synapse update.
trace_learningRate = 10; %0.1; %0.001; % 0.0001                                                 % Similar to learningRate but used in a specific computation. CHECK IF THIS IS NECESSARY.

sensory_threshold = 1; % Necessary to adjust these to compensate for different                  % Threshold constants used when normalising synapses. Don't know if these are necessary but...
motor_threshold = 0.25; % numbers of synapses to sensory and motor cells.                       % ...the previous comments seem to imply so.
trace_threshold = 4; %0.01

eta = 0.0;                                                                                      % Determines the length of the memory trace used in function getTrace(). See function for more details.

% Analysis
sensory_tracked = {};                                                                           % Cell array to store experienced states for post-simulation analysis.
SA_tracked = {};                                                                                % Cell array to store experienced state-action combinations for post-simulation analysis.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Setup

disp('Setting up...')

% Make networks of sensory, motor and SA neurons.
motor_cells = zeros(num_motor);                                                                 % Create matrix of motor cell firing rates (currently all zeros)
SA_cells = zeros(num_SA);                                                                       % Create matrix of SA cell firing rates (currently all zeros)
SA_trace = zeros(num_SA);                                                                       % Create matrix to store the SA firing rates of the last timestep.

% Create synapse weights between SA cells and motor<=>SA.
SAtoSA_synapses = zeros(numel(SA_cells), numel(SA_cells));                                      % Create matrix of synapses connecting every SA cell to every other SA cell (currently all zeros). 
motortoSA_synapses = rand(numel(motor_cells), numel(SA_cells));                                 % Create matrix of synapses connecting every SA cell to every motor cell. Currently random values between 0 and 1.
SAtomotor_synapses = rand(numel(SA_cells), numel(motor_cells));                                 % Create matrix of synapses connecting every motor cell to every SA cell. Currently random values between 0 and 1.

% Create sensory => SA synapses that are identical for all cells in a
% column.

sensorytoSA_synapses = rand(numel(sensory_cells), numel(SA_cells)); % ALTERED!!!! Create synapse matrix randomly, as motortoSA_synapses

% Normalise synapses
sensorytoSA_synapses = normalise(sensorytoSA_synapses, sensory_threshold);                      % Normalise each column of the sensorytoSA_synapses matrix to the value of sensory_threshold; in other words normalise the input weights to each SA cell from the sensory layer.
motortoSA_synapses = normalise(motortoSA_synapses, motor_threshold);                            % As previous line, but with the motortoSA_synapses and motor threshold.
SAtomotor_synapses = normalise(SAtomotor_synapses', motor_threshold);                           % As previous line, but transposes the matrix so normalises the rows rather than the columns of the SAtomotor synapse matrix; in other words normalise the output weights from each SA cell to the sensory layer.
SAtomotor_synapses = SAtomotor_synapses';                                                       % Re-transpose the matrix back to its original position.

disp('...Setup Complete.')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% RUN TRIAL

for time = 1:steps
    
        
    %% Firing of SA cells and Recruitment of Column
    
    % GET SENSORY DATA FROM WORLD (ONLY 1 CELL ACTIVE AT A TIME)
    [sensory_cells, ~] = world_update_function('', 'yes');
    %sensory_cells = zeros(10);
    
    % SA cells fire based on current sensory cells (state).
    SA_cells = dot((repmat(sensory_cells(:),[1,numel(SA_cells)])), sensorytoSA_synapses);       % Calculate activation of SA cell from dot product of sensory cell firing rates and sensorytoSA_synapses.
    SA_cells = reshape(SA_cells, num_SA);                                                       % Unimportant. Makes sure that the SA cell layer retains its initial dimensions.
    
    % TRANSFER FUNCTION
    SA_cells(SA_cells > 0.1 & SA_cells < 0.95) = 0;                                             % Sets all SA_cells with activation above 0.1 and below 0.95 to zero. Important when there are multiple cells active in sensory layer.
    
    % columnar WTA
    SA_cells(SA_cells == max(max(SA_cells))) = 1;                                               % Sets all SA cells within most activated SA column to 1.
    SA_cells(SA_cells ~= max(max(SA_cells))) = 0;                                               % Sets all other SA cells to 0.
    
    % RECORD STATES EXPERIENCED
    sensory_info = sensory_cells;                                                               % Records the current sensory input...
    sensory_tracked{time} = sensory_info;                                                       % ...and saves it to a cell array.
    
    % Update sensorytoSA weights.
    sensorytoSA_synapses = sensorytoSA_synapses + (learningRate * (sensory_cells(:) * SA_cells(:)')); % For each sensory cell, adds (constant . sensory cell activation (presynaptic SA) . postsynaptic SA)
    sensorytoSA_synapses = normalise(sensorytoSA_synapses, sensory_threshold);                  % Normalise sensorytoSA synapses to sensory_threshold.
    
    %% TRACE LEARNING: Update connections from ACTIVE COLUMN to PREVIOUS (trace) SA_CELL:
    
    % Update SAtoSA synapses
    SAtoSA_synapses = SAtoSA_synapses + (trace_learningRate * SA_cells(:) * SA_trace(:)');      % Update the synapses between SA cells with (constant . current SA firing . SA trace firing from previous timesteps). SA_trace is zeros in first timestep.
    
    % Normalise synapses
    SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);                              % Normalise SAtoSA synapses as previous.
    SAtoSA_synapses(isnan(SAtoSA_synapses)) = 0;                                                % Gets rid of any NaN values that the normalisation may have introduced.
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% Select an action and activate the appropriate motor cell.
    
    action = actions{randi(size(actions,2))};                                                   % Select a random action out of the subset of possible actions.
    
    % UPDATE WORLD FROM ACTION (ONLY 1 CELL ACTIVE AT A TIME)
    world_update_function(action, 'no');                                                        % Output that function into the world, changing the world in some way.
    %sensory_cells = zeros(10);
    
    [~, idx] = ismember(action, actions);                                                       % Activate the motor cell that represents the action you just took.
    motor_cells(idx) = 1;
    
    % Activation of SA cells from sensory and motor cells simulataneously.
    SA_cells(:) = 0;                                                                            % Reset all SA cells to 0.
    SA_cells = dot(([repmat(motor_cells(:), [1,numel(SA_cells)]); repmat(sensory_cells(:), [1,numel(SA_cells)])]), [motortoSA_synapses; sensorytoSA_synapses]); % Activate SA cells using the dot product of (motor cell firing . motortoSA_synapses) plus the dot product of (sensory_cells . sensorytoSA)
    SA_cells = reshape(SA_cells, num_SA);
    
    % WTA:
    SA_cells = WTA_Competition(SA_cells(:));                                                    % The strongest cell in the SA layer (if there is more than one, choose randomly) is set to one, and all else to 0.
    SA_cells = reshape(SA_cells, num_SA);                                                       % Unimportant.
   
    % Weights updated.
    
    motortoSA_synapses = motortoSA_synapses + learningRate * motor_cells(:) * SA_cells(:)';     % Update motortoSA synapses...
    SAtomotor_synapses = SAtomotor_synapses + learningRate * SA_cells(:) * motor_cells(:)';     % ...and SAtomotor synapses.
    
    % Weights normalised
    
    motortoSA_synapses = normalise(motortoSA_synapses, motor_threshold);                        % Normalise over columns of motortoSA synapses and rows of SAtomotor.
    SAtomotor_synapses = normalise(SAtomotor_synapses', motor_threshold);
    SAtomotor_synapses = SAtomotor_synapses';
    
    % RECORD STATE/ACTION COMBINATIONS EXPERIENCED
    SA_info{1} = sensory_cells;                                                                 % Record sensory input, motor output and firing SA celll in a cell array.
    SA_info{2} = find(motor_cells);
    SA_info{3} = find(SA_cells > 0.1)';
    SA_tracked{time} = SA_info;
    
    
    %% Calculate SA trace
    
    % Calculate the trace value for all cells
    SA_trace = getTrace(SA_cells, SA_trace, eta);                                               % Calculate a memory trace for each SA cell from their current firing, in preparation for the next timestep.
    
    %% Reset Cells and Update parameters
    
    
    SA_cells(:) = 0;                                                                            % Reset all cells to 0.
    sensory_cells(:) = 0;
    motor_cells(:) = 0;
    
end



disp('Complete')
end















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;                                     % The memory trace for each SA cell is its current firing added to the memory trace from the last timestep in a ratio determined by eta. 
end



function matrix = normalise(matrix, threshold)

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));                 % Normalises a matrix so that each column sums to the threshold value given. If you want to normalise rows, transpose the matrix before using this function.

end