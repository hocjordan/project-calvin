function [SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_tracked, sensory_tracked] = Clean_150413_LEARNER(steps, actions, world_update_function)

% Based on Experiment66_learner_SAFE.

% Input parameter 'steps' is an integer describing the number of
% exploration steps to be made.

% Input parameter 'actions' is a cell array of strings designating certain
% actions, e.g.:
% ['RED_ON', 'RED_OFF', 'GREEN_ON', 'GREEN_OFF']
% ['Forward', 'Backward', 'Left', 'Right']

% Outputs synapse matrices.

% Outputs sensory_tracked, which tracks the various states the model has
% experienced.

% Outputs SA_decoded, which tracks the state/action combination currently
% associated with each neuron.

% Calls a WORLD function to receive sensory input.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Change-Log

% Created on April 13th 2015




%% Parameters

% 
global world_state

% GET SENSORY DATA FROM WORLD (ONLY 1 CELL ACTIVE AT A TIME)
[sensory_cells] = world_update_function('', 'yes');


% Cells
num_sensory = [size(sensory_cells)];                                                            % Get layer size of sensory (state) cells.
num_motor = [1 size(actions, 2)];                                                               % Set layer size of motor (action) cells as [1 * number of possible actions].
num_SAcol = prod(num_sensory);                                                                  % Set the number of columns in the SA (state-action) layer as equal to the size of the state layer.
num_SAcellsinCol = prod(num_motor);                                                             % Set the number of cells in each SA column as equal to the number of possible actions.
num_SA = [num_SAcellsinCol 1 num_SAcol];                                                        % Set layer size of SA cells as [number of cells in column * 1 * number of columns]. This is important for competition in CONTROLLER function.

% Synapses
sensorytoSA_dilution = 1; %0.3
motortoSA_dilution = 1; %0.2
SAtomotor_dilution = 1; %0.2

% Competition and Learning
learningRate = 100; %100
trace_learningRate = 10; %0.1; %0.001; % 0.0001

normalisation_threshold = 1;
sensory_threshold = 1; % Necessary to adjust these to compensate for different
motor_threshold = 0.25; % numbers of synapses to sensory and motor cells.
trace_threshold = 4; %0.01

eta = 0.0;

% Noise
state_noise = 0.002;

% Analysis
SA_tracked = {};
sensory_tracked = {};
text_on = 0;


%% Setup

disp('Setting up...')

% Make networks of sensory, motor and SA neurons.
motor_cells = zeros(num_motor);
SA_cells = zeros(num_SA);
SA_trace = zeros(num_SA);

% Create synapse weights between SA cells and motor<=>SA.
SAtoSA_synapses = GenerateZeroWeights(numel(SA_cells), numel(SA_cells), 1);
motortoSA_synapses = Generate_Diluted_Weights(motor_cells, SA_cells, motortoSA_dilution, 1);
SAtomotor_synapses = Generate_Diluted_Weights(SA_cells, motor_cells, SAtomotor_dilution, 1);

% Create sensory => SA synapses that are identical for all cells in a
% column.
sensorytoSA_synapses = [];
for column = 1:num_SAcol
    sensorytoSA_synapses = [sensorytoSA_synapses repmat(Generate_Diluted_Weights(sensory_cells, 1, sensorytoSA_dilution, 1), [1 size(actions,2)])];
end

% Normalise synapses
sensorytoSA_synapses = normalise(sensorytoSA_synapses, sensory_threshold);
motortoSA_synapses = normalise(motortoSA_synapses, motor_threshold);
SAtomotor_synapses = normalise(SAtomotor_synapses', motor_threshold);
SAtomotor_synapses = SAtomotor_synapses';

disp('...Setup Complete.')

%% RUN TRIAL

%% Timing

tic;
duration = 0;
for time = 1:steps
    
    
    noNaNsensorytoSA = sensorytoSA_synapses;
    noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
    
    noNaNmotortoSA = motortoSA_synapses;
    noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
    
    noNaNSAtomotor = SAtomotor_synapses;
    noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
    
    %% Firing of SA cells and Recruitment of Column
    
    % GET SENSORY DATA FROM WORLD (ONLY 1 CELL ACTIVE AT A TIME)
    [sensory_cells] = world_update_function('', 'yes');
    
    % SA cells fire based on current sensory cells (state).
    SA_cells = dot((repmat(sensory_cells(:),[1,numel(SA_cells)])), noNaNsensorytoSA);
    SA_cells = reshape(SA_cells, num_SA);
    
    % TRANSFER FUNCTION
    SA_cells(SA_cells > 0.1 & SA_cells < 0.95) = 0;
    
    % columnar WTA
    SA_cells(SA_cells == max(max(SA_cells))) = 1;
    SA_cells(SA_cells ~= max(max(SA_cells))) = 0;
    
    % RECORD STATES EXPERIENCED
    sensory_info = world_state;
    sensory_tracked{time} = sensory_info;
    
    % Update sensorytoSA weights.
    sensorytoSA_synapses = sensorytoSA_synapses + (learningRate * (sensory_cells(:) * SA_cells(:)'));
    sensorytoSA_synapses = normalise(sensorytoSA_synapses, sensory_threshold);
    noNaNsensorytoSA = noNaN(sensorytoSA_synapses);
    
    %% TRACE LEARNING: Update connections from ACTIVE COLUMN to PREVIOUS (trace) SA_CELL:
    
    % Update synapses using lr * state firing * trace rule (backwards)
    SAtoSA_synapses = SAtoSA_synapses + (trace_learningRate * SA_cells(:) * SA_trace(:)');
    
    % Normalise synapses
    SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);
    SAtoSA_synapses(isnan(SAtoSA_synapses)) = 0;
    
    %% Select an action and activate the appropriate motor cell.
    
    action = actions{randi(size(actions,2))};
    
    % UPDATE WORLD FROM ACTION (ONLY 1 CELL ACTIVE AT A TIME)
    world_update_function(action, 'no');
    
    [~, idx] = ismember(action, actions);
    motor_cells(idx) = 1;
    
    % Activation of SA cells from sensory and motor cells simulataneously.
    SA_cells(:) = 0;
    SA_cells = dot(([repmat(motor_cells(:), [1,numel(SA_cells)]); repmat(sensory_cells(:), [1,numel(SA_cells)])]), [noNaNmotortoSA; noNaNsensorytoSA]);
    SA_cells = reshape(SA_cells, num_SA);
    
    % WTA:
    SA_cells = WTA_Competition(SA_cells(:));
    SA_cells = reshape(SA_cells, num_SA);
    
    if text_on == 1
        fprintf('SA = %s', num2str(find(SA_cells > 0.1)'))
        disp(' ')
    end
    
    % Weights updated.
    
    motortoSA_synapses = motortoSA_synapses + learningRate * motor_cells(:) * SA_cells(:)';
    SAtomotor_synapses = SAtomotor_synapses + learningRate * SA_cells(:) * motor_cells(:)';
    
    % Weights normalised
    
    motortoSA_synapses = normalise(motortoSA_synapses, motor_threshold);
    SAtomotor_synapses = normalise(SAtomotor_synapses', motor_threshold);
    SAtomotor_synapses = SAtomotor_synapses';
    
    % RECORD STATE/ACTION COMBINATIONS EXPERIENCED
    SA_info{1} = sensory_cells;
    SA_info{2} = find(motor_cells);
    SA_info{3} = find(SA_cells > 0.1)';
    SA_tracked{time} = SA_info;
    
    
    %% Calculate SA trace
    
    % Calculate the trace value for all cells
    SA_trace = getTrace(SA_cells, SA_trace, eta);
    
    %% Reset Cells and Update parameters
    
    
    SA_cells(:) = 0;
    sensory_cells(:) = 0;
    motor_cells(:) = 0;
    
end



disp('Complete')
end















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));

end