function [firingRates] = WTA_Competition(firingRates)

[~, idx]=nanmax(firingRates);

firingRates(:) = 0;
firingRates(idx) = 1;
end
