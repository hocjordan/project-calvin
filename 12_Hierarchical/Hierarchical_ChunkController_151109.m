function [sensory_tracked, chunktoSA_synapses, SAtochunk_synapses] = Hierarchical_ChunkController_151109(steps, ind_steps, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, chunktoSA_synapses, SAtochunk_synapses, SA_decoded, actions, world_update_function)

%%%%%%%%%%%%%%

%150811: Based on Clean_1504. Has not yet been altered.

%%%%%%%%%%%%%%%

global world_state


% Cells

% GET INPUT FROM SENSES
[sensory_cells, reward_cells] = world_update_function('', 'yes');
num_sensory = [size(sensory_cells)];

% GET REWARD
SArewardtoSA_synapses = zeros(size(SAtoSA_synapses));
SArewardtoSA_synapses(logical(eye(size(SArewardtoSA_synapses)))) = 1;
SArewardtoSA_synapses = reshape(SArewardtoSA_synapses, size(SArewardtoSA_synapses));


num_motor = [1 size(actions, 2)];
num_SAcol = prod(num_sensory);
num_SAcellsinCol = prod(num_motor);
num_SA = [num_SAcellsinCol 1 num_SAcol];
num_chunk = [1 round(prod(num_SA)/3)];
reward_firing = 1;

% Synapses
chunk_learningRate = 1; %11 %0.00001 %0.000001; %0.01
eta = 0.9;

% Competition
SA_modifier = 0.9; %0.02; % 0.2
chunk_modifier = 1.8;
chunk_threshold = 1;

% Analysis
display_tracked = {};

%% Setup

% Create cells
SA_cells = zeros(num_SA);
motor_cells = zeros(num_motor);
SA_trace = zeros(num_SA);
chunk_cells = zeros(num_chunk);
chunk_trace = zeros(num_chunk);

%% RUN TRIAL
for time = 1:steps
    
    % Allow activity to spread for 1:ind_time
    for ind_time = 1:ind_steps
        
        
        %% Set Up Reward Gradient
        % Place reward.
        % GET REWARD POSITION
        [~, reward_cells] = world_update_function('', 'yes');
        
        % SA cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
        SA_reward = dot((repmat(reward_cells(:),[1,numel(SA_cells)])), sensorytoSA_synapses);
        
        % filter/transfer SA_reward function MAKE GENERAL LATER!!!!!!!!!!!!
        SA_reward(SA_reward < 0.5) = 0;
        
        % final SA update
        SA_cells = dot(([repmat(SA_reward(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [SArewardtoSA_synapses; SAtoSA_synapses]);
        SA_cells = reshape(SA_cells,num_SA);
        
        
        % Competition in state cells:
        
        % subtractive.
        for column = 1:size(SA_cells,3)
            SA_cells(:,:,column) = SA_cells(:,:,column) - SA_modifier * mean(SA_cells(:,:,column));
        end
        
        % divisive.
        SA_cells = SA_cells/max(max(SA_cells));
        sum_save = sum(SA_cells);
        SA_cells(SA_cells < 0) = 0;
        for col = 1:size(SA_cells,3)
            SA_cells(:,:,col) = normalise(SA_cells(:,:,col), sum_save(:,:,col));
            SA_cells(isnan(SA_cells)) = 0;
        end
        SA_cells = reshape(SA_cells,num_SA);
        
        
        
    end
    
    %% Record agent position
    sensory_info = world_state;
    sensory_tracked{time} = sensory_info;
    
    
    
    %% Check for Completion
    % ALTER FOR GENERALISATION, OR JUST CALL PART OF THE SIMULATION
    %if max(abs(current_state - reward_location)) < 0.5
    % If all nonzero elements of the reward representation are also
    % represented in the sensory cells, then end.
    [sensory_cells, ~] = world_update_function('', 'yes');
    if ~any(~ismember(find(reward_cells), find(sensory_cells)))
        result = 'Y';
        disp(time)
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Move Agent through Bursting Sensory Activity
    [sensory_cells, ~] = world_update_function('', 'yes');
    sensory_cells(sensory_cells > 0) = 100;
    reward_cells(reward_cells > 0) = reward_firing;
    
    SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [sensorytoSA_synapses; sensorytoSA_synapses; SAtoSA_synapses]);
    SA_cells = WTA_Competition(SA_cells);
    SA_cells = reshape(SA_cells,num_SA);
    
    
    % Calculate activation to motor cells
    motor_cells = dot(repmat(SA_cells(:),[1,numel(motor_cells)]), SAtomotor_synapses);
    
    % Move agent.
    action = actions{find(motor_cells == max(motor_cells))};
    disp(action)
    world_update_function(action, 'no');
    
    %% Feed Activity to Chunk Cells
    
    % Uses a full trace rule -- preSynaptic_trace *
    % postSynaptic_trace
    
    % Calculate activation of chunk cells from SA cells
    chunk_cells = dot(repmat(SA_cells(:),[1,numel(chunk_cells)]), SAtochunk_synapses);
    chunk_cells = reshape(chunk_cells,num_chunk);
    chunk_save = chunk_cells;
    
    % Competition in chunk cells:
    % Subtractive.
    chunk_cells = chunk_cells - chunk_modifier * mean(mean(chunk_cells));
    chunk_cells(chunk_cells < 0) = 0;
    
    if any(chunk_cells) == 0
        disp('No chunk cells firing')
    else
        % divisive
        chunk_cells = 1 * (chunk_cells/max(max(chunk_cells)));
    end
    
    % Calculate memory trace for all cells.
    chunk_trace = getTrace(chunk_cells, chunk_trace, eta);
    SA_trace = getTrace(SA_cells, SA_trace, eta);
    
        % Display firing cells and traces
    fprintf('SA = %d.\n' , find(SA_cells))
    fprintf('SA (trace) = %s.\n' , num2str(find(SA_trace)'))
    fprintf('Chunk = %s.\n', num2str(find(chunk_cells > 0.9)))
    fprintf('Chunk (trace) = %s.\n', num2str(find(chunk_trace > 0.09)))
    %fprintf('Super = %s.\n', num2str(find(super_cells > 0.9)))
    %fprintf('Super (trace) = %s.\n', num2str(find(super_trace > 0.09)))
    disp(' ')
    
    % Update synapses to and from chunk cells.
    chunktoSA_synapses = chunktoSA_synapses + chunk_learningRate * chunk_trace(:) * SA_trace(:)';
    SAtochunk_synapses = SAtochunk_synapses + chunk_learningRate * SA_trace(:) * chunk_trace(:)';
    
    % Normalise synapse weights to and from chunk cells.
    SAtochunk_synapses = normalise(SAtochunk_synapses, chunk_threshold);
    chunktoSA_synapses = normalise(chunktoSA_synapses', chunk_threshold);
    chunktoSA_synapses = chunktoSA_synapses';
    
    %% Clear cells
    sensory_cells(:) = 0;
    reward_cells(:) = 0;
    SA_cells(:) = 0;
    motor_cells(:) = 0;
    chunk_cells(:) = 0;
    
end



end














function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));

end