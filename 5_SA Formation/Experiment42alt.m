

function [list] = Experiment42alt(steps)

% An alternate version of Experiment42 that works with only one layer.

%% Parameters
% World
worldSize_x = 4;
worldSize_y = 4;
agent_x = 2;
agent_y = 2;
reward_x = 3;
reward_y = 3;

% Cells
num_sensory = [worldSize_y, worldSize_x];
num_SA = [12 12];
num_motor = [1 4];

% Synapses
sensorytoSA_dilution = 1; %0.3
motortoSA_dilution = 1; %0.2
SAtomotor_dilution = 1; %0.2

% Competition and Learning
eta = 0.8;
sparseness = 95;
slope = 9000000;
learningRate = 100; % 0.004
mapping_threshold = 0.6;
normalisation_threshold = 1;
SA_tracked = [];

%% SETUP TRIAL

% Create 4x4 world and place agent within it.
world = create_world3(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Make networks of sensory (4x4), motor (4) and SA (4x4x4) neurons.
sensory_cells = zeros(num_sensory);
motor_cells = zeros(num_motor);
SA_cells = zeros(num_SA);

% Create synapse weights between sensory-SA and motor-SA.
sensorytoSA_synapses = Generate_Diluted_Weights(sensory_cells, SA_cells, sensorytoSA_dilution, 1);
motortoSA_synapses = Generate_Diluted_Weights(motor_cells, SA_cells, motortoSA_dilution, 1);
SAtomotor_synapses = Generate_Diluted_Weights(SA_cells, motor_cells, SAtomotor_dilution, 1);

% Normalise synapses
sensorytoSA_synapses = normalise(sensorytoSA_synapses, normalisation_threshold);
motortoSA_synapses = normalise(motortoSA_synapses, normalisation_threshold);
SAtomotor_synapses = normalise(SAtomotor_synapses', normalisation_threshold);
SAtomotor_synapses = SAtomotor_synapses';

list = ones(numel(world(:,:,1)), numel(motor_cells));

%% RUN TRIAL
% Begin timestep:
disp('Running Timesteps')
disp('...')
for time = 1:steps
    
    if time == round(steps/4)
        disp('...')
    elseif time == round(steps/2)
        disp('...')
    elseif time == round(3*steps/4)
        disp('...')
    end
    
    noNaNsensorytoSA = sensorytoSA_synapses;
    noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
    
    noNaNmotortoSA = motortoSA_synapses;
    noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
    
    noNaNSAtomotor = SAtomotor_synapses;
    noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
    
    % Sensory cells fire according to agent position (1 cell per place).
    sensory_cells = world(:,:,1);
    fprintf('State = %d', find(sensory_cells))
    disp(' ')
    
    % SA cells fire based on current S cells (first layer).
    SA_cells = dot((repmat(sensory_cells(:),[1,numel(SA_cells)])), noNaNsensorytoSA);
    
    % sigmoid
    SA_cells = softCompetition(sparseness, slope, SA_cells(:));
    SA_cells = SA_cells';
    %SA_cells = reshape(SA_cells,num_SA);
    
    % Activate a random motor cell
    actions = {'N' 'E' 'S' 'W'};
    motor_cells(:) = 0;
    motor_cells(randi(4)) = 1;
    action = actions{find(motor_cells)};
    fprintf('Moved %c (%d)', action, find(motor_cells))
    disp(' ')
    
    % Update sensorytoS weights.
    sensorytoSA_synapses = sensorytoSA_synapses + (learningRate * (sensory_cells(:) * SA_cells(:)'));
    sensorytoSA_synapses = normalise(sensorytoSA_synapses, normalisation_threshold);
    
    % Extra activation of SA cells from motor cells.
    %SA_cells = dot((repmat([S_cells(:); motor_cells'], [1,numel(SA_cells)])), [noNaNStoSA; noNaNmotortoSA]);
    SA_cells = SA_cells + dot((repmat(motor_cells', [1,numel(SA_cells)])), noNaNmotortoSA);
    
    % WTA:  
    SA_cells = WTA_Competition(SA_cells);
    
    fprintf('SA = %s', num2str(find(SA_cells > 0.1)'))
    disp(' ')
    
    % Weights updated.
    
    motortoSA_synapses = motortoSA_synapses + learningRate * motor_cells(:) * SA_cells(:)';
    SAtomotor_synapses = SAtomotor_synapses + learningRate * SA_cells(:) * motor_cells(:)';
    
    % Weights normalised
    
    motortoSA_synapses = normalise(motortoSA_synapses, normalisation_threshold);
    SAtomotor_synapses = normalise(SAtomotor_synapses', normalisation_threshold);
    SAtomotor_synapses = SAtomotor_synapses';
    
    % Agent moves
    world = update_world4(world, action, 1);
    
    % End timestep:
    world_tracked{time} = world(:,:,1);
    
    SA_info = [find(sensory_cells), find(motor_cells), find(SA_cells > 0.1)'];
    SA_tracked{time} = SA_info;
    
    list(find(sensory_cells), find(motor_cells)) = 0;
    disp(' ')
    
end
disp('Complete')

%% ANALYSE RESULTANT MAPPING
% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
disp('Complete')

disp('Calculating state/action mappings.')
%HML_mappings(sensorytoSA_synapses, motortoSA_synapses, mapping_threshold);
disp('Complete')

figure(); imagesc(sensorytoSA_synapses); colorbar; title('Sensory => S Synapses'); xlabel('State Cells'); ylabel('Sensory Cells)');
figure(); imagesc(motortoSA_synapses); colorbar; title('Motor => SA Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');
figure(); imagesc(SAtomotor_synapses'); colorbar; title('SA => Motor Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');

% Display repeatedly firing SA cells and the state/action that causes them
% to fire. This allows you to check that they are always firing for the
% same combo.
thirdElement = cellfun(@(x)x(3), SA_tracked);
[~, idx] = sort(thirdElement);
SA_tracked = SA_tracked(idx);
thirdElement = cellfun(@(x)x(3), SA_tracked);
thirdElement_repeats = thirdElement;
SA_tracked(~ismember(thirdElement_repeats, thirdElement_repeats(diff(thirdElement_repeats) == 0))) = [];

for count = 1:numel(SA_tracked)
    disp(SA_tracked{count})
end

end















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold divide each row in that column by
        %that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end
