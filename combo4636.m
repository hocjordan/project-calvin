function combo4636

[statetostate_synapses, walls] = Experiment46(400);
Experiment36(50, 10, statetostate_synapses, walls);
end






function [statetostate_synapses, walls] = Experiment46(steps, extrawalls)

% As Experiment 39, with a user-controlled interface to create and import custom
% mazes.

%% Parameters

% trial
eta = 0.9;
learningRate = 0.1; % 0.0001
worldSize_x = 20;
worldSize_y = 20;
agent_x = 4;
agent_y = 2;
reward_x = 7;
reward_y = 7;

% World

% Cells
num_state = [worldSize_y worldSize_x];
agent_firing = 2;

% Noise
state_noise = 0.002;

% Synapses
normalisation_threshold = 4; %0.01

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Analysis

%% Setup
% Create World
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);
world_summed = zeros(size(world(:,:,1)));

% Create grid of place cells
state_cells = zeros(num_state);
trace = zeros(num_state);

% Create synapses with full connectivity, save for self-self
%statetostate_synapses = Generate_Diluted_Weights(state_cells, state_cells, 1, 0.000001);
statetostate_synapses = GenerateZeroWeights(numel(state_cells), numel(state_cells), 1);

%% Walls.

world(:,:,3) = zeros(size(world(:,:,2)));

rows = size(world,1);
col = size(world,2);

walls = [];

top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

walls = [walls top_wall];

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

walls = [walls bottom_wall];

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

walls = [walls left_wall];
walls = [walls right_wall];

% add extra walls if given
if exist('extrawalls', 'var') == 1
    walls = [walls extrawalls'];
end

% walls are represented in the world
%
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

% user interface to create maze
user_walls = figure(); imagesc(world(:,:,3));
while ishandle(user_walls) == 1;
    try
        [x, y] = ginput(1);
        x=round(x); y=round(y);
        if world(y,x,3) == 3;
            world(y,x,3) = 0;
        else
            world(y,x,3) = 3;
        end
        imagesc(world(:,:,3));
    end
    
end
%}

walls = find(world(:,:,3) == 3);



%% RUN TRIAL

% Begin timestep:
state_cells(agent_y, agent_x) = agent_firing;
initial = find(state_cells);

for time = 1:steps
    
    if time == round(steps/10)
        disp('1/10')
    elseif time == round(2*steps/10)
        disp('2/10')
    elseif time == round(3*steps/10)
        disp('3/10')
    elseif time == round(4*steps/10)
        disp('4/10')
    elseif time == round(5*steps/10)
        disp('5/10')
    elseif time == round(6*steps/10)
        disp('6/10')
    elseif time == round(7*steps/10)
        disp('7/10')
    elseif time == round(8*steps/10)
        disp('8/10')
    elseif time == round(9*steps/10)
        disp('9/10')
    elseif time == round(10*steps/10)
        disp('10/10')
        
    end
    
    % Update state cells
    state_cells(agent_y, agent_x) = agent_firing;
    
    % Calculate the trace value for all cells
    trace = getTrace(state_cells, trace, eta);
    
    % Update synapses using lr * state firing * trace rule
    statetostate_synapses = statetostate_synapses+(learningRate * state_cells(:) * trace(:)');
    
    % Update synapses using lr * trace * state firing
    statetostate_synapses = statetostate_synapses+(learningRate * trace(:) * state_cells(:)');
    
    % Block off self-self synapses
    statetostate_synapses(logical(eye(size(statetostate_synapses)))) = NaN;
    
    % Normalise synapses
    statetostate_synapses = normalise(statetostate_synapses, normalisation_threshold);
    
    % Record agent position
    world_tracked{time} = state_cells;
    world_summed = world_summed + world_tracked{time};
    
    % End timestep. Repeat.
    if time == steps
        final = find(state_cells);
    else
        % Select an action:
        valid_movement_selected = 0;
        world_summed_copy = world_summed;
        world_summed_copy(world(:,:,3) == 3) = NaN;
        
        while valid_movement_selected == 0
            
            
            % find the least visited square next to the current position
            rows = size(world_summed_copy,1);
            col = size(world_summed_copy,2);
            
            current_state = sub2ind(size(world_summed), agent_y, agent_x);
            
            w = current_state-rows; n = current_state-1; s = current_state+1; e = current_state+rows;
            nw = w-1; sw = w+1; ne = e-1; se = e+1;
            
            test = [n e s w nw ne se sw];
            idx = find(test <= 0);
            test(idx) = test(idx) + rows*col;
            idx = find(test > rows*col);
            test(idx) = test(idx) - rows*col;
            
            n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
            
            neighbours = zeros(3);
            
            neighbours(1) = world_summed_copy(nw);
            neighbours(2) = world_summed_copy(w);
            neighbours(3) = world_summed_copy(sw);
            neighbours(4) = world_summed_copy(n);
            neighbours(5) = NaN;
            neighbours(6) = world_summed_copy(s);
            neighbours(7) = world_summed_copy(ne);
            neighbours(8) = world_summed_copy(e);
            neighbours(9) = world_summed_copy(se);
            
            
            next_turn = find(neighbours == min(min(neighbours)));
            if size(next_turn,1) > 1
                %disp('RANDOM ACTION SELECTED')
            end
            
            actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};
            action = actions{next_turn(randi(size(next_turn,1)))};
            %disp(action)
            [world, valid_movement_selected] = update_world4(world, action, 0);
            if valid_movement_selected == 1
                break
            else
                % add noise
                world_summed_copy = world_summed_copy(:)' + 0.001 .* std(world_summed_copy(:)) * randn(1,size(world_summed_copy(:),1));
                world_summed_copy(world_summed_copy < 0) = 0;
                world_summed_copy = reshape(world_summed_copy,num_state);
                disp('Noise added.')
            end
        end
    end
    
    % Update parameters
    state_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
end

% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);


for count = 1:numel(world_tracked)
    
end
figure(); imagesc(world_summed); title('Sum of Time Spent in Each Location')
disp('Complete')
end





function Experiment36(steps, ind_steps, statetostate_synapses, walls)

% Will use the learned place (state) cells to form l.a. as in Exp33, and
% move the agent toward the target.

%% Parameters

% trial
worldSize_x = 20;
worldSize_y = 20;
agent_x = 13;
agent_y = 18;
reward_x = 13;
reward_y = 3;

% World

% Cells
num_state = [worldSize_y worldSize_x];
agent_firing = 0;
reward_firing = 2;

% Synapses
normalisation_threshold = 2;

% Competition
state_modifier = 0.002; %0.05; % 0.2

% Noise
state_noise = 0.02;

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Analysis
activation_tracked = {};
%% Setup
% Create World
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Create grid of place cells
state_cells = zeros(num_state);
trace = zeros(num_state);


%% Walls.
%{
rows = size(state_cells,1);
col = size(state_cells,2);

walls = [];

top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

walls = [walls top_wall];

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

walls = [walls bottom_wall];

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

walls = [walls left_wall];
walls = [walls right_wall];

% Maze walls
%
% H-Maze
wall_a = left_wall + 40;                                            walls = [walls wall_a];
wall_b = left_wall + 120; wall_b = [wall_b(1:4), wall_b(8:20)];     walls = [walls wall_b];
wall_c = left_wall + 200; wall_c = [wall_c(1:10), wall_c(14:20)];   walls = [walls wall_c];
wall_d = left_wall + 280;                                           walls = [walls wall_d];
%}

% Circular Maze

%{
wall_a = left_wall + 20;                                            walls = [walls wall_a];
wall_b = left_wall + 140; wall_b([3:7 14:18]) = [];                 walls = [walls wall_b];
wall_c = right_wall - 40;                                           walls = [walls wall_c];
wall_d = right_wall - 140; wall_d([3:7 14:18]) = [];                walls = [walls wall_d];
wall_e = top_wall + 1;                                              walls = [walls wall_e];
wall_f = top_wall + 7; wall_f([3:7 14:18]) = [];                    walls = [walls wall_f];
wall_g = bottom_wall - 1;                                           walls = [walls wall_g];
wall_h = bottom_wall - 7; wall_h([3:7 14:18]) = [];                 walls = [walls wall_h];
%}

% walls are represented in the world
%
world(:,:,3) = zeros(size(world(:,:,2)));
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;
%}
%% RUN TRIAL
% For each step:
for time = 1:steps
    
    % Allow activity to spread for 1:ind_time
    for ind_time = 1:ind_steps
        %{
    if ind_time == round(ind_steps/4)
        disp('...')
    elseif ind_time == round(ind_steps/2)
        disp('...')
    elseif ind_time == round(3*ind_steps/4)
        disp('...')
    end
        %}
        % Set firing rate of agent & reward representations.
        state_cells(reward_y, reward_x) = reward_firing; %state_cells(agent_y, agent_x) = agent_firing;
        %state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        noNaNstatetostate = statetostate_synapses;
        noNaNstatetostate(isnan(noNaNstatetostate)) = 0;
        
        % Calculate activation of state cells from the recurrent
        % connections within that layer, and save it.
        state_cells = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNstatetostate);
        state_cells = reshape(state_cells,num_state);
        state_cells(reward_y, reward_x) = reward_firing; %state_cells(agent_y, agent_x) = agent_firing;
        %state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        % Competition in state cells:
        
        state_cells(reward_y, reward_x) = reward_firing; %state_cells(agent_y, agent_x) = agent_firing;
        %state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        % subtractive.
        state_cells = state_cells - state_modifier * mean(mean(state_cells));
        
        % divisive.
        state_cells = state_cells/max(max(state_cells));
        state_cells(state_cells < 0) = 0;
        
        state_cells = reshape(state_cells,num_state);
        
        state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
        %state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        % add noise
        state_cells = state_cells(:)' + state_noise .* std(state_cells(:)) * randn(1,size(state_cells(:),1));
        state_cells(state_cells < 0) = 0;
        state_cells = reshape(state_cells,num_state);
        
    end
    
    % Save activation
    activation_display = state_cells;
    activation_display(agent_y, agent_x) = 0;
    activation_display(reward_y, reward_x) = 0;
    activation_tracked{time} = activation_display;
    
    %% Check for Completion
    current_state = sub2ind(size(state_cells), agent_y, agent_x);
    reward_location = sub2ind([worldSize_x worldSize_y], reward_y, reward_x);
    if current_state == reward_location
        result = 'Y';
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Move Agent
    
    valid_movement_selected = 0; count = 0;
    while valid_movement_selected == 0 && count < 500
        count = count+1;
        % find the most active state cell(s) next to the current position
        rows = size(state_cells,1);
        col = size(state_cells,2);
        
        current_state = sub2ind(size(state_cells), agent_y, agent_x);
        
        w = current_state-rows; n = current_state-1; s = current_state+1; e = current_state+rows;
        nw = w-1; sw = w+1; ne = e-1; se = e+1;
        
        test = [n e s w nw ne se sw];
        idx = find(test <= 0);
        test(idx) = test(idx) + rows*col;
        idx = find(test > rows*col);
        test(idx) = test(idx) - rows*col;
        
        n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
        
        neighbours = zeros(3);
        
        neighbours(1) = state_cells(nw);
        neighbours(2) = state_cells(w);
        neighbours(3) = state_cells(sw);
        neighbours(4) = state_cells(n);
        neighbours(5) = 0;
        neighbours(6) = state_cells(s);
        neighbours(7) = state_cells(ne);
        neighbours(8) = state_cells(e);
        neighbours(9) = state_cells(se);
        
        %neighbours = round(neighbours * 100)/100;
        
        next_turn = find(neighbours == max(max(neighbours)));
        if size(next_turn,1) > 1
            disp('RANDOM ACTION SELECTED')
        end
        
        % Activate one cell
        
        actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};
        action = actions{next_turn(randi(size(next_turn,1)))};
        disp(action)
        [world, valid_movement_selected] = update_world4(world, action, 0);
        if valid_movement_selected == 1
            break
        end
    end
    
    %% Update Parameters
    
    state_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

%% Display Activation
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
end


function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end