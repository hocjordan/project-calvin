function testFGM()
clear all
disp('Beginning testing. Please set variable "testing" to true, then press enter.')
pause
profile on
[agent, ~, walls] = ChunkShell([],[]);
ChunkShell(agent, [], walls);
profile off
disp('Ending testing. Please set variable "testing" to false, then press enter.')
pause
end