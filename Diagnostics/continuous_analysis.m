function [world, representation] = continuous_analysis(sensory_size, worldSize, agent, cellNo, sensory_tracked, create_figs)

representationSize = 50;

if create_figs == true; figure(); end

%% Graph of Rough Receptive Field
world = zeros(sensory_size);

values = agent.sensorytoSA_synapses(:, cellNo);
values = reshape(values, sensory_size);

world = world+values;

if create_figs == true; subplot(2, 2, 1); imagesc(world); colorbar; title('Rough Receptive Field'); end

%% Graph of Historical Agent Positions
representation = zeros(representationSize+1);
decoded = sensory_tracked{cellNo};
%decoded = agent.SA_decoded(agent.SA_decoded(:,4)== cellNo,:);
for i = 1:(size(decoded,1))
    xy_pos = [decoded(i,1) decoded(i,2)];
representation = representation + create_world_representation(xy_pos, worldSize, representationSize);
end

if create_figs == true; subplot(2, 2, 2); imagesc(representation); colorbar; title('Historical Agent Positions'); end

%% Important Info

if create_figs == true; h4 = subplot(2, 2, 4); text(0, 1, sprintf('Cell No. is %d\nCell Action is: %d', cellNo, ...
    find(agent.SAtomotor_synapses(cellNo,:)>0.0001)), 'Parent', h4); set(h4, 'Visible', 'off'); end

end