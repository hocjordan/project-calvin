function [map_results] = testStateMap(agent, world, switches, create_figures)

assert(isequal(switches.main.worldType, 'maze'))

switch switches.main.worldType
    case 'maze'
        world_update_function = @maze_update;                                                                                       % Sets the maze_update function (see below) as a function handle that can be given to the learner and controller functions as arguments.
    case 'arm'
        world_update_function = @arm_update;
    case 'keypress'
        world_update_function = @keypress_update;
end

free_states = find(world.state(:,:,3) == 0);
num_states = numel(free_states);
actions = world.actions;
num_actions = numel(actions);
transitionMat = zeros(numel(world.state(:,:,1)), numel(world.state(:,:,1)));
filtered_synapses = agent.synapses > 0.01;
decoded_transitions = zeros(size(transitionMat));

for i = 1:numel(world.state(:,:,1))
    
    if ~ismember(i, free_states)
        continue
    end
    
    world.state(:,:,1) = 0; world.state(i) = 1;
    agent.place_cells(:) = 0; agent.place_cells(i) = 1;
    
    %tmp_fr(end+1, 1:num_actions) = cell(1);
    
    for j = 1:num_actions
        
        action = actions{j};
        
        if isequal(action, '')
            continue
        else
            [~, ~, ~, world2] =  update_world(world, action, switches);
            new_state = find(world2.state(:,:,1));
            transitionMat(i, new_state) = 1;
        end
        
    end
    
end



%{
for i = 1:size(tmp_fr, 1)
for j = 1:size(tmp_fr, 2)
mat_fr(i, j, :) = cell2mat(tmp_fr(i, j));
end
end

IRs = analysis_singleCell(numel(agent.SA_cells(:)), (num_states * num_actions), 1, mat_fr, 1, create_figures, false);
maxinfo = log2(num_states*num_actions);
%}

%state_counter = 0;
for i = 1:numel(world.state(:,:,1))
    
    if ~ismember(i, free_states)
        continue
    end
    
    % BACKWARDS model (I think)
    tmp_transitions = agent.synapses(:, i) > 0.01;
    decoded_transitions(i, :) = tmp_transitions;
end

true_positive = numel(find((decoded_transitions == 1) & transitionMat == 1));
true_negative = numel(find((decoded_transitions ~= 1) & transitionMat~= 1));
false_positive = numel(find((decoded_transitions == 1) & transitionMat ~= 1));
false_negative = numel(find((decoded_transitions ~= 1) & transitionMat == 1));

precision = true_positive / (true_positive + false_positive);
recall = true_positive / (true_positive + false_negative);

if create_figures
    bar_width = 0.4;
    figure(); bar(categorical({'Precision', 'Recall'}), [precision recall]*100, ...
        bar_width); ylabel('Percent')
    
end

map_results = struct();
map_results.tp = true_positive;
map_results.fp = false_positive;
map_results.tn = true_negative;
map_results.fn = false_negative;

map_results.precision = precision;
map_results.recall = recall;

end