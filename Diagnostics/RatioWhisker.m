%{
load('161021_results_with_largeopen_chunks.mat')
ratioWithLargeOpen = ratio;
stdratioWithLargeOpen = std(ratioMat);

load('161021_results_with_open_chunks.mat')
ratioWithOpen = ratio;
stdratioWithOpen = std(ratioMat);

load('161021_results_with_gate_chunks.mat')
ratioWithGate = ratio;
stdratioWithGate = std(ratioMat);

load('161021_results_without_largeopen_chunks.mat')
ratioWithoutLargeOpen = ratio;
stdratioWithoutLargeOpen = std(ratioMat);

load('161021_results_without_open_chunks.mat')
ratioWithoutOpen = ratio;
stdratioWithoutOpen = std(ratioMat);

load('161021_results_without_gate_chunks.mat')
ratioWithoutGate = ratio;
stdratioWithoutGate = std(ratioMat);
%}

function RatioWhisker()

load('161021_results_with_largeopen_chunks.mat')
ratioWithLargeOpen = ratioMat(:);

load('161021_results_with_open_chunks.mat')
ratioWithOpen = ratioMat(:);

load('161021_results_with_gate_chunks.mat')
ratioWithGate = ratioMat(:);

load('161021_results_without_largeopen_chunks.mat')
ratioWithoutLargeOpen = ratioMat(:);

load('161021_results_without_open_chunks.mat')
ratioWithoutOpen = ratioMat(:);

load('161021_results_without_gate_chunks.mat')
ratioWithoutGate = ratioMat(:);


figure()
boxplot([ratioWithGate, ratioWithoutGate ratioWithOpen, ratioWithoutOpen, ratioWithLargeOpen, ratioWithoutLargeOpen])
ylim([0 inf]);
set(gca, 'XTickLabel', {'Gate World (w)' 'Gate World (wo)' 'Open World (w)' 'Open World (wo)' 'Large Open World (w)' 'Large Open World (wo)'})
ylabel('Processing/Steps Ratio')