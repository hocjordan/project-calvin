trials = 10;

stepMat = [];
processingMat = [];
ratioMat = [];

for i = 1:trials
    
    [steps, process, ~, ~] = ChunkMaze_World(false, agent, walls);

    if (steps < 1 || process < 1)
        error('No tracking info being returned.')
    end
    
    stepMat(i) = steps;
    processingMat(i) = process;
    ratioMat(i) = process/steps;
end