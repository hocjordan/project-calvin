function continuous_synapses(cell, agent, tracked, sensorySize, worldSize)
synapses_to = find(agent.SAtoSA_synapses(cell,:) > 0.1);
synapses_from = find(agent.SAtoSA_synapses(:,cell) > 0.1);
figure()
to_plot = zeros(sensorySize);

create_figs = false;

for i = synapses_to
    [world, ~] = continuous_analysis([sensorySize sensorySize], [worldSize worldSize], agent, i, tracked, create_figs);
    to_plot = to_plot + world;
end

[identity_plot, ~] = continuous_analysis([sensorySize sensorySize], [worldSize worldSize], agent, cell, tracked, create_figs);
[from_plot, ~] = continuous_analysis([sensorySize sensorySize], [worldSize worldSize], agent, synapses_from(1), tracked, create_figs);

subplot(2,2,1); imagesc(to_plot); title('Synapse To');
subplot(2,2,2); imagesc(identity_plot); title('Cell');
subplot(2,2,3); imagesc(from_plot); title('Synapse From');
end