function [display] = SA_analyseAct(worldSize, SA_cells, SA_decoded, agent_location, goal_location, display_figure, WTA_activity)

%% Import Arrows
arrows = repmat({uint8(zeros(16))}, [3 3]);
arrows{1} = imread('arrow_nw.png');
arrows{2} = imread('arrow_w.png');
arrows{3} = imread('arrow_sw.png');
arrows{4} = imread('arrow_n.png');
arrows{6} = imread('arrow_s.png');
arrows{7} = imread('arrow_ne.png');
arrows{8} = imread('arrow_e.png');
arrows{9} = imread('arrow_se.png');

for arrow = 1:numel(arrows);
    arrowNormalised = arrows{arrow};
    arrowNormalised(arrowNormalised > 0) = 255;
    arrows{arrow} = arrowNormalised;
end

% Make an array to display the arrows in.
display = repmat({uint8(zeros(16))}, [worldSize*3 worldSize*3]);

%% Retrieve the SA layer's activation and display
% Thresholding by the least active SA cell (within SA_decoded).
minimum_activation = min(min(SA_cells(SA_decoded(:,3))));
SA_cells = SA_cells - minimum_activation;

% Get maximum activation that remains.
maximum_activation = max(max(SA_cells));

if WTA_activity == true
    [~, idx] = max(SA_cells);
    idx = squeeze(idx);
    for i = 1:size(SA_cells, 3)
    SA_cells(:,:,i) = 0;
    SA_cells(idx(i),:,i) = 1;
    end
end

for cell = 1:numel(SA_cells)
    
    % Check that cell has a sensory and motor conneciton i.e. is within
    % SA_decoded.
    if any(ismember(SA_decoded(:,3), cell)) == 1
        
        [y, x] = ind2sub([worldSize worldSize], SA_decoded(SA_decoded(:,3) == cell,1));
        SA_state = (y * 3 - 1) + ((worldSize*3) + (worldSize*3) * 3 * (x-1));
        
        if SA_decoded(SA_decoded(:,3)==cell,1)==agent_location
            display{SA_state}(:) = 255;
        elseif SA_decoded(SA_decoded(:,3)==cell,1)==goal_location
                display{SA_state}(:) = 255;
        else
            display{SA_state}(:) = 100;
        end
        %agent_x = 
        %agent_state = (y * 3 - 1) + ((worldSize*3) + (worldSize*3) * 3 * (x-1));
        %reward_state = (y * 3 - 1) + ((worldSize*3) + (worldSize*3) * 3 * (x-1));
        %[(500 - 0.5), (500 - 0.5), 1, 1]
%rectangle('Position',ROI, 'LineWidth',2,'EdgeColor','y')
%rectangle('Position',ROI, 'LineWidth',2,'EdgeColor','y')

        switch SA_decoded(find(SA_decoded(:,3) == cell),2)
            
            case 1
                display{SA_state - worldSize*3 - 1} = arrows{1} * (SA_cells(cell) / maximum_activation);
                
            case 2
                display{SA_state - worldSize*3} = arrows{2} * (SA_cells(cell) / maximum_activation);
                
            case 3
                display{SA_state - worldSize*3 + 1} = arrows{3} * (SA_cells(cell) / maximum_activation);
                
            case 4
                display{SA_state - 1} = arrows{4} * (SA_cells(cell) / maximum_activation);
                
            case 6
                display{SA_state + 1} = arrows{6} * (SA_cells(cell) / maximum_activation);
                
            case 7
                display{SA_state + worldSize*3 - 1} = arrows{7} * (SA_cells(cell) / maximum_activation);
                
            case 8
                display{SA_state + worldSize*3} = arrows{8} * (SA_cells(cell) / maximum_activation);
                
            case 9
                display{SA_state + worldSize*3 + 1} = arrows{9} * (SA_cells(cell) / maximum_activation);
                
        end
    end
end

if display_figure == true
    try
        figure(); image(cell2mat(display)); colormap(copper); title('Activation (adjusted) in SA layer')
    catch err
        disp(err.message)
    end
end
end