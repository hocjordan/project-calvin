function agent = createAgent(actions, world_observation_function)

disp('Setting up...')

global allowAssertions
global goal_formation

agent = struct();

% Cells
agent.sensory_cells = world_observation_function();
agent.num_sensory = [size(agent.sensory_cells)];                                                            % Get layer size of sensory (state) cells.
agent.num_sensory = [size(agent.sensory_cells)];                                                            % Get layer size of sensory (state) cells.
agent.num_motor = [1 size(actions, 2)];                                                               % Set layer size of motor (action) cells as [1 * number of possible actions].
agent.num_SAcol = prod(agent.num_sensory);                                                                  % Set the number of columns in the SA (state-action) layer as equal to the size of the state layer.
agent.num_SAcellsinCol = prod(agent.num_motor);                                                             % Set the number of cells in each SA column as equal to the number of possible actions.
agent.num_SA = [agent.num_SAcellsinCol 1 agent.num_SAcol];                                                        % Set layer size of SA cells as [number of cells in column * 1 * number of columns]. This is important for competition in CONTROLLER function.
if goal_formation; agent.num_goal = [1 30]; end;

% Synapses
sensorytoSA_dilution = 1; %0.3
motortoSA_dilution = 1; %0.2
SAtomotor_dilution = 1; %0.2

agent.normalisation_threshold = 1;
agent.sensory_threshold = 1; % Necessary to adjust these to compensate for different
agent.motor_threshold = 0.025; % numbers of synapses to sensory and motor cells.
agent.trace_threshold = 4; %0.01

% Make networks of sensory, motor and SA neurons.
agent.motor_cells = zeros(agent.num_motor); if allowAssertions; assert(numel(agent.motor_cells)>0); end;
agent.SA_cells = zeros(agent.num_SA); if allowAssertions; assert(numel(agent.SA_cells)>0); end;
agent.SA_trace = zeros(agent.num_SA); if allowAssertions; assert(numel(agent.SA_cells)>0); end;
if goal_formation; agent.goal_cells = zeros(agent.num_goal); end;

% Create synapse weights between SA cells and motor<=>SA.
agent.SAtoSA_synapses = GenerateZeroWeights(numel(agent.SA_cells), numel(agent.SA_cells), 1); if allowAssertions; assert(numel(agent.SAtoSA_synapses)>0); end;
agent.motortoSA_synapses = Generate_Diluted_Weights(agent.motor_cells, agent.SA_cells, motortoSA_dilution, 1); if allowAssertions; assert(numel(agent.motortoSA_synapses)>0); end;
agent.SAtomotor_synapses = Generate_Diluted_Weights(agent.SA_cells, agent.motor_cells, SAtomotor_dilution, 1); if allowAssertions; assert(numel(agent.SAtomotor_synapses)>0); end;
agent.SArewardtoSA_synapses = zeros(size(agent.SAtoSA_synapses)); if allowAssertions; assert(numel(agent.SArewardtoSA_synapses)>0); end;
agent.SArewardtoSA_synapses(logical(eye(size(agent.SArewardtoSA_synapses)))) = 1; if allowAssertions; assert(numel(agent.SArewardtoSA_synapses)>0); end;
agent.SArewardtoSA_synapses = reshape(agent.SArewardtoSA_synapses, size(agent.SArewardtoSA_synapses)); if allowAssertions; assert(numel(agent.SArewardtoSA_synapses)>0); end;
if goal_formation; agent.goaltoSA_synapses = GenerateZeroWeights(numel(agent.goal_cells), numel(agent.SA_cells), 1); end; if allowAssertions; assert(numel(agent.SAtoSA_synapses)>0); end
if goal_formation; agent.SAtogoal_synapses = agent.goaltoSA_synapses'; end; if allowAssertions; assert(numel(agent.SAtoSA_synapses)>0); end
% Create sensory => SA synapses that are identical for all cells in a
% column.
agent.sensorytoSA_synapses = [];
for column = 1:agent.num_SAcol
    agent.sensorytoSA_synapses = [agent.sensorytoSA_synapses repmat(Generate_Diluted_Weights(agent.sensory_cells, 1, sensorytoSA_dilution, 1), [1 size(actions,2)])];
end

% Normalise synapses
agent.sensorytoSA_synapses = normalise(agent.sensorytoSA_synapses, agent.sensory_threshold, true);
agent.motortoSA_synapses = normalise(agent.motortoSA_synapses, agent.motor_threshold, true);
agent.SAtomotor_synapses = normalise(agent.SAtomotor_synapses', agent.motor_threshold, true);
agent.SAtomotor_synapses = agent.SAtomotor_synapses';

clear agent.sensory_cells

disp('...Setup Complete.')

end