function [sensory_cells, reward_cells] = maze_observe()

% The update function. Takes an action string and updates the world, returning sensory feedback as a layer of sensory cells.

% You should be able to ignore the details of this function, as well as the
% 'calculate_sensory' parameter.

%% Unpackage World Parameters

global world

world_state = world.state;

% find parameters of world
[size_y, size_x] = size(world_state(:,:,1));

% find the position of the agent within the world
[agent_y, agent_x] = find(world_state(:,:,1) == 1);

% find position of the reward within the world
[reward_y, reward_x] = find(world_state(:,:,2) == 2);

world_state(:,:,1) = 0;
world_state(agent_y,agent_x,1) = 1;

%% Reward
world_state(reward_y,reward_x,2) = 1;

%% Produce distributed sensory representation

sensory_cells = world_state(:,:,1);
reward_cells = world_state(:,:,2);

%% Repackage World Parameters

world.state = world_state;

end