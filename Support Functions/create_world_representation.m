function representation = create_world_representation(xy_pos, worldSize, representationSize)

representation = zeros(representationSize+1);
representation_r = round(xy_pos(1) / worldSize(1) * representationSize) + 1;
representation_c = round(xy_pos(2) / worldSize(2) * representationSize) + 1; % +1 accounts for 1-indexing
%if representation_r == 0; representation_r = 1; end;
%if representation_c == 0; representation_c = 1; end;
representation(representation_r, representation_c) = 1;

end