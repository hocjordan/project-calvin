function activity = cellPropagate(postSyn, preSyn1, preSyn2, preSyn3, weights1, weights2, weights3)

global allowAssertions
% Should provide a matrix of activities in the same shape as the postSyn.

if allowAssertions == true
    if sum([numel(preSyn1), numel(preSyn2), numel(preSyn3)]) == 0 || sum([numel(weights1), numel(weights2), numel(weights3)]) == 0
        error('Invalid inputs')
    end
end

%activity = dot(([repmat(preSyn1(:),[1,numel(postSyn)]); repmat(preSyn2(:),[1,numel(postSyn)]); repmat(preSyn3(:),[1,numel(postSyn)])]  ), [weights1; weights2; weights3]);
activity = [preSyn1(:); preSyn2(:); preSyn3(:)]' * [weights1; weights2; weights3];
activity = reshape(activity, size(postSyn));

if allowAssertions == true
    assert(numel(postSyn) > 0);
    assert(all(isreal(activity(:))));
    assert(all(activity(:)>=0));
    assert(isequal(size(activity), size(postSyn)));
    assert(all(isfinite(activity(:))));
end

end