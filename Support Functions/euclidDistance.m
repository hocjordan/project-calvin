function distance = euclidDistance(x1, y1, x2, y2)

loc1 = [x1 y1];
loc2 = [x2 y2];

length1 = (x1 - x2)^2;
length2 = (y1 - y2)^2;

distance = sqrt(length1 + length2);

assert(distance >= 0)

end