function make_continuous(worldSize)

global world

world = struct();

world.agent_r = 5.0;
world.agent_c = 5.0;

world.Size = worldSize;
world.representationSize = 10;

world.actions = {'N', 'E', 'S', 'W', 'NE', 'NW', 'SE', 'SW'};

world.representation = zeros(world.representationSize, world.representationSize);
xy_pos = [world.agent_r, world.agent_c];
representationSize = world.representationSize;
world.representation = create_world_representation(xy_pos, [worldSize worldSize], representationSize);

assert(world.agent_r <= world.Size)
assert(world.agent_c <= world.Size)
assert(world.agent_r >= 0);
assert(world.agent_c >= 0);
assert(isequal(size(world.representation), [world.representationSize+1 world.representationSize+1]));

end