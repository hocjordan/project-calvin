function [reward_x, reward_y, walls] = create_maze(worldSize_x, worldSize_y, randomisedPositions, allowUserWalls, walls)

% Creates the world as described above. Again, you should be able to ignore
% this function.

global world

%   Creates an x by y by 3 matrix
world_state = zeros(worldSize_y,worldSize_x,3);

% Define bounding walls


if exist('walls', 'var') == 1
    
    % add pre-existing walls
    world_state(walls + numel(world_state(:,:,1:2))) = 3;
    
else
    
    % ask for wall input
    walls = [];
    
    top_wall = [];
    top_wall = [top_wall, 1:worldSize_y:worldSize_x*worldSize_y];
    
    walls = [walls top_wall];
    
    bottom_wall = [];
    bottom_wall = [bottom_wall, worldSize_y-1+1:worldSize_y:worldSize_y*worldSize_x];
    
    walls = [walls bottom_wall];
    
    left_wall = 1:worldSize_y;
    right_wall = worldSize_y*(worldSize_x-1)+1:worldSize_y*worldSize_x;
    
    walls = [walls left_wall];
    walls = [walls right_wall];
    
    % add walls into world_state
    walls = unique(walls);
    world_state(walls + numel(world_state(:,:,1:2))) = 3;
    
    % user interface to create maze
    if allowUserWalls == true
        user_walls = figure(); imagesc(world_state(:,:,3)); title('Walls: Click to Add, Click to Remove');
        while ishandle(user_walls) == 1;
            try
                [x, y] = ginput(1);
                x=round(x); y=round(y);
                if world_state(y,x,3) == 3;
                    world_state(y,x,3) = 0;
                else
                    world_state(y,x,3) = 3;
                end
                imagesc(world_state(:,:,3));
            end
        end
    end
    
    walls = find(world_state(:,:,3) == 3);
    
end

if randomisedPositions == true
    
    % Generate positions that are in the world_state and not in the wall
    validAll = false;
    while validAll == false
        [agent_x, agent_y] = makePositions(10, 10);
        [reward_x, reward_y] = makePositions(10, 10);
        if world_state(agent_y, agent_x, 3) ~= 3 && world_state(reward_y, reward_x, 3) ~= 3
            validAll = true;
        end
    end
else
    
    % place agent
    fig = figure(); imagesc(world_state(:,:,3)); title('Please select starting position.');
    [x, y] = ginput(1);
    x=round(x); y=round(y);
    agent_x = x; agent_y = y;
    close(fig)
    
    % place reward
    fig = figure(); imagesc(world_state(:,:,3)); title('Please select goal position.');
    [x, y] = ginput(1);
    x=round(x); y=round(y);
    reward_x = x; reward_y = y;
    close(fig)
    
end

world_state(agent_y,agent_x,1) = 1;
world_state(reward_y,reward_x,2) = 1;

world.state = world_state;
end