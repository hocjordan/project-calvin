function net = broadRF_net(size)

global world

net = struct();
net.num_state = size;
net.state = zeros(net.num_state);
net.centers = zeros(numel(net.state), 2);
net.learningRate = 1;
net.threshold = 1;
net.mu = 0;
net.sigma = 3;

tmp = 0:(world.Size/9):world.Size;
for i = 1:10
    for j = 1:10
        net.centers(10*(i-1)+j, 1) = tmp(j);
        net.centers(10*(i-1)+j, 2) = tmp(i); % to ensure that centers are stored in matrix format i.e. [rows columns]
    end
end

end