function output = continuous_observation()

global world
global sensory_cells

% Update network

for cell = 1:numel(sensory_cells.state)
    distance = euclidDistance(sensory_cells.centers(cell, 1), sensory_cells.centers(cell, 2), world.agent_r, world.agent_c);
    sensory_cells.state(cell) = normpdf(distance, sensory_cells.mu, sensory_cells.sigma);
end

output = sensory_cells.state;

assert(any(output));
assert(all(isfinite(output(:))));

world = makeWorldRep(world);

end