function world = makeWorldRep(world)

xy_pos = [world.agent_r, world.agent_c];
worldSize = world.Size;
representationSize = world.representationSize;
world.representation = create_world_representation(xy_pos, [worldSize worldSize], representationSize);

end