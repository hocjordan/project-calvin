function maze_update(action)

% The update function. Takes an action string and updates the world, returning sensory feedback as a layer of sensory cells.

% You should be able to ignore the details of this function, as well as the
% 'calculate_sensory' parameter.

%% Unpackage World Parameters

global world

world_state = world.state;

% find parameters of world
[size_y, size_x] = size(world_state(:,:,1));

% find the position of the agent within the world
[agent_y, agent_x] = find(world_state(:,:,1) == 1);

% find position of the reward within the world
[reward_y, reward_x] = find(world_state(:,:,2) == 2);

%% Action parameters
% NONE

%% Calculate effect of action.

switch action
    case 'W'
        agent_x = agent_x-1;
    case 'SW'
        agent_x = agent_x-1;
        agent_y = agent_y+1;
    case 'S'
        agent_y = agent_y+1;
    case 'SE'
        agent_x = agent_x+1;
        agent_y = agent_y+1;
    case 'E'
        agent_x = agent_x+1;
    case 'NE'
        agent_x = agent_x+1;
        agent_y = agent_y-1;
    case 'N'
        agent_y = agent_y-1;
    case 'NW'
        agent_x = agent_x-1;
        agent_y = agent_y-1;
end

if agent_x == 0 || agent_x > size(world_state,2) || agent_y == 0 || agent_y > size(world_state,1)
    valid = 0;
    return
end
if world_state(agent_y, agent_x, 3) == 3
    valid = 0;
    return
end

world_state(:,:,1) = 0;
world_state(agent_y,agent_x,1) = 1;


%% Reward
world_state(reward_y,reward_x,2) = 1;

%% Produce distributed sensory representation

sensory_cells = world_state(:,:,1);
reward_cells = world_state(:,:,2);

%% Show figure

%% Repackage World Parameters

world.state = world_state;

end