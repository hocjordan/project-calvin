function agent = resetAgent(agent)

assert(isstruct(agent));

agent.motor_cells(:) = 0;
agent.SA_cells(:) = 0;
agent.SA_trace(:) = 0;
agent.sensory_cells(:) = 0;

end