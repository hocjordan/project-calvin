function [firingRates] = WTA_Competition(firingRates)

% Implements WTA competition. No cell will not fire if no firing cells
% originally. All cells with maximum firing will fire.

sizeWTA = size(firingRates);
assert(any(firingRates(:)))

[~, idx]=nanmax(firingRates(:));

firingRates(:) = 0;
firingRates(idx) = 1;

assert(any(firingRates(:)))
assert(isequal(size(firingRates), sizeWTA));

end