

function continuous_update(direction, distance)

global world

switch direction

    case 'N'
        agent_newr = world.agent_r - distance;
        agent_newc = world.agent_c;
    case 'E'
        agent_newr = world.agent_r;
        agent_newc = world.agent_c + distance;
    case 'S'
        agent_newr = world.agent_r + distance;
        agent_newc = world.agent_c;
    case 'W'
        agent_newr = world.agent_r;
        agent_newc = world.agent_c - distance;
    case 'NE'
        agent_newr = world.agent_r - distance;
        agent_newc = world.agent_c + distance;
    case 'SE'
        agent_newr = world.agent_r + distance;
        agent_newc = world.agent_c + distance;
    case 'SW'
        agent_newr = world.agent_r + distance;
        agent_newc = world.agent_c - distance;
    case 'NW'
        agent_newr = world.agent_r - distance;
        agent_newc = world.agent_c - distance;
    otherwise
        error('Invalid Action')
end

% catch overshoots
dist_r = distance;
dist_c = distance;

if agent_newr > world.Size
    dist_r = abs(world.Size - world.agent_r);
    agent_newr = world.Size;
end

if agent_newc > world.Size
    dist_c = abs(world.Size - world.agent_c);
    agent_newc = world.Size;
end

if agent_newr < 0
    dist_r = abs(world.agent_r); % check this
    agent_newr = 0;
end

if agent_newc < 0
    dist_c = abs(world.agent_c); % check this
    agent_newc = 0;
end

dist = min([dist_r dist_c]);

switch direction
    case 'NW'
        % overshoot north
        if agent_newr == 0
            agent_newc = world.agent_c - dist;
        end
        % overshoot west
        if agent_newc == 0
            agent_newr = world.agent_r - dist;
        end
    case 'SW'
        % overshoot south
        if agent_newr == world.Size
            agent_newc = world.agent_c - dist;
        end
        % overshoot west
        if agent_newc == 0
            agent_newr = world.agent_r + dist;
        end
    case 'SE'
        % overshoot south
        if agent_newr == world.Size
            agent_newc = world.agent_c + dist;
        end
        % overshoot east
        if agent_newc == world.Size
            agent_newr = world.agent_r + dist;
        end
    case 'NE'
        % overshoot north
        if agent_newr == 0
            agent_newc = world.agent_c + dist;
        end
        % overshoot east
        if agent_newc == world.Size
            agent_newr = world.agent_r - dist;
        end
end

world.agent_r = agent_newr;
world.agent_c = agent_newc;

assert(world.agent_r <= world.Size)
assert(world.agent_c <= world.Size)
assert(world.agent_r >= 0);
assert(world.agent_c >= 0);

world = makeWorldRep(world);

end