function postSyn = maxPropagate(postSyn, afferentArray, synapseArray)

% afferents must be cell, so must synapses

for cell = 1:numel(postSyn)
    maxafferent = 0;
    for i = 1:numel(afferentArray)
        afferent = afferentArray{i};
        synapses = synapseArray{i};
        for afferentcell = (find(synapses(:, cell) > 0.01))'
            afferent_activity = (afferent(afferentcell) * synapses(afferentcell, cell));
            if  afferent_activity > maxafferent
                maxafferent = afferent_activity;
            end
        end
    end
    postSyn(cell) = maxafferent;
end

activity = reshape(postSyn, size(postSyn));