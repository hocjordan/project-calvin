function synapses = generateTrueGaussianSynapses(rows, columns, sigma)

mean = 0;

if length(rows) ~= 1 || length(columns) ~= 1;
    error('rows & columns must be scalars')
end

% create zeros synapse matrix (all to all)
synapses = zeros(rows*columns, rows*columns);

% for each presynaptic cell (row)
for i = 1:size(synapses,1)
    
    %  find Cheby Distance to all other cells
    for j = 1:size(synapses,2)
        cheby(j) = chebyshevDistance(rows, columns, i,j);
    end
    
% assign synapse weights to all other cells
synapses(i,:) = normpdf(cheby, mean, sigma);
synapses(i,:) = synapses(i,:)/(max(synapses(i,:)));

end





end