
function SA_analysis(synapses, SA_decoded, worldSize_x, worldSize_y, num_motor)

%% Separate SA cells into columns based on action

% get SA_tracked

% create a 3D index of xy(states) and z(action)

% create a worldSize_x by worldSize_y by numel(num_motor) matrix
index = zeros(worldSize_y, worldSize_x, num_motor(2));

% assign each SA cell to appropriate position
index(12, 1, 1) = SA_decoded(1, 3) % can't assume all positions filled

[x, y] = ind2sub([10 10], SA_decoded(:,1));
z = SA_decoded(:,2);
for cell = 1:size(list_copy, 1);
    index(x(cell), y(cell), z(cell)) = SA_decoded(cell, 3);
end
    
 % use this to produce a graph of which cells are synapsed to
 
    % get the cells that are synapsed to

    [~, idx] = ismember(index, find(b(874,:)));
activation = zeros(size(idx));  
    % for eachlayer
    for z = 1:size(idx,3)
        for y = 1:size(idx, 2)
            for x = 1:size(idx,1)
                
                if idx(x, y, z) ~= 0
    activation(x, y, z) = b(874,idx(x,y,z));
                end
            end
        end
    end
    
    [~, ndx] = ismember(874, list(:,3));
    ndx = list(ndx, 1)
    [x, y] = ind2sub([10 10], world(ndx);
    
    for z = 1:size(idx,3)
    figure(); imagesc(activation(:,:,z));
    end
    %activation(:,:,1) = reshape(b(95,:),[10, 10])
    
end