%% Arm Diagram

% Create Arm
j1_angle = 45;
j2_angle = -45;

% Calculate Arm Position
[j1_pos_x, j1_pos_y, j2_pos_x, j2_pos_y] = Experiment66_forward_arm(j1_angle, j2_angle);

% Draw Figure
armFig = armFigure({j1_angle j2_angle j1_pos_x j1_pos_y j2_pos_x j2_pos_y}, j2_pos_y, j2_pos_y, false); %armAx = gca; set(armAx,'XTickLabel',[]); set(armAx,'YTickLabel',[]);

% Add Sensory Data
x_line = annotation('doublearrow');
set(x_line, 'parent', gca)
set(x_line, 'position', [j2_pos_x j2_pos_y 5-j2_pos_x 0])

y_line = annotation('doublearrow');
set(y_line, 'parent', gca)
set(y_line, 'position', [j2_pos_x j2_pos_y 0 5-j2_pos_y])

j1_line = annotation('arrow');
set(j1_line, 'parent', gca)
set(j1_line, 'position', [0 0 cos(deg2rad(0)) sin(deg2rad(0))])

j2_line = annotation('arrow');
set(j2_line, 'parent', gca)
set(j2_line, 'position', [j1_pos_x j1_pos_y cos(deg2rad(45)) sin(deg2rad(45))])

%% Sensory Cells Diagram
% Create sensory cells
position_cells = Experiment66_position_gauss(j2_pos_x, j2_pos_y, 0.05);
angle_cells = Experiment66_angles_gauss(j1_angle, j2_angle, 0.05);

% WTA
%     position_cells{1} = WTA_Competition(position_cells{1});
%     position_cells{2} = WTA_Competition(position_cells{2});
%     angle_cells{1} = WTA_Competition(angle_cells{1});
%     angle_cells{2} = WTA_Competition(angle_cells{2});

sensory_cells = [position_cells{1}, position_cells{2}, angle_cells{1}, angle_cells{2}];

% Draw Diagram
sensFig = figure(); plot(sensory_cells);
sensAx = gca;

% Add Sensory Labels
set(sensAx,'XTickLabel',[]);
