%% For binary cells
cells = zeros(1, 100);
cells(50) = 1;
figure(); plot(cells); xlabel('Cells'); ylabel('Firing Rate');

%% For normalised cells
normalised_cells = gaussian_cells(100, 0.5, 0.05);
figure(); plot(normalised_cells); xlabel('Cells'); ylabel('Firing Rate');