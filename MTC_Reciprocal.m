function [statetochunk_synapses, chunktostate_synapses] = ManyTrials_Calvin3(worldSize, trials, steps, ind_steps, use_hierarchy, hierarchy_learning, agent_to_reward, swr_recall, statetochunk_synapses, chunktostate_synapses)

% Receives handle @ExperimentXX and iterates it over many trials, saving
% the synapses between chunk and state cells and using them again for the
% next.
%
% Agent position will vary, but reward will not. Should therefore see weak
% sequence learning far from the reward but strong sequence learning close
% to it, where agent paths converge.

%% Options
%use_hierarchy = 1;

if use_hierarchy == 0
    disp('Chunk => State connections INACTIVE.')
else
    disp('Chunk => State Connections ACTIVE')
end

if hierarchy_learning == 1
    disp('Learning ALLOWED')
else
    disp('Learning NOT ALLOWED')
end

if agent_to_reward == 0
    disp('All agent positions possible.')
else
    fprintf('Agent will be placed within %d squares of reward.', agent_to_reward)
end

disp(' ')

if swr_recall == 1
    disp('Each sequence will be replayed upon completion to aid consolidation.')
end

display_synapses = 1;

%% Parameters

% Trials
worldSize_x = worldSize;
worldSize_y = worldSize;

correct = 0;
incorrect = 0;
distance = [];
steps_tracked = 0;
result_tracked = [];

% Cells
num_state = [worldSize_x worldSize_y];
num_chunk = [1 1000]; %300

% Synapses
internal_weights = 0.01;
chunk_weights = 0.00001; % 0.01
radius = 2;
chunk_connectivity = 1; %1

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Analysis
mapping_threshold = 0.0005; % 0.005
chunk_allTrials = [];

%% Generate Synapses

% Generation.
disp('Generating internal state synapses.')

state_cells = zeros(num_state);
chunk_cells = zeros(num_chunk);
rows = size(state_cells,1);
col = size(state_cells,2);

internal_synapses = gaussian_synapses(state_cells, radius, internal_weights);

disp('Generating internal chunk synapses')

inchunk_synapses = GenerateZeroWeights(num_chunk(2), num_chunk(2), 1);

if ~exist('statetochunk_synapses','var')
    disp('Generating state => chunk synapses.')
    statetochunk_synapses = Generate_Diluted_Weights(state_cells, chunk_cells, chunk_connectivity, chunk_weights);
    
    
    %{
    
    statetochunk_synapses = nan(numel(state_cells), numel(chunk_cells));

for presynaptic_cell = 1:numel(chunk_cells)
    statetochunk_synapses(randi(numel(state_cells), 1),presynaptic_cell) = chunk_weights;
end
    %}
    
    
    
    
else
    disp('Using provided state => chunk synapses.')
end
if ~exist('chunktostate_synapses','var')
    disp('Generating chunk => state synapses.')
    %chunktostate_synapses = Generate_Diluted_Weights(chunk_cells, state_cells, chunk_connectivity, chunk_weights);
    chunktostate_synapses = GenerateZeroWeights(numel(chunk_cells), numel(state_cells), chunk_connectivity);
else
    disp('Using provided chunk => state synapses.')
end

%% Walls
disp('Blocking off walls.')

% ascertain position of walls
top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

% walls have no presynaptic connections
internal_synapses(top_wall,:) = NaN;
internal_synapses(bottom_wall,:) = NaN;
internal_synapses(left_wall,:) = NaN;
internal_synapses(right_wall,:) = NaN;

% walls have no postsynaptic connections
internal_synapses(:, top_wall) = NaN;
internal_synapses(:, bottom_wall) = NaN;
internal_synapses(:, left_wall) = NaN;
internal_synapses(:, right_wall) = NaN;

%% Run Trials
for trial = 1:trials
    
    disp(trial)
    %
    reward_x = randi(worldSize);
    reward_y = randi(worldSize);
    
    if agent_to_reward == 0
        agent_x = randi(worldSize);
        agent_y = randi(worldSize);
    else
        agent_x = randi([reward_x-agent_to_reward reward_x+agent_to_reward]);
        agent_y = randi([reward_y-agent_to_reward reward_y+agent_to_reward]);
    end
    %}
    %{
    if randi(2) == 1
    reward_x = 4;
    reward_y = 5;
    agent_x = 7;
    agent_y = 5;
    %else
        reward_x = 15;
        reward_y = 18;
        agent_x = 18;
        agent_y = 18;
    end
    %}
    try
        if agent_x > worldSize_x || agent_y > worldSize_y ...
                error('Specified agent position does not exist in world.')
        elseif reward_x > worldSize_x || reward_y > worldSize_y
            error('Specified reward position does not exist in world.')
        end
        
        % check if agent or reward is within a wall
        agent_location = sub2ind([worldSize_x worldSize_y], agent_y, agent_x);
        reward_location = sub2ind([worldSize_x worldSize_y], reward_y, reward_x);
        
    catch err
        disp(err.message)
        result = [];
        continue
    end
    
    
    
    try
        if ismember(agent_location, top_wall)             || ...
                ismember(agent_location, bottom_wall)     || ...
                ismember(agent_location, left_wall)       || ...
                ismember(agent_location, right_wall)
            
            error('Agent is located in a wall.')
            
        elseif ismember(reward_location, top_wall)         || ...
                ismember(reward_location, bottom_wall)     || ...
                ismember(reward_location, left_wall)       || ...
                ismember(reward_location, right_wall)
            
            error('Reward is located in a wall.')
            
        end
    catch err
        disp(err.message)
        result = 'X';
        result = [];
        continue
    end
    
    try
        [statetochunk_synapses, chunktostate_synapses, inchunk_synapses, result, total_steps, chunk_activation_tracked] = ...
            Timestep29(use_hierarchy, hierarchy_learning, steps, ind_steps, worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y, num_state, num_chunk, internal_synapses, statetochunk_synapses, chunktostate_synapses, inchunk_synapses);
    catch err
        disp(err.message)
        disp(err.stack)
        result = 'X';
    end
    
    % SHARP WAVE RIPPLE -- DELETE AFTER USE!!
    if swr_recall == 1
        try
            [statetochunk_synapses, chunktostate_synapses, result, total_steps] = ...
                Experiment(use_hierarchy, hierarchy_learning, steps, ind_steps, worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y, num_state, num_chunk, internal_synapses, statetochunk_synapses, chunktostate_synapses);
        catch err
            disp(err.message)
            result = 'X';
        end
    end
    %}
    
    if result == 'Y'
        correct = correct + 1;
        steps_tracked = steps_tracked + 1;
        result = 1;
    elseif result == 'N'
        incorrect = incorrect + 1;
        result = 0;
    elseif result == 'X'
        distance(trial) = [];
        result = [];
        continue
    end
    
    result_tracked = [result_tracked result];
    steps_tracked = steps_tracked + total_steps;
    distance = [distance sqrt((agent_x - reward_x)^2 + (agent_y - reward_y)^2)];
    chunk_allTrials = [chunk_allTrials chunk_activation_tracked];
    
end

fprintf('Number of correct trials = %d', correct)
disp(' ')
fprintf('Number of incorrect trials = %d', incorrect)
disp(' ')
fprintf('Average of %f steps taken', steps_tracked/(correct+incorrect));
disp(' ')
fprintf('Chunk cells fired: %s', num2str(sort(chunk_allTrials)));
disp(' ')

distance = int32(distance);
[distance, idx] = sort(distance);
result_tracked = result_tracked(idx);
figure(); plot(distance, result_tracked)

%% Display Final Synapses

if display_synapses == 1;
    
    figure()
    displayed = 0;
    idx = [];
    
    %{
    indices = chunktostate_synapses < mapping_threshold;
    thresholded = chunktostate_synapses;
    thresholded(indices) = 0;
    thresholded(isnan(thresholded)) = 0;
    %}
    
    for cell = 1:numel(chunk_cells)
        if nansum(chunktostate_synapses(cell,:)) > mapping_threshold
        %if any(thresholded(cell,:))
            idx = [idx cell];
        end
    end
    
    %disp(idx)
    
    for count = 1:numel(idx)
        displayed = displayed+1;
        cell = idx(count);
        if mod(displayed,25) == 0;
            displayed = 1;
            figure()
        end
        subplot(5,5,displayed); imagesc(reshape(chunktostate_synapses(cell,:),num_state)); colorbar
        %subplot(5,5,displayed); imagesc(reshape(thresholded(cell,:),num_state)); colorbar
    end
    
end


end
























function [statetochunk_synapses, chunktostate_synapses, inchunk_synapses, result, total_steps, chunk_activation_tracked] = Timestep29(use_hierarchy, hierarchy_learning, steps, ind_steps, worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y, num_state, num_chunk, internal_synapses, statetochunk_synapses, chunktostate_synapses, inchunk_synapses)



% As Expt. 28, with reciprocal connections learnt between chunk cells.



%% Parameters

% world



if agent_x > worldSize_x || agent_y > worldSize_y
    error('Specified agent position does not exist in world.')

elseif reward_x > worldSize_x || reward_y > worldSize_y
    error('Specified reward position does not exist in world.')

end



% attractors
agent_firing = 2;
reward_firing = 0.5; %0.5

% connections
learningRate = 0.00001; %1 % 0.05
eta = 0.8; %0.8

% competition
state_modifier = 0.2;
chunk_modifier = 1.75; %1.75

% noise
state_noise = 0.002;
chunk_noise = 0.5; %1

% normalisation
normalisation_threshold = 0.05; %0.01

% tracking
chunk_activation_tracked = [];



%% Setup



% create world
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);



% create cells
%disp('Creating state cells.')

state_cells = zeros(num_state);
chunk_cells = zeros(num_chunk);
state_trace = zeros(num_state);
chunk_trace = zeros(num_chunk);

%disp('Setup complete.')


%% Run Step:



for time = 1:steps
    
    
    

    %% Induction

    
    % Activate reward state(s)
    
    state_cells(reward_y, reward_x) = reward_firing;
    
    % Activate agent state

    state_cells(agent_y, agent_x) = agent_firing;

    
    % Feed activation back into network

    noNaNinternal = internal_synapses;
    noNaNinternal(isnan(noNaNinternal)) = 0;

    noNaNstatetochunk = statetochunk_synapses;
    noNaNstatetochunk(isnan(noNaNstatetochunk)) = 0;

    noNaNchunktostate = chunktostate_synapses;
    noNaNchunktostate(isnan(noNaNchunktostate)) = 0;

    for ind_time = 1:ind_steps

        

        % Set firing rate of agent & reward representations.
        state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;

        

        % Calculate activation of state cells from the recurrent
        % connections within that layer, and save it.

        state_cells = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNinternal);
        state_cells = reshape(state_cells,num_state);
        state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
        state_save = state_cells;
        

        % Calculate activation of chunk cells from state cells
        chunk_cells = dot(repmat(state_cells(:),[1,numel(chunk_cells)]), noNaNstatetochunk);
        chunk_cells = reshape(chunk_cells,num_chunk);
        
        % Competition in chunk cells:
        
        % Subtractive.
        chunk_cells = chunk_cells - chunk_modifier * mean(mean(chunk_cells));
        chunk_cells(chunk_cells < 0) = 0;
        
        if any(chunk_cells) == 0
            disp('No chunk cells firing')
        else
            % divisive.
            chunk_cells = chunk_cells/max(max(chunk_cells));
        end
        
        % add noise
        chunk_cells = chunk_cells(:)' + chunk_noise * std(chunk_cells(:)) * randn(1,size(chunk_cells(:),1));
        chunk_cells(chunk_cells < 0) = 0;
        chunk_cells = reshape(chunk_cells,num_chunk);

        
        if use_hierarchy == 1;
            
            % Reset state cells.
            state_cells(:) = 0;
            
            %Calculate activation of state cells from the combined input of
            %recurrent and hierarchical synapses
            state_cells = dot([repmat(state_save(:),[1,numel(state_cells)]); repmat(chunk_cells(:),[1,numel(state_cells)])], [noNaNinternal; noNaNchunktostate]);
            state_cells = reshape(state_cells,num_state);
        end
        
        % Competition in state cells:
       
        state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
        
        % subtractive.
        state_cells = state_cells - state_modifier * mean(mean(state_cells));
        
        % divisive.
        state_cells = state_cells/max(max(state_cells));
        state_cells(state_cells < 0) = 0;
        
        state_cells = reshape(state_cells,num_state);
        
        state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
        
        % add noise
        state_cells = state_cells(:)' + state_noise .* std(state_cells(:)) * randn(1,size(state_cells(:),1));
        state_cells(state_cells < 0) = 0;
        state_cells = reshape(state_cells,num_state);
        
        activation_display = vec2mat(state_cells, sqrt(numel(state_cells)));
        chunk_activation_display = find(chunk_cells > 0.5);
        
        if hierarchy_learning == 1
            % LEARNING:
            % Uses a reversed trace rule -- preSynaptic_trace * postSynaptic_fr
            
            % Calculate memory trace for state and chunk cells.
            chunk_trace = getTrace(chunk_cells, chunk_trace, eta);
            state_trace = getTrace(state_cells, state_trace, eta);
           
            % Update synapses from chunk to state cells.
            chunktostate_synapses = chunktostate_synapses + learningRate * chunk_trace(:) * state_cells(:)';
            statetochunk_synapses = statetochunk_synapses + learningRate * state_cells(:) * chunk_trace(:)';
            inchunk_synapses = inchunk_synapses + learningRate * chunk_trace(:) * chunk_cells(:)';
            inchunk_synapses = inchunk_synapses + learningRate * chunk_cells(:) * chunk_trace(:)';
           
            % Normalise synapse weights.
            statetochunk_synapses = normalise(statetochunk_synapses, normalisation_threshold);
            chunktostate_synapses = normalise(chunktostate_synapses', normalisation_threshold);
            chunktostate_synapses = chunktostate_synapses';
            inchunk_synapses = normalise(inchunk_synapses);
        end
        
    end
    %disp('Induction complete.')
    
    %% Gather Step Data
    
    % Save activation pathway
    activation_tracked{time} = activation_display;
    chunk_activation_tracked = [chunk_activation_tracked chunk_activation_display];
    world_tracked{time} = world(:,:,1);
    
    
    %% Check for Completion
    current_state = sub2ind(size(state_cells), agent_y, agent_x);
    reward_location = sub2ind([worldSize_x worldSize_y], reward_y, reward_x);
    if current_state == reward_location
        result = 'Y';
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Move Agent
    
    % find the most active state cell(s) next to the current position
    rows = size(state_cells,1);
    col = size(state_cells,2);
    
    current_state = sub2ind(size(state_cells), agent_y, agent_x);
    
    w = current_state-rows; n = current_state-1; s = current_state+1; e = current_state+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    test = [n e s w nw ne se sw];
    idx = find(test <= 0);
    test(idx) = test(idx) + rows*col;
    idx = find(test > rows*col);
    test(idx) = test(idx) - rows*col;
    
    n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);

    neighbours = zeros(3);
    
    neighbours(1) = state_cells(nw);
    neighbours(2) = state_cells(w);
    neighbours(3) = state_cells(sw);
    neighbours(4) = state_cells(n);
    neighbours(5) = 0;
    neighbours(6) = state_cells(s);
    neighbours(7) = state_cells(ne);
    neighbours(8) = state_cells(e);
    neighbours(9) = state_cells(se);
    
    %neighbours = round(neighbours * 100)/100;

    next_turn = find(neighbours == max(max(neighbours)));
    if size(next_turn,1) > 1
        disp('RANDOM ACTION SELECTED')
    end
    
    % Activate one cell

    actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};
    action = actions{next_turn(randi(size(next_turn,1)))};
    disp(action)
    world = update_world2(world, action);
   
    
    %% Update Parameters
    
    state_cells(:) = 0;
    chunk_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

%disp(sort(chunk_activation_tracked))
chunk_activation_tracked = unique(chunk_activation_tracked);

end















function matrix = normalise(matrix, threshold)



% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.



%get number of rows and columns in matrix



[rows, columns] = size(matrix);



% sum each column

summed = nansum(matrix);

for column = 1:columns

    if summed(column) > threshold

        %divide each row in that column by that sum

        for row = 1:rows

            matrix(row,column) = threshold * (matrix(row,column)/summed(column));

        end

    end

end

end





function [trace] = getTrace(firingRate, trace, eta)



trace = ((1-eta)*firingRate) + eta*trace;

end
