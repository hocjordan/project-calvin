%% Bidirectional Test 1

function activation_display = Experiment19(steps)

% As Expt. 14, with the ability to control gaussian ranges and wall thickness.

%% Parameters
% world
worldSize_x = 20;
worldSize_y = 20;
agent_x = 15;
agent_y = 8;
reward_x = 9;
reward_y = 18;

if agent_x > worldSize_x || agent_y > worldSize_y
    error('Specified agent position does not exist in world.')
elseif reward_x > worldSize_x || reward_y > worldSize_y
    error('Specified reward position does not exist in world.')
end

% wall
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;
    
% cells
num_state = [worldSize_x worldSize_y];

% attractors
agent_firing = 2;
reward_firing = 1;

% connections
radius = 10;
internal_weights = 0.01;
learningRate = 1;

% competition
mean_modifier = 0.1;
sparseness = 99;
slope = 1;

% analysis
mapping_threshold = 0.2;

%% Setup
disp('Setup begun.')

% create world

disp('Creating world.')
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% create state cells

disp('Creating state cells.')
state_cells = zeros(num_state);

% generate bidirectional internal synapses

disp('Generating synapses.')
internal_synapses = zeros(numel(state_cells));

rows = size(state_cells,1);
col = size(state_cells,2);

internal_synapses = gaussian_synapses(state_cells, radius, internal_weights);

disp('Blocking off walls.')

% ascertain position of walls
top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

% walls have no presynaptic connections
internal_synapses(top_wall,:) = NaN;
internal_synapses(bottom_wall,:) = NaN;
internal_synapses(left_wall,:) = NaN;
internal_synapses(right_wall,:) = NaN;

% walls have no postsynaptic connections
internal_synapses(:, top_wall) = NaN;
internal_synapses(:, bottom_wall) = NaN;
internal_synapses(:, left_wall) = NaN;
internal_synapses(:, right_wall) = NaN;

% check if agent or reward is within a wall
agent_location = find(world(:,:,1),1);
reward_location = find(world(:,:,2),2);

if ismember(agent_location, top_wall)             || ...
        ismember(agent_location, bottom_wall)     || ...
        ismember(agent_location, left_wall)       || ...
        ismember(agent_location, right_wall)
    
    error('Agent is located in a wall.')
    
elseif ismember(reward_location, top_wall)         || ...
        ismember(reward_location, bottom_wall)     || ...
        ismember(reward_location, left_wall)       || ...
        ismember(reward_location, right_wall)
    
    error('Reward is located in a wall.')
    
end

disp('Setup complete.')

%% Induction

disp('Induction begun:')
% Activate agent state
state_cells(agent_y, agent_x) = agent_firing;

% Activate reward state(s)
state_cells(reward_y, reward_x) = reward_firing;

% Feed activation back into network
noNaNweights = internal_synapses;
noNaNweights(isnan(noNaNweights)) = 0;

for time = 1:steps
    
    if time == round(steps/4)
        disp('...')
    elseif time == round(steps/2)
        disp('...')
    elseif time == round(3*steps/4)
        disp('...')
    end
    
state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
activation = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNweights);
state_cells = reshape(activation,num_state);

% Competition:

state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;

% subtractive
state_cells = state_cells - mean_modifier * mean(mean(state_cells));

% divisive
state_cells = state_cells/max(max(state_cells));
state_cells(state_cells < 0) = 0;

state_cells = reshape(state_cells,num_state);

state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
activation_display = vec2mat(state_cells, sqrt(numel(state_cells)));
activation_tracked{time} = activation_display;

end
disp('Induction complete.')

%% Analyse Data
% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]); colorbar

end