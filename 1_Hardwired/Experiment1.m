%% Trace Experiment

function Experiment1(places, steps, dilution)

% Creates a network of place cells connected together with trace-learning
% synapses. Dilution as specified by 'dilution' variable. Normalises and
% displays synapses at end of test.

% Assign parameters
eta = 0.5;
learningRate = 1;

% Create grid of place cells
place_cells = zeros(places);
trace = zeros(places);

% Create synapses with full connectivity, save for self-self
synapses = GenerateZeroWeights(numel(place_cells), numel(place_cells), dilution);

% Begin timestep:
for time = 1:steps
    % Activate one cell
    place_cells(randi(numel(place_cells))) = 1
    
    % Calculate the trace value for all cells
    trace = getTrace(place_cells, trace, eta);
    
    % Calculate the firing rate of all cells
    synapses = synapses+(learningRate*(repmat(place_cells(:),[1,numel(place_cells)]) .* repmat(trace(:)',[numel(place_cells),1])));
    synapses(logical(eye(size(synapses)))) = NaN;
    
    % Normalise synapses
    synapses = normalise(synapses)
    
    % End timestep. Repeat.
    place_cells(:) = 0;
end

% Print final synapse values.
disp(synapses);

end

function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)
    
    trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end

function y = normalise(matrix)

% Normalise matrix so that each column's sum approaches 1. Ignores NaN.

%get number of rows and columns in matrix

[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);
for column = 1:columns
    if summed(column) ~= 0
        %divide each row in that column by that sum
        for row = 1:rows
            matrix(row,column) = (matrix(row,column)/summed(column));
        end
    end
end
y = matrix;


end