%% Trace Experiment 5

function Experiment5(steps)

% As Experiment 4, but allows for variable number of SA cells.


% Parameters
eta = 0.8;
sparseness = 70;
slope = 90;
learningRate = 0.004;
size_x = 4;
size_y = 4;
agent_x = 2;
agent_y = 2;
reward_x = 3;
reward_y = 3;
mapping_threshold = 0.6;

%% SETUP TRIAL


% Create 4x4 world and place agent within it.
world = create_world2(size_x, size_y, agent_x, agent_y, reward_x, reward_y);

% Make networks of sensory (4x4), motor (4) and SA (4x4x4) neurons.
place_cells = zeros(4);
motor_cells = zeros(1,4);
SA_cells = zeros(12);

% Create synapse weights between sensory-SA and motor-SA.
sensory_weights = Generate_Diluted_Weights(numel(SA_cells), numel(place_cells), 0.3, 1);
motor_weights = Generate_Diluted_Weights(numel(SA_cells), 4, 0.2, 1);

%% RUN TRIAL
% Begin timestep:
disp('Running Timesteps')
disp('...')
for time = 1:steps
    
    if time == round(steps/4)
        disp('...')
    elseif time == round(steps/2)
        disp('...')
    elseif time == round(3*steps/4)
        disp('...')
    end
    
    % Sensory cells fire according to agent position (1 cell per place).
    place_cells = world(:,:,1);
    
    % SA cells fire based on current state (sensory input).
    noNaNweights = sensory_weights;
    noNaNweights(isnan(noNaNweights)) = 0;
    SA_cells = dot((repmat(place_cells(:),[1,numel(SA_cells)])), noNaNweights);
    
    % Agent moves.
    actions = {'N' 'E' 'S' 'W'};
    motor_cells(:) = 0;
    motor_cells(randi(4)) = 1;
    action = actions{find(motor_cells)};
    %disp(action)
    world = update_world2(world, action);
    
    % SA cells activate based on combined state (original) and action inputs.
    noNaNweights = [sensory_weights; motor_weights];
    noNaNweights(isnan(noNaNweights)) = 0;
    activation = dot((repmat([place_cells(:); motor_cells'], [1,numel(SA_cells)])), noNaNweights);
    
    % Competition
    SA_cells = WTA_Competition(activation);
    %disp(find(SA_cells))
    
    % Weights updated.
    sensory_weights = sensory_weights + (learningRate * (place_cells(:) * SA_cells));
    motor_weights = motor_weights + (learningRate * (motor_cells(:) * SA_cells));
    
    % Weights normalised
    sensory_weights = normalise(sensory_weights);
    motor_weights = normalise(motor_weights);
    
% End timestep:
world_tracked{time} = world(:,:,1);
end
disp('Complete')

%% ANALYSE RESULTANT MAPPING
% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
disp('Complete')

disp('Calculating state/action mappings.')
[mappings, firingRates,] = HML_mappings(sensory_weights, motor_weights, mapping_threshold);
disp('Complete')

end















function y = normalise(matrix)

% Normalise matrix so that each column's sum approaches 1. Ignores NaN.

%get number of rows and columns in matrix

[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);
for column = 1:columns
    %divide each row in that column by that sum
    for row = 1:rows
        matrix(row,column) = (matrix(row,column)/summed(column));
    end
end
y = matrix;


end
