%% Bidirectional Test 3

function activation_display = Experiment13(steps, ind_steps)

% As experiment 10, but incorporates a second layer of (hardwired) hierarchical cells, encoding sequences of movement to pass through the 'doors'.

%% Parameters
% world
worldSize_x = 20;
worldSize_y = 20;
agent_x = 14;
agent_y = 13;
reward_x = 8;
reward_y = 3;

if agent_x > worldSize_x || agent_y > worldSize_y
    error('Specified agent position does not exist in world.')
elseif reward_x > worldSize_x || reward_y > worldSize_y
    error('Specified reward position does not exist in world.')
end

% cells
num_state = [worldSize_x worldSize_y];
num_chunk = [1 8];

% attractors
agent_firing = 0.1;
reward_firing = 2;

% connections
internal_weights = 1;
chunk_weights = 1;
learningRate = 1;

% competition
mean_modifier = 0.01;

%% Setup
disp('Setup begun.')

% create world

disp('Creating world.')
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% create cells

disp('Creating cells.')

chunk_cells = zeros(num_chunk);
state_cells = zeros(num_state);

% generate synapses

disp('Generating synapses.')

% create bidirectional internal synapses
internal_synapses = nan(numel(state_cells));
rows = size(state_cells,1);
col = size(state_cells,2);
for presynaptic_cell = 1:numel(state_cells)
    
    if presynaptic_cell == round(numel(state_cells)/4)
        disp('...')
    elseif presynaptic_cell == round(numel(state_cells)/2)
        disp('...')
    elseif presynaptic_cell == round(3*numel(state_cells)/4)
        disp('...')
    end
    
    w = presynaptic_cell-rows; n = presynaptic_cell-1; s = presynaptic_cell+1; e = presynaptic_cell+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    test = [n e s w nw ne se sw];
    idx = find(test <= 0);
    test(idx) = test(idx) + rows*col;
    idx = find(test > rows*col);
    test(idx) = test(idx) - rows*col;
    
    n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
    
    internal_synapses(presynaptic_cell,n) = internal_weights;
    internal_synapses(presynaptic_cell,e) = internal_weights;
    internal_synapses(presynaptic_cell,s) = internal_weights;
    internal_synapses(presynaptic_cell,w) = internal_weights;
    internal_synapses(presynaptic_cell,nw) = internal_weights;
    internal_synapses(presynaptic_cell,ne) = internal_weights;
    internal_synapses(presynaptic_cell,se) = internal_weights;
    internal_synapses(presynaptic_cell,sw) = internal_weights;
    
end

% delete synapses to 'walls' so that they cannot become active
%vertical - to
internal_synapses(:,round((rows*(col/2)))+1:round((rows*(col/2)))+round((rows/5))) = NaN;
internal_synapses(:,round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5)) = NaN;
internal_synapses(:,round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows) = NaN;
% top/bottom
internal_synapses(:,rows:rows:rows*col) = NaN;
internal_synapses(:,1:rows:rows*col) = NaN;
% left/right
internal_synapses(:,1:rows) = NaN;
internal_synapses(:,(rows-1)*col + 1 : rows*col) = NaN;
% horizontal
internal_synapses(:,rows/2:rows:rows*col/5) = NaN; internal_synapses(:,rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5)) = NaN; internal_synapses(:,rows/2 + rows*(4*col/5):rows:rows*col) = NaN;

% check if agent or reward was placed within one of these walls
agent_location = find(world(:,:,1),1);
reward_location = find(world(:,:,2),2);

if ismember(agent_location, (round((rows*(col/2)))+1:round((rows*(col/2)))+round((rows/5))))                          || ...
        ismember(agent_location, (round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5)))     || ...
        ismember(agent_location, (round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows))                  || ...
        ismember(agent_location, (rows:rows:rows*col))                                                                || ...
        ismember(agent_location, (1:rows:rows*col))                                                                   || ...
        ismember(agent_location, (1:rows))                                                                            || ...
        ismember(agent_location, ((rows-1)*col + 1 : rows*col))                                                       || ...
        ismember(agent_location, (rows/2:rows:rows*col/5))                                                            || ...
        ismember(agent_location, (rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5)))                              || ...
        ismember(agent_location, (rows/2 + rows*(4*col/5):rows:rows*col))
    
    error('Agent is located in a wall.')
    
elseif ismember(reward_location, (round((rows*(col/2)))+1:round((rows*(col/2)))+round((rows/5))))                      || ...
        ismember(reward_location, (round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5)))     || ...
        ismember(reward_location, (round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows))                  || ...
        ismember(reward_location, (rows:rows:rows*col))                                                                || ...
        ismember(reward_location, (1:rows:rows*col))                                                                   || ...
        ismember(reward_location, (1:rows))                                                                            || ...
        ismember(reward_location, ((rows-1)*col + 1 : rows*col))                                                       || ...
        ismember(reward_location, (rows/2:rows:rows*col/5))                                                            || ...
        ismember(reward_location, (rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5)))                              || ...
        ismember(reward_location, (rows/2 + rows*(4*col/5):rows:rows*col))   
    
    error('Reward is located in a wall.')
    
end

% create hierarchical synapses (sequences) from cell (9,6) to (13,6) and reverse,
% (9,14) to (13,14) and reverse, (6,8) to (6,12) and reverse, and finally
% (15,8) to (15,12) and reverse. Remember that x and y are reversed.

chunktostate_synapses = nan(numel(chunk_cells),numel(state_cells));


chunktostate_synapses(1, (sub2ind(size(state_cells), 6, 9)):rows:sub2ind(size(state_cells), 6, 13)) = chunk_weights;
chunktostate_synapses(3, (sub2ind(size(state_cells), 14, 9)):rows:sub2ind(size(state_cells), 14, 13)) = chunk_weights;
chunktostate_synapses(5, (sub2ind(size(state_cells), 8, 6)):sub2ind(size(state_cells), 12, 6)) = chunk_weights;
chunktostate_synapses(7, (sub2ind(size(state_cells), 8, 15)):sub2ind(size(state_cells), 12, 15)) = chunk_weights;

chunktostate_synapses(2, (sub2ind(size(state_cells), 6, 9)):rows:sub2ind(size(state_cells), 6, 13)) = chunk_weights;
chunktostate_synapses(4, (sub2ind(size(state_cells), 14, 9)):rows:sub2ind(size(state_cells), 14, 13)) = chunk_weights;
chunktostate_synapses(6, (sub2ind(size(state_cells), 8, 6)):sub2ind(size(state_cells), 12, 6)) = chunk_weights;
chunktostate_synapses(8, (sub2ind(size(state_cells), 8, 15)):sub2ind(size(state_cells), 12, 15)) = chunk_weights;

statetochunk_synapses = nan(numel(state_cells), numel(chunk_cells));

%
statetochunk_synapses((sub2ind(size(state_cells), 6, 9)) ,1) = chunk_weights;
statetochunk_synapses((sub2ind(size(state_cells), 14, 9)) ,3) = chunk_weights;
statetochunk_synapses((sub2ind(size(state_cells), 8, 6)) ,5) = chunk_weights;
statetochunk_synapses((sub2ind(size(state_cells), 8, 15)) ,7) = chunk_weights;
%}

%{
statetochunk_synapses((sub2ind(size(state_cells), 6, 13)) ,2) = chunk_weights;
statetochunk_synapses((sub2ind(size(state_cells), 14, 13)) ,4) = chunk_weights;
statetochunk_synapses((sub2ind(size(state_cells), 12, 6)) ,6) = chunk_weights;
statetochunk_synapses((sub2ind(size(state_cells), 12, 15)) ,8) = chunk_weights;
%}

disp('Setup complete.')

%% Run Step:

for time = 1:steps
    disp(time)
    
    %% Induction
    
    disp('Induction begun:')
    % Activate agent state
    state_cells(agent_y, agent_x) = agent_firing;
    
    % Activate reward state(s)
    state_cells(reward_y, reward_x) = reward_firing;
    
    % Feed activation back into network
    noNaNinternal = internal_synapses;
    noNaNinternal(isnan(noNaNinternal)) = 0;
    
    noNaNstatetochunk = statetochunk_synapses;
    noNaNstatetochunk(isnan(noNaNstatetochunk)) = 0;
    
    noNaNchunktostate = chunktostate_synapses;
    noNaNchunktostate(isnan(noNaNchunktostate)) = 0;
    
    for ind_time = 1:ind_steps
        
        if ind_time == round(ind_steps/4)
            disp('...')
        elseif ind_time == round(ind_steps/2)
            disp('...')
        elseif ind_time == round(3*ind_steps/4)
            disp('...')
        end
        
        % state_cells(agent_y, agent_x) = max(max(state_cells)); state_cells(reward_y, reward_x) = max(max(state_cells));
        state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        % calc activation of state cells from recurrent connections and
        % save
        state_cells = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNinternal);
        state_cells = reshape(state_cells,num_state);
        state_save = state_cells;
        % calc activation of hierarchical cells from state cells
        chunk_cells = dot(repmat(state_cells(:),[1,numel(chunk_cells)]), noNaNstatetochunk);
        chunk_cells = reshape(chunk_cells,num_chunk);
        % reset state cells and calc activation from recurrent connections and hierarchical inputs
        %state_cells = state_cells(:)' + dot(repmat(chunk_cells(:),[1,numel(state_cells)]), noNaNchunktostate);
        state_cells(:) = 0;
        state_cells = dot([repmat(state_save(:),[1,numel(state_cells)]); repmat(chunk_cells(:),[1,numel(state_cells)])], [noNaNinternal; noNaNchunktostate]);
        state_cells = reshape(state_cells,num_state);
        
        % competition
        state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        state_cells = state_cells - mean_modifier * mean(mean(state_cells));
        state_cells = state_cells/max(max(state_cells));
        state_cells(state_cells < 0) = 0;
        %state_cells = softCompetition(sparseness, slope, state_cells(:));
        state_cells = reshape(state_cells,num_state);
        state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        activation_display = vec2mat(state_cells, sqrt(numel(state_cells)));
        
    end
    disp('Induction complete.')
    
    %% Gather Step Data
    
    % Save activation pathway
    activation_tracked{time} = activation_display;
    
    % Save
    world_tracked{time} = world(:,:,1);
    
    %% Move Agent
    
    % find the most active state cell(s) next to the current position
    rows = size(state_cells,1);
    col = size(state_cells,2);
    
    current_state = sub2ind(size(state_cells), agent_y, agent_x);
    
    w = current_state-rows; n = current_state-1; s = current_state+1; e = current_state+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    test = [n e s w nw ne se sw];
    idx = find(test <= 0);
    test(idx) = test(idx) + rows*col;
    idx = find(test > rows*col);
    test(idx) = test(idx) - rows*col;
    
    n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
    
    neighbours = zeros(3);
    
    neighbours(1) = state_cells(nw);
    neighbours(2) = state_cells(w);
    neighbours(3) = state_cells(sw);
    neighbours(4) = state_cells(n);
    neighbours(5) = 0;
    neighbours(6) = state_cells(s);
    neighbours(7) = state_cells(ne);
    neighbours(8) = state_cells(e);
    neighbours(9) = state_cells(se);
    
    %neighbours = round(neighbours * 100)/100;
    next_turn = find(neighbours == max(max(neighbours)));
    if size(next_turn,1) > 1
        disp('RANDOM ACTION SELECTED')
    end
    
    % Activate one cell
    actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};
    action = actions{next_turn(randi(size(next_turn,1)))};
    disp(action)
    world = update_world2(world, action);
    
     if ismember(2, neighbours) == 1
        result = 'Y';
        disp(result)
        break
    elseif time == steps
        result = 'N';
        disp(result)
        break
    end
    
    %% Update Parameters
    
    state_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

%% Analyse Step

slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1])
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1])

end