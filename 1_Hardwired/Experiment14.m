%% Bidirectional Test 1

function activation_display = Experiment14(steps)

% As Expt. 8, but connects cells to neighbours cell via
% Gaussian synaptic weights proportional to the distance between those
% cells.

%% Parameters
% world
worldSize_x = 20;
worldSize_y = 20;
agent_x = 15;
agent_y = 9;
reward_x = 15;
reward_y = 12;

if agent_x > worldSize_x || agent_y > worldSize_y
    error('Specified agent position does not exist in world.')
elseif reward_x > worldSize_x || reward_y > worldSize_y
    error('Specified reward position does not exist in world.')
end
    
% cells
num_place = [worldSize_x worldSize_y];
num_state = [worldSize_x worldSize_y];

% attractors
agent_firing = 2;
reward_firing = 1;

% connections
weakinternal_weights = NaN;
internal_weights = 1;
learningRate = 1;

% competition
mean_modifier = 2;
sparseness = 99;
slope = 1;

% analysis
mapping_threshold = 0.2;

%% Setup
disp('Setup begun.')

disp('Creating world.')
% create world
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

disp('Creating cells.')
% create place_cells
place_cells = zeros(num_place);

% create state cells
state_cells = zeros(num_state);

% create reward representation
reward_rep = 0;

disp('Generating synapses.')
% create bidirectional internal synapses
internal_synapses = zeros(numel(state_cells));
internal_synapses(:) = weakinternal_weights;
rows = size(state_cells,1);
col = size(state_cells,2);
for presynaptic_cell = 1:numel(state_cells)
    
    if presynaptic_cell == round(numel(state_cells)/4)
        disp('...')
    elseif presynaptic_cell == round(numel(state_cells)/2)
        disp('...')
    elseif presynaptic_cell == round(3*numel(state_cells)/4)
        disp('...')
    end
    
    w = presynaptic_cell-rows; n = presynaptic_cell-1; s = presynaptic_cell+1; e = presynaptic_cell+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    nn = n-1; ss = s+1; ee = e+rows; ww = w-rows;
    nnw = nw-1; nne = ne-1; ssw = sw+1; sse = se+1;
    wnw = ww-1; ene = ee-1; wsw = ww+1; ese = ee+1;
    nwnw = wnw-1; nene = ene-1; swsw = wsw+1; sese = ese+1;
    
    test = [n e s w nw ne se sw nn ss ee ww nnw nne ssw sse wnw ene wsw ese nwnw nene swsw sese];
    idx = find(test <= 0);
    test(idx) = test(idx) + rows*col;
    idx = find(test > rows*col);
    test(idx) = test(idx) - rows*col;
    
    n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
        nn = test(9); ss = test(10); ee = test(11); ww = test(12); nnw = test(13); nne = test(14); ssw = test(15); sse = test(16);
        wnw = test(17); ene = test(18); wsw = test(19); ese = test(20); nwnw = test(21); nene = test(22); swsw = test(23); sese = test(24);
    
    internal_synapses(presynaptic_cell,n) = internal_weights;
    internal_synapses(presynaptic_cell,e) = internal_weights;
    internal_synapses(presynaptic_cell,s) = internal_weights;
    internal_synapses(presynaptic_cell,w) = internal_weights;
    internal_synapses(presynaptic_cell,nw) = internal_weights;
    internal_synapses(presynaptic_cell,ne) = internal_weights;
    internal_synapses(presynaptic_cell,se) = internal_weights;
    internal_synapses(presynaptic_cell,sw) = internal_weights;
    internal_synapses(presynaptic_cell,nn) = internal_weights/5;
    internal_synapses(presynaptic_cell,ss) = internal_weights/5;
    internal_synapses(presynaptic_cell,ee) = internal_weights/5;
    internal_synapses(presynaptic_cell,ww) = internal_weights/5;
    internal_synapses(presynaptic_cell,nnw) = internal_weights/5;
    internal_synapses(presynaptic_cell,nne) = internal_weights/5;
    internal_synapses(presynaptic_cell,ssw) = internal_weights/5;
    internal_synapses(presynaptic_cell,sse) = internal_weights/5;
    internal_synapses(presynaptic_cell,wnw) = internal_weights/5;
    internal_synapses(presynaptic_cell,ene) = internal_weights/5;
    internal_synapses(presynaptic_cell,wsw) = internal_weights/5;
    internal_synapses(presynaptic_cell,ese) = internal_weights/5;
    internal_synapses(presynaptic_cell,nwnw) = internal_weights/5;
    internal_synapses(presynaptic_cell,nene) = internal_weights/5;
    internal_synapses(presynaptic_cell,swsw) = internal_weights/5;
    internal_synapses(presynaptic_cell,sese) = internal_weights/5;
    
end

% walls
%vertical - to
internal_synapses(:,round((rows*(col/2)))+1:round((rows*(col/2)))+round((rows/5))) = NaN;
internal_synapses(:,round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5)) = NaN;
internal_synapses(:,round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows) = NaN;
% top/bottom
internal_synapses(:,rows:rows:rows*col) = NaN;
internal_synapses(:,1:rows:rows*col) = NaN;
% left/right
internal_synapses(:,1:rows) = NaN;
internal_synapses(:,(rows-1)*col + 1 : rows*col) = NaN;
% horizontal
internal_synapses(:,rows/2:rows:rows*col/5) = NaN; internal_synapses(:,rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5)) = NaN; internal_synapses(:,rows/2 + rows*(4*col/5):rows:rows*col) = NaN;

% check if agent or reward is within a wall
agent_location = find(world(:,:,1),1);
reward_location = find(world(:,:,2),2);

if ismember(agent_location, (round((rows*(col/2)))+1:round((rows*(col/2)))+round((rows/5))))                          || ...
        ismember(agent_location, (round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5)))     || ...
        ismember(agent_location, (round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows))                  || ...
        ismember(agent_location, (rows:rows:rows*col))                                                                || ...
        ismember(agent_location, (1:rows:rows*col))                                                                   || ...
        ismember(agent_location, (1:rows))                                                                            || ...
        ismember(agent_location, ((rows-1)*col + 1 : rows*col))                                                       || ...
        ismember(agent_location, (rows/2:rows:rows*col/5))                                                            || ...
        ismember(agent_location, (rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5)))                              || ...
        ismember(agent_location, (rows/2 + rows*(4*col/5):rows:rows*col))
    
    error('Agent is located in a wall.')
    
elseif ismember(reward_location, (round((rows*(col/2)))+1:round((rows*(col/2)))+round((rows/5))))                      || ...
        ismember(reward_location, (round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5)))     || ...
        ismember(reward_location, (round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows))                  || ...
        ismember(reward_location, (rows:rows:rows*col))                                                                || ...
        ismember(reward_location, (1:rows:rows*col))                                                                   || ...
        ismember(reward_location, (1:rows))                                                                            || ...
        ismember(reward_location, ((rows-1)*col + 1 : rows*col))                                                       || ...
        ismember(reward_location, (rows/2:rows:rows*col/5))                                                            || ...
        ismember(reward_location, (rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5)))                              || ...
        ismember(reward_location, (rows/2 + rows*(4*col/5):rows:rows*col))   
    
    error('Reward is located in a wall.')
    
end


% create sensory synapses
sensory_weights = zeros;

% create reward synapse

disp('Setup complete.')

%% Induction

disp('Induction begun:')
% Activate agent state
state_cells(agent_y, agent_x) = agent_firing;

% Activate reward state(s)
state_cells(reward_y, reward_x) = 1;

% Feed activation back into network
noNaNweights = internal_synapses;
noNaNweights(isnan(noNaNweights)) = 0;

for time = 1:steps
    
    if time == round(steps/4)
        disp('...')
    elseif time == round(steps/2)
        disp('...')
    elseif time == round(3*steps/4)
        disp('...')
    end
    
state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
activation = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNweights);
state_cells = reshape(activation,num_state);

% Competition:

state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;

% subtractive
state_cells = state_cells - mean_modifier * mean(mean(state_cells));

% divisive
state_cells = state_cells/max(max(state_cells));
state_cells(state_cells < 0) = 0;

% sigmoid
%state_cells = softCompetition(sparseness, slope, state_cells(:));
state_cells = reshape(state_cells,num_state);

state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
activation_display = vec2mat(state_cells, sqrt(numel(state_cells)));
activation_tracked{time} = activation_display;

end
disp('Induction complete.')

%% Analyse Data
% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]); colorbar

end