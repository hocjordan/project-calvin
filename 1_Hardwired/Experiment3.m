%% Trace Experiment

function Experiment3(steps, dilution)

% As Experiment2, but introduces competition into the network as well as
% 'actions' that take the agent from one state to another. Also includes a
% display of agent progress and the linear attractor.

%% SETUP TRIAL

% Assign parameters
eta = 0.5;
learningRate = 1;
agent_x = 5;
agent_y = 5;
reward_x = 7;
reward_y = 7;

% Create world
world = create_world(agent_x, agent_y, reward_x, reward_y);

% Create grid of place cells
place_cells = world(:,:,1);
trace = zeros(10);

% Create synapses with full connectivity, save for self-self
synapses = GenerateZeroWeights(numel(place_cells), numel(place_cells), dilution);

%% RUN TRIAL

% Begin timestep:
for time = 1:steps
    %tic
    % Update place cells
    place_cells = world(:,:,1);
    find(place_cells)
    
    % Calculate the trace value for all cells
    trace = getTrace(place_cells, trace, eta);
    
    % Calculate the firing rate of all cells and update synapses
    synapses = synapses+(learningRate*(repmat(place_cells(:),[1,numel(place_cells)]) .* repmat(trace(:)',[numel(place_cells),1])));
    synapses(logical(eye(size(synapses)))) = NaN;
    
    % Normalise synapses
    synapses = normalise(synapses);
    
    % Record agent position
    world_tracked{time} = place_cells;
    
    % End timestep. Repeat.
    if time == steps
        final = find(place_cells);
    else
        % Activate one cell
        actions = {'N' 'E'};
        action = actions{randi(2)};
        disp(action)
        world = update_world(world, action);
    end
    %toc
end

% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
disp('Complete')

%% BACKWARD INDUCTION

% set reward representation at final position.
place_cells(:) = 0;
place_cells(final) = 1;

% feed current to final position and see how it affects the other cells in
% the network.
noNaNweights=synapses;
noNaNweights(isnan(noNaNweights)) = 0;
activation = dot(repmat(place_cells(:),[1,numel(place_cells)]), noNaNweights); activation(final) = 1
for time = 1:steps
    activation = dot(repmat(activation(:),[1,numel(place_cells)]), noNaNweights); activation(final) = 1;
    %activation = softCompetition(49,90,activation);
    activation_display = reshape(activation,[10,10]);
    activation_tracked{time} = activation_display;
end

% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);

end


















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)
    
    trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end


function y = normalise(matrix)

% Normalise matrix so that each column's sum approaches 1. Ignores NaN.

%get number of rows and columns in matrix

[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);
for column = 1:columns
    if summed(column) ~= 0
        %divide each row in that column by that sum
        for row = 1:rows
            matrix(row,column) = (matrix(row,column)/summed(column));
        end
    end
end
y = matrix;


end