%% Trace Experiment 7

function Experiment7(steps)

% Combines trace learning architecture of Experiment 3 with
% state/action formation of Experiment 6. Motor control is still done
% through generation of random numbers and is not driven by agent. DOES NOT
% USE BACKWARD INDUCTION.

%% Parameters

% world
worldSize_x = 5;
worldSize_y = 4;
agent_x = 2;
agent_y = 2;
reward_x = 3;
reward_y = 3;

% actions
actions = {'N' 'E' 'S' 'W'};

% cells
num_place = [worldSize_x worldSize_y];
num_motor = [1 numel(actions)];
num_SA = [1 144];

% connections
eta = 0.8;
learningRate = 1;
sensory_dilution = 0.05;
motor_dilution = 0.2;
internal_dilution = 1;

% competition
sparseness = 70;
slope = 90;

% analysis
mapping_threshold = 0.6;

%% Setup World

disp('Setting up:')
disp('World')
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

%% Setup Agent

disp('Agent')
place_cells = zeros(num_place);
motor_cells = zeros(num_motor);
SA_cells = zeros(num_SA);
trace = zeros(num_SA);

%% Setup Internal Connections (trace learning)

disp('Internal Connections')
internal_weights = GenerateZeroWeights(numel(SA_cells), numel(SA_cells), internal_dilution);

%% Setup External Connections (competitive formation of state/action cells)

disp('External Connections')
sensory_weights = GenerateWeights(numel(SA_cells), numel(place_cells), sensory_dilution);
motor_weights = GenerateWeights(numel(SA_cells), numel(motor_cells), motor_dilution);
disp('Complete')

%% Run Timestep

% Begin Timestep

disp('Running Timesteps')
disp('...')
for time = 1:steps
    %tic
    if time == round(steps/4)
        disp('...')
    elseif time == round(steps/2)
        disp('...')
    elseif time == round(3*steps/4)
        disp('...')
    end
    
    % Update place cells
    place_cells = world(:,:,1);
    
    % Calculate trace value for SA cells (consider moving below SA update)
    trace = getTrace(SA_cells, trace, eta);
    
    % Update SA cells from current state (all cells with that state should
    % activate, but one will be most powerful)
    noNaNweights = sensory_weights;
    noNaNweights(isnan(noNaNweights)) = 0;
    SA_cells = dot((repmat(place_cells(:),[1,numel(SA_cells)])), noNaNweights);
    find(SA_cells)
    
    % Implement WTA Competition
    SA_cells = WTA_Competition(SA_cells);
    find(SA_cells)
    
    % Update internal synapses via trace learning and normalise
    internal_weights = internal_weights + (learningRate .* (trace(:) * SA_cells));
    internal_weights(logical(eye(size(internal_weights)))) = NaN;
        
    % Agent moves
    action = actions{randi(4)};
    %disp(action)
    world = update_world2(world, action);
    
    % SA cells activate based on the original state and the action taken
    noNaNweights = [sensory_weights; motor_weights];
    noNaNweights(isnan(noNaNweights)) = 0;
    activation = dot((repmat([place_cells(:); motor_cells'], [1,numel(SA_cells)])), noNaNweights);
    
    % Competition (WTA)
    SA_cells = WTA_Competition(activation);
    find(SA_cells)
    
    % Update external synapses via hebb learning and normalise
    sensory_weights = sensory_weights + (learningRate * (place_cells(:) * SA_cells));
    motor_weights = motor_weights + (learningRate * (motor_cells(:) * SA_cells));
    
    % End timestep
    world_tracked{time} = world(:,:,1);
    if time == steps
        final = find(SA_cells); 
    end

end

%% Backward Induction
% set reward representation at final position. Using SA cells, could use
% place cells and allow activity to propagate to SA from that.
SA_cells(:) = 0;
SA_cells(final) = 1;

% feed current to final position and see how it affects the other cells in
% the network.
noNaNweights=internal_weights;
noNaNweights(isnan(noNaNweights)) = 0;
activation = dot(repmat(SA_cells(:),[1,numel(SA_cells)]), noNaNweights); activation(final) = 1


for time = 1:steps
    activation = dot(repmat(activation(:),[1,size(noNaNweights,2)]), noNaNweights); activation(final) = 1
    %activation = softCompetition(49,90,activation);
    activation_display = vec2mat(activation, sqrt(numel(SA_cells)));
    activation_tracked{time} = activation_display;
end


%{
noNaNweights=synapses;
noNaNweights(isnan(noNaNweights)) = 0;
activation = dot(repmat(place_cells(:),[1,numel(place_cells)]), noNaNweights); activation(final) = 1
for time = 1:steps
    activation = dot(repmat(activation(:),[1,numel(place_cells)]), noNaNweights); activation(final) = 1;
    %activation = softCompetition(49,90,activation);
    activation_display = reshape(activation,[10,10]);
    activation_tracked{time} = activation_display;
end
%}





% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);

%% Analyse Data
% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
disp('Complete')
disp(' ')

disp('Calculating state/action mappings.')
disp('...')
[~ , firingRates,] = HML_mappings(sensory_weights, motor_weights, mapping_threshold);
disp('Complete')



end











%% Secondary Functions

function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)
    
    trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end




function y = normalise(matrix)

% Normalise matrix so that each column's sum approaches 1. Ignores NaN.

%get number of rows and columns in matrix

[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);
for column = 1:columns
    if summed(column) ~= 0
        %divide each row in that column by that sum
        for row = 1:rows
            matrix(row,column) = (matrix(row,column)/summed(column));
        end
    end
end
y = matrix;


end