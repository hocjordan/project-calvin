%% Bidirectional Test 1

function activation_display = Experiment18(steps)

% As expt. 8, but with a single room, trying to prevent attractors from
% forming in the middle.

%% Parameters
% world
worldSize_x = 20;
worldSize_y = 20;
agent_x = 3;
agent_y = 5;
reward_x = 3;
reward_y = 10;

if agent_x > worldSize_x || agent_y > worldSize_y
    error('Specified agent position does not exist in world.')
elseif reward_x > worldSize_x || reward_y > worldSize_y
    error('Specified reward position does not exist in world.')
end
    
% cells
num_place = [worldSize_x worldSize_y];
num_state = [worldSize_x worldSize_y];

% attractors
agent_firing = 1;
reward_firing = 1;

% connections
weakinternal_weights = NaN;
internal_weights = 1;
learningRate = 1;

% competition
mean_modifier = 1;
sparseness = 99;
slope = 1;

% analysis
mapping_threshold = 0.2;

%% Setup
disp('Setup begun.')

disp('Creating world.')
% create world
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

disp('Creating cells.')
% create place_cells
place_cells = zeros(num_place);

% create state cells
state_cells = zeros(num_state);

% create reward representation
reward_rep = 0;

disp('Generating synapses.')
% create bidirectional internal synapses
%internal_synapses = nan(numel(state_cells));
internal_synapses = zeros(numel(state_cells));
internal_synapses(:) = weakinternal_weights;

rows = size(state_cells,1);
col = size(state_cells,2);
for presynaptic_cell = 1:numel(state_cells)
    
        if presynaptic_cell == round(numel(state_cells)/4)
        disp('...')
    elseif presynaptic_cell == round(numel(state_cells)/2)
        disp('...')
    elseif presynaptic_cell == round(3*numel(state_cells)/4)
        disp('...')
    end
    
    w = presynaptic_cell-rows; n = presynaptic_cell-1; s = presynaptic_cell+1; e = presynaptic_cell+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    try
        state_cells(n);
        internal_synapses(presynaptic_cell,n) = internal_weights;
    end
    
    try
        state_cells(e);
        internal_synapses(presynaptic_cell,e) = internal_weights;
    end
    
    try
        state_cells(s);
        internal_synapses(presynaptic_cell,s) = internal_weights;
    end
    
    try
        state_cells(w);
        internal_synapses(presynaptic_cell,w) = internal_weights;
    end
    
    try
        state_cells(nw);
        internal_synapses(presynaptic_cell,nw) = internal_weights;
    end
    
    try
        state_cells(ne);
        internal_synapses(presynaptic_cell,ne) = internal_weights;
    end
    
    try
        state_cells(se);
        internal_synapses(presynaptic_cell,se) = internal_weights;
    end
    
    try
        state_cells(sw);
        internal_synapses(presynaptic_cell,sw) = internal_weights;
    end
end

%% Create Walls
% Walls have no postsynaptic connections:
% top/bottom
internal_synapses(:,rows:rows:rows*col) = NaN;
internal_synapses(:,1:rows:rows*col) = NaN;
% left/right
internal_synapses(:,1:rows) = NaN;
internal_synapses(:,(rows-1)*col + 1 : rows*col) = NaN;

% Walls have no presynaptic connections:
% top/bottom
internal_synapses(rows:rows:rows*col,:) = NaN;
internal_synapses(1:rows:rows*col,:) = NaN;
% left/right
internal_synapses(1:rows,:) = NaN;
internal_synapses((rows-1)*col + 1 : rows*col,:) = NaN;


% check if agent or reward is within a wall
agent_location = find(world(:,:,1),1);
reward_location = find(world(:,:,2),2);

if ismember(agent_location, (rows:rows:rows*col))                                                                || ...
        ismember(agent_location, (1:rows:rows*col))                                                                   || ...
        ismember(agent_location, (1:rows))                                                                            || ...
        ismember(agent_location, ((rows-1)*col + 1 : rows*col))                                                       

    error('Agent is located in a wall.')
    
elseif ismember(reward_location, (rows:rows:rows*col))                                                                || ...
        ismember(reward_location, (1:rows:rows*col))                                                                   || ...
        ismember(reward_location, (1:rows))                                                                            || ...
        ismember(reward_location, ((rows-1)*col + 1 : rows*col))
    
    error('Reward is located in a wall.')
    
end

disp('Setup complete.')

%% Induction

disp('Induction begun:')
% Activate agent state
state_cells(agent_y, agent_x) = agent_firing;

% Activate reward state(s)
state_cells(reward_y, reward_x) = reward_firing;

% Feed activation back into network
noNaNneighbour = internal_synapses;
noNaNneighbour(isnan(noNaNneighbour)) = 0;

for time = 1:steps
    
    if time == round(steps/4)
        disp('...')
    elseif time == round(steps/2)
        disp('...')
    elseif time == round(3*steps/4)
        disp('...')
    end
    
state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
%activation = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNweights);

%% phi nought
for postSynaptic_cell = 1:numel(state_cells)
    
    % check no. of recurrent connections
    w = postSynaptic_cell-rows; n = postSynaptic_cell-1; s = postSynaptic_cell+1; e = postSynaptic_cell+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    test = [n e s w nw ne se sw];
    idx = find(test <= 0);
    test(idx) = test(idx) + rows*col;
    idx = find(test > rows*col);
    test(idx) = test(idx) - rows*col;
    
    n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
    
    neighbour_synapses = zeros(3);
    neighbour_synapses(1) = internal_synapses(nw,postSynaptic_cell);
    neighbour_synapses(2) = internal_synapses(w,postSynaptic_cell);
    neighbour_synapses(3) = internal_synapses(sw,postSynaptic_cell);
    neighbour_synapses(4) = internal_synapses(n,postSynaptic_cell);
    neighbour_synapses(5) = NaN;
    neighbour_synapses(6) = internal_synapses(s,postSynaptic_cell);
    neighbour_synapses(7) = internal_synapses(ne,postSynaptic_cell);
    neighbour_synapses(8) = internal_synapses(e,postSynaptic_cell);
    neighbour_synapses(9) = internal_synapses(se,postSynaptic_cell);
    
    noNaNneighbour = neighbour_synapses;
    noNaNneighbour(isnan(noNaNneighbour)) = 0;
    
    
    phi_nought = 1/(numel(find(neighbour_synapses > 0)));
    
    if phi_nought == Inf
        phi_nought = 0;
    end
    
    n_firing = state_cells(n)*noNaNneighbour(4);
    e_firing = state_cells(e)*noNaNneighbour(8);
    s_firing = state_cells(s)*noNaNneighbour(6);
    w_firing = state_cells(w)*noNaNneighbour(2);
    nw_firing = state_cells(nw)*noNaNneighbour(1);
    sw_firing = state_cells(sw)*noNaNneighbour(3);
    ne_firing = state_cells(ne)*noNaNneighbour(7);
    se_firing = state_cells(se)*noNaNneighbour(9);
    
    
    firingRate = 10 * phi_nought * (n_firing + e_firing + s_firing + w_firing + ...
        nw_firing + sw_firing + ne_firing + se_firing);
    
    if isnan(firingRate)
        firingRate = 0;
    end
    
    activation(postSynaptic_cell) = firingRate;
    
end
    
state_cells = reshape(activation,num_state);

%% noise
%state_cells = state_cells + 1*normrnd(0,1,num_state);

%% Competition:

state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;

% subtractive
state_cells = state_cells - mean_modifier * mean(mean(state_cells));

% divisive
state_cells = state_cells/max(max(state_cells));
state_cells(state_cells < 0) = 0;

% sigmoid
%state_cells = softCompetition(sparseness, slope, state_cells(:));
state_cells = reshape(state_cells,num_state);

state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
activation_display = vec2mat(state_cells, sqrt(numel(state_cells)));
activation_tracked{time} = activation_display;

end
disp('Induction complete.')

%% Analyse Data
% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]); colorbar

end