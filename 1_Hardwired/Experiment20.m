%% Bidirectional Test 1

function activation_display = Experiment20(steps, ind_steps)

% As Expt. 19, with the gradient-following algorithm from Expt. 10 and 12.

%% Parameters
% world
worldSize_x = 40;
worldSize_y = 40;
agent_x = 36;
agent_y = 10;
reward_x = 9;
reward_y = 32;

if agent_x > worldSize_x || agent_y > worldSize_y
    error('Specified agent position does not exist in world.')
elseif reward_x > worldSize_x || reward_y > worldSize_y
    error('Specified reward position does not exist in world.')
end

% wall
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% cells
num_state = [worldSize_x worldSize_y];

% attractors
agent_firing = 2;
reward_firing = 1;

% connections
radius = 10;
internal_weights = 0.01;
learningRate = 1;

% competition
mean_modifier = 0.1;
sparseness = 99;
slope = 1;

% analysis
mapping_threshold = 0.2;

%% Setup
disp('Setup begun.')

% create world

disp('Creating world.')
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% create state cells

disp('Creating state cells.')
state_cells = zeros(num_state);

% generate bidirectional internal synapses

disp('Generating synapses.')
internal_synapses = zeros(numel(state_cells));

rows = size(state_cells,1);
col = size(state_cells,2);

internal_synapses = gaussian_synapses(state_cells, radius, internal_weights);

disp('Blocking off walls.')

% ascertain position of walls
top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

% walls have no presynaptic connections
internal_synapses(top_wall,:) = NaN;
internal_synapses(bottom_wall,:) = NaN;
internal_synapses(left_wall,:) = NaN;
internal_synapses(right_wall,:) = NaN;

% walls have no postsynaptic connections
internal_synapses(:, top_wall) = NaN;
internal_synapses(:, bottom_wall) = NaN;
internal_synapses(:, left_wall) = NaN;
internal_synapses(:, right_wall) = NaN;

% check if agent or reward is within a wall
agent_location = find(world(:,:,1),1);
reward_location = find(world(:,:,2),2);

if ismember(agent_location, top_wall)             || ...
        ismember(agent_location, bottom_wall)     || ...
        ismember(agent_location, left_wall)       || ...
        ismember(agent_location, right_wall)
    
    error('Agent is located in a wall.')
    
elseif ismember(reward_location, top_wall)         || ...
        ismember(reward_location, bottom_wall)     || ...
        ismember(reward_location, left_wall)       || ...
        ismember(reward_location, right_wall)
    
    error('Reward is located in a wall.')
    
end

disp('Setup complete.')

%% Run Step:

for time = 1:steps
    disp(time)
    
    %% Induction
    
    disp('Induction begun:')
    % Activate agent state
    state_cells(agent_y, agent_x) = agent_firing;
    
    % Activate reward state(s)
    state_cells(reward_y, reward_x) = reward_firing;
    
    % Feed activation back into network
    noNaNweights = internal_synapses;
    noNaNweights(isnan(noNaNweights)) = 0;
    
    for ind_time = 1:ind_steps
        
        if ind_time == round(ind_steps/4)
            disp('...')
        elseif ind_time == round(ind_steps/2)
            disp('...')
        elseif ind_time == round(3*ind_steps/4)
            disp('...')
        end
        
        state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        activation = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNweights);
        state_cells = reshape(activation,num_state);
        
        % Competition:
        
        state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        % subtractive
        state_cells = state_cells - mean_modifier * mean(mean(state_cells));
        
        % divisive
        state_cells = state_cells/max(max(state_cells));
        state_cells(state_cells < 0) = 0;
        
        state_cells = reshape(state_cells,num_state);
        
        state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        activation_display = vec2mat(state_cells, sqrt(numel(state_cells)));
        
    end
    disp('Induction complete.')
    
    %% Gather Step Data
    
    % Save activation pathway
    activation_tracked{time} = activation_display;
    
    % Save
    world_tracked{time} = world(:,:,1);
    
    %% Move Agent
    
    % find the most active state cell(s) next to the current position
    rows = size(state_cells,1);
    col = size(state_cells,2);
    
    current_state = sub2ind(size(state_cells), agent_y, agent_x);
    
    w = current_state-rows; n = current_state-1; s = current_state+1; e = current_state+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    test = [n e s w nw ne se sw];
    idx = find(test <= 0);
    test(idx) = test(idx) + rows*col;
    idx = find(test > rows*col);
    test(idx) = test(idx) - rows*col;
    
    n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
    
    neighbours = zeros(3);
    
    neighbours(1) = state_cells(nw);
    neighbours(2) = state_cells(w);
    neighbours(3) = state_cells(sw);
    neighbours(4) = state_cells(n);
    neighbours(5) = 0;
    neighbours(6) = state_cells(s);
    neighbours(7) = state_cells(ne);
    neighbours(8) = state_cells(e);
    neighbours(9) = state_cells(se);
    
    %neighbours = round(neighbours * 100)/100;
    next_turn = find(neighbours == max(max(neighbours)));
    if size(next_turn,1) > 1
        disp('RANDOM ACTION SELECTED')
    end
    
    % Activate one cell
    actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};
    action = actions{next_turn(randi(size(next_turn,1)))};
    disp(action)
    world = update_world2(world, action);
    
    if ismember(reward_firing, neighbours) == 1
        result = 'Y';
        disp(result)
        break
    elseif time == steps
        result = 'N';
        disp(result)
        break
    end
    
        %% Update Parameters
    
    state_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

%% Analyse Step

slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1])
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1])

end