%% Bidirectional Test 1

function activation_display = Experiment11(steps)

% As Experiment 8, but with SA cells that are bound in a diagonal pattern:
%{
   *    *
  * *--* *
   *    *
   |    |
   *    *
  * *--* *
   *    *

%}
%% Parameters
% world
worldSize_x = 40;
worldSize_y = 40;
agent_x = 5;
agent_y = 5;
reward_x = 29;
reward_y = 35;

if agent_x > worldSize_x || agent_y > worldSize_y
    error('Specified agent position does not exist in world.')
elseif reward_x > worldSize_x || reward_y > worldSize_y
    error('Specified reward position does not exist in world.')
end
    
% cells
num_SA = [3*worldSize_x 3*worldSize_y];

% connections
internal_weights = 0.2;
learningRate = 1;

% competition
sparseness = 80;
slope = 5;

% analysis
mapping_threshold = 0.6;

%% Setup
disp('Setup begun.')

disp('Creating world.')
% create world
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

disp('Creating cells.')

% create state cells
SA_cells = zeros(num_SA);

% create reward representation
reward_rep = 0;

disp('Generating synapses.')
% create bidirectional internal synapses
internal_synapses = nan(numel(SA_cells));
rows = size(SA_cells,1);
col = size(SA_cells,2);
for presynaptic_cell = 1+rows:3*rows:numel(SA_cells)-1-rows
    w = presynaptic_cell-rows; n = presynaptic_cell-1; s = presynaptic_cell+1; e = presynaptic_cell+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    try
        SA_cells(n);
        internal_synapses(presynaptic_cell,n) = internal_weights;
    end
    
    try
        SA_cells(e);
        internal_synapses(presynaptic_cell,e) = internal_weights;
    end
    
    try
        SA_cells(s);
        internal_synapses(presynaptic_cell,s) = internal_weights;
    end
    
    try
        SA_cells(w);
        internal_synapses(presynaptic_cell,w) = internal_weights;
    end
    
    try
        SA_cells(nw);
        internal_synapses(presynaptic_cell,nw) = internal_weights;
    end
    
    try
        SA_cells(ne);
        internal_synapses(presynaptic_cell,ne) = internal_weights;
    end
    
    try
        SA_cells(se);
        internal_synapses(presynaptic_cell,se) = internal_weights;
    end
    
    try
        SA_cells(sw);
        internal_synapses(presynaptic_cell,sw) = internal_weights;
    end
end

% walls
internal_synapses(:,round((rows*col/2))+1:round((rows*col/2))+round((rows/5))) = NaN; internal_synapses(:,round((rows*col/2))+round((2*rows/5)):round((rows*col/2))+round(3*rows/5)) = NaN; internal_synapses(:,round((rows*col/2))+round(4*rows/5):round((rows*col/2))+rows) = NaN;
internal_synapses(:,rows:rows:rows*col) = NaN;
internal_synapses(:,rows/2:rows:rows*col/5) = NaN; internal_synapses(:,rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5)) = NaN; internal_synapses(:,rows/2 + rows*(4*col/5):rows:rows*col) = NaN;

% create sensory synapses
sensory_weights = zeros;

% create reward synapse

disp('Setup complete.')

%% Induction

disp('Induction begun')
% Activate agent state
SA_cells(agent_y, agent_x) = 1;

% Activate reward state(s)
SA_cells(reward_y, reward_x) = 1;

% Feed activation back into network
noNaNweights = internal_synapses;
noNaNweights(isnan(noNaNweights)) = 0;

for time = 1:steps
    
    if time == round(steps/4)
        disp('...')
    elseif time == round(steps/2)
        disp('...')
    elseif time == round(3*steps/4)
        disp('...')
    end
    
% state_cells(agent_y, agent_x) = max(max(state_cells)); state_cells(reward_y, reward_x) = max(max(state_cells));
activation = dot(repmat(SA_cells(:),[1,numel(SA_cells)]), noNaNweights);
SA_cells = reshape(activation,num_SA);

% competition
SA_cells(agent_y, agent_x) = max(max(SA_cells)); SA_cells(reward_y, reward_x) = max(max(SA_cells));
SA_cells = SA_cells - mean(mean(SA_cells));
SA_cells = SA_cells/max(max(SA_cells));
%state_cells = softCompetition(sparseness, slope, state_cells(:));
SA_cells = reshape(SA_cells,num_SA);

activation_display = vec2mat(SA_cells, sqrt(numel(SA_cells)));
activation_tracked{time} = activation_display;

end
disp('Induction complete.')

%% Analyse Data
% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]); colorbar

end