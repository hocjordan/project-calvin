%% Bidirectional Test 1

function activation_display = Experiment8(steps)

% uses 5x5 world

%% Parameters
% world
worldSize_x = 20;
worldSize_y = 20;
agent_x = 8;
agent_y = 2;
reward_x = 14;
reward_y = 3;

if agent_x > worldSize_x || agent_y > worldSize_y
    error('Specified agent position does not exist in world.')
elseif reward_x > worldSize_x || reward_y > worldSize_y
    error('Specified reward position does not exist in world.')
end
    
% cells
num_place = [worldSize_x worldSize_y];
num_state = [worldSize_x worldSize_y];

% attractors
agent_firing = 2;
reward_firing = 2;

% connections
weakinternal_weights = NaN;
internal_weights = 1;
learningRate = 1;

% competition
mean_modifier = 0.01;
sparseness = 99;
slope = 1;

% analysis
mapping_threshold = 0.2;

%% Setup
disp('Setup begun.')

disp('Creating world.')
% create world
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

disp('Creating cells.')
% create place_cells
place_cells = zeros(num_place);

% create state cells
state_cells = zeros(num_state);

% create reward representation
reward_rep = 0;

disp('Generating synapses.')
% create bidirectional internal synapses
%internal_synapses = nan(numel(state_cells));
internal_synapses = zeros(numel(state_cells));
internal_synapses(:) = weakinternal_weights;

rows = size(state_cells,1);
col = size(state_cells,2);
for presynaptic_cell = 1:numel(state_cells)
    
        if presynaptic_cell == round(numel(state_cells)/4)
        disp('...')
    elseif presynaptic_cell == round(numel(state_cells)/2)
        disp('...')
    elseif presynaptic_cell == round(3*numel(state_cells)/4)
        disp('...')
    end
    
    w = presynaptic_cell-rows; n = presynaptic_cell-1; s = presynaptic_cell+1; e = presynaptic_cell+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    try
        state_cells(n);
        internal_synapses(presynaptic_cell,n) = internal_weights;
    end
    
    try
        state_cells(e);
        internal_synapses(presynaptic_cell,e) = internal_weights;
    end
    
    try
        state_cells(s);
        internal_synapses(presynaptic_cell,s) = internal_weights;
    end
    
    try
        state_cells(w);
        internal_synapses(presynaptic_cell,w) = internal_weights;
    end
    
    try
        state_cells(nw);
        internal_synapses(presynaptic_cell,nw) = internal_weights;
    end
    
    try
        state_cells(ne);
        internal_synapses(presynaptic_cell,ne) = internal_weights;
    end
    
    try
        state_cells(se);
        internal_synapses(presynaptic_cell,se) = internal_weights;
    end
    
    try
        state_cells(sw);
        internal_synapses(presynaptic_cell,sw) = internal_weights;
    end
end

% walls
%vertical - to
internal_synapses(:,round((rows*(col/2)))+1:round((rows*(col/2)))+round((rows/5))) = NaN;
internal_synapses(:,round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5)) = NaN;
internal_synapses(:,round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows) = NaN;
%vertical - from
%internal_synapses(round((rows*(col/2)))+1:round((rows*(col/2-1)))+round((rows/5)),:) = NaN;
%internal_synapses(round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5),:) = NaN;
%internal_synapses(round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows,:) = NaN;

% top/bottom
internal_synapses(:,rows:rows:rows*col) = NaN;
internal_synapses(:,1:rows:rows*col) = NaN;

% horizontal-to
internal_synapses(:,rows/2:rows:rows*col/5) = NaN; internal_synapses(:,rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5)) = NaN; internal_synapses(:,rows/2 + rows*(4*col/5):rows:rows*col) = NaN;
% horizontal-from
%internal_synapses(rows/2:rows:rows*col/5,:) = NaN; internal_synapses(rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5),:) = NaN; internal_synapses(rows/2 + rows*(4*col/5):rows:rows*col,:) = NaN;

% create sensory synapses
sensory_weights = zeros;

% create reward synapse

disp('Setup complete.')

%% Induction

disp('Induction begun:')
% Activate agent state
state_cells(agent_y, agent_x) = agent_firing;

% Activate reward state(s)
state_cells(reward_y, reward_x) = reward_firing;

% Feed activation back into network
noNaNweights = internal_synapses;
noNaNweights(isnan(noNaNweights)) = 0;

for time = 1:steps
    
    if time == round(steps/4)
        disp('...')
    elseif time == round(steps/2)
        disp('...')
    elseif time == round(3*steps/4)
        disp('...')
    end
    
state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
activation = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNweights);
state_cells = reshape(activation,num_state);

% Competition:

state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;

% subtractive
state_cells = state_cells - mean_modifier * mean(mean(state_cells));

% divisive
state_cells = state_cells/max(max(state_cells));
state_cells(state_cells < 0) = 0;

% sigmoid
%state_cells = softCompetition(sparseness, slope, state_cells(:));
state_cells = reshape(state_cells,num_state);

state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
activation_display = vec2mat(state_cells, sqrt(numel(state_cells)));
activation_tracked{time} = activation_display;

end
disp('Induction complete.')

%% Analyse Data
% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]); colorbar

end