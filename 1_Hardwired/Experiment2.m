%% Trace Experiment

function Experiment2(places, steps, dilution)

% As Experiment1, but introduces a reward system that creates a low-level
% linear attractor from the reward position to that of the agent.

% Assign parameters
eta = 0.5;
learningRate = 1;

% Create grid of place cells
place_cells = zeros(places);
trace = zeros(places);

% Create synapses with full connectivity, save for self-self
synapses = GenerateZeroWeights(numel(place_cells), numel(place_cells), dilution);

% Begin timestep:
for time = 1:steps
    % Activate one cell
    place_cells(randi(numel(place_cells))) = 1
    
    % Calculate the trace value for all cells
    trace = getTrace(place_cells, trace, eta);
    
    % Calculate the firing rate of all cells
    synapses = synapses+(learningRate*(repmat(place_cells(:),[1,numel(place_cells)]) .* repmat(trace(:)',[numel(place_cells),1])));
    synapses(logical(eye(size(synapses)))) = NaN;
    
    % Normalise synapses
    synapses = normalise(synapses);
    
    % End timestep. Repeat.
    if time == steps
        final = find(place_cells);
    else
        place_cells(:) = 0;
    end
end

% Print final synapse values.
disp(synapses);

% set reward representation at final position.
place_cells(:) = 0;
place_cells(final) = 1;

% feed current to final position and see how it affects the other cells in
% the network.
noNaNweights=synapses;
noNaNweights(isnan(noNaNweights)) = 0;
activation = dot(repmat(place_cells(:),[1,numel(place_cells)]), noNaNweights); activation(final) = 0.5

for time = 1:steps
    activation = dot(repmat(activation(:),[1,numel(place_cells)]), noNaNweights); activation(final) = 0.5
end

end

function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)
    
    trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end

function y = normalise(matrix)

% Normalise matrix so that each column's sum approaches 1. Ignores NaN.

%get number of rows and columns in matrix

[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);
for column = 1:columns
    if summed(column) ~= 0
        %divide each row in that column by that sum
        for row = 1:rows
            matrix(row,column) = (matrix(row,column)/summed(column));
        end
    end
end
y = matrix;


end