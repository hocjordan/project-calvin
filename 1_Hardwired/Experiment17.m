%% Bidirectional Test 1

function activation_display = Experiment17(steps)

% As Expt. 8, but normalises synapses so that a cell next to a wall, with
% five functional synapses rather than 8, will have the strength of those
% synapses increased to compensate.

%% Parameters
% world
worldSize_x = 20;
worldSize_y = 20;
agent_x = 16;
agent_y = 8;
reward_x = 13;
reward_y = 19;

if agent_x > worldSize_x || agent_y > worldSize_y
    error('Specified agent position does not exist in world.')
elseif reward_x > worldSize_x || reward_y > worldSize_y
    error('Specified reward position does not exist in world.')
end
    
% cells
num_place = [worldSize_x worldSize_y];
num_state = [worldSize_x worldSize_y];

% attractors
agent_firing = 1;
reward_firing = .1;

% connections
weakinternal_weights = NaN;
internal_weights = 0.1;
learningRate = 2;

% competition
mean_modifier = 0.0001;
sparseness = 99;
slope = 1;

% analysis
mapping_threshold = 0.2;

%% Setup Cells
disp('Setup begun.')

disp('Creating world.')
% create world
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

disp('Creating cells.')
% create place_cells
place_cells = zeros(num_place);

% create state cells
state_cells = zeros(num_state);

% create reward representation
reward_rep = 0;

disp('Generating synapses.')
%% Generate Synapses
internal_synapses = zeros(numel(state_cells));
internal_synapses(:) = weakinternal_weights;
rows = size(state_cells,1);
col = size(state_cells,2);
for presynaptic_cell = 1:numel(state_cells)
    
    if presynaptic_cell == round(numel(state_cells)/4)
        disp('...')
    elseif presynaptic_cell == round(numel(state_cells)/2)
        disp('...')
    elseif presynaptic_cell == round(3*numel(state_cells)/4)
        disp('...')
    end
    
    w = presynaptic_cell-rows; n = presynaptic_cell-1; s = presynaptic_cell+1; e = presynaptic_cell+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    test = [n e s w nw ne se sw];
    idx = find(test <= 0);
    test(idx) = test(idx) + rows*col;
    idx = find(test > rows*col);
    test(idx) = test(idx) - rows*col;
    
    n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
    
    internal_synapses(presynaptic_cell,n) = internal_weights;
    internal_synapses(presynaptic_cell,e) = internal_weights;
    internal_synapses(presynaptic_cell,s) = internal_weights;
    internal_synapses(presynaptic_cell,w) = internal_weights;
    internal_synapses(presynaptic_cell,nw) = internal_weights;
    internal_synapses(presynaptic_cell,ne) = internal_weights;
    internal_synapses(presynaptic_cell,se) = internal_weights;
    internal_synapses(presynaptic_cell,sw) = internal_weights;

end

%% Create Walls
% Walls have no postsynaptic connections:
%vertical
internal_synapses(:,round((rows*(col/2)))+1:round((rows*(col/2)))+round((rows/5))) = NaN;
internal_synapses(:,round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5)) = NaN;
internal_synapses(:,round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows) = NaN;
% top/bottom
internal_synapses(:,rows:rows:rows*col) = NaN;
internal_synapses(:,1:rows:rows*col) = NaN;
% left/right
internal_synapses(:,1:rows) = NaN;
internal_synapses(:,(rows-1)*col + 1 : rows*col) = NaN;
% horizontal
internal_synapses(:,rows/2:rows:rows*col/5) = NaN; internal_synapses(:,rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5)) = NaN; internal_synapses(:,rows/2 + rows*(4*col/5):rows:rows*col) = NaN;

% Walls have no presynaptic connections:
%vertical
internal_synapses(round((rows*(col/2)))+1:round((rows*(col/2)))+round((rows/5)),:) = NaN;
internal_synapses(round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5),:) = NaN;
internal_synapses(round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows,:) = NaN;
% top/bottom
internal_synapses(rows:rows:rows*col,:) = NaN;
internal_synapses(1:rows:rows*col,:) = NaN;
% left/right
internal_synapses(1:rows,:) = NaN;
internal_synapses((rows-1)*col + 1 : rows*col,:) = NaN;
% horizontal
internal_synapses(rows/2:rows:rows*col/5,:) = NaN;
internal_synapses(rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5),:) = NaN;
internal_synapses(rows/2 + rows*(4*col/5):rows:rows*col,:) = NaN;

%% Normalise Synapses:

% for each postsynaptic cell
for postSynaptic_cell = 1:numel(state_cells)
    
    if postSynaptic_cell == round(numel(state_cells)/4)
        disp('...')
    elseif postSynaptic_cell == round(numel(state_cells)/2)
        disp('...')
    elseif postSynaptic_cell == round(3*numel(state_cells)/4)
        disp('...')
    end
    
    % get synapse weight of connection with eight neighbour cells
    w = postSynaptic_cell-rows; n = postSynaptic_cell-1; s = postSynaptic_cell+1; e = postSynaptic_cell+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    test = [n e s w nw ne se sw];
    idx = find(test <= 0);
    test(idx) = test(idx) + rows*col;
    idx = find(test > rows*col);
    test(idx) = test(idx) - rows*col;
    
    n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
    
    neighbour_synapses = zeros(3);
    neighbour_synapses(1) = internal_synapses(nw,postSynaptic_cell);
    neighbour_synapses(2) = internal_synapses(w,postSynaptic_cell);
    neighbour_synapses(3) = internal_synapses(sw,postSynaptic_cell);
    neighbour_synapses(4) = internal_synapses(n,postSynaptic_cell);
    neighbour_synapses(5) = NaN;
    neighbour_synapses(6) = internal_synapses(s,postSynaptic_cell);
    neighbour_synapses(7) = internal_synapses(ne,postSynaptic_cell);
    neighbour_synapses(8) = internal_synapses(e,postSynaptic_cell);
    neighbour_synapses(9) = internal_synapses(se,postSynaptic_cell);
    
    % normalise them to 8
    neighbour_synapses = 0.1 * neighbour_synapses/nansum(nansum(neighbour_synapses));
    %neighbour_synapses = 0.2 * neighbour_synapses + 0.8 * 8 * neighbour_synapses/nansum(nansum(neighbour_synapses));
    
    % insert back into weight table
    %
    internal_synapses(nw,postSynaptic_cell) = neighbour_synapses(1);
    internal_synapses(w,postSynaptic_cell) = neighbour_synapses(2);
    internal_synapses(sw,postSynaptic_cell) = neighbour_synapses(3);
    internal_synapses(n,postSynaptic_cell) = neighbour_synapses(4);
    internal_synapses(s,postSynaptic_cell) = neighbour_synapses(6);
    internal_synapses(ne,postSynaptic_cell) = neighbour_synapses(7);
    internal_synapses(e,postSynaptic_cell) = neighbour_synapses(8);
    internal_synapses(se,postSynaptic_cell) = neighbour_synapses(9);
    %}
    %{
    internal_synapses(postSynaptic_cell,nw) = neighbour_synapses(1);
    internal_synapses(postSynaptic_cell,w) = neighbour_synapses(2);
    internal_synapses(postSynaptic_cell,sw) = neighbour_synapses(3);
    internal_synapses(postSynaptic_cell,n) = neighbour_synapses(4);
    internal_synapses(postSynaptic_cell,s) = neighbour_synapses(6);
    internal_synapses(postSynaptic_cell,ne) = neighbour_synapses(7);
    internal_synapses(postSynaptic_cell,e) = neighbour_synapses(8);
    internal_synapses(postSynaptic_cell,se) = neighbour_synapses(9);
    %}
end


% check if agent or reward is within a wall
agent_location = find(world(:,:,1),1);
reward_location = find(world(:,:,2),2);

if ismember(agent_location, (round((rows*(col/2)))+1:round((rows*(col/2)))+round((rows/5))))                          || ...
        ismember(agent_location, (round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5)))     || ...
        ismember(agent_location, (round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows))                  || ...
        ismember(agent_location, (rows:rows:rows*col))                                                                || ...
        ismember(agent_location, (1:rows:rows*col))                                                                   || ...
        ismember(agent_location, (1:rows))                                                                            || ...
        ismember(agent_location, ((rows-1)*col + 1 : rows*col))                                                       || ...
        ismember(agent_location, (rows/2:rows:rows*col/5))                                                            || ...
        ismember(agent_location, (rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5)))                              || ...
        ismember(agent_location, (rows/2 + rows*(4*col/5):rows:rows*col))
    
    error('Agent is located in a wall.')
    
elseif ismember(reward_location, (round((rows*(col/2)))+1:round((rows*(col/2)))+round((rows/5))))                      || ...
        ismember(reward_location, (round((rows*(col/2)))+round((2*rows/5)):round((rows*(col/2)))+round(3*rows/5)))     || ...
        ismember(reward_location, (round((rows*(col/2)))+round(4*rows/5):round((rows*(col/2)))+rows))                  || ...
        ismember(reward_location, (rows:rows:rows*col))                                                                || ...
        ismember(reward_location, (1:rows:rows*col))                                                                   || ...
        ismember(reward_location, (1:rows))                                                                            || ...
        ismember(reward_location, ((rows-1)*col + 1 : rows*col))                                                       || ...
        ismember(reward_location, (rows/2:rows:rows*col/5))                                                            || ...
        ismember(reward_location, (rows/2 + rows*(2*col/5):rows:rows/2 + rows*(3*col/5)))                              || ...
        ismember(reward_location, (rows/2 + rows*(4*col/5):rows:rows*col))   
    
    error('Reward is located in a wall.')
    
end


% create sensory synapses
sensory_weights = zeros;

% create reward synapse

disp('Setup complete.')

%% Induction

disp('Induction begun:')
% Activate agent state
state_cells(agent_y, agent_x) = agent_firing;

% Activate reward state(s)
state_cells(reward_y, reward_x) = reward_firing;

% Feed activation back into network
noNaNweights = internal_synapses;
noNaNweights(isnan(noNaNweights)) = 0;

for time = 1:steps
    
    if time == round(steps/4)
        disp('...')
    elseif time == round(steps/2)
        disp('...')
    elseif time == round(3*steps/4)
        disp('...')
    end
    
state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
activation = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNweights);
state_cells = reshape(activation,num_state);

% Competition:

state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;

% subtractive
state_cells = state_cells - mean_modifier * mean(mean(state_cells));

% divisive
state_cells = state_cells/max(max(state_cells));
state_cells(state_cells < 0) = 0;

% sigmoid
%state_cells = softCompetition(sparseness, slope, state_cells(:));
state_cells = reshape(state_cells,num_state);

state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
activation_display = vec2mat(state_cells, sqrt(numel(state_cells)));
activation_tracked{time} = activation_display;

end
disp('Induction complete.')

%% Analyse Data
% Display diagram of activation pathway
slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]); colorbar

end