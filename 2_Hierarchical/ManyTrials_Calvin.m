function [correct, incorrect] = ManyTrials_Calvin(Experiment, worldSize, trials, steps, ind_steps, use_hierarchy)

% Receives handle @ExperimentXX and iterates it over many trials, saving
% the synapses between chunk and state cells and using them again for the
% next.
%
% Agent position will vary, but reward will not. Should therefore see weak
% sequence learning far from the reward but strong sequence learning close
% to it, where agent paths converge.

%% Options
%use_hierarchy = 1;

if use_hierarchy == 1
    disp('Chunk => State connections ACTIVE.')
else
    disp('Chunk => State Connections INACTIVE')
end

%% Setup Trials
worldSize_x = worldSize;
worldSize_y = worldSize;

correct = 0;
incorrect = 0;
steps_tracked = 0;

%% Generate Synapses

% Parameters.
num_state = [worldSize_x worldSize_y];
num_chunk = [1 10];
radius = 5;
internal_weights = 0.01;
chunk_weights = 0.01;

% Generation.
disp('Generating internal synapses.')

state_cells = zeros(num_state);
chunk_cells = zeros(num_chunk);
rows = size(state_cells,1);
col = size(state_cells,2);

internal_synapses = gaussian_synapses(state_cells, radius, internal_weights);

disp('Generating external synapses.')
statetochunk_synapses = nan(numel(state_cells), numel(chunk_cells));

for presynaptic_cell = 1:numel(chunk_cells)
    statetochunk_synapses(randi(numel(state_cells), 1),presynaptic_cell) = chunk_weights;
end

chunktostate_synapses = zeros(numel(chunk_cells), numel(state_cells));

%% Walls
disp('Blocking off walls.')
% Parameters:
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% ascertain position of walls
top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

% walls have no presynaptic connections
internal_synapses(top_wall,:) = NaN;
internal_synapses(bottom_wall,:) = NaN;
internal_synapses(left_wall,:) = NaN;
internal_synapses(right_wall,:) = NaN;

% walls have no postsynaptic connections
internal_synapses(:, top_wall) = NaN;
internal_synapses(:, bottom_wall) = NaN;
internal_synapses(:, left_wall) = NaN;
internal_synapses(:, right_wall) = NaN;

%% Run Trials
for trial = 1:trials
    
    disp(trial)
    
    agent_x = randi(worldSize);
    agent_y = randi(worldSize);
    
    %reward_x = randi(worldSize);
    reward_x = 2;
    
    %reward_y = randi(worldSize);
    reward_y = 10;
    
    % check if agent or reward is within a wall
    agent_location = sub2ind([worldSize_x worldSize_y], agent_y, agent_x);
    reward_location = sub2ind([worldSize_x worldSize_y], reward_y, reward_x);
    
    try
        if ismember(agent_location, top_wall)             || ...
                ismember(agent_location, bottom_wall)     || ...
                ismember(agent_location, left_wall)       || ...
                ismember(agent_location, right_wall)
            
            error('Agent is located in a wall.')
            
        elseif ismember(reward_location, top_wall)         || ...
                ismember(reward_location, bottom_wall)     || ...
                ismember(reward_location, left_wall)       || ...
                ismember(reward_location, right_wall)
            
            error('Reward is located in a wall.')
            
        end
    catch err
        disp(err.message)
        result = 'X';
        continue
    end
    
    try
        [statetochunk_synapses, chunktostate_synapses, result, total_steps] = ...
            Experiment(use_hierarchy, steps, ind_steps, worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y, num_state, num_chunk, internal_synapses, statetochunk_synapses, chunktostate_synapses);
    catch err
        disp(err.message)
        result = 'X';
        continue
    end
    
    if result == 'Y'
        correct = correct + 1;
        steps_tracked = steps_tracked + 1;
    elseif result == 'N'
        incorrect = incorrect + 1;
    end
    
    steps_tracked = steps_tracked + total_steps;
    
end


disp(correct)
disp(incorrect)
disp(steps_tracked/(correct+incorrect))

%% Display Final Synapses

figure()
displayed = 0;
idx = 0;
for cell = 1:numel(chunk_cells)
    
    indices = chunktostate_synapses < 0.001;
    thresholded = chunktostate_synapses;
    thresholded(indices) = 0;
    displayed = displayed + 1;
    if mod(displayed,25) == 0;
       idx = 0;
        figure()
    end
    
    if any(thresholded(cell,:))
        idx = idx + 1;
        subplot(5,5,idx); imagesc(reshape(thresholded(cell,:),num_state)); colorbar
    end
end

end