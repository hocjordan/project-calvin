function [statetochunk_synapses, chunktostate_synapses, chunktosuper_synapses, supertochunk_synapses] = ...
    ManyTrials_Calvin29(Experiment, worldSize, trials, steps, ind_steps, ...
    use_chunk, use_super, chunk_learning, super_learning, agent_to_reward, swr_recall, ...
    statetochunk_synapses, chunktostate_synapses, chunktosuper_synapses, supertochunk_synapses)

% As MTC 2, but with 'super chunk' cells. Accompanies Experiment 29.

%% Options
%use_hierarchy = 1;

if use_chunk == 0
    disp('Chunk => State connections INACTIVE.')
else
    disp('Chunk => State Connections ACTIVE')
end

if use_super == 0
    disp('Super => Chunk connections INACTIVE.')
else
    disp('Super => Chunk Connections ACTIVE')
end

if chunk_learning == 1
    disp('Chunk Learning ALLOWED')
else
    disp('Chunk Learning NOT ALLOWED')
end

if super_learning == 1
    disp('Super-Chunk Learning ALLOWED')
else
    disp('Super-Chunk Learning NOT ALLOWED')
end

if agent_to_reward == 0
    disp('All agent positions possible.')
else
    fprintf('Agent will be placed within %d squares of reward.', agent_to_reward)
end

disp(' ')

if swr_recall == 1
    disp('Each sequence will be replayed upon completion to aid consolidation.')
end

display_chunk_synapses = 1;
display_super = 1;
%% Parameters

% Trials
worldSize_x = worldSize;
worldSize_y = worldSize;

correct = 0;
incorrect = 0;
distance = [];
steps_tracked = 0;
result_tracked = [];

% Cells
num_state = [worldSize_x worldSize_y];
num_chunk = [1 1000]; %300
num_super = [1 100];

% Synapses
internal_weights = 0.01;
chunk_weights = 0.0000001; % 0.01
super_weights = 0.00000001;
radius = 3;
chunk_connectivity = 1; %1
super_connectivity = 1;

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Analysis
mapping_threshold = 0.00005; % 0.005
chunk_allTrials = [];
super_allTrials = [];

%% Generate Cells and Synapses

state_cells = zeros(num_state);
chunk_cells = zeros(num_chunk);
super_cells = zeros(num_super);

disp('Generating state => state synapses.')

rows = size(state_cells,1);
col = size(state_cells,2);

% Internal.
internal_synapses = gaussian_synapses(state_cells, radius, internal_weights);

% Chunk.
if ~exist('statetochunk_synapses','var')
    disp('Generating state => chunk synapses.')
    statetochunk_synapses = Generate_Diluted_Weights(state_cells, chunk_cells, chunk_connectivity, chunk_weights);
else
    disp('Using provided state => chunk synapses.')
end
if ~exist('chunktostate_synapses','var')
    disp('Generating chunk => state synapses.')
    chunktostate_synapses = Generate_Diluted_Weights(chunk_cells, state_cells, chunk_connectivity, chunk_weights);
    %chunktostate_synapses = GenerateZeroWeights(numel(chunk_cells), numel(state_cells), chunk_connectivity);
else
    disp('Using provided chunk => state synapses.')
end


% Super.
if ~exist('chunktosuper_synapses','var')
    disp('Generating chunk => super synapses.')
    chunktosuper_synapses = Generate_Diluted_Weights(chunk_cells, super_cells, super_connectivity, super_weights);
    
else
    disp('Using provided chunk => super synapses.')
end
if ~exist('supertochunk_synapses','var')
    disp('Generating super => chunk synapses.')
    supertochunk_synapses = Generate_Diluted_Weights(super_cells, chunk_cells, super_connectivity, super_weights);
    %supertochunk_synapses = GenerateZeroWeights(numel(super_cells), numel(chunk_cells), super_connectivity);
else
    disp('Using provided super => chunk synapses.')
end

%% Walls
disp('Blocking off walls.')

% ascertain position of walls
top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

% walls have no presynaptic connections
internal_synapses(top_wall,:) = NaN;
internal_synapses(bottom_wall,:) = NaN;
internal_synapses(left_wall,:) = NaN;
internal_synapses(right_wall,:) = NaN;

% walls have no postsynaptic connections
internal_synapses(:, top_wall) = NaN;
internal_synapses(:, bottom_wall) = NaN;
internal_synapses(:, left_wall) = NaN;
internal_synapses(:, right_wall) = NaN;

%% Run Trials
for trial = 1:trials
    
    disp(trial)
    %{
    reward_x = randi(worldSize);
    reward_y = randi(worldSize);
    
    if agent_to_reward == 0
        agent_x = randi(worldSize);
        agent_y = randi(worldSize);
    else
        agent_x = randi([reward_x - agent_to_reward reward_x + agent_to_reward]);
        agent_y = randi([reward_y - agent_to_reward reward_y + agent_to_reward]);
    end
    %}
    %
    
    agent_x = randi([2 19]);
    %agent_x = 2+trial-1
    agent_y = 5;
    reward_x = randsample([agent_x-1 : 2 : agent_x+1],1);
    %reward_x = randsample([agent_x-3 : 6 : agent_x+3],1)
    %reward_x = randsample([agent_x-4 : 8 : agent_x+4],1)
    %reward_x = randsample([agent_x-5 : 10 : agent_x+5],1)
    %reward_x = randsample([agent_x-6 : 12 : agent_x+6],1)
    %reward_x = 3 + trial-1
    %reward_x = 14;
    reward_y = 5;
    %}
    try
        if agent_x > worldSize_x || agent_x <= 0 || agent_y > worldSize_y || agent_y <= 0
                error('Specified agent position does not exist in world.')
        elseif reward_x > worldSize_x || reward_x <= 0 || reward_y > worldSize_y || reward_y <= 0
            error('Specified reward position does not exist in world.')
        end
        
        % check if agent or reward is within a wall
        agent_location = sub2ind([worldSize_x worldSize_y], agent_y, agent_x);
        reward_location = sub2ind([worldSize_x worldSize_y], reward_y, reward_x);
        
    catch err
        disp(err.message)
        result = [];
        continue
    end
    
    
    
    try
        if ismember(agent_location, top_wall)             || ...
                ismember(agent_location, bottom_wall)     || ...
                ismember(agent_location, left_wall)       || ...
                ismember(agent_location, right_wall)
            
            error('Agent is located in a wall.')
            
        elseif ismember(reward_location, top_wall)         || ...
                ismember(reward_location, bottom_wall)     || ...
                ismember(reward_location, left_wall)       || ...
                ismember(reward_location, right_wall)
            
            error('Reward is located in a wall.')
            
        end
    catch err
        disp(err.message)
        result = 'X';
        result = [];
        continue
    end
    
    % Run One Trial
    try
        [statetochunk_synapses, chunktostate_synapses, chunktosuper_synapses, supertochunk_synapses, result, total_steps, chunk_activation_tracked, super_activation_tracked] = ...
            Experiment(use_chunk, use_super, chunk_learning, super_learning, steps, ind_steps, worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y, ...
            num_state, num_chunk, num_super, internal_synapses, statetochunk_synapses, chunktostate_synapses, chunktosuper_synapses, supertochunk_synapses);
    catch err
        disp(err.message)
        result = 'X';
    end
    
    % SHARP WAVE RIPPLE -- Run Extra Trials.
    if swr_recall > 0
        for ripple = 1:swr_recall
            try
                [statetochunk_synapses, chunktostate_synapses, chunktosuper_synapses, supertochunk_synapses, result, total_steps, chunk_activation_tracked, super_activation_tracked] = ...
                    Experiment(use_chunk, use_super, chunk_learning, super_learning, steps, ind_steps, worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y, ...
                    num_state, num_chunk, num_super, internal_synapses, statetochunk_synapses, chunktostate_synapses, chunktosuper_synapses, supertochunk_synapses);
            catch err
                disp(err.message)
                result = 'X';
            end
        end
    end
    %}
    
    if result == 'Y'
        correct = correct + 1;
        steps_tracked = steps_tracked + 1;
        result = 1;
    elseif result == 'N'
        incorrect = incorrect + 1;
        result = 0;
    elseif result == 'X'
        distance(trial) = [];
        result = [];
        continue
    end
    
    result_tracked = [result_tracked result];
    steps_tracked = steps_tracked + total_steps;
    distance = [distance sqrt((agent_x - reward_x)^2 + (agent_y - reward_y)^2)];
    chunk_allTrials = [chunk_allTrials chunk_activation_tracked];
    super_allTrials = [super_allTrials super_activation_tracked];
    
end

fprintf('Number of correct trials = %d', correct)
disp(' ')
fprintf('Number of incorrect trials = %d', incorrect)
disp(' ')
fprintf('Average of %f steps taken', steps_tracked/(correct+incorrect));
disp(' ')
fprintf('Chunk cells fired: %s', num2str(sort(chunk_allTrials)));
disp(' ')
fprintf('Super-Chunk cells fired: %s', num2str(sort(super_allTrials)));
disp(' ')

distance = int32(distance);
[distance, idx] = sort(distance);
result_tracked = result_tracked(idx);
figure(); plot(distance, result_tracked)

%% Display Final Synapses

if display_chunk_synapses == 1;
    
    figure()
    displayed = 0;
    idx = [];
    
    %
    indices = chunktostate_synapses < mapping_threshold/10;
    thresholded = chunktostate_synapses;
    thresholded(indices) = 0;
    thresholded(isnan(thresholded)) = 0;
    %}
    
    for cell = 1:numel(chunk_cells)
        %if nansum(chunktostate_synapses(cell,:)) > mapping_threshold
        if any(thresholded(cell,:))
            idx = [idx cell];
        end
    end
    
    idx
    
    for count = 1:numel(idx)
        displayed = displayed+1;
        cell = idx(count);
        if mod(displayed,25) == 0;
            displayed = 1;
            figure()
        end
        subplot(5,5,displayed); imagesc(reshape(chunktostate_synapses(cell,:),num_state)); colorbar; title('Chunk => State Synapses');
        %subplot(5,5,displayed); imagesc(reshape(thresholded(cell,:),num_state)); colorbar
    end
    
end

if display_super == 1;
    
    figure()
    displayed = 0;
    idx = [];
    
    %
    indices = supertochunk_synapses < 0;
    thresholded = supertochunk_synapses;
    thresholded(indices) = 0;
    thresholded(isnan(thresholded)) = 0;
    %}
    
    for cell = 1:numel(super_cells)
        %if nansum(chunktostate_synapses(cell,:)) > mapping_threshold
        if any(thresholded(cell,:))
            idx = [idx cell];
        end
    end
    
    %disp(idx)
    
    for count = 1:numel(idx)
        displayed = displayed+1;
        cell = idx(count);
        if mod(displayed,25) == 0;
            displayed = 1;
            figure()
        end
        subplot(5,5,displayed); plot(supertochunk_synapses(cell,:)); title('Super => Chunk Synapses');
        %subplot(5,5,displayed); imagesc(reshape(thresholded(cell,:),num_state)); colorbar
    end
    
end

end