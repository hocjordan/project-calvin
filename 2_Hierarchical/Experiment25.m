%% Bidirectional Test 1

function activation_display = Experiment25(steps, ind_steps, use_hierarchy)

% As Expt. 22, but using a many-many hierarchy rather than one-to-one,
% one-to-many. Also incorporates the noise mechanism from Expt. 24.

%% Options
if use_hierarchy == 1
    disp('Chunk => State connections ACTIVE.')
else
    disp('Chunk => State Connections INACTIVE')
end

%% Parameters
% world
worldSize_x = 20;
worldSize_y = 20;
agent_x = 18;
agent_y = 10;
reward_x = 12;
reward_y = 5;

if agent_x > worldSize_x || agent_y > worldSize_y
    error('Specified agent position does not exist in world.')
elseif reward_x > worldSize_x || reward_y > worldSize_y
    error('Specified reward position does not exist in world.')
end

% wall
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% cells
num_state = [worldSize_x worldSize_y];
num_chunk = [1 100];

% attractors
agent_firing = 2;
reward_firing = 1;

% connections
radius = 5;
chunk_connectivity = 0.5;
internal_weights = 0.01;
chunk_weights = 0.001;
learningRate = .1;
eta = 0.8;

% competition
state_modifier = 0.1;
chunk_modifier = 0.1;
sparseness = 99;
slope = 1;

% analysis
mapping_threshold = 0.2;

%% Setup
disp('Setup begun:')

% create world

disp('Creating world.')
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% create cells

disp('Creating state cells.')
state_cells = zeros(num_state);
chunk_cells = zeros(num_chunk);
chunk_trace = zeros(num_chunk);

% generate bidirectional internal synapses

disp('Generating internal synapses.')

rows = size(state_cells,1);
col = size(state_cells,2);

internal_synapses = gaussian_synapses(state_cells, radius, internal_weights);

disp('Generating external synapses.')
statetochunk_synapses = Generate_Diluted_Weights(state_cells, chunk_cells, chunk_connectivity, 0.0001);

chunktostate_synapses = Generate_Diluted_Weights(chunk_cells, state_cells, chunk_connectivity, 0.0001);

disp('Blocking off walls.')

% ascertain position of walls
top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

% walls have no presynaptic connections
internal_synapses(top_wall,:) = NaN;
internal_synapses(bottom_wall,:) = NaN;
internal_synapses(left_wall,:) = NaN;
internal_synapses(right_wall,:) = NaN;

% walls have no postsynaptic connections
internal_synapses(:, top_wall) = NaN;
internal_synapses(:, bottom_wall) = NaN;
internal_synapses(:, left_wall) = NaN;
internal_synapses(:, right_wall) = NaN;

% check if agent or reward is within a wall
agent_location = find(world(:,:,1),1);
reward_location = find(world(:,:,2),2);

if ismember(agent_location, top_wall)             || ...
        ismember(agent_location, bottom_wall)     || ...
        ismember(agent_location, left_wall)       || ...
        ismember(agent_location, right_wall)
    
    error('Agent is located in a wall.')
    
elseif ismember(reward_location, top_wall)         || ...
        ismember(reward_location, bottom_wall)     || ...
        ismember(reward_location, left_wall)       || ...
        ismember(reward_location, right_wall)
    
    error('Reward is located in a wall.')
    
end

disp('Setup complete.')

%% Run Step:

for time = 1:steps
    disp(time)
    
    %% Induction
    
    disp('Induction begun:')
    % Activate agent state
    state_cells(agent_y, agent_x) = agent_firing;
    
    % Activate reward state(s)
    state_cells(reward_y, reward_x) = reward_firing;
    
    % Feed activation back into network
    noNaNinternal = internal_synapses;
    noNaNinternal(isnan(noNaNinternal)) = 0;
    
    noNaNstatetochunk = statetochunk_synapses;
    noNaNstatetochunk(isnan(noNaNstatetochunk)) = 0;
    
    noNaNchunktostate = chunktostate_synapses;
    noNaNchunktostate(isnan(noNaNchunktostate)) = 0;
    
    for ind_time = 1:ind_steps
        
        if ind_time == round(ind_steps/4)
            disp('...')
        elseif ind_time == round(ind_steps/2)
            disp('...')
        elseif ind_time == round(3*ind_steps/4)
            disp('...')
        end
        
        % Set firing rate of agent & reward representations.
        state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        % Calculate activation of state cells from the recurrent
        % connections within that layer, and save it.
        state_cells = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNinternal);
        state_cells = reshape(state_cells,num_state);
        state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        state_save = state_cells;
        
        % Calculate activation of chunk cells from state cells
        chunk_cells = dot(repmat(state_cells(:),[1,numel(chunk_cells)]), noNaNstatetochunk);
        chunk_cells = reshape(chunk_cells,num_chunk);
        
        % Competition in chunk cells:
        
        % Subtractive.
        chunk_cells = chunk_cells - chunk_modifier * mean(mean(chunk_cells));
        chunk_cells(chunk_cells < 0) = 0;
        
        if use_hierarchy == 1;
            % Reset state cells.
            state_cells(:) = 0;
            
            %Calculate activation of state cells from the combined input of
            %recurrent and hierarchical synapses
            state_cells = dot([repmat(state_save(:),[1,numel(state_cells)]); repmat(chunk_cells(:),[1,numel(state_cells)])], [noNaNinternal; noNaNchunktostate]);
            state_cells = reshape(state_cells,num_state);
        end
        
        % Competition in state cells:
        
        state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        % subtractive.
        state_cells = state_cells - state_modifier * mean(mean(state_cells));
        
        % divisive.
        state_cells = state_cells/max(max(state_cells));
        state_cells(state_cells < 0) = 0;
        
        state_cells = reshape(state_cells,num_state);
        
        state_cells(agent_y, agent_x) = agent_firing; state_cells(reward_y, reward_x) = reward_firing;
        
        % add noise
        state_cells = state_cells(:)' + 0.002.*std(state_cells(:))*randn(1,size(state_cells(:),1));
        state_cells(state_cells < 0) = 0;
        state_cells = reshape(state_cells,num_state);
        
        
        activation_display = vec2mat(state_cells, sqrt(numel(state_cells)));
        
        % LEARNING:
        % Uses a reversed trace rule -- preSynaptic_trace * postSynaptic_fr
        
        % Calculate memory trace for chunk cells.
        chunk_trace = getTrace(chunk_cells, chunk_trace, eta);
        
        % Update synapses from chunk to state cells.
        chunktostate_synapses = chunktostate_synapses + learningRate * chunk_trace(:) * state_cells(:)';
        
        % Normalise synapse weights.
        chunktostate_synapses = normalise(chunktostate_synapses');
        chunktostate_synapses = chunktostate_synapses';
        
    end
    disp('Induction complete.')
    
    %% Gather Step Data
    
    % Save activation pathway
    activation_tracked{time} = activation_display;
    
    % Save
    world_tracked{time} = world(:,:,1);
    
    %% Move Agent
    
    % find the most active state cell(s) next to the current position
    rows = size(state_cells,1);
    col = size(state_cells,2);
    
    current_state = sub2ind(size(state_cells), agent_y, agent_x);
    
    w = current_state-rows; n = current_state-1; s = current_state+1; e = current_state+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    test = [n e s w nw ne se sw];
    idx = find(test <= 0);
    test(idx) = test(idx) + rows*col;
    idx = find(test > rows*col);
    test(idx) = test(idx) - rows*col;
    
    n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);
    
    neighbours = zeros(3);
    
    neighbours(1) = state_cells(nw);
    neighbours(2) = state_cells(w);
    neighbours(3) = state_cells(sw);
    neighbours(4) = state_cells(n);
    neighbours(5) = 0;
    neighbours(6) = state_cells(s);
    neighbours(7) = state_cells(ne);
    neighbours(8) = state_cells(e);
    neighbours(9) = state_cells(se);
    
    %neighbours = round(neighbours * 100)/100;
    next_turn = find(neighbours == max(max(neighbours)));
    if size(next_turn,1) > 1
        disp('RANDOM ACTION SELECTED')
    end
    
    % Activate one cell
    actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};
    action = actions{next_turn(randi(size(next_turn,1)))};
    disp(action)
    world = update_world2(world, action);
    
    reward_location = sub2ind([worldSize_x worldSize_y], reward_y, reward_x); 
    if current_state == reward_location
        result = 'Y';
        disp(result)
        break
    elseif time == steps
        result = 'N';
        disp(result)
        break
    end
    
    %% Update Parameters
    
    state_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

%% Analyse Steps

slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1])
%slider_display(activation_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1])

%% Display Chunk Cell Synapses

figure()
displayed = 0;
idx = [];

indices = chunktostate_synapses < 0.001;
thresholded = chunktostate_synapses;
thresholded(indices) = 0;
thresholded(isnan(thresholded)) = 0;


for cell = 1:numel(chunk_cells)
    if any(thresholded(cell,:))
        idx = [idx cell];
    end
end

for count = 1:numel(idx)
    displayed = displayed+1;
    cell = idx(count);
    if mod(displayed,25) == 0;
        displayed = 1;
        figure()
    end
    subplot(5,5,displayed); imagesc(reshape(thresholded(cell,:),num_state)); colorbar
end

end











function matrix = normalise(matrix)

% Normalise matrix so that each column's sum approaches 1. Ignores NaN.

%get number of rows and columns in matrix

[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);
for column = 1:columns
    if summed(column) > 1
        %divide each row in that column by that sum
        for row = 1:rows
            matrix(row,column) = (matrix(row,column)/summed(column));
        end
    end
end
end


function [trace] = getTrace(firingRate, trace, eta)

trace = ((1-eta)*firingRate) + eta*trace;
end