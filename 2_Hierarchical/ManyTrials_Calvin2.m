function [statetochunk_synapses, chunktostate_synapses] = ManyTrials_Calvin2(Experiment, worldSize, trials, steps, ind_steps, use_hierarchy, hierarchy_learning, agent_to_reward, swr_recall, statetochunk_synapses, chunktostate_synapses)

% Receives handle @ExperimentXX and iterates it over many trials, saving
% the synapses between chunk and state cells and using them again for the
% next.
%
% Agent position will vary, but reward will not. Should therefore see weak
% sequence learning far from the reward but strong sequence learning close
% to it, where agent paths converge.

%% Options
%use_hierarchy = 1;

if use_hierarchy == 0
    disp('Chunk => State connections INACTIVE.')
else
    disp('Chunk => State Connections ACTIVE')
end

if hierarchy_learning == 1
    disp('Learning ALLOWED')
else
    disp('Learning NOT ALLOWED')
end

if agent_to_reward == 0
    disp('All agent positions possible.')
else
    fprintf('Agent will be placed within %d squares of reward.', agent_to_reward)
end

disp(' ')

if swr_recall == 1
    disp('Each sequence will be replayed upon completion to aid consolidation.')
end

display_synapses = 1;

%% Parameters

% Trials
worldSize_x = worldSize;
worldSize_y = worldSize;

correct = 0;
incorrect = 0;
distance = [];
steps_tracked = 0;
result_tracked = [];

% Cells
num_state = [worldSize_x worldSize_y];
num_chunk = [1 1000]; %300

% Synapses
internal_weights = 0.01;
chunk_weights = 0.00001; % 0.01
radius = 2;
chunk_connectivity = 1; %1

% Walls
left_thick = 1;
right_thick = 1;
top_thick = 1;
bottom_thick = 1;

% Analysis
mapping_threshold = 0.001; % 0.005
chunk_allTrials = [];

%% Generate Synapses

% Generation.
disp('Generating internal synapses.')

state_cells = zeros(num_state);
chunk_cells = zeros(num_chunk);
rows = size(state_cells,1);
col = size(state_cells,2);

internal_synapses = gaussian_synapses(state_cells, radius, internal_weights);

if ~exist('statetochunk_synapses','var')
    disp('Generating state => chunk synapses.')
    statetochunk_synapses = Generate_Diluted_Weights(state_cells, chunk_cells, chunk_connectivity, chunk_weights);
    
    
    %{
    
    statetochunk_synapses = nan(numel(state_cells), numel(chunk_cells));

for presynaptic_cell = 1:numel(chunk_cells)
    statetochunk_synapses(randi(numel(state_cells), 1),presynaptic_cell) = chunk_weights;
end
    %}
    
    
    
    
else
    disp('Using provided state => chunk synapses.')
end
if ~exist('chunktostate_synapses','var')
    disp('Generating chunk => state synapses.')
    chunktostate_synapses = Generate_Diluted_Weights(chunk_cells, state_cells, chunk_connectivity, chunk_weights);
    %chunktostate_synapses = GenerateZeroWeights(numel(chunk_cells), numel(state_cells), chunk_connectivity);
else
    disp('Using provided chunk => state synapses.')
end

%% Walls
disp('Blocking off walls.')

% ascertain position of walls
top_wall = [];
while top_thick > 0
    top_wall = [top_wall, top_thick:rows:col*rows];
    top_thick = top_thick - 1;
end

bottom_wall = [];
while bottom_thick > 0
    bottom_wall = [bottom_wall, rows-bottom_thick+1:rows:rows*col];
    bottom_thick = bottom_thick - 1;
end

left_wall = 1:rows*left_thick;
right_wall = rows*(col-right_thick)+1:rows*col;

% walls have no presynaptic connections
internal_synapses(top_wall,:) = NaN;
internal_synapses(bottom_wall,:) = NaN;
internal_synapses(left_wall,:) = NaN;
internal_synapses(right_wall,:) = NaN;

% walls have no postsynaptic connections
internal_synapses(:, top_wall) = NaN;
internal_synapses(:, bottom_wall) = NaN;
internal_synapses(:, left_wall) = NaN;
internal_synapses(:, right_wall) = NaN;

%% Run Trials
for trial = 1:trials
    
    disp(trial)
    %{
    reward_x = randi(worldSize);
    reward_y = randi(worldSize);
    
    if agent_to_reward == 0
        agent_x = randi(worldSize);
        agent_y = randi(worldSize);
    else
        agent_x = randi([reward_x - agent_to_reward reward_x + agent_to_reward]);
        agent_y = randi([reward_y - agent_to_reward reward_y + agent_to_reward]);
    end
    %}
    %
    
    agent_x = randi([5 15])
    agent_y = 5;
    %reward_x = randsample([agent_x-1 : 2 : agent_x+1],1)
    reward_x = 14;
    reward_y = 5;
    %}
    try
        if agent_x > worldSize_x || agent_y > worldSize_y ...
                error('Specified agent position does not exist in world.')
        elseif reward_x > worldSize_x || reward_y > worldSize_y
            error('Specified reward position does not exist in world.')
        end
        
        % check if agent or reward is within a wall
        agent_location = sub2ind([worldSize_x worldSize_y], agent_y, agent_x);
        reward_location = sub2ind([worldSize_x worldSize_y], reward_y, reward_x);
        
    catch err
        disp(err.message)
        result = [];
        continue
    end
    
    
    
    try
        if ismember(agent_location, top_wall)             || ...
                ismember(agent_location, bottom_wall)     || ...
                ismember(agent_location, left_wall)       || ...
                ismember(agent_location, right_wall)
            
            error('Agent is located in a wall.')
            
        elseif ismember(reward_location, top_wall)         || ...
                ismember(reward_location, bottom_wall)     || ...
                ismember(reward_location, left_wall)       || ...
                ismember(reward_location, right_wall)
            
            error('Reward is located in a wall.')
            
        end
    catch err
        disp(err.message)
        result = 'X';
        result = [];
        continue
    end
    
    try
        [statetochunk_synapses, chunktostate_synapses, result, total_steps, chunk_activation_tracked] = ...
            Experiment(use_hierarchy, hierarchy_learning, steps, ind_steps, worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y, num_state, num_chunk, internal_synapses, statetochunk_synapses, chunktostate_synapses);
    catch err
        disp(err.message)
        result = 'X';
    end
    
    % SHARP WAVE RIPPLE -- DELETE AFTER USE!!
    if swr_recall > 0
        for ripple = 1:swr_recall
            try
                [statetochunk_synapses, chunktostate_synapses, result, total_steps] = ...
                    Experiment(use_hierarchy, hierarchy_learning, steps, ind_steps, worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y, num_state, num_chunk, internal_synapses, statetochunk_synapses, chunktostate_synapses);
            catch err
                disp(err.message)
                result = 'X';
            end
        end
    end
    %}
    
    if result == 'Y'
        correct = correct + 1;
        steps_tracked = steps_tracked + 1;
        result = 1;
    elseif result == 'N'
        incorrect = incorrect + 1;
        result = 0;
    elseif result == 'X'
        distance(trial) = [];
        result = [];
        continue
    end
    
    result_tracked = [result_tracked result];
    steps_tracked = steps_tracked + total_steps;
    distance = [distance sqrt((agent_x - reward_x)^2 + (agent_y - reward_y)^2)];
    chunk_allTrials = [chunk_allTrials chunk_activation_tracked];
    
end

fprintf('Number of correct trials = %d', correct)
disp(' ')
fprintf('Number of incorrect trials = %d', incorrect)
disp(' ')
fprintf('Average of %f steps taken', steps_tracked/(correct+incorrect));
disp(' ')
fprintf('Chunk cells fired: %s', num2str(sort(chunk_allTrials)));
disp(' ')

distance = int32(distance);
[distance, idx] = sort(distance);
result_tracked = result_tracked(idx);
figure(); plot(distance, result_tracked)

%% Display Final Synapses

if display_synapses == 1;
    
    figure()
    displayed = 0;
    idx = [];
    
    %
    indices = chunktostate_synapses < mapping_threshold/10;
    thresholded = chunktostate_synapses;
    thresholded(indices) = 0;
    thresholded(isnan(thresholded)) = 0;
    %}
    
    for cell = 1:numel(chunk_cells)
        %if nansum(chunktostate_synapses(cell,:)) > mapping_threshold
        if any(thresholded(cell,:))
            idx = [idx cell];
        end
    end
    
    %disp(idx)
    
    for count = 1:numel(idx)
        displayed = displayed+1;
        cell = idx(count);
        if mod(displayed,25) == 0;
            displayed = 1;
            figure()
        end
        subplot(5,5,displayed); imagesc(reshape(chunktostate_synapses(cell,:),num_state)); colorbar
        %subplot(5,5,displayed); imagesc(reshape(thresholded(cell,:),num_state)); colorbar
    end
    
end


end