function [statetochunk_synapses, chunktostate_synapses, chunktosuper_synapses, supertochunk_synapses, result, total_steps, chunk_activation_tracked, super_activation_tracked] = ...
    Experiment29(use_chunk, use_super, chunk_learning, super_learning, steps, ind_steps, worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y, ...
    num_state, num_chunk, num_super, internal_synapses, statetochunk_synapses, chunktostate_synapses, chunktosuper_synapses, supertochunk_synapses)



% As Expt. 28, with an extra layer of 'super_chunk' cells that learn sequences of chunk cells.



%% Parameters

% world



if agent_x > worldSize_x || agent_y > worldSize_y
    error('Specified agent position does not exist in world.')

elseif reward_x > worldSize_x || reward_y > worldSize_y
    error('Specified reward position does not exist in world.')

end



% attractors
agent_firing = 2;
reward_firing = 0.5; %0.5

% connections
chunk_learningRate = 0.000001; %1 % 0.05 %0.000001
super_learningRate = 0.0000001;
eta = 0.8; %0.8

% competition
state_modifier = 0.2;
chunk_modifier = 1.75; %1.75
super_modifier = 1;

% noise
state_noise = 0.001;
chunk_noise = 0; %1
super_noise = 0;

% normalisation
normalisation_threshold = 0.0001;  %0.0001; %0.00025; %0.001 %0.05

% tracking
chunk_activation_tracked = [];
super_activation_tracked = [];


%% Setup



% create world
world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);



% create cells
%disp('Creating state cells.')

state_cells = zeros(num_state);
chunk_cells = zeros(num_chunk);
super_cells = zeros(num_super);

state_trace = zeros(num_state);
chunk_trace = zeros(num_chunk);
super_trace = zeros(num_super);

%disp('Setup complete.')


%% Run Step:



for time = 1:steps
    
    
    

    %% Induction

    
    % Activate reward state(s)
    
    state_cells(reward_y, reward_x) = reward_firing;
    
    % Activate agent state

    state_cells(agent_y, agent_x) = agent_firing;

    
    % Remove NaN values from synapse matrices to make dot-product calculations
    % work.

    noNaNstatetostate = internal_synapses;
    noNaNstatetostate(isnan(noNaNstatetostate)) = 0;

    noNaNstatetochunk = statetochunk_synapses;
    noNaNstatetochunk(isnan(noNaNstatetochunk)) = 0;

    noNaNchunktostate = chunktostate_synapses;
    noNaNchunktostate(isnan(noNaNchunktostate)) = 0;
    
    noNaNchunktosuper = chunktosuper_synapses;
    noNaNchunktosuper(isnan(noNaNchunktosuper)) = 0;
    
    noNaNsupertochunk = supertochunk_synapses;
    noNaNsupertochunk(isnan(noNaNsupertochunk)) = 0;

    for ind_time = 1:ind_steps

        

        % Set firing rate of agent & reward representations.
        state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;

        
        %% Initial State Activation
        % Calculate activation of state cells from the recurrent
        % connections within that layer, and save it.

        state_cells = dot(repmat(state_cells(:),[1,numel(state_cells)]), noNaNstatetostate);
        state_cells = reshape(state_cells,num_state);
        state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
        state_save = state_cells;
        
        %% Initial Chunk Activation
        % Calculate activation of chunk cells from state cells
        chunk_cells = dot(repmat(state_cells(:),[1,numel(chunk_cells)]), noNaNstatetochunk);
        chunk_cells = reshape(chunk_cells,num_chunk);
        chunk_save = chunk_cells;
        
        %% Super Activation
        % Calculate activation of super-chunk cells from chunk cells.
        super_cells = dot(repmat(chunk_cells(:),[1,numel(super_cells)]), noNaNchunktosuper);
        super_cells = reshape(super_cells,num_super);
        
        % Competition in super-chunk cells:
        
        %super_cells = WTA_Competition(super_cells);
        %
        % Subtractive.
        super_cells = super_cells - super_modifier * mean(mean(super_cells));
        super_cells(super_cells < 0) = 0;
        %}
        if any(super_cells) == 0
            disp('No super-chunk cells firing')
        else
            % divisive
            super_cells = super_cells/max(max(super_cells));
        end
        
        % add noise
        super_cells = super_cells(:)' + super_noise * std(super_cells(:)) * randn(1,size(super_cells(:),1));
        super_cells(super_cells < 0) = 0;
        super_cells = reshape(super_cells,num_super);
        
        %% Final Chunk Activation
        if use_super == 1
            
            % Reset chunk cells
            chunk_cells(:) = 0;
            
            % Calculate activation of chunk cells from the combined input
            % of state and super-chunk synapses.
            chunk_cells = dot([repmat(state_cells(:),[1,numel(chunk_cells)]); repmat(super_cells(:),[1,numel(chunk_cells)])], [noNaNstatetochunk; noNaNsupertochunk]);
            chunk_cells = reshape(chunk_cells,num_chunk);
            
        end
        % Competition in chunk cells:
        %
        % Subtractive.
        chunk_cells = chunk_cells - chunk_modifier * mean(mean(chunk_cells));
        chunk_cells(chunk_cells < 0) = 0;
        
        if any(chunk_cells) == 0
            disp('No chunk cells firing')
        else
            % divisive ALTERED!!!!
            chunk_cells = 1 * (chunk_cells/max(max(chunk_cells)));
        end
        
        % add noise
        chunk_cells = chunk_cells(:)' + chunk_noise * std(chunk_cells(:)) * randn(1,size(chunk_cells(:),1));
        chunk_cells(chunk_cells < 0) = 0;
        chunk_cells = reshape(chunk_cells,num_chunk);
        %}
        %chunk_cells = WTA_Competition(chunk_cells);
        
        %% Final State Activation
        if use_chunk == 1;
            
            % Reset state cells.
            state_cells(:) = 0;
            
            %Calculate activation of state cells from the combined input of
            %recurrent and hierarchical synapses
            state_cells = dot([repmat(state_save(:),[1,numel(state_cells)]); repmat(chunk_cells(:),[1,numel(state_cells)])], [noNaNstatetostate; noNaNchunktostate]);
            state_cells = reshape(state_cells,num_state);
        end
        
        % Competition in state cells:
       
        state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
        
        % subtractive.
        state_cells = state_cells - state_modifier * mean(mean(state_cells));
        
        % divisive.
        state_cells = state_cells/max(max(state_cells));
        state_cells(state_cells < 0) = 0;
        
        state_cells = reshape(state_cells,num_state);
        
        state_cells(reward_y, reward_x) = reward_firing; state_cells(agent_y, agent_x) = agent_firing;
        
        % add noise
        state_cells = state_cells(:)' + state_noise .* std(state_cells(:)) * randn(1,size(state_cells(:),1));
        state_cells(state_cells < 0) = 0;
        state_cells = reshape(state_cells,num_state);
        
        activation_display = vec2mat(state_cells, sqrt(numel(state_cells)));
        chunk_activation_display = find(chunk_cells > 0.5);
        super_activation_display = find(super_cells > 0.5);
        
        %% Learning.
        
            % LEARNING:
            % Uses a full trace rule -- preSynaptic_trace *
            % postSynaptic_trace
            
            % Calculate memory trace for all cells.
            super_trace = getTrace(super_cells, super_trace, eta);
            chunk_trace = getTrace(chunk_cells, chunk_trace, eta);
            state_trace = getTrace(state_cells, state_trace, eta);
            
            if chunk_learning == 1
                
                % Update synapses between chunk and state cells.
                %chunktostate_synapses = chunktostate_synapses + learningRate * chunk_trace(:) * state_cells(:)';
                %statetochunk_synapses = statetochunk_synapses + learningRate * state_cells(:) * chunk_trace(:)';
                chunktostate_synapses = chunktostate_synapses + chunk_learningRate * chunk_trace(:) * state_trace(:)';
                statetochunk_synapses = statetochunk_synapses + chunk_learningRate * state_trace(:) * chunk_trace(:)';
                
                % Normalise synapse weights to and from chunk cells.
                statetochunk_synapses = normalise(statetochunk_synapses, normalisation_threshold);
                chunktostate_synapses = normalise(chunktostate_synapses', normalisation_threshold);
                chunktostate_synapses = chunktostate_synapses';
                
            end
            
            if super_learning == 1
                
                % Update synapses between chunk and super-chunk cells.
                chunktosuper_synapses = chunktosuper_synapses + super_learningRate * chunk_trace(:) * super_trace(:)';
                supertochunk_synapses = supertochunk_synapses + super_learningRate * super_trace(:) * chunk_trace(:)';
                
                % Normalise synapses between chunk and super-chunk cells.
                chunktosuper_synapses = normalise(chunktosuper_synapses, normalisation_threshold);
                supertochunk_synapses = normalise(supertochunk_synapses', normalisation_threshold);
                supertochunk_synapses = supertochunk_synapses';
                
            end
        
    end
    %disp('Induction complete.')
    
    %% Gather Step Data
    
    % Save activation pathway
    activation_tracked{time} = activation_display;
    chunk_activation_tracked = [chunk_activation_tracked chunk_activation_display];
    super_activation_tracked = [super_activation_tracked super_activation_display];
    world_tracked{time} = world(:,:,1);
    
    
    %% Check for Completion
    current_state = sub2ind(size(state_cells), agent_y, agent_x);
    reward_location = sub2ind([worldSize_x worldSize_y], reward_y, reward_x);
    if current_state == reward_location
        result = 'Y';
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Move Agent
    
    % find the most active state cell(s) next to the current position
    rows = size(state_cells,1);
    col = size(state_cells,2);
    
    current_state = sub2ind(size(state_cells), agent_y, agent_x);
    
    w = current_state-rows; n = current_state-1; s = current_state+1; e = current_state+rows;
    nw = w-1; sw = w+1; ne = e-1; se = e+1;
    
    test = [n e s w nw ne se sw];
    idx = find(test <= 0);
    test(idx) = test(idx) + rows*col;
    idx = find(test > rows*col);
    test(idx) = test(idx) - rows*col;
    
    n = test(1); e = test(2); s = test(3); w = test(4); nw = test(5); ne = test(6); se = test(7); sw = test(8);

    neighbours = zeros(3);
    
    neighbours(1) = state_cells(nw);
    neighbours(2) = state_cells(w);
    neighbours(3) = state_cells(sw);
    neighbours(4) = state_cells(n);
    neighbours(5) = 0;
    neighbours(6) = state_cells(s);
    neighbours(7) = state_cells(ne);
    neighbours(8) = state_cells(e);
    neighbours(9) = state_cells(se);
    
    %neighbours = round(neighbours * 100)/100;

    next_turn = find(neighbours == max(max(neighbours)));
    if size(next_turn,1) > 1
        disp('RANDOM ACTION SELECTED')
    end
    
    % Activate one cell

    actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};
    action = actions{next_turn(randi(size(next_turn,1)))};
    disp(action)
    world = update_world2(world, action);
   
    
    %% Update Parameters
    
    state_cells(:) = 0;
    chunk_cells(:) = 0;
    super_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

%disp(sort(chunk_activation_tracked))
chunk_activation_tracked = unique(chunk_activation_tracked);

end















function matrix = normalise(matrix, threshold)



% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.



%get number of rows and columns in matrix



[rows, columns] = size(matrix);



% sum each column

summed = nansum(matrix);

for column = 1:columns

    %if summed(column) > threshold

        %divide each row in that column by that sum

        for row = 1:rows

            matrix(row,column) = threshold * (matrix(row,column)/summed(column));

        end

    %end

end

end





function [trace] = getTrace(firingRate, trace, eta)



trace = ((1-eta)*firingRate) + eta*trace;

end