function SinglePatternBackProp(lrate, epochs, slope, normalise)

    cells=3;

    target=[1; 0; 0];
    
    v=[1; 0; 1; 0; 1; 0];
    
    weights=[0.2, 0.1, 0.4, 0.35, 0.4, 0.6; ...
             0.6, 0.5, 0.2, 0.2, 0.7, 0.15; ...
             0.8, 0.6, 0.5, 0.21, 0.7, 0.3];
    
    if normalise
        weights=normr(weights);
    end
    
    rmsTime=zeros(epochs, 1);
    
    for epoch=1:epochs
        
        activations=weights*v;
        rates=1./(1+exp(-2*slope*(activations-5)));
        
        weights=weights+(lrate*(target-rates)*v');
        
        weights(weights<0)=0;
        
        if normalise
            weights=normr(weights);
        end
        
        rmsTime(epoch)=sqrt(sum((target-rates).^2)/cells);
        
    end
    
    %{
    figure
    
    plot(x, target)
    hold on
    plot(x, v, 'r')
    xlabel('Output Cell')
    xlim([1, cells])
    set(gca, 'XTick', 1:cells)
    ylabel('Value')
    ylim([-0.01, 1.01])
    title('Targets and Probes')
    legend({'Targets', 'Probes'}, 'Location', 'NorthEastOutside')
    %}
    
    x=1:cells;
    
    figure
    
    plot(x, rates)
    hold on
    plot(x, target, 'r')
    xlabel('Output Cell')
    xlim([1, cells])
    set(gca, 'XTick', 1:cells)
    ylabel('Firing Rate')
    ylim([-0.01, 1.01])
    title('Targets and Firing Rates After Training')
    legend({'Cell Rates', 'Targets'}, 'Location', 'NorthEastOutside')
    
    figure
    
    time=1:epochs;
    
    plot(time, rmsTime)
    xlabel('Epoch Number')
    xlim([1, epochs])
    set(gca, 'XTick', 1:epochs)
    ylabel('RMS Error')
    title('RMS Error through Time')
    
    figure
    
    imagesc(weights)
    axis xy
    xlabel('Input Cell')
    ylabel('Output Cell')
    title('Trained Weight Matrix')
    colorbar
    
    rates=round(rates);
    
    Hamming=sum((rates.*(1-target))+(target.*(1-rates)));
    
    disp(['Hamming distance is ', int2str(Hamming)])
    
end