function [SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses] = ExperimentWrapper66()
%% Parameters
% Actions


[combo_cells, combo_synapses] = arm_orthogonal(j1_angle, j2_angle, j3_angle, j4_angle, combo_synapses)

end

function [SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded, walls] = Experiment66_internal(steps, num_sensory, sensory_cells)

% Learns backward SA_column synapses in undirected fashion, as Exp63.
% Receives sensory info from another function rather than an internal
% world. Recieves actions as a parameter.

% Cells
num_sensory = [worldSize_y, worldSize_x];
num_motor = [1 size(actions, 2)];
num_SAcol = prod(num_sensory);
num_SAcellsinCol = prod(num_motor);
num_SA = [num_SAcellsinCol 1 num_SAcol];

% Synapses
sensorytoSA_dilution = 1; %0.3
motortoSA_dilution = 1; %0.2
SAtomotor_dilution = 1; %0.2

% Competition and Learning
learningRate = 100;
trace_learningRate = 10; %0.1; %0.001; % 0.0001

normalisation_threshold = 1;
trace_threshold = 4; %0.01

eta = 0.0;

% Noise
state_noise = 0.002;

% Analysis
SA_tracked = [];
text_on = 0;


%% Setup

% Make networks of sensory, motor and SA neurons.
motor_cells = zeros(num_motor);
SA_cells = zeros(num_SA);
SA_trace = zeros(num_SA);

% Create synapse weights between SA cells and motor<=>SA.
SAtoSA_synapses = GenerateZeroWeights(numel(SA_cells), numel(SA_cells), 1);
motortoSA_synapses = Generate_Diluted_Weights(motor_cells, SA_cells, motortoSA_dilution, 1);
SAtomotor_synapses = Generate_Diluted_Weights(SA_cells, motor_cells, SAtomotor_dilution, 1);

% Create sensory => SA synapses that are identical for all cells in a
% column.
sensorytoSA_synapses = [];
for column = 1:num_SAcol
sensorytoSA_synapses = [sensorytoSA_synapses repmat(Generate_Diluted_Weights(sensory_cells, 1, sensorytoSA_dilution, 1), [1 size(actions,2)])];
end

% Normalise synapses
SAtoSA_synapses = normalise(SAtoSA_synapses, normalisation_threshold);
sensorytoSA_synapses = normalise(sensorytoSA_synapses, normalisation_threshold);
motortoSA_synapses = normalise(motortoSA_synapses, normalisation_threshold);
SAtomotor_synapses = normalise(SAtomotor_synapses', normalisation_threshold);
SAtomotor_synapses = SAtomotor_synapses';


%% RUN TRIAL

%% Timing

tic;
duration = 0;
for time = 1:steps
    
    if time == round(steps/10)
        disp('1/10')
        duration = toc;
        fprintf('Time remaining = %s', datestr((9*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(2*steps/10)
        disp('2/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((8*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(3*steps/10)
        disp('3/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((7*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(4*steps/10)
        disp('4/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((6*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(5*steps/10)
        disp('5/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((5*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(6*steps/10)
        disp('6/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((4*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(7*steps/10)
        disp('7/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((3*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(8*steps/10)
        disp('8/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((2*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(9*steps/10)
        disp('9/10')
        duration = (toc+duration)/2
        fprintf('Time remaining = %s', datestr((1*duration)/86400, 'HH:MM:SS.FFF'))
        disp(' ')
        tic;
    elseif time == round(10*steps/10)
        disp('10/10')
    end
    
    noNaNsensorytoSA = sensorytoSA_synapses;
    noNaNsensorytoSA(isnan(noNaNsensorytoSA)) = 0;
    
    noNaNmotortoSA = motortoSA_synapses;
    noNaNmotortoSA(isnan(noNaNmotortoSA)) = 0;
    
    noNaNSAtomotor = SAtomotor_synapses;
    noNaNSAtomotor(isnan(noNaNSAtomotor)) = 0;
    
    %% Firing of SA cells and Recruitment of Column
    
    % Sensory cells fire according to agent position (1 cell per place).
    [sensory_cells, combo_synapses] = arm_orthogonal(j1_angle, j2_angle, j3_angle, j4_angle, combo_synapses);
    %sensory_cells = world(:,:,1);
    
    if text_on == 1
        fprintf('State = %d', find(sensory_cells))
        disp(' ')
    end
    
    % SA cells fire based on current sensory cells (state).
    SA_cells = dot((repmat(sensory_cells(:),[1,numel(SA_cells)])), noNaNsensorytoSA);
    SA_cells = reshape(SA_cells, num_SA);
    
    % columnar WTA
    SA_cells(SA_cells == max(max(SA_cells))) = 1;
    SA_cells(SA_cells ~= max(max(SA_cells))) = 0;
    
    if text_on == 1
        fprintf('SA = %s', num2str(find(SA_cells > 0.1)))
        disp(' ')
    end
    
    % Update sensorytoSA weights.
    sensorytoSA_synapses = sensorytoSA_synapses + (learningRate * (sensory_cells(:) * SA_cells(:)'));
    sensorytoSA_synapses = normalise(sensorytoSA_synapses, normalisation_threshold);
    noNaNsensorytoSA = noNaN(sensorytoSA_synapses);
    
    %% TRACE LEARNING: Update connections from ACTIVE COLUMN to PREVIOUS (trace) SA_CELL:
    
    % Calculate the trace value for all cells
    %SA_trace = getTrace(SA_cells, SA_trace, eta);
    
    % Update synapses using lr * state firing * trace rule (backwards)
    SAtoSA_synapses = SAtoSA_synapses + (trace_learningRate * SA_cells(:) * SA_trace(:)');
    
    % Block off self-self synapses
    %SAtoSA_synapses(logical(eye(size(SAtoSA_synapses)))) = NaN;
    
    % Normalise synapses
    SAtoSA_synapses = normalise(SAtoSA_synapses, trace_threshold);
    
    %% Select an action and activate the appropriate motor cell.
        
        action = actions{randi(size(actions,1))};
        %disp(action)
        [world, valid_movement_selected, intended_outcome] = update_world5(world, action, 0);
        if valid_movement_selected == 0
            %disp('Invalid Move')
        end
    
    [~, idx] = ismember(action, actions);
    motor_cells(idx) = 1;
    
    if text_on == 1
        fprintf('Moved %s (%d)', action, find(motor_cells))
        disp(' ')
    end
    
    % Activation of SA cells from sensory and motor cells simulataneously.
    %SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(motor_cells(:)],[1,numel(SA_cells)]), [noNaNsensorytoSA; noNaNsensorytoSA]));
    SA_cells(:) = 0;
    SA_cells = dot(([repmat(motor_cells(:), [1,numel(SA_cells)]); repmat(sensory_cells(:), [1,numel(SA_cells)])]), [noNaNmotortoSA; noNaNsensorytoSA]);
    SA_cells = reshape(SA_cells, num_SA);
    %SA_cells = SA_cells + dot((repmat(motor_cells', [1,numel(SA_cells)])), noNaNmotortoSA);
    
    % WTA:
    SA_cells = WTA_Competition(SA_cells(:));
    SA_cells = reshape(SA_cells, num_SA);
    
    if text_on == 1
        fprintf('SA = %s', num2str(find(SA_cells > 0.1)'))
        disp(' ')
    end
    
    % Weights updated.
    
    motortoSA_synapses = motortoSA_synapses + learningRate * motor_cells(:) * SA_cells(:)';
    SAtomotor_synapses = SAtomotor_synapses + learningRate * SA_cells(:) * motor_cells(:)';
    
    % Weights normalised
    
    motortoSA_synapses = normalise(motortoSA_synapses, normalisation_threshold);
    SAtomotor_synapses = normalise(SAtomotor_synapses', normalisation_threshold);
    SAtomotor_synapses = SAtomotor_synapses';
    
    SA_info = [find(sensory_cells), find(motor_cells), find(SA_cells > 0.1)'];
    SA_tracked{time} = SA_info;
        
    if text_on == 1
        disp(' ')
    end
    
    %% Calculate SA trace
    
        % Calculate the trace value for all cells
    SA_trace = getTrace(SA_cells, SA_trace, eta);
    
    %% Reset Cells and Update parameters
    
    % Record agent position
    world_tracked{time} = world(:,:,1);
    world_summed = world_summed + intended_outcome;
    
    SA_cells(:) = 0;
    sensory_cells(:) = 0;
    motor_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
end

% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);

figure(); imagesc(world_summed); title('Sum of Time Spent in Each Location')

%% ANALYSE RESULTANT MAPPING
% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
disp('Complete')

disp('Calculating state/action mappings.')
%HML_mappings(sensorytoSA_synapses, motortoSA_synapses, mapping_threshold);
disp('Complete')

%figure(); imagesc(sensorytoSA_synapses); colorbar; title('Sensory => S Synapses'); xlabel('State Cells'); ylabel('Sensory Cells)');
%figure(); imagesc(motortoSA_synapses); colorbar; title('Motor => SA Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');
%figure(); imagesc(SAtomotor_synapses'); colorbar; title('SA => Motor Synapses'); xlabel('SA Cells'); ylabel('Motor Cells');

% Display repeatedly firing SA cells and the state/action that causes them
% to fire. This allows you to check that they are always firing for the
% same combo.
%{
SA_tracked_copy = SA_tracked;
thirdElement = cellfun(@(x)x(3), SA_tracked_copy);
[~, idx] = sort(thirdElement);
SA_tracked_copy = SA_tracked_copy(idx);
thirdElement = cellfun(@(x)x(3), SA_tracked_copy);
thirdElement_repeats = thirdElement;
SA_tracked_copy(~ismember(thirdElement_repeats, thirdElement_repeats(diff(thirdElement_repeats) == 0))) = [];

for count = 1:numel(SA_tracked_copy)
    disp(SA_tracked_copy{count})
end
%}
% Record every state/action combo.
SA_decoded = sortrows(cell2mat(SA_tracked(:)),[1 2]);
SA_decoded = unique(SA_decoded, 'rows');


%% Package Synaptic Data

%synapticData = {SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded};

disp('Complete')
end















function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end



function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches threshold. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end
end