function [SAtochunk_synapses, chunktoSA_synapses] = ExperimentWrapper65(worldSize, trials, steps, ind_steps,SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded, walls)

% Using synapses learned by Experiments 61 or 64, runs a set of trials with
% an added layer of chunk cells, learning sequences which are frequently
% repeated between trials. An H shaped maze works well.
%% Parameters

% World
worldSize_x = worldSize;
worldSize_y = worldSize;

% Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};

% Cells
num_sensory = [worldSize_y, worldSize_x];
num_reward = [num_sensory];
num_motor = [1 size(actions, 2)];
num_SAcol = prod(num_sensory);
num_SAcellsinCol = prod(num_motor);
num_SA = [num_SAcellsinCol 1 num_SAcol];
num_chunk = [1 round(prod(num_SA)/3)];
agent_firing = 0;
reward_firing = 1;

% Synapses
chunk_connectivity = 1;
chunk_weights = 0.00001;
chunk_learningRate = 0.0001; %0.00001; %0.00001 %0.000001; %0.01
eta = 0.9;

% Competition and Learning
SA_modifier = 0.9; %0.02; % 0.2
chunk_modifier = 1.8;
chunk_threshold = 0.01; %0.001; %0.0001;

% Analysis
Ycount = 0;
Ncount = 0;
average_time = 0;

%% Create list of Trials
programme = randi([2 worldSize_x-1], [trials 4]);


% give disallowed conditions for agent and reward placement.

condition1 = 'ismember(sub2ind([worldSize_y worldSize_x], programme(trial, 1), programme(trial, 2)), walls) == 1';
condition2 = 'ismember(sub2ind([worldSize_y worldSize_x], programme(trial, 3), programme(trial, 4)), walls) == 1';
condition3 = 'programme(trial, 1) == programme(trial, 3) && programme(trial, 2) == programme(trial,4)';
%condition4 = 'abs(programme(trial, 2) - programme(trial, 4)) ~= 3';
condition4 = 'programme(trial, 2) > 5';




for trial = 1:size(programme,1)

    while eval(condition1) || eval(condition2) || eval(condition3) || eval(condition4)
        while eval(condition1)
            programme(trial,1) = randi([1 worldSize_x],1);
            programme(trial,2) = randi([1 worldSize_x],1);
        end
        while eval(condition2)
            programme(trial,3) = randi([1 worldSize_x],1);
            programme(trial,4) = randi([1 worldSize_x],1);
        end
        while eval(condition3)
            programme(trial,:) = randi([1 worldSize_x], [1 4]);
        end
        while eval(condition4)
            programme(trial,2) = randi([1 worldSize_x],1);
            programme(trial,4) = randi([1 worldSize_x],1);
        end
    end
end
world_tracked = zeros([worldSize_y worldSize_x]);


%% Create Synapses

if ~exist('SAtochunk_synapses','var')
    disp('Generating state => chunk synapses.')
    SAtochunk_synapses = Generate_Diluted_Weights(zeros(num_SA), zeros(num_chunk), chunk_connectivity, chunk_weights);
else
    disp('Using provided state => chunk synapses.')
end
if ~exist('chunktoSA_synapses','var')
    disp('Generating chunk => state synapses.')
    chunktoSA_synapses = Generate_Diluted_Weights(zeros(num_chunk), zeros(num_SA), chunk_connectivity, chunk_weights);
else
    disp('Using provided chunk => state synapses.')
end


%% Wrap Variables
cellParam = {num_sensory, num_reward, num_motor, num_SAcol, num_SAcellsinCol, num_SA, num_chunk, agent_firing, reward_firing};
competitionParam = {SA_modifier, chunk_modifier, chunk_learningRate, eta, chunk_threshold};
synapticData = {SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_decoded, SAtochunk_synapses, chunktoSA_synapses};

%% Run Trials
for trial = 1:trials
    
    % Time the trial
    tic;
    
    % Read the agent and reward position from the programme (list of
    % trials) and wrap all the trial parameters into an array
    agent_x = programme(trial, 2); agent_y = programme(trial, 1); reward_x = programme(trial, 4); reward_y = programme(trial,3);
    trialParam = {steps, ind_steps, worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y, walls, actions};
    
    % Prevent relevant details.
    disp(' ')
    fprintf('TRIAL %d.', trial)
    disp(' ')
    fprintf('Agent at (%d,%d); Reward at (%d,%d).', agent_x, agent_y, reward_x, reward_y)
    disp(' ')
    
    % Carry out trial
    [SAtochunk_synapses, chunktoSA_synapses, result, world_summed] = Experiment65_internal(trialParam, cellParam, competitionParam, synapticData);
    
    % Update chunk <=> SA synapse records
    synapticData{6} = SAtochunk_synapses;
    synapticData{7} = chunktoSA_synapses;
    
    % Track what routes have been taken over the course of many trials.
    world_tracked = world_tracked + world_summed;
    
    % Record Result
    if result == 'Y'
        Ycount = Ycount + 1;
    elseif result == 'N'
        Ncount = Ncount + 1;
    end
    
    % End timing and calculate approximate time for all remaining trials to
    % run.
    time = toc;
    average_time = ((average_time*(trial-1)) + time) / trial;
    fprintf('Time remaining = %s', datestr(((trials-trial)*average_time)/86400, 'HH:MM:SS.FFF'))
    disp(' ')
end

%% Analyse Results

disp(Ycount)
disp(Ncount)

figure(); imagesc(world_tracked);

end















function [SAtochunk_synapses, chunktoSA_synapses, result, world_summed] = Experiment65_internal(trialParam, cellParam, competitionParam, synapticData)

% As Experiment62, altered to snip synapses so as to avoid a 'painful'
% situation such as running into a wall.

% Trial
steps = trialParam{1};
ind_steps = trialParam{2};

% World
worldSize_x = trialParam{3};
worldSize_y = trialParam{4};
agent_x = trialParam{5};
agent_y = trialParam{6};
reward_x = trialParam{7};
reward_y = trialParam{8};
walls = trialParam{9};

% Actions
actions = trialParam{10};

% Cells
num_sensory = cellParam{1};
num_reward = cellParam{2};
num_motor = cellParam{3};
num_SAcol = cellParam{4};
num_SAcellsinCol = cellParam{5};
num_SA = cellParam{6};
num_chunk = cellParam{7};
agent_firing = cellParam{8};
reward_firing = cellParam{9};

% Competition
SA_modifier = competitionParam{1};
chunk_modifier = competitionParam{2};

% Learning
chunk_learningRate = competitionParam{3};
eta = competitionParam{4};
chunk_threshold = competitionParam{5};

% Synapses
SAtoSA_synapses = synapticData{1};
sensorytoSA_synapses = synapticData{2};
motortoSA_synapses = synapticData{3};
SAtomotor_synapses = synapticData{4};
SA_decoded = synapticData{5};
SAtochunk_synapses = synapticData{6};
chunktoSA_synapses = synapticData{7};

% Analysis
display_tracked = {};

%% Setup
% Create World
world = create_world3(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

% Create cells
sensory_cells = zeros(num_sensory);
reward_cells = zeros(num_reward);
SA_cells = zeros(num_SA);
chunk_cells = zeros(num_chunk);
motor_cells = zeros(num_motor);
SA_trace = zeros(num_SA);
chunk_trace = zeros(num_chunk);

%% Walls.

% Add Walls to World
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

world = create_world2(worldSize_x, worldSize_y, agent_x, agent_y, reward_x, reward_y);

world(:,:,3) = zeros(size(world(:,:,2)));
walls = unique(walls);
world(walls + numel(world(:,:,1:2))) = 3;

% Create representation of world to record progress.
world_summed = zeros(size(world(:,:,1)));

%% RUN TRIAL
for time = 1:steps
    %tic
    % Allow activity to spread for 1:ind_time
    for ind_time = 1:ind_steps
        %{
        if ind_time == round(ind_steps/4)
            disp('...')
        elseif ind_time == round(ind_steps/2)
            disp('...')
        elseif ind_time == round(3*ind_steps/4)
            disp('...')
        end
        %}
        noNaNsensorytoSA = noNaN(sensorytoSA_synapses);
        noNaNmotortoSA = noNaN(motortoSA_synapses);
        noNaNSAtomotor = noNaN(SAtomotor_synapses);
        noNaNSAtoSA= noNaN(SAtoSA_synapses);
        noNaNSAtochunk = noNaN(SAtochunk_synapses);
        noNaNchunktoSA = noNaN(chunktoSA_synapses);
        
        
        %% Set Up Reward Gradient
        % Sensory cells fire based on current position.
        
        sensory_cells(find(world(:,:,1))) = agent_firing;
        reward_cells(find(world(:,:,2))) = reward_firing;
        
        
        
        % SA cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
        SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
        SA_cells = reshape(SA_cells,num_SA);
        
        
        % Competition in state cells:
        
        % subtractive.
        %SA_cells = SA_cells - SA_modifier * mean(mean(SA_cells));
        for column = 1:size(SA_cells,3)
            SA_cells(:,:,column) = SA_cells(:,:,column) - SA_modifier * mean(SA_cells(:,:,column));
            %SA_cells(:,:,column) = mean(SA_cells(:,:,column)) * WTA_Competition(SA_cells(:,:,column));
        end
        
        % divisive.
        SA_cells = SA_cells/max(max(SA_cells));
        sum_save = sum(SA_cells);
        SA_cells(SA_cells < 0) = 0;
        for col = 1:size(SA_cells,3)
            SA_cells(:,:,col) = normalise(SA_cells(:,:,col), sum_save(:,:,col));
        end
        SA_cells = reshape(SA_cells,num_SA);
        
        
        % add noise
        %SA_cells = SA_cells(:)' + SA_noise .* std(SA_cells(:)) * randn(1,size(SA_cells(:),1));
        %SA_cells(SA_cells < 0) = 0;
        %SA_cells = reshape(SA_cells,num_SA);
        
        display = SAcol_analyseAct_internal(worldSize_y, SA_cells, SA_decoded);
        display_tracked{ind_time} = cell2mat(display);
        
    end
    
    %slider_display(display_tracked, []);
    
    % Record agent position
    world_tracked{time} = world(:,:,1) + world(:,:,2) + world(:,:,3);
    
    %world(:,:,1) + world(:,:,2)
    %SA_decoded(find(SA_decoded(:,3) == find(SA_cells == max(SA_cells))),:)
    
    %% Check for Completion
    current_state = sub2ind(size(world(:,:,1)), agent_y, agent_x);
    reward_location = sub2ind(size(world(:,:,1)), reward_y, reward_x);
    
    % Record progress
    world_summed(current_state) = world_summed(current_state) + 1;
    
    if current_state == reward_location
        result = 'Y';
        disp(time)
        disp(result)
        total_steps = time;
        break
    elseif time == steps
        result = 'N';
        disp(result)
        total_steps = time;
        break
    end
    
    %% Move Agent through Bursting Sensory Activity
    
    sensory_cells(find(world(:,:,1))) = 100;
    reward_cells(find(world(:,:,2))) = reward_firing;
    
    SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [noNaNsensorytoSA; noNaNsensorytoSA; noNaNSAtoSA]);
    SA_cells = WTA_Competition(SA_cells);
    SA_cells = reshape(SA_cells,num_SA);
    
    %SA_cells = SA_cells + dot(repmat(sensory_cells(:),[1,numel(SA_cells)]), noNaNsensorytoSA);
    
    
    
    disp(world(:,:,1) + world(:,:,2))
    %SA_decoded(find(SA_decoded(:,3) == find(SA_cells == max(SA_cells))),:)
    
    % Calculate activation to motor cells
    motor_cells = dot(repmat(SA_cells(:),[1,numel(motor_cells)]), noNaNSAtomotor);
    
    % Move agent.
    action = actions{find(motor_cells == max(motor_cells))};
    fprintf('%d: %s', time, action)
    disp(' ')
    [world, valid, intended_outcome] = update_world5(world, action, 0);
    
    %% Feed Activity to Chunk Cells
    
    % Uses a full trace rule -- preSynaptic_trace *
    % postSynaptic_trace
    
    % Calculate activation of chunk cells from SA cells
    chunk_cells = dot(repmat(SA_cells(:),[1,numel(chunk_cells)]), noNaNSAtochunk);
    chunk_cells = reshape(chunk_cells,num_chunk);
    chunk_save = chunk_cells;
    
    % Competition in chunk cells:
    % Subtractive.
    chunk_cells = chunk_cells - chunk_modifier * mean(mean(chunk_cells));
    chunk_cells(chunk_cells < 0) = 0;
    
    if any(chunk_cells) == 0
        disp('No chunk cells firing')
    else
        % divisive
        chunk_cells = 1 * (chunk_cells/max(max(chunk_cells)));
    end
    
    % Calculate memory trace for all cells.
    chunk_trace = getTrace(chunk_cells, chunk_trace, eta);
    SA_trace = getTrace(SA_cells, SA_trace, eta);
    
        % Display firing cells and traces
    fprintf('SA = %d.\n' , find(SA_cells))
    fprintf('SA (trace) = %s.\n' , num2str(find(SA_trace)'))
    fprintf('Chunk = %s.\n', num2str(find(chunk_cells > 0.9)))
    fprintf('Chunk (trace) = %s.\n', num2str(find(chunk_trace > 0.09)))
    %fprintf('Super = %s.\n', num2str(find(super_cells > 0.9)))
    %fprintf('Super (trace) = %s.\n', num2str(find(super_trace > 0.09)))
    disp(' ')
    
    % Update synapses to and from chunk cells.
    %chunktostate_synapses = chunktostate_synapses + learningRate * chunk_trace(:) * state_cells(:)';
    %statetochunk_synapses = statetochunk_synapses + learningRate * state_cells(:) * chunk_trace(:)';
    chunktoSA_synapses = chunktoSA_synapses + chunk_learningRate * chunk_trace(:) * SA_trace(:)';
    SAtochunk_synapses = SAtochunk_synapses + chunk_learningRate * SA_trace(:) * chunk_trace(:)';
    
    % Normalise synapse weights to and from chunk cells.
    SAtochunk_synapses = normalise(SAtochunk_synapses, chunk_threshold);
    chunktoSA_synapses = normalise(chunktoSA_synapses', chunk_threshold);
    chunktoSA_synapses = chunktoSA_synapses';
    
    %% Clear cells
    sensory_cells(:) = 0;
    reward_cells(:) = 0;
    SA_cells(:) = 0;
    chunk_cells(:) = 0;
    motor_cells(:) = 0;
    [agent_y, agent_x, ~] = ind2sub(size(world), find(world == 1));
    
    %step_duration = toc;
    %fprintf('Time for step = %s', datestr(step_duration/86400, 'HH:MM:SS.FFF'))
    %disp(' ')
    
end


% Display agent progress.
disp('Analysing Agent Trajectory.')
disp('...')
%slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);













end











function [display] = SAcol_analyseAct_internal(worldSize, SA_cells, SA_decoded)

%% Import Arrows
arrows = repmat({uint8(zeros(16))}, [3 3]);
arrows{1} = imread('arrow_nw.png');
arrows{2} = imread('arrow_w.png');
arrows{3} = imread('arrow_sw.png');
arrows{4} = imread('arrow_n.png');
arrows{6} = imread('arrow_s.png');
arrows{7} = imread('arrow_ne.png');
arrows{8} = imread('arrow_e.png');
arrows{9} = imread('arrow_se.png');

for arrow = 1:numel(arrows);
    arrowNormalised = arrows{arrow};
    arrowNormalised(arrowNormalised > 0) = 255;
    arrows{arrow} = arrowNormalised;
end

% Make an array to display the arrows in.
display = repmat({uint8(zeros(16))}, [worldSize*3 worldSize*3]);

%% Retrieve the SA layer's activation and display
% Thresholding by the least active SA cell (within SA_decoded).
minimum_activation = min(min(SA_cells(SA_decoded(:,3))));
SA_cells = SA_cells - minimum_activation;

% Get maximum activation that remains.
maximum_activation = max(max(SA_cells));

for column = 1:size(SA_cells,3)
    
    [~, cell] = max(SA_cells(:,:,column));
    cell = sub2ind(size(SA_cells), cell, 1, column);
    
    % Check that cell has a sensory and motor conneciton i.e. is within
    % SA_decoded.
    if any(ismember(SA_decoded(:,3), cell)) == 1
        
        [y, x] = ind2sub([worldSize worldSize], SA_decoded(find(SA_decoded(:,3) == cell),1));
        SA_state = (y * 3 - 1) + ((worldSize*3) + (worldSize*3) * 3 * (x-1));
        display{SA_state}(:) = 255;
        
        switch SA_decoded(find(SA_decoded(:,3) == cell),2)
            
            case 1
                display{SA_state - worldSize*3 - 1} = arrows{1};
                
            case 2
                display{SA_state - worldSize*3} = arrows{2};
                
            case 3
                display{SA_state - worldSize*3 + 1} = arrows{3};
                
            case 4
                display{SA_state - 1} = arrows{4};
                
            case 6
                display{SA_state + 1} = arrows{6};
                
            case 7
                display{SA_state + worldSize*3 - 1} = arrows{7};
                
            case 8
                display{SA_state + worldSize*3} = arrows{8};
                
            case 9
                display{SA_state + worldSize*3 + 1} = arrows{9};
                
        end
    end
end



end





function [display] = SA_analyseAct_internal(worldSize, SA_cells, SA_decoded)

%% Import Arrows
arrows = repmat({uint8(zeros(16))}, [3 3]);
arrows{1} = imread('arrow_nw.png');
arrows{2} = imread('arrow_w.png');
arrows{3} = imread('arrow_sw.png');
arrows{4} = imread('arrow_n.png');
arrows{6} = imread('arrow_s.png');
arrows{7} = imread('arrow_ne.png');
arrows{8} = imread('arrow_e.png');
arrows{9} = imread('arrow_se.png');

for arrow = 1:numel(arrows);
    arrowNormalised = arrows{arrow};
    arrowNormalised(arrowNormalised > 0) = 255;
    arrows{arrow} = arrowNormalised;
end

% Make an array to display the arrows in.
display = repmat({uint8(zeros(16))}, [worldSize*3 worldSize*3]);

%% Retrieve the SA layer's activation and display
% Thresholding by the least active SA cell (within SA_decoded).
minimum_activation = min(min(SA_cells(SA_decoded(:,3))));
SA_cells = SA_cells - minimum_activation;

% Get maximum activation that remains.
maximum_activation = max(max(SA_cells));

for cell = 1:numel(SA_cells)
    
    % Check that cell has a sensory and motor conneciton i.e. is within
    % SA_decoded.
    if any(ismember(SA_decoded(:,3), cell)) == 1
        
        [y, x] = ind2sub([worldSize worldSize], SA_decoded(find(SA_decoded(:,3) == cell),1));
        SA_state = (y * 3 - 1) + ((worldSize*3) + (worldSize*3) * 3 * (x-1));
        display{SA_state}(:) = 255;
        
        switch SA_decoded(find(SA_decoded(:,3) == cell),2)
            
            case 1
                display{SA_state - worldSize*3 - 1} = arrows{1} * (SA_cells(cell) / maximum_activation);
                
            case 2
                display{SA_state - worldSize*3} = arrows{2} * (SA_cells(cell) / maximum_activation);
                
            case 3
                display{SA_state - worldSize*3 + 1} = arrows{3} * (SA_cells(cell) / maximum_activation);
                
            case 4
                display{SA_state - 1} = arrows{4} * (SA_cells(cell) / maximum_activation);
                
            case 6
                display{SA_state + 1} = arrows{6} * (SA_cells(cell) / maximum_activation);
                
            case 7
                display{SA_state + worldSize*3 - 1} = arrows{7} * (SA_cells(cell) / maximum_activation);
                
            case 8
                display{SA_state + worldSize*3} = arrows{8} * (SA_cells(cell) / maximum_activation);
                
            case 9
                display{SA_state + worldSize*3 + 1} = arrows{9} * (SA_cells(cell) / maximum_activation);
                
        end
    end
end

end


function [trace] = getTrace(postSynaptic_fr, postSynaptic_trace, eta)

trace = ((1-eta)*postSynaptic_fr) + eta*postSynaptic_trace;
end

function matrix = normalise(matrix, threshold)
% Normalises a matrix so that each column sums to the threshold value given. If you want to normalise rows, transpose the matrix before using this function.

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));                 % Divides each element in the matrix by (the sum of that element's column multiplied by one over the threshold).

end

%{
function matrix = normalise(matrix, threshold)

% Normalise matrix so that each column's sum approaches 0.1. Ignores NaN.

%get number of rows and columns in matrix
[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);

for column = 1:columns
    if summed(column) ~= 0
        %if summed(column) > threshold
        %divide each row in that column by that sum
        
        for row = 1:rows
            matrix(row,column) = threshold * (matrix(row,column)/summed(column));
        end
    end
end

end
%}
