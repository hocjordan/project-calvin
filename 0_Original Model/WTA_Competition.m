function [firingRates] = WTA_Competition(firingRates)

outputcells = size(firingRates,2);
    
[~, idx]=nanmax(firingRates);
        
        for postsynapticCell=1:outputcells
            if postsynapticCell==idx
                firingRates(postsynapticCell)=1;
            else
                firingRates(postsynapticCell)=0;
            end
        end
end
