/**************************
       INCLUDES
*************************/
#include <Servo.h>
//#include <ArduinoRobot.h>
#include <Wire.h>
//#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>

/**************************
   GLOBAL VARIABLES
*************************/
Servo myServo;
const byte pingPin  = 13;
int sensorcount0 = 0;
int sensorcount1 = 0;
byte tab[18];
const byte E1 = 6; //M1
const byte E2 = 5; //M2
const byte M1 = 8; //M1
const byte M2 = 7; //M2
//const byte leftspeed = 255; //255 is maximum speed
//const byte rightspeed = 255;
int dir = 0;
int count = 0;

/* Assign a unique ID to the compass */
Adafruit_LSM303_Mag_Unified mag = Adafruit_LSM303_Mag_Unified(12345);

/**************************
   SETUP FUNCTION
*************************/
void setup() {
  // initialize serial communication:
  Serial.begin(9600);
  myServo.attach(10);

  // Init pinModes
  for (int i = 5; i <= 8; i++)
    pinMode(i, OUTPUT);
    
  // Init tab of values
  for (int j = 0; j < 18; j++) {
    tab[j] = 0;
  }
}


/**************************
      INFINITE LOOP
*************************/
void loop()
{
  //controler();
  //moveHead();
  //runaway();
  //forward();
  //Serial.println(updateSonic());
  updateCompass();
  Serial.println(updateCompass());
}


/**************************
   MAIN FUNCTION
*************************/
void controler() { // Uses distance encoders to perform WASD control.
  while (Serial.available() < 1) {} // Wait until a character is received
  char val = Serial.read();
  //serialFlush();
  switch (val) { // Perform an action depending on the command
    case 'w'://Move Forward
      forward();
      moveHead();
      Serial.println('Y');
      break;
    case 's'://Move Backwards
      reverse();
      forward();
      moveHead();
      Serial.println('Y');
      break;
    case 'a'://Turn Left
      left();
      forward();
      moveHead();
      Serial.println('Y');
      break;
    case 'd'://Turn Right
      right();
      forward();
      moveHead();
      Serial.println('Y');
      break;
    default:
      stop();
      break;
  }

}

void compass_controller() { // Uses compass to perform WASD control NOT FINISHED

if(!mag.begin())
  {
    /* There was a problem detecting the LSM303 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while(1);
  }
  
  while (Serial.available() < 1) {} // Wait until a character is received
  char val = Serial.read();
  //serialFlush();
  switch (val) { // Perform an action depending on the command
    case 'w'://Move Forward
      forward();
      moveHead();
      Serial.println('Y');
      break;
    case 's'://Move Backwards
      reverse();
      forward();
      moveHead();
      Serial.println('Y');
      break;
    case 'a'://Turn Left
      left();
      forward();
      moveHead();
      Serial.println('Y');
      break;
    case 'd'://Turn Right
      right();
      forward();
      moveHead();
      Serial.println('Y');
      break;
    default:
      stop();
      break;
  }

}


/**************************
   SENSOR FUNCTIONS
*************************/
void moveHead() {
  for (int i = 0; i <= 17; i++) {
    myServo.write((i+1) * 10);
    delay(500);
    tab[i] = updateSonic();
    Serial.println(tab[i]);
  }
  myServo.write(90);
}

long updateSonic() {
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(15);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(20);
  pinMode(pingPin, INPUT);
  return pulseIn(pingPin, HIGH) / 29 / 2;
}

float updateCompass(){
  
  // Active compass
  mag.begin();
  
  // Get a new sensor event */ 
  sensors_event_t event; 
  mag.getEvent(&event);
  float Pi = 3.14159;
  //Serial.println('activated');
  // Calculate the angle of the vector y,x
  float heading = (atan2(event.magnetic.y,event.magnetic.x) * 180) / Pi;

  // Normalize to 0-360
  if (heading < 0)
  {
    heading = 360 + heading;
  }
  //Serial.print("Compass Heading: ");
  //Serial.println(heading);
  return heading;
  //delay(500);
}

/**************************
   MOTOR FUNCTIONS
*************************/
void stop(void) {
  digitalWrite(E1, LOW);
  digitalWrite(E2, LOW);
  count = 0;
}
void forward(void) {
  while (countFunction() < 90) {
    analogWrite (E1, 255);
    digitalWrite(M1, LOW);
    analogWrite (E2, 255);
    digitalWrite(M2, LOW);
  }
  stop();
}
void reverse (void) {
  dir +=180;
  while (countFunction() < 60) {
    analogWrite (E1, 255);
    digitalWrite(M1, HIGH);
    analogWrite (E2, 255);
    digitalWrite(M2, LOW);
  }
  stop();

}
void left (void) {
  dir -= 90;
  while (countFunction() < 20) {
    analogWrite (E1, 255);
    digitalWrite(M1, HIGH);
    analogWrite (E2, 255);
    digitalWrite(M2, LOW);
  }
  stop();

}
void right (void) {
  dir += 90;
  while (countFunction() < 15) {
    analogWrite (E1, 255);
    digitalWrite(M1, LOW);
    analogWrite (E2, 255);
    digitalWrite(M2, HIGH);
  }
  stop();
}

void runaway (void) {
  myServo.write(90);
  while (updateSonic() <40) {
    stop();
    delay(100);
        analogWrite (E1, 255);
    digitalWrite(M1, LOW);
    analogWrite (E2, 255);
    digitalWrite(M2, HIGH);
    delay(100);
    stop();
  }
  analogWrite (E1, 255);
    digitalWrite(M1, LOW);
    analogWrite (E2, 255);
    digitalWrite(M2, LOW);
  delay(100);
}
  


/**************************
   ENCODER FUNCTION
*************************/
int countFunction(void) {

  const int rawsensorValue = analogRead(0); // variable to store the value coming from the sensor
  if (rawsensorValue < 600) { //Min value is 400 and max value is 800, so state chance can be done at 600.
    sensorcount1 = 1;
  }
  else {
    sensorcount1 = 0;
  }
  if (sensorcount1 != sensorcount0) {
    count ++;
  }
  sensorcount0 = sensorcount1;
  return count;
}


/**************************
  CORRECTION FUNCTION
*************************/
void correction(const int objctv) {
  
}

/**************************
     CLEAN SERIAL
*************************/
void serialFlush() {
  while (Serial.available() > 0) {
    char t = Serial.read();
  }
}

