function [sensory_tracked_matrix, SA_decoded, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses] = Maze_World()
% Runs one full navigational experiment:

% The model starts off with a 10x10 world surrounded by walls (I don't advise using more than 20x20).

% It then gives you the opportunity to add or remove more walls.

% It defines a set of possible actions (here, the 8 cardinal directions
% plus inaction.

% It sets the 'maze_update' function given below as the update function
% that the learning/control models will use to perform actions and recieve
% sensory feedback. The learning/control functions are written so that
% many types of update function can swapped in and out.

% It then calls the learner function for 1000 timesteps. This function will
% explore the world for 1000 timesteps, learning its structure and how the
% model can interact with it -- in this case by moving along the cardinal
% directions. It returns a set of synapses that reflect the learning
% process.

% The results of the learning process are analysed to give 'SA_decoded', a
% list of all the SA cells that fired during the learning process and the
% state/action combinations that they fired to.

% Maze_World then tests this learning. It asks for a start and end
% position, then runs the controller function to move an agent between
% them.

% The controller function takes as parameters the synapses produced by the learner
% function as well as the action set and the world update function. It will
% spend 50 iterations performing reasoning for every timestep, and has an
% upper limit of 100 timesteps to reach its goal before it gives up.

% After the controller function succeeds or fails in moving the agent to
% its goal, the model will analyse the agent's progress and display it as a
% figure. The slider on this figure can be moved and represents time.


%% Define Sensory Information and Starting Point

global world_state                                                                                                          % Sets the world as a global variable, available to the learner, controller and update functions without being explictly called as an argument.

worldSize_x = 20;                                                                                                           % Set the x and y size of the simulated 2D world.
worldSize_y = 20;

[~, ~, walls] = create_maze(worldSize_x, worldSize_y);                                                                      % Creates the world as a set of matrices, where the first layer shows the position of the agent, the second layer show the position of the agent's goal, and the third layer records the position of walls.

%% Define Actions
actions = {'NW' 'W' 'SW' 'N' '' 'S' 'NE' 'E' 'SE'};                                                                         % The actions available to the agent, represented as a set of strings.

%% Define World Functions
world_update_function = @maze_update;                                                                                       % Sets the maze_update function (see below) as a function handle that can be given to the learner and controller functions as arguments.

%% Call Learner function
[SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, SA_tracked, sensory_tracked] = YUTING_151020_LEARNER(10000, actions, world_update_function); % The learner function (see above). Runs for 1000 timesteps.

%% Analyse and Save Data

sensory_tracked_matrix = [];
for time = 1:size(sensory_tracked,2)
    sensory_tracked_matrix(:,:,:,time) = sensory_tracked{time};
end


world_tracked = {};
for time = 1:size(sensory_tracked,2)
    world_tracked{time} = sensory_tracked_matrix(:,:,1,time);% + sensory_tracked_matrix(:,:,3,time);
end

slider_display(world_tracked, [(5 - 0.5), (5 - 0.5), 1, 1]);%[(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
%}

% Decode SA information
%SA_decoded = [];
%for time = 1:size(sensory_tracked,2)
%    SA_tracked{time}{1} = find(SA_tracked{time}{1});
%    SA_decoded(time,:) = cell2mat(SA_tracked{time}(:)');
%end


% Record every state/action combo.
%SA_decoded = unique(SA_decoded,'rows');
%SA_decoded = sortrows(SA_decoded, 3);

%% Ask for New Agent/Goal Positions

[reward_x, reward_y, ~] = create_maze(worldSize_x, worldSize_y, walls);                                                     % Obtains a new start and end position.

%% Call Goal Function
sensory_tracked = YUTING_151020_CONTROLLER(100, 50, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses,  actions, world_update_function);  % The controller function: runs for a maximum of 100 timesteps or until the goal is reached. Reasons over 50 iterations. If the agent is not moving in the right direction, increase the reasoning value.

%% Analyse and Save Data
sensory_tracked_matrix = [];
for time = 1:size(sensory_tracked,2)
    sensory_tracked_matrix(:,:,:,time) = sensory_tracked{time};
end

%
world_tracked = {};
for time = 1:size(sensory_tracked,2)
    world_tracked{time} = sensory_tracked_matrix(:,:,1,time) + sensory_tracked_matrix(:,:,3,time);
end

slider_display(world_tracked, [(reward_x - 0.5), (reward_y - 0.5), 1, 1]);
%}

end

function [sensory_cells, reward_cells] = maze_update(action, calculate_sensory)

% The update function. Takes an action string and updates the world, returning sensory feedback as a layer of sensory cells.

% You should be able to ignore the details of this function, as well as the
% 'calculate_sensory' parameter.

%% Unpackage World Parameters

global world_state

world = world_state;

% find parameters of world
[size_y, size_x] = size(world(:,:,1));

% find the position of the agent within the world
[agent_y, agent_x] = find(world(:,:,1) == 1);

% find position of the reward within the world
[reward_y, reward_x] = find(world(:,:,2) == 2);

%% Action parameters
% NONE

%% Calculate effect of action.

switch action
    case 'W'
        agent_x = agent_x-1;
    case 'SW'
        agent_x = agent_x-1;
        agent_y = agent_y+1;
    case 'S'
        agent_y = agent_y+1;
    case 'SE'
        agent_x = agent_x+1;
        agent_y = agent_y+1;
    case 'E'
        agent_x = agent_x+1;
    case 'NE'
        agent_x = agent_x+1;
        agent_y = agent_y-1;
    case 'N'
        agent_y = agent_y-1;
    case 'NW'
        agent_x = agent_x-1;
        agent_y = agent_y-1;
end

if agent_x == 0 || agent_x > size(world,2) || agent_y == 0 || agent_y > size(world,1)
    valid = 0;
    return
end
if world(agent_y, agent_x, 3) == 3
    valid = 0;
    return
end

world(:,:,1) = 0;
world(agent_y,agent_x,1) = 1;


%% Reward
world(reward_y,reward_x,2) = 1;

%% Produce distributed sensory representation

sensory_cells = world(:,:,1);
reward_cells = world(:,:,2);

%% Show figure

%% Repackage World Parameters

world_state = world;

end

function [reward_x, reward_y, walls] = create_maze(worldSize_x, worldSize_y, walls)

% Creates the world as described above. Again, you should be able to ignore
% this function.

global world_state

%   Creates an x by y by 3 matrix
world = zeros(worldSize_y,worldSize_x,3);

% Define bounding walls


if exist('walls', 'var') == 1
    
    % add pre-existing walls
    world(walls + numel(world(:,:,1:2))) = 3;
    
else
    
    % ask for wall input
    walls = [];
    
    top_wall = [];
    top_wall = [top_wall, 1:worldSize_y:worldSize_x*worldSize_y];
    
    walls = [walls top_wall];
    
    bottom_wall = [];
    bottom_wall = [bottom_wall, worldSize_y-1+1:worldSize_y:worldSize_y*worldSize_x];
    
    walls = [walls bottom_wall];
    
    left_wall = 1:worldSize_y;
    right_wall = worldSize_y*(worldSize_x-1)+1:worldSize_y*worldSize_x;
    
    walls = [walls left_wall];
    walls = [walls right_wall];
    
    % add walls into world
    walls = unique(walls);
    world(walls + numel(world(:,:,1:2))) = 3;
    
    % user interface to create maze
    user_walls = figure(); imagesc(world(:,:,3)); title('Walls: Click to Add, Click to Remove');
    while ishandle(user_walls) == 1;
        try
            [x, y] = ginput(1);
            x=round(x); y=round(y);
            if world(y,x,3) == 3;
                world(y,x,3) = 0;
            else
                world(y,x,3) = 3;
            end
            imagesc(world(:,:,3));
        end
    end
    
    walls = find(world(:,:,3) == 3);
    
end

% place agent
fig = figure(); imagesc(world(:,:,3)); title('Please select starting position.');
[x, y] = ginput(1);
x=round(x); y=round(y);
agent_x = x; agent_y = y;
close(fig)

% place reward
fig = figure(); imagesc(world(:,:,3)); title('Please select goal position.');
[x, y] = ginput(1);
x=round(x); y=round(y);
reward_x = x; reward_y = y;
close(fig)

world(agent_y,agent_x,1) = 1;
world(reward_y,reward_x,2) = 1;

world_state = world;
end
