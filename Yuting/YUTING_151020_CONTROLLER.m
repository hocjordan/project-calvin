 function [sensory_tracked] = CONTROLLER(steps, ind_steps, SAtoSA_synapses, sensorytoSA_synapses, motortoSA_synapses, SAtomotor_synapses, actions, world_update_function)

% Based on Experiment66_controller

% Calls a WORLD function to receive sensory input and to perform a planned
% action.                                                                                                                                                      %                                                                                                                                                                                                                                      %
                                                                                                                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get world state (self and reward)

global world_state


% Cells

% GET INPUT FROM SENSES
[sensory_cells, reward_cells] = world_update_function('', 'yes');                                               
num_sensory = [size(sensory_cells)];

% Create identity matrix
identitySynapses = zeros(size(SAtoSA_synapses));                                                           
identitySynapses(logical(eye(size(identitySynapses)))) = 1;
identitySynapses = reshape(identitySynapses, size(identitySynapses));
firingcell = zeros(1,1000);


num_motor = [1 size(actions, 2)];                                                                               % Define the number of motor neurons equal to the number of possible actions.
num_SAcol = prod(num_sensory);                                                                                  % Define the number of columns in the SA layer as equal to the number of possible states.
num_SAcellsinCol = prod(num_motor);                                                                             % Define the number of SA cells in each SA column as equal to the number of motor cells.
num_SA = [num_SAcellsinCol 1 num_SAcol];                                                                        % Define the dimensions of the SA layer.
reward_firing = 1;

% Competition
SA_modifier = 0.9; %0.02; % 0.2                                                                                 % Modifies the subtractive inhibition of the SA cells in the inductive feedback loop.

%% Setup

% Create cells
SA_cells = zeros(num_SA);                                                                                       % Create SA layer, with all activations at zero.
motor_cells = zeros(num_motor);                                                                                 % Create motor layer, with all motor cells at zero.

%% RUN TRIAL
for time = 1:steps
    
    % Allow activity to spread for 1:ind_time
    for ind_time = 1:ind_steps
        
        
        %% Set Up Reward Gradient
        % Place reward.
        % GET REWARD POSITION
        [~, reward_cells] = world_update_function('', 'yes');                                                   % Activate a set of reward cells (of the same number and structure as the state cells), denoting the position
        
        % SA reward cells fire based on current sensory cells (agent position), current reward cells (reward position), and current SA cells.
        SA_reward = dot((repmat(reward_cells(:),[1,numel(SA_cells)])), sensorytoSA_synapses);                   % Take the SA reward (filter layer) and activate each cell with the dot product of the reward cells and the sensorytoSA synapses.
        
        % filter/transfer SA_reward function MAKE GENERAL LATER!!!!!!!!!!!!
        % SA_reward(SA_reward < 0.5) = 0;                                                                       % Filter out rewards that are not the primary (to avoid x/y coordinate confusion). Value depends on number of possible conflicts.3
        
        % soft competition of SA_reward output
        SA_reward  = transfer(SA_reward);
        
        %input for subtractive competition
        subtractive = zeros(size(SA_cells));
        subtractive(find(SA_cells))= 1;
        subtractive(isnan(subtractive)) = 0;
        
        
        % final SA update
        SA_cells = dot(([repmat(SA_reward(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [identitySynapses; SAtoSA_synapses]) ; % SA cells actived by [SA_reward . identity] + [SA_cells . SAtoSA_synapses].
        SA_cells = reshape(SA_cells,num_SA); % Unimportant; makes sure SA layer has same dimensions as previous.
        
        
        %normalize or sigmoid
        %SA_cells = SA_cells/max(max(SA_cells));  
        %SA_cells = 1/(1+exp(SA_cells.*-1));
          
          
        %try soft competition             
        %SA_cells(SA_cells > 0.1 & SA_cells < 0.95) = 0;                                       
        %SA_cells = 1/(1+exp(SA_cells.*-1));
        %SA_cells = transfer2(SA_cells);
        % Competition in state cells (order can be reversed):
                                                                                                                                                                                                    
        
        %Subtractive Competition
        %for column = 1:size(SA_cells,3)                                                                        % For each column in the SA layer:
         %   SA_cells(:,:,column) = SA_cells(:,:,column) - SA_modifier * mean(SA_cells(:,:,column));            % Subtract the mean activation of that column, modified by a constant, from the activation of every cell in that column.
        %end
        subtractive(find(subtractive))= SA_modifier * mean(mean(SA_cells(find(SA_cells))));                     % give the input a multiplier and prevent all zero situation(multiply by mean of current non-zero cell)
        SA_cells = SA_cells - subtractive; 
        
        % Divisive Inhibition
        SA_cells = SA_cells/max(max(SA_cells));                                                                 % Divide every cell in the SA.
        
        
        % Clean Up
        % sum_save = sum(SA_cells);                                                                             % Save the sum of every SA column.
        % SA_cells(SA_cells < 0) = 0;                                                                           % Make all cells with negative activation = 0.
        % for col = 1:size(SA_cells,3)                                                                          % For every column:
        %    SA_cells(:,:,col) = normalise(SA_cells(:,:,col), sum_save(:,:,col));                               % Normalise the activation of these columns so that they sum to the sum saved before removing negative cells (i.e. redistribute the added activation)
        SA_cells(SA_cells < 0) = 0;
        
        
        sum_save = sum(sum(SA_cells));
        if sum_save ~= 0
        SA_cells = normalise(SA_cells(:),sum_save);
        end
        SA_cells(isnan(SA_cells)) = 0;                                                                          % Remove the NaN values created when a column sums to 1 (or could just 
        % end
        SA_cells = reshape(SA_cells,num_SA);                                                                    % See above.
                                                                                                                                                                                                                                                      
        
        
    end
    
    %% Record agent position
    sensory_info = world_state;                                                                                 % Records the states the agent passes through.
    sensory_tracked{time} = sensory_info;
    
    
    
    %% Check for Completion
    % ALTER FOR GENERALISATION, OR JUST CALL PART OF THE SIMULATION
    [sensory_cells, ~] = world_update_function('', 'yes');                                                                     % Get sensory data from world update function.
    if ~any(~ismember(find(reward_cells), find(sensory_cells)))                                                      % If all nonzero elements of the reward representation are also represented in the sensory cells:
        result = 'Y';
        disp(time)                  
        disp(result)
        total_steps = time;
        break                                                                                                   % End the simulation and return a positive result.
    elseif time == steps                                                                                        % If the agent has failed to reach the goal after a set number of timesteps:
        result = 'N';
        disp(result)
        total_steps = time;
        break                                                                                                   % Return a negative result.
    end
    
    %% Move Agent through Bursting Sensory Activity
    [sensory_cells, ~] = world_update_function('', 'yes');                                                      % Get sensory data from world update function.
    sensory_cells(sensory_cells > 0) = 100 ;                                                                     % Boost the firing rate of firing sensory cells (represents a phasic burst)
    reward_cells(reward_cells > 0) = reward_firing;                                                             % Set the firing rate of reward cells to that specified by the parameter 'reward_firing'. Usually 1.
    
    SA_cells = dot(([repmat(sensory_cells(:),[1,numel(SA_cells)]); repmat(reward_cells(:),[1,numel(SA_cells)]); repmat(SA_cells(:),[1,numel(SA_cells)])]  ), [sensorytoSA_synapses; sensorytoSA_synapses; SAtoSA_synapses]); % Activate SA cells using a combination of inputs from the (boosted) sensory cells and the reward gradient, to read off the SA cell that records the optimal action in the current state.
    %SA_cells(firingcell(find(firingcell)))=0;
             
             
    SA_cells = WTA_Competition(SA_cells);                                                                       % Isolates the SA cell that encodes the optimal action in the current state and sets its activation to 1; sets all others to 0.
    firingcell(time) =find(SA_cells);
    %SA_cells = transfer(SA_cells);
             
    SA_cells = reshape(SA_cells,num_SA);                                                                        % See above.
    
    
    % Calculate activation to motor cells
    motor_cells = dot(repmat(SA_cells(:),[1,numel(motor_cells)]), SAtomotor_synapses);                          % Activates the motor cells using the firing SA cell, effectively 'reading off' the appropriate action.
    
    % Move agent.
    action = actions{find(motor_cells == max(motor_cells))};                                                    % Read off the action encoded by the motor cell.
    disp(action)                                                                                                % Display that action.
    world_update_function(action, 'no');                                                                        % Perform that action and update the world with the results.
    
    %% Clear cells
    sensory_cells(:) = 0;                                                                                       % Set all cell firing rates to 0, ready for the next timestep.
    reward_cells(:) = 0;
    SA_cells(:) = 0;
    motor_cells(:) = 0;
    
end



end


















function matrix = normalise(matrix, threshold)
% Takes a matrix and float as parameters. Normalises the matrix so that
% each column sums to the threshold value.

matrix = matrix ./ ( (1/threshold) * repmat( sum(matrix), size(matrix, 1), 1));                                 % Divides each element in the matrix by (the sum of that element's column multiplied by one over the threshold).

end

function outputfiring = transfer(activation)

softcomp = sortrows(activation(:));
number = softcomp(numel(softcomp)*0.99);
activation(activation>number)=1;
activation(activation <= number)=0;
outputfiring = activation;
end

function outputfiring = transfer2(activation)

softcomp = sortrows(activation(:));
number = softcomp(numel(softcomp)*0.98);
activation(activation>number)=1;
activation(activation <= number)=0;
outputfiring = activation;
end

