function BackPropagation(lrate, epochs, slope, sigmoid, normalise)

% Set cell parameters
outputCells=3;
inputCells=3;

% Set inputs to the network, along with the desired outputs, as two
% vectors.


inputs=[1, 0, 0; ...
    0.9, 0, 0; ...
    0.8, 0, 0; ...
    0, 1, 0; ...
    0, 0.9, 0; ...
    0, 0.8, 0; ...
    0, 0, 1; ...
    0, 0, 0.9; ...
    0, 0, 0.8];

%{
outputs=[1, 0, 0; ...
    1, 0, 0; ...
    1, 0, 0; ...
    0, 1, 0; ...
    0, 1, 0; ...
    0, 1, 0; ...
    0, 0, 1; ...
    0, 0, 1; ...
    0, 0, 1];
    %}
    
    outputs = inputs;
    
    
    % Random weights between input and output cells
    weights=rand(outputCells, inputCells);
    
    if normalise
        weights=normr(weights);
    end
    
    % Some fom of time  measure
    rmsTime=zeros(1, epochs);
    
    %for each epoch
    for epoch=1:epochs
        
        % for each pattern
        for pattern=1:size(inputs, 1)
            
            % fire input cells
            inputFiring=inputs(pattern, :);
            
            % calculate feedforward activation
            activations=weights*inputFiring';   
                
            % calclate firing rate through non-linear process; either
            % signoidal or thresholded
            if sigmoid
                rates=1./(1+exp(-2*slope*activations));
            else
                rates=zeros(outputCells,1);
                rates(activations>0.8)=1;
            end
            
            % update weights from learning_rate * (output - firing) *
            % inputs
            weights=weights+ ...
                (lrate*((outputs(pattern, :)'-rates)*inputFiring));    
            
            weights(weights<0)=0;
            
            % normalise
            if normalise
                weights=normr(weights);
            end
            
        end
        
        % calculate error value for each pattern, and store in the rmsTime
        % array
        error=outputs(pattern, :)'-rates;
        rmsTime(epoch)=sqrt(sum(error.^2)/outputCells);
        
    end
        
    % reset firing rates
    rates(:)=0;
        
    %% Testing
    % calculate firing of learned network given original patterns
    testRates=zeros(outputCells, size(inputs, 1));
    
    for pattern=1:size(inputs, 1)
        
        inputFiring=inputs(pattern, :);
        
        activations=weights*inputFiring';
        
        if sigmoid
            rates=1./(1+exp(-2*slope*activations));
        else
            rates(:)=0;
            rates(activations>0.8)=1;
        end
        
        testRates(:, pattern)=rates';
            
    end
    
    %% Plotting
    figure
    
    x=1:size(inputs, 1);
    
    % Plot firing rates during testing
    plot(x, testRates(1, :), 'b');
    hold on;
    plot(x, testRates(2, :), 'r');
    plot(x, testRates(3, :), 'k');
    ylim([0, 1.1]);
    xlabel('Input Pattern');
    ylabel('Cell Firing Rate');
    title('Output Cell Firing Rates to Input Patterns');
    legend({'Cell 1', 'Cell 2', 'Cell 3'},...
        'Location', 'NorthEastOutside');
    
    figure
    
    %plot weight matrix
    imagesc(weights)
    axis xy
    set(gca, 'XTick', [1, 2, 3]);
    set(gca, 'YTick', [1, 2, 3]);
    xlabel('Input Cell');
    ylabel('Output Cell');
    title('Trained Synaptic Weight Matrix');
    set(gca, 'CLim', [0, max(max(weights))]);
    colorbar;
    
    figure
    
    % Plot mean-square regression error
    x2=1:epochs;
    
    plot(x2, rmsTime)
    xlabel('Epoch Number')
    ylabel('RMS Error')
    title('RMS Error by Epoch')
    
end
      