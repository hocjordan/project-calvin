function [ mappings, firingRates ] = HML_mappings( sensory_weights, motor_weights, threshold )
%Identifies the cells that respond to each state/action combination
%combination:

% Calculates number of possible states, number of possible actions:

[num_states, num_cells] = size(sensory_weights);
[num_actions, ~] = size(motor_weights);

% Defines no. bins

num_bins = 10;

% Combines sensory/motor weights and firing.

sensorimotor_weights = [sensory_weights; motor_weights];

% Creates empty firingRates matrix, binMatrix and binMatrixTrans

firingRates = zeros(num_states, num_actions, num_cells);
binMatrix = zeros(num_cells, num_states, num_bins); %number of times when fr is classified into a specific bin
binMatrixTrans = zeros(num_cells, num_states, num_bins, num_actions);  %TF table to show if a certain cell is classified into a certain bin at a certain transformation


% Presents cells with every state/action combination.

for state = 1:num_states
    
    for action = 1:num_actions
        
        sensorimotor_fr = zeros(1, size(sensorimotor_weights,1));
        sensorimotor_fr(state) = 1;
        sensorimotor_fr(num_states + action) = 1;
        
        % Records the responses of each cell.
        noNaNweights = sensorimotor_weights;
        noNaNweights(isnan(noNaNweights)) = 0;
        firingRates(state, action, :) = sensorimotor_fr * noNaNweights;
        
    end
end

%% Analysing Cells Individually
        
% Threshold firingRates

indices = firingRates < threshold;
thresholded = firingRates;
thresholded(indices) = 0;
[~, idx]=max(thresholded, [], 3);

% Get cells with max. firing for each state/action combination
for cell = 1:(size(sensory_weights, 1) * size(motor_weights, 1))
    SA_maps{cell} = thresholded(:,:,idx(cell));
end

% Plot in a slider imagesc graph
slider_display(SA_maps, [])
xlabel('Actions')
ylabel('States')

% Get thresholded firing rates
for cell = 1:num_cells
    firing{cell} = thresholded(:,:,cell);
end

% Plot in a slider imagesc graph
slider_display(firing, [])
xlabel('Actions')
ylabel('States')


%% Analysing Cells Collectively

% Sorts them into bins.

for state = 1:num_states;
    %disp([num2str(state) '/' num2str(num_states)]);
    for action = 1:num_actions;
        for cell = 1:num_cells;
            for bin=1:num_bins
                if(bin<num_bins)
                    
                    % assumes firing rate between 0 and 1, will spread bins evenly between 0 and 1. If firing rate >= bottom threshold and less than top threshold of bin, will be binned. 
                    if ((bin-1)*(1/num_bins)<=firingRates(state,action,cell))&&(firingRates(state,action,cell)<(bin)*(1/num_bins)) % have switched permutation and grip around
                        
                        % adds a 1 to binMatrix and binTrans in appropriate position to signify this
                        binMatrix(cell,state,bin)=binMatrix(cell,state,bin)+1;
                        binMatrixTrans(cell,state,bin,action)=1;
                    end
                else
                        % same for top bin but bins those equal to upper threshold as well
                    if ((bin-1)*(1/num_bins)<=firingRates(state,action,cell))&&(firingRates(state,action,cell)<=(bin)*(1/num_bins))
                        binMatrix(cell,state,bin)=binMatrix(cell,state,bin)+1;
                        binMatrixTrans(cell,state,bin,action)=1;
                    end
                end
            end
        end
    end
end





% Display list of cells for each combo.
mappings = {};

for state = 1:num_states
    
    for action = 1:num_actions
        
        firing = find(binMatrixTrans(:,state,2:num_bins,action));
        mappings{state, action} = firing;
        
    end
end
end

