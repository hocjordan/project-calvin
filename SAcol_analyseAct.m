function [display] = SA_analyseAct(worldSize, SA_cells, SA_decoded)

%% Import Arrows
arrows = repmat({uint8(zeros(16))}, [3 3]);
arrows{1} = imread('arrow_nw.png');
arrows{2} = imread('arrow_w.png');
arrows{3} = imread('arrow_sw.png');
arrows{4} = imread('arrow_n.png');
arrows{6} = imread('arrow_s.png');
arrows{7} = imread('arrow_ne.png');
arrows{8} = imread('arrow_e.png');
arrows{9} = imread('arrow_se.png');

for arrow = 1:numel(arrows);
    arrowNormalised = arrows{arrow};
    arrowNormalised(arrowNormalised > 0) = 255;
    arrows{arrow} = arrowNormalised;
end

% Make an array to display the arrows in.
display = repmat({uint8(zeros(16))}, [worldSize*3 worldSize*3]);

%% Retrieve the SA layer's activation and display
% Thresholding by the least active SA cell (within SA_decoded).
minimum_activation = min(min(SA_cells(SA_decoded(:,3))));
SA_cells = SA_cells - minimum_activation;

% Get maximum activation that remains.
maximum_activation = max(max(SA_cells));

for column = 1:size(SA_cells,3)
    
    [~, cell] = max(SA_cells(:,:,column));
    cell = sub2ind(size(SA_cells), cell, 1, column);
    
    % Check that cell has a sensory and motor conneciton i.e. is within
    % SA_decoded.
    if any(ismember(SA_decoded(:,3), cell)) == 1
        
        [y, x] = ind2sub([worldSize worldSize], SA_decoded(find(SA_decoded(:,3) == cell),1));
        SA_state = (y * 3 - 1) + ((worldSize*3) + (worldSize*3) * 3 * (x-1));
        display{SA_state}(:) = 255;
        
        switch SA_decoded(find(SA_decoded(:,3) == cell),2)
            
            case 1
                display{SA_state - worldSize*3 - 1} = arrows{1};
                
            case 2
                display{SA_state - worldSize*3} = arrows{2};
                
            case 3
                display{SA_state - worldSize*3 + 1} = arrows{3};
                
            case 4
                display{SA_state - 1} = arrows{4};
                
            case 6
                display{SA_state + 1} = arrows{6};
                
            case 7
                display{SA_state + worldSize*3 - 1} = arrows{7};
                
            case 8
                display{SA_state + worldSize*3} = arrows{8};
                
            case 9
                display{SA_state + worldSize*3 + 1} = arrows{9};
                
        end
    end
end

try
    figure(); image(cell2mat(display)); colormap(copper); title('Activation (adjusted) in SA layer')
catch err
    disp(err.message)
end

end